`default_nettype none

`define PADDLE_WIDTH 4
`define PADDLE_HEIGHT 48
`define BALL_RADIUS 4
`define PADDLE_SPEED 3

module object
    (output logic [8:0] row,
     output logic [9:0] col,
     input logic [8:0] setRow,
     input logic [9:0] setCol,
     input logic shouldSet,
     input logic signed [3:0] xVelocity, yVelocity,
     input logic onFrame,
     input logic clock,
     input logic freeze);
    
    logic [8:0] nextRow;
    logic [9:0] nextCol;
    Register #(9) rowReg(nextRow, clock, onFrame, 0, row);
    Register #(10) colReg(nextCol, clock, onFrame, 0, col);
    
    
    always_comb begin
        nextRow = row;
        nextCol = col;
        if(!freeze) begin
            nextRow = signed'(row) + yVelocity;
            nextCol = signed'(col) + xVelocity;
        end
        
        if(shouldSet) begin
            nextRow = setRow;
            nextCol = setCol;
        end
       
    end
    
endmodule: object

module shouldDraw
    (input logic [8:0] row,
     input logic [9:0] col,
     input logic [8:0] objectRow,
     input logic [9:0] objectCol,
     input logic [5:0] width, height,
     output logic enable);
    
    
    // Draw Pixel in Bounds
    logic [1:0] inBounds;
    offset_checkWithWrap #(9) rowCheck(row, objectRow, height, inBounds[0]);
    offset_checkWithWrap #(10) colCheck(col, objectCol, width, inBounds[1]);
    
    assign enable = &inBounds;
    
endmodule: shouldDraw


module scoreControl
    (input logic player1Scored, player2Scored,
     output logic [3:0] player1Score, player2Score,
     output logic isPlayer1Turn, gameOver,
     input logic clock, reset, serve);

    
    logic incPlayer1, incPlayer2;
    
    AsyncCounter #(4) p1(clock, reset, incPlayer1, player1Score);
    AsyncCounter #(4) p2(clock, reset, incPlayer2, player2Score);

    assign gameOver = 0;
    
    scoreControlFSM fsm(player1Scored, player2Scored, serve, isPlayer1Turn, incPlayer1, incPlayer2, clock, reset);
    
endmodule: scoreControl

module scoreControlFSM
    (input logic player1Scored, player2Scored, serve,
     output isPlayer1Turn, incPlayer1, incPlayer2,
     input logic clock, reset);
    

    enum logic [1:0] {P1, P2, InPlay} state, nextState;
    
    assign isPlayer1Turn = state == P1;
    
    
    always_comb begin
        nextState = state;
        if(player1Scored && state == InPlay)
            nextState = P1;
        if(player2Scored && state == InPlay)
            nextState = P2;
        if(serve)
            nextState = InPlay;
    end
    
    always_comb begin
        incPlayer1 = 0;
        incPlayer2 = 0;
        if(state == InPlay && player1Scored)
            incPlayer1 = 1;
        if(state == InPlay && player2Scored)
            incPlayer2 = 1;
        
    end
    
    always_ff @(posedge clock, posedge reset)
        if(reset)
            state <= InPlay;
        else
            state <= nextState;
    
endmodule: scoreControlFSM

module game
    (input logic [8:0] drawRow,
     input logic [9:0] drawCol,
     input logic [1:0] player1Control, player2Control,
     input logic serve, reset, clock, onFrame,
     output logic [3:0] player1Score, player2Score,
     output logic [23:0] RGB,
     output logic [3:0] debug);

    logic shouldDrawPaddle1, shouldDrawPaddle2, shouldDrawBall;
    
    logic [8:0] paddle1Row, paddle2Row, ballRow;
    logic [9:0] paddle1Col, paddle2Col, ballCol;

    logic detectLeft, detectRight;
    
    logic freeze;
    logic gameOver;
    

    
    makePaddle paddle1(drawRow,216,drawCol,60,serve,onFrame,clock,player1Control,shouldDrawPaddle1, paddle1Row, paddle1Col, freeze);
    makePaddle paddle2(drawRow,216,drawCol,580,serve,onFrame,clock,player2Control,shouldDrawPaddle2, paddle2Row, paddle2Col, freeze);

    logic setDown, setRight;
    
    makeBall ball(drawRow,238,drawCol,318,serve,onFrame,clock,detectLeft, detectRight, setDown, setRight, shouldDrawBall, ballRow, ballCol, freeze);
    
    collisionDetection detectLeftCollide(paddle1Row, paddle1Col, ballRow, ballCol, detectLeft);
    collisionDetection detectRightCollide(paddle2Row, paddle2Col, ballRow, ballCol, detectRight);

    logic player1Scored, player2Scored;
    
    logic trigger;
    assign trigger = player1Scored || player2Scored;
    
    holdFreeze hf(trigger, freeze, clock, serve);
    
    detectScore pointScored(ballRow, ballCol, player1Scored, player2Scored, clock, serve, reset);

    assign debug = 0;

    logic isPlayer1Turn;
    
    scoreControl sc(player1Scored, player2Scored, player1Score, player2Score, isPlayer1Turn, gameOver, clock, reset, serve);
   
    
    assign setDown = 1;
    
    PresetRegister #(1) rightReg(isPlayer1Turn, clock, trigger, reset, setRight);
    
    always_comb begin
        RGB = 24'h000000;
        
        // Boom, Bang, Pow
        if(detectLeft)
            RGB = 24'hFF0000;
        if(detectRight)
            RGB = 24'h00FF00;
        if(ballCol < 5)
            RGB = 24'hAA55B1;
        if(ballCol > 10'd630)
            RGB = 24'h2266DD;
        if(gameOver)
            RGB = 24'hCA3589;
        
        if(shouldDrawPaddle1)
            RGB = 24'hFFFF00;
        if (shouldDrawPaddle2)
            RGB = 24'h00FFFF;
        if (shouldDrawBall)
            RGB = 24'hFFFFFF;
        
    end

endmodule: game

module PresetRegister
    #(parameter W = 16)
    (input logic [W-1:0] D,
     input logic clock, en, preset,
     output logic [W-1:0] Q);

    always_ff @(posedge clock) begin
        if(en)
            Q <= D;
        else if(preset)
            Q <= 1;
    end

endmodule: PresetRegister

module holdFreeze
    (input logic trigger,
     output logic hold,
     input logic clock, reset);

    always_ff @(posedge clock)
        if(reset)
            hold = 0;
        else if(hold)
            hold = hold;
        else
            hold = trigger;
    
endmodule: holdFreeze

module detectScore
    (input logic [8:0] ballRow,
     input logic [9:0] ballCol,
     output logic player1Scored, player2Scored,
     input logic clock, serve, reset);

    assign player1Scored = ballCol < 10'd5;
    assign player2Scored = ballCol > 10'd630;
    
endmodule: detectScore

module collisionDetection
    (input logic [8:0] paddleRow,
     input logic [9:0] paddleCol,
     input logic [8:0] ballRow,
     input logic [9:0] ballCol,
     output logic isHit);
    
    
    
    logic [1:0] collide;
    
    shouldDraw overlap(ballRow, ballCol, paddleRow, paddleCol, `PADDLE_WIDTH, `PADDLE_HEIGHT, collide[0]);
    
    logic [8:0] bottomRightRow;
    logic [9:0] bottomRightCol;
    
    assign bottomRightRow = ballRow + `BALL_RADIUS;
    assign bottomRightCol = ballCol + `BALL_RADIUS;
    
    shouldDraw overlap2(bottomRightRow, bottomRightCol, paddleRow, paddleCol, `PADDLE_WIDTH, `PADDLE_HEIGHT, collide[1]);
    
    assign isHit = |collide;
    
    
endmodule: collisionDetection

module inBounds
    (input logic [8:0] row,
     input logic [9:0] column,
     input logic [5:0] width, height,
     output logic isOutOfTop, isOutOfBottom);

    assign isOutOfTop = row + height >= 496; // 480 + 16
    assign isOutOfBottom = 480 <= (row + height) && (row + height) <= 496;
    
endmodule: inBounds

module makePaddle
    (input logic [8:0] row, setRow,
     input logic [9:0] col, setCol,
     input logic serve, onFrame, clock,
     input logic [1:0] playerControl,
     output logic shouldDrawPaddle,
     output logic [8:0] paddleRow,
     output logic [9:0] paddleCol,
     input logic freeze);
    
    logic signed [3:0] paddleYVelocity;
    logic isOutOfTop, isOutOfBottom;
    
    always_comb begin
        paddleYVelocity = 0;
        if(!isOutOfTop && playerControl == 2)
            paddleYVelocity = -`PADDLE_SPEED;
        else if(!isOutOfBottom && playerControl == 1)
            paddleYVelocity = `PADDLE_SPEED;
    end
    
    inBounds boundsChecker(paddleRow, paddleCol, `PADDLE_WIDTH, `PADDLE_HEIGHT, isOutOfTop, isOutOfBottom);
    
    object paddle(paddleRow, paddleCol, setRow, setCol, serve, 0, paddleYVelocity, onFrame, clock, freeze);
    
    

    shouldDraw paddleDraw(row, col, paddleRow, paddleCol, `PADDLE_WIDTH, `PADDLE_HEIGHT, shouldDrawPaddle);

endmodule: makePaddle

module makeBall
    (input logic [8:0] row, setRow,
     input logic [9:0] col, setCol,
     input logic serve, onFrame, clock,
     input logic hitLeft, hitRight,
     input logic setDown, setRight,
     output logic shouldDrawBall,
     output logic [8:0] ballRow,
     output logic [9:0] ballCol,
     input logic freeze);

     logic signed [3:0] ballXVelocity, ballYVelocity;
     logic right, down;
    
    logic resetBall;
    
    assign resetBall = serve||freeze;

     always_ff @(posedge clock) begin
         if (serve) begin
             down = setDown;
             right = setRight;
         end
         else begin
             if (ballRow == 0)
                 down = 1;
             else if(ballRow == 10'd476) //480-4
                 down = 0;
             if (hitRight)
                 right = 0;
             else if(hitLeft)
                 right = 1;
                 
         end
     end


     always_comb begin
        ballXVelocity = -2;
         ballYVelocity = 1;
         if(right)
            ballXVelocity = 2;
         else
             ballXVelocity = -2;
         if (down)
             ballYVelocity = 1;
         else
             ballYVelocity = -1; // Want to go up
    end

     object ball(ballRow, ballCol, setRow, setCol, resetBall, ballXVelocity, ballYVelocity, onFrame, clock, freeze);

     shouldDraw ballDraw(row,col,ballRow,ballCol,`BALL_RADIUS, `BALL_RADIUS, shouldDrawBall);

endmodule: makeBall





