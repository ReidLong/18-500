`default_nettype none


module ChipInterface
    (input logic CLOCK_50,
     input logic [3:0] KEY,
     input logic [17:0] SW,
     output logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
     output logic [7:0] VGA_R, VGA_G, VGA_B,
     output logic VGA_BLANK_N, VGA_CLK, VGA_SYNC_N,
     output logic VGA_VS, VGA_HS);


    logic [8:0] row;
    logic [9:0] col;
    logic blank;
    vga control(CLOCK_50, SW[5], VGA_HS, VGA_VS, blank, row, col);
    
    logic redRange;
    logic [1:0] greenRange;
    logic [3:0] blueRange;
    logic black;
    range_check #(10) redCheck(col, 320, 640, redRange);
    range_check #(10) greenCheck(col, 160, 320, greenRange[0]);
    range_check #(10) greenCheck2(col, 480, 640, greenRange[1]);
    range_check #(10) blueCheck(col,80,160,blueRange[0]);
    range_check #(10) blueCheck2(col,240,320,blueRange[1]);
    range_check #(10) blueCheck3(col,400,480,blueRange[2]);
    range_check #(10) blueCheck4(col,560,640,blueRange[3]);
    range_check #(10) rowCheck(row, 240, 480, black);

    
    assign VGA_G = {8{|greenRange}} & {8{~black}};
    assign VGA_R = {8{redRange}} & {8{~black}};
    assign VGA_B = {8{|blueRange}} & {8{~black}};
    assign VGA_BLANK_N = ~blank;
    assign VGA_SYNC_N = 0;
    assign VGA_CLK = ~CLOCK_50;
    
endmodule: ChipInterface