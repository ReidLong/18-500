`default_nettype none


module shouldDraw_test();
    
    logic [9:0] drawCol, objectCol;
    logic [8:0] drawRow, objectRow;
    logic enable;
    
    shouldDraw dut(drawRow, drawCol, objectRow, objectCol, 4, 48, enable);
    
    initial begin
        $monitor($time, "DR: %d DC: %d OR: %d OC: %d, En: %b InBounds: %b High: %d", drawRow, drawCol, objectRow, objectCol, enable, dut.inBounds, dut.rowCheck.high);
        
        drawRow = 10;
        drawCol = 40;
        objectRow = 511;
        objectCol = 40;
        #5;
        
        $finish;
    end


    
endmodule: shouldDraw_test


module shouldDraw
    (input logic [8:0] row,
     input logic [9:0] col,
     input logic [8:0] objectRow,
     input logic [9:0] objectCol,
     input logic [5:0] width, height,
     output logic enable);


    // Draw Pixel in Bounds
    logic [1:0] inBounds;
    offset_checkWithWrap #(9) rowCheck(row, objectRow, height, inBounds[0]);
    offset_checkWithWrap #(10) colCheck(col, objectCol, width, inBounds[1]);

    assign enable = &inBounds;
    
endmodule: shouldDraw