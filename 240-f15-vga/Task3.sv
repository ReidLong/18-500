`default_nettype none


module ChipInterface
    (input logic CLOCK_50,
     input logic [3:0] KEY,
     input logic [17:0] SW,
     output logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
     output logic [7:0] VGA_R, VGA_G, VGA_B,
     output logic VGA_BLANK_N, VGA_CLK, VGA_SYNC_N,
     output logic VGA_VS, VGA_HS);


    logic [8:0] row;
    logic [9:0] col;
    logic blank;
    vga control(CLOCK_50, SW[5], VGA_HS, VGA_VS, blank, row, col);
    
    logic onFrame;
    logic [3:0] player1Score, player2Score;
    logic [3:0] player1BCD1, player1BCD0, player2BCD1, player2BCD0;
    
    hexDisplay display(debug, 0, 0, 0, player2BCD0, player2BCD1, player1BCD0, player1BCD1, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);
    
    frameDriver driver(row, col, onFrame);
    
    binaryToBCD bToB1(player1Score, player1BCD1, player1BCD0);
    binaryToBCD bToB2(player2Score, player2BCD1, player2BCD0);
    
    logic reset, clock;
    assign clock = CLOCK_50;
    
    logic [3:0] debug;
    
    
    logic [1:0] player1Control, player2Control;
    logic serve;
    sync s1(SW[17], player1Control[1], clock, reset);
    sync s2(SW[16], player1Control[0], clock, reset);
    sync s3(SW[1], player2Control[1], clock, reset);
    sync s4(SW[0], player2Control[0], clock, reset);
    sync sR(~KEY[0], reset, clock, 0);
    sync s5(~KEY[3], serve, clock, reset);
    
    
    game pong(row, col, player1Control, player2Control, serve, reset, CLOCK_50, onFrame,
              player1Score, player2Score, {VGA_R, VGA_G, VGA_B}, debug);


    assign VGA_BLANK_N = ~blank;
    assign VGA_SYNC_N = 0;
    assign VGA_CLK = ~CLOCK_50;
    
endmodule: ChipInterface

module frameDriver
    (input logic [8:0] row,
     input logic [9:0] col,
     output logic onFrame);

    assign onFrame = row == 480 && col == 640;

    
endmodule: frameDriver

module binaryToBCD
    (input logic [3:0] binary,
     output logic [3:0] bcd1, bcd0);

    always_comb begin
        bcd1 = binary / 10;
        bcd0 = binary % 10;
    end
    
    
endmodule: binaryToBCD


module sync
    (input logic asyncInput,
     output logic syncedInput,
     input logic clock, reset);

    logic middleInput;
    always_ff @(posedge clock) begin
        if(reset)
            middleInput <= 0;
        else begin
            middleInput <= asyncInput;
            syncedInput <= middleInput;
        end
    end

endmodule: sync