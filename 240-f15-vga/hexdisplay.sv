`default_nettype none

module hexDisplay
    (input logic [3:0] num0, num1, num2, num3, num4, num5, num6, num7,
     output logic [7:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);

    
    logic [7:0] turn_on;
    
    always_comb begin
        turn_on = 8'hff;
        /*if(num0 == 0)
            turn_on[0] = 0;
        if(num1 == 0)
            turn_on[1] = 0;
        if(num2 == 0)
            turn_on[2] = 0;
        if(num3 == 0)
            turn_on[3] = 0;
        if(num4 == 0)
            turn_on[4] = 0;
        if(num5 == 0)
            turn_on[5] = 0;
        if(num6 == 0)
            turn_on[6] = 0;
        if(num7 == 0)
            turn_on[7] = 0;
        */
    end
    
    SevenSegmentControl ssc(HEX7, HEX6, HEX5, HEX4, HEX3, HEX2, HEX1, HEX0,
                            num7, num6, num5, num4, num3, num2, num1, num0,
                            turn_on);
   
endmodule: hexDisplay


// From Lab2a
module BCDtoSevenSegment
	(input logic [3:0] bcd,
	 output logic [6:0] segment);

	always_comb begin
	  unique case (bcd)
	     4'd0: segment = 7'b1000_000;
	     4'd1: segment = 7'b1111_001;
	     4'd2: segment = 7'b0100_100;
	     4'd3: segment = 7'b0110_000;
	     4'd4: segment = 7'b0011_001;
	     4'd5: segment = 7'b0010_010;
	     4'd6: segment = 7'b0000_010;
	     4'd7: segment = 7'b1111_000;
	     4'd8: segment = 7'b0000_000;
	     4'd9: segment = 7'b0010_000;
	   endcase // case (bcd)
	end // always_comb
   

endmodule: BCDtoSevenSegment

// From Lab2a
module SevenSegmentDigit
  (input logic [3:0] bcd,
   output logic [6:0] segment,
   input logic 	      blank);

   logic [6:0] 	      decoded;

   BCDtoSevenSegment b2ss(bcd, decoded);

   always_comb begin
      segment = decoded;

      if(!blank)
		segment = 7'b1111_111;
   end
   

endmodule: SevenSegmentDigit

// From Lab2a
module SevenSegmentControl
  (output logic [6:0] HEX7, HEX6, HEX5, HEX4,
   output logic [6:0] HEX3, HEX2, HEX1, HEX0,
   input logic [3:0]  BCD7, BCD6, BCD5, BCD4,
   input logic [3:0]  BCD3, BCD2, BCD1, BCD0,
   input logic [7:0]  turn_on);

   SevenSegmentDigit h7(BCD7, HEX7, turn_on[7]),
     h6(BCD6, HEX6, turn_on[6]),
     h5(BCD5, HEX5, turn_on[5]),
     h4(BCD4, HEX4, turn_on[4]),
     h3(BCD3, HEX3, turn_on[3]),
     h2(BCD2, HEX2, turn_on[2]),
     h1(BCD1, HEX1, turn_on[1]),
     h0(BCD0, HEX0, turn_on[0]);
   
endmodule: SevenSegmentControl
