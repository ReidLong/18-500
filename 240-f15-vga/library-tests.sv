`default_nettype none

module MagComp_test;
    logic [2:0] A, B;
    logic AltB, AeqB, AgtB;

    logic [6:0] data;
    assign A = data[2:0];
    assign B = data[5:3];

    MagComp #(3) dut(.*);

    initial begin
        for(data = 0; data < 64; data++) begin
            #5;
            if(A == B && !AeqB || A != B && AeqB) $display("Ooops");
            if(A < B && !AltB || A >= B && AltB) $display("Ooops");
            if(A > B && !AgtB || A <= B && AgtB) $display("Ooops");
        end
    end

endmodule: MagComp_test

module Adder_test;
    logic [2:0] A, B, S;
    logic Cin, Cout;
    logic [7:0] data;

    assign {A, B, Cin} = data[6:0];

    Adder #(3) dut(.*);

    initial begin
        for(data = 0; data < 128; data++)
            #5 if((A + B + Cin) != {Cout, S})
                $display("Oops: A: %b B: %b Cin: %b S: %b Cout: %b", A, B, Cin, S, Cout);
    end

endmodule: Adder_test

module Multiplexer_test;

    logic [2:0] I;
    logic [1:0] S;
    logic Y;

    logic [3:0] data;

    assign I = data[2:0];

    Multiplexer #(3) dut(.*);

    initial begin
        for(data = 0; data < 8; data++)
            for(S = 0; S < 3; S++)
                #5 if(Y != I[S]) $display("Ooops");
    end

endmodule: Multiplexer_test

module Mux2to1_test;
    logic [1:0] I0, I1, Y;
    logic S;
    logic [6:0] data;
    assign {I0, I1, S} = data[5:0];
    
    Mux2to1 #(2) dut(.*);
    
    initial begin
        for(data = 0; data < 64; data++)
            #5 if(S && Y != I1 || !S && Y != I0) $display("OOops");
    end
    
endmodule: Mux2to1_test

module Decoder_test;
    logic [1:0] I;
    logic en;
    logic [2:0] D;
    
    Decoder #(3) dut(.*);
    initial begin
        for(I = 0; I < 3; I++) begin
            en = 0;
            #5 if(D != 0) $display("Oops");
            en = 1;
            #5 if(D != (1 << I)) $display("Ooops: D: %b I: %b", D, I);
        end
    end

endmodule: Decoder_test


module Register_test;
    logic Q, D, clock, en, clear;
    
    Register #(1) dut(.*);
    
    initial begin
        $monitor($time,, " D: %b Q: %B en: %b, cl: %b", D, Q, en, clear);
        
        clock = 0;
        forever #5 clock = ~clock;
    end
    
    initial begin
        en = 1;
        clear = 0;
        D = 1;
        @(posedge clock); #1;
        // Q = 1
        if(Q != 1) $display("Ooops");
        en = 0;
        D = 0;
        @(posedge clock); #1;
        // Q = 1
        if(Q != 1) $display("Ooops");
        en = 1;
        @(posedge clock); #1;
        // Q = 0
        if(Q != 0) $display("Ooops");
        D = 1;
        @(posedge clock); #1;
        en = 0;
        // Q = 1
        if(Q != 1) $display("Ooops");
        clear = 1;
        @(posedge clock); #1;
        // Q = 0
        if(Q != 0) $display("Ooops");
        #5;
        $finish;
    end

endmodule: Register_test

module range_check_test;
    logic [1:0] val, low, high;
    logic is_between;
    logic [6:0] data;

    assign {val, low, high} = data[5:0];

    range_check #(2) (val, low, high, is_between);

    initial begin
        for(data = 0; data < 64; data++)
            #5 if(low <= val && val <= high && !is_between || (val < low || val > high) && is_between)
                $display("OOops");
    end

endmodule: range_check_test

module offset_check_test;
    logic [1:0] val, low, offset;
    logic is_between;
    logic [6:0] data;
    
    assign {val, low, offset} = data[5:0];
    
    offset_check #(2) oc(val, low, offset, is_between);
    
    initial begin
        for(data = 0; data < 64; data++)
            #5 if(val + offset <= 3)
                if(low <= val && val <= low + offset && !is_between || (val < low || val > low + offset) && is_between)
                $display("Ooops: Val: %d Low: %d Offset: %d Is_between: %b: High: %d", val, low, offset, is_between, oc.high);
        end
    
endmodule: offset_check_test

