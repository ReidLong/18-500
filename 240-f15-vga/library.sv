`default_nettype none

module MagComp
    #(parameter W = 16)
    (input logic [W-1:0] A, B,
     output logic AltB, AeqB, AgtB);

    assign AltB = A < B;
    assign AeqB = A == B;
    assign AgtB = A > B;

endmodule: MagComp

module Adder
    #(parameter W = 16)
    (input logic [W-1:0] A, B,
     input logic Cin,
     output logic [W-1:0] S,
     output logic Cout);

    always_comb begin
        S = A + B + Cin;
        Cout = (A + B + Cin) / (1 << W);
    end
endmodule: Adder

module Multiplexer
    #(parameter W = 16)
    (input logic [W-1:0] I,
     input logic [$clog2(W)-1:0] S,
     output logic Y);

    always_comb begin
        Y = I[S];
    end

endmodule: Multiplexer

module Mux2to1
    #(parameter W = 16)
    (input logic [W-1:0] I0, I1,
     output logic [W-1:0] Y,
     input logic S);

    assign Y = S ? I1 : I0;

endmodule: Mux2to1

module Decoder
    #(parameter W = 16)
    (input logic en,
     input logic [$clog2(W)-1:0] I,
     output logic [W-1:0] D);

    always_comb begin
        if (!en) D = 0;
        else D = 1 << I;
    end

endmodule: Decoder

module Register
    #(parameter W = 16)
    (input logic [W-1:0] D,
     input logic clock, en, clear,
     output logic [W-1:0] Q);

    always_ff @(posedge clock) begin
        if(en)
            Q <= D;
        else if(clear)
            Q <= 0;
    end

endmodule: Register


module ShiftReg_PISO_Right
    #(parameter w = 8)
    (output logic lowbit,
     input logic [w-1:0] d,
     input logic clock, ld_L, sh_L);

    logic [w-1:0] q;
    assign lowbit = q[0];

    always_ff @(posedge clock)
        if(~ld_L)
            q <= d;
        else if (~sh_L)
            q <= (q >> 1);

endmodule: ShiftReg_PISO_Right

module Counter
    #(parameter W = 8)
    (input logic clock, clear, enable,
     output logic [W-1:0] q);

    always_ff @(posedge clock)
        if(clear && enable)
            q <= 0;
        else if (enable)
            q <= q + 1;

endmodule: Counter


module AsyncCounter
    #(parameter W = 8)
    (input logic clock, reset, enable,
     output logic [W-1:0] q);

    always_ff @(posedge clock, posedge reset)
        if(reset)
            q <= 0;
        else if (enable)
            q <= q + 1;

endmodule: AsyncCounter

module range_check
  #(parameter W = 3)
  (input logic [W-1:0] val, low, high,
   output logic is_between);

  assign is_between = (low <= val ) && (val <= high);

endmodule: range_check

module offset_check
  #(parameter W = 3)
  (input [W-1:0] val, low, delta,
   output logic is_between);

    logic [W-1:0] high;

    Adder #(W) a1(.A(low),.B(delta),.Cin(0),.S(high), .Cout());
    //assign high = low + delta;
    range_check #(W) rc(val, low, high, is_between);

endmodule: offset_check

module offset_checkWithWrap
    #(parameter W = 3)
    (input logic [W-1:0] val, low, delta,
     output logic is_between);

    logic [W-1:0] high;
    logic inRangeBetween;
    assign high = low + delta;
    range_check #(W) rc(val, low, high, inRangeBetween);

    always_comb begin
        if(high < low) //Wrap around
            is_between = low <= val || val <= high;
        else
            is_between = inRangeBetween;
    end


endmodule: offset_checkWithWrap

module Mux8to1
    #(parameter size = 8)
    (input logic [size-1:0] inputs [7:0],
     input logic [2:0] select,
     output logic [size-1:0] muxOut);

    // TODO

endmodule: Mux8to1



