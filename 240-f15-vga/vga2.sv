`default_nettype none


`define Tpw 192
`define Tbp 96

`define TpwTbp 288
`define NUMBER_OF_COLUMN_CLOCKS 1280
`define TbpTdispTfp 1408
`define Ts 1600
`define MAX_LINE 521
`define TpwTbp_LINES 31
`define Tpw_LINES 2
`define TbpTdispTfp_LINES 519

module vga
    (input logic CLOCK_50, reset,
     output logic HS, VS, blank,
     output logic [8:0] row,
     output logic [9:0] col);


    logic [9:0] rowCountOut;
    logic [10:0] columnCountOut;
    logic maxColumnCount, maxRowCount;
    logic clearRowCounter, clearColumnCounter;
    
    assign clearRowCounter = reset || maxRowCount;
    assign clearColumnCounter = reset || maxColumnCount;
    
    logic rowCountEnable;
    assign rowCountEnable = clearRowCounter || maxColumnCount;
    
    Counter #(10) rowCounter(CLOCK_50, clearRowCounter, rowCountEnable, rowCountOut);
    Counter #(11) columnCounter(CLOCK_50, clearColumnCounter, 1, columnCountOut);
    
    logic columnBlank, rowBlank;
    assign blank = ~(rowBlank && columnBlank);
    
    offset_check #(11) blankColumnCheck(columnCountOut, 11'd288, 11'd1280, columnBlank);
    offset_check #(10) blankRowCheck(rowCountOut, 10'd31, 10'd480, rowBlank);
    range_check #(11) hsCheck(columnCountOut, 11'd192, 11'd1600, HS);
    range_check #(10) vsCheck(rowCountOut, 10'd2, 10'd521, VS);
    
    
    assign col = (columnCountOut - 11'd288) >> 1;
    assign row = rowCountOut - `TpwTbp_LINES;
    
    MagComp #(11) lineWrap(columnCountOut, 11'd1600,, maxColumnCount,);
    MagComp #(10) gridWrap(rowCountOut, 10'd521,, maxRowCount,);
    
endmodule: vga

module vga_test;
    
    logic clock, reset, HS, VS, blank;
    logic [8:0] row;
    logic [9:0] col;
    
    logic [9:0] rowCountOut;
    logic [10:0] columnCountOut;
    logic maxColumnCount, maxRowCount;
    logic clearRowCounter, clearColumnCounter;
    

    vga dut(clock, reset, HS, VS, blank, row, col);
    
    assign rowCountOut = dut.rowCountOut;
    assign columnCountOut = dut.columnCountOut;
    assign maxColumnCount = dut.maxColumnCount;
    assign maxRowCount = dut.maxRowCount;
    assign clearRowCounter = dut.clearRowCounter;
    assign clearColumnCounter = dut.clearColumnCounter;
    
    
    initial begin
        $monitor($time, "HS: %b, VS: %b, blank: %b Row: %d Col: %d", HS, VS, blank, row, col);
        
        
        clock = 0;
        forever #5 clock = ~clock;
    end
    
    initial begin
        reset = 1;
        @(posedge clock);
        reset = 0;
        #5000000;
        $finish;
    end
    
endmodule: vga_test

