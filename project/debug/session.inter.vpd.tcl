# Begin_DVE_Session_Save_Info
# DVE view(Wave.1 ) session
# Saved on Sat Apr 7 17:05:36 2018
# Toplevel windows open: 2
# 	TopLevel.1
# 	TopLevel.2
#   Wave.1: 22 signals
# End_DVE_Session_Save_Info

# DVE version: K-2015.09_Full64
# DVE build date: Aug 25 2015 21:36:02


#<Session mode="View" path="/afs/ece.cmu.edu/usr/relong/Private/18-500/repo/project/output/session.inter.vpd.tcl" type="Debug">

#<Database>

gui_set_time_units 1s
#</Database>

# DVE View/pane content session: 

# Begin_DVE_Session_Save_Info (Wave.1)
# DVE wave signals session
# Saved on Sat Apr 7 17:05:36 2018
# 22 signals
# End_DVE_Session_Save_Info

# DVE version: K-2015.09_Full64
# DVE build date: Aug 25 2015 21:36:02


#Add ncecessay scopes

gui_set_time_units 1s

set _wave_session_group_2 Group1
if {[gui_sg_is_group -name "$_wave_session_group_2"]} {
    set _wave_session_group_2 [gui_sg_generate_new_name]
}
set Group1 "$_wave_session_group_2"

gui_sg_addsignal -group "$_wave_session_group_2" { {Sim:top.chip.core.controlFlow.controlFlowInterface} {Sim:top.chip.core.fetch_stage.savedFetchExport.isValid} {Sim:top.chip.core.fetch_stage.savedFetchExport.fetchState.pc} {Sim:top.chip.core.fetch_stage.newFetchExport.isValid} {Sim:top.chip.core.fetch_stage.newFetchExport.fetchState.pc} {Sim:top.chip.core.fetch_stage.savedStall} {Sim:top.chip.core.fetch_stage.isValidFetch} {Sim:top.chip.core.fetch_stage.pc} }
gui_sg_addsignal -group "$_wave_session_group_2" { {Divider} } -divider
gui_sg_addsignal -group "$_wave_session_group_2" { {Sim:top.chip.core.decode_stage.fetchExport_ID_START.isValid} {Sim:top.chip.core.decode_stage.fetchExport_ID_START.fetchState.pc} }
gui_sg_addsignal -group "$_wave_session_group_2" { {Divider} } -divider
gui_sg_addsignal -group "$_wave_session_group_2" { {Sim:top.chip.core.execute_stage.decodeExport_EX_START.fetch.isValid} {Sim:top.chip.core.execute_stage.decodeExport_EX_START.fetch.fetchState.pc} }
gui_sg_addsignal -group "$_wave_session_group_2" { {Divider} } -divider
gui_sg_addsignal -group "$_wave_session_group_2" { {Sim:top.chip.core.memory_stage.executeExport_MEM_START.fetch.isValid} {Sim:top.chip.core.memory_stage.executeExport_MEM_START.fetch.fetchState.pc} {Sim:top.chip.core.memory_stage.memoryGenerateStall} {Sim:top.chip.core.memory_stage.memoryFlushing} {Sim:top.chip.core.memory_stage.savedFlush} {Sim:top.chip.core.memory_stage.memoryExport_WB_START_PATCHED.memory.isValid} {Sim:top.chip.core.memory_stage.memoryExport_WB_START_EARLY.fetch.isValid} {Sim:top.chip.core.memory_stage.memoryExport_WB_START_EARLY.fetch.fetchState.pc} }
gui_sg_addsignal -group "$_wave_session_group_2" { {Divider} } -divider
gui_sg_addsignal -group "$_wave_session_group_2" { {Sim:top.chip.core.writeBack_stage.memoryExport_WB_START.fetch.isValid} {Sim:top.chip.core.writeBack_stage.memoryExport_WB_START.fetch.fetchState.pc} }
if {![info exists useOldWindow]} { 
	set useOldWindow true
}
if {$useOldWindow && [string first "Wave" [gui_get_current_window -view]]==0} { 
	set Wave.1 [gui_get_current_window -view] 
} else {
	set Wave.1 [lindex [gui_get_window_ids -type Wave] 0]
if {[string first "Wave" ${Wave.1}]!=0} {
gui_open_window Wave
set Wave.1 [ gui_get_current_window -view ]
}
}

set groupExD [gui_get_pref_value -category Wave -key exclusiveSG]
gui_set_pref_value -category Wave -key exclusiveSG -value {false}
set origWaveHeight [gui_get_pref_value -category Wave -key waveRowHeight]
gui_list_set_height -id Wave -height 25
set origGroupCreationState [gui_list_create_group_when_add -wave]
gui_list_create_group_when_add -wave -disable
gui_marker_set_ref -id ${Wave.1}  C1
gui_wv_zoom_timerange -id ${Wave.1} 9002 9507
gui_list_add_group -id ${Wave.1} -after {New Group} [list ${Group1}]
gui_list_expand -id ${Wave.1} top.chip.core.controlFlow.controlFlowInterface
gui_list_expand -id ${Wave.1} top.chip.core.controlFlow.controlFlowInterface.fetchControl
gui_list_select -id ${Wave.1} {top.chip.core.memory_stage.memoryExport_WB_START_PATCHED.memory.isValid }
gui_seek_criteria -id ${Wave.1} {Any Edge}

gui_list_alias -id ${Wave.1} -group ${Group1} -index 0 -signal Sim:top.chip.core.memory_stage.memoryExport_WB_START_PATCHED.memory.isValid -add isMemoryResultValid 

gui_set_pref_value -category Wave -key exclusiveSG -value $groupExD
gui_list_set_height -id Wave -height $origWaveHeight
if {$origGroupCreationState} {
	gui_list_create_group_when_add -wave -enable
}
if { $groupExD } {
 gui_msg_report -code DVWW028
}
gui_list_set_filter -id ${Wave.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {LibBaseMember 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {BaseMembers 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Wave.1} -text {*}
gui_list_set_insertion_bar  -id ${Wave.1} -group ${Group1}  -item top.chip.core.memory_stage.memoryExport_WB_START_PATCHED.memory.isValid -position below

gui_marker_move -id ${Wave.1} {C1} 9251
gui_view_scroll -id ${Wave.1} -vertical -set 414
gui_show_grid -id ${Wave.1} -enable false
#</Session>

