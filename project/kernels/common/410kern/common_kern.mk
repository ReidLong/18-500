410KERNEL_LIBS = \
				 libelf.a \
				 liblmm.a \
				 libmalloc.a \
				 libmisc.a \
				 libRNG.a \
				 libstdio.a \
				 libstdlib.a \
				 libstring.a \
				 libriscv.a \

RISCV_SOURCES = asm.S csr.S
STDIO_SOURCES = puts.c putchar.c printf.c doprnt.c sprintf.c
STDLIB_SOURCES = panic.c
STRING_SOURCES = memcpy.c memset.c strlen.c memcmp.c
MALLOC_SOURCES = malloc_lmm.c calloc.c malloc.c free.c memalign.c realloc.c sfree.c smalloc.c smemalign.c
LMM_SOURCES = lmm_init.c lmm_add_region.c lmm_add_free.c lmm_free.c lmm_dump.c lmm_alloc.c lmm_alloc_aligned.c lmm_alloc_gen.c
MISC_SOURCES = environment.c
# This breaks all kinds of things! Don't include in common_kern
# RNG_SOURCES = mt19937int.c
# MALLOC_SOURCES =
# LMM_SOURCES =
# RNG_SOURCES =

COMMON_KERNEL_SOURCES = $(410KDIR)/entry.c $(addprefix $(410KDIR)/riscv/,$(RISCV_SOURCES)) $(addprefix $(410KDIR)/stdio/,$(STDIO_SOURCES)) $(addprefix $(410KDIR)/stdlib/,$(STDLIB_SOURCES)) $(addprefix $(410KDIR)/string/,$(STRING_SOURCES)) $(addprefix $(410KDIR)/malloc/,$(MALLOC_SOURCES)) $(addprefix $(410KDIR)/lmm/,$(LMM_SOURCES)) $(addprefix $(410KDIR)/misc/,$(MISC_SOURCES)) $(addprefix $(410KDIR)/RNG/,$(RNG_SOURCES))

COMMON_KERNEL_INCLUDE_PATHS = $(410KDIR) $(410KDIR)/inc $(410SDIR) $(patsubst lib%.a,$(410KDIR)/%,$(410KERNEL_LIBS))