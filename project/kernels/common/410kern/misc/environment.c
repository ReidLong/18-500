#include "environment.h"
#include <asm.h>
#include <stdarg.h>
#include <stdio/stdio.h>

void environment_exit(int exitStatus) {
    disable_interrupts();
    environment_printf("Exiting with status: %d", exitStatus);
    halt(exitStatus);
}

void environment_vprintf(const char* fmt, va_list vl) {
    char str[256];
    vsnprintf(str, sizeof(str) - 1, fmt, vl);
    // TODO: Do something with this string
}

void environment_printf(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    environment_vprintf(fmt, ap);
    va_end(ap);
}
