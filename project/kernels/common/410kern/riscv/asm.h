/** @file x86/asm.h
 *  @brief x86-specific assembly functions
 *  @author matthewj S2008
 */

#ifndef X86_ASM_H
#define X86_ASM_H

#include <stdint.h>


/** @brief Disables interrupts */
void disable_interrupts(void);
/** @brief Enables interrupts */
void enable_interrupts(void);

void* set_mtvec(void* base);
void* set_mscratch(void* scratch);

uint32_t get_mcause(void);
uint32_t get_mtval(void);

void halt(int x1);

#endif /* !X86_ASM_H */
