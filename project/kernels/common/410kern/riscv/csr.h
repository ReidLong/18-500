#ifndef __CSR_H
#define __CSR_H

#define INTERRUPTS_ENABLED_INDEX 3
#define ENABLE_INTERRUPTS_MASK (1 << INTERRUPTS_ENABLED_INDEX)

#define SATP 0x180      // Virtual Memory Page Directory
#define MSTATUS 0x300   // Enables/Disables interrupts
#define MTVEC 0x305     // Holds the address of the interrupt handler
#define MSCRATCH 0x340  // R/W scratch register (holds kernel SP)
#define MEPC 0x341      // Faulting instruction address
#define MCAUSE 0x342    // Holds the cause of the exception
#define MTVAL 0x343     // Faulting address (for page faults)
#define CYCLE 0xC00
#define INSTRET 0xC02
#define HPMCOUNTER3 0xC03   // INSTRUCTIONS_FETCHED
#define HPMCOUNTER4 0xC04   // FORWARD_BRANCH_RETIRED
#define HPMCOUNTER5 0xC05   // BACKWARD_BRANCH_RETIRED
#define HPMCOUNTER6 0xC06   // FORWARD_BRANCH_PREDICTED_CORRECT
#define HPMCOUNTER7 0xC07   // BACKWARD_BRANCH_PREDICTED_CORRECT
#define HPMCOUNTER8 0xC08   // FORWARD_BRANCH_PREDICTED_INCORRECT
#define HPMCOUNTER9 0xC09   // BACKWARD_BRANCH_PREDICTED_INCORRECT
#define HPMCOUNTER10 0xC0A  // INSTRUCTION_CACHE_HIT
#define HPMCOUNTER11 0xC0B  // INSTRUCTION_CACHE_MISS
#define HPMCOUNTER12 0xC0C  // DATA_CACHE_HIT
#define HPMCOUNTER13 0xC0D  // DATA_CACHE_MISS
#define HPMCOUNTER14 0xC0E  // DATA_VRAM_HIT
#define HPMCOUNTER15 0xC0F  // STORE_CONDITIONAL_SUCCESS
#define HPMCOUNTER16 0xC10  // STORE_CONDITIONAL_REJECT
#define HPMCOUNTER17 0xC11  // KEYBOARD_INTERRUPT_USER
#define HPMCOUNTER18 0xC12  // KEYBOARD_INTERRUPT_MACHINE
#define HPMCOUNTER19 0xC13  // EXECEPTION_USER
#define HPMCOUNTER20 0xC14  // EXCEPTION_MACHINE
#define HPMCOUNTER21 0xC15  // TIMER_INTERRUPT_USER
#define HPMCOUNTER22 0xC16  // TIMER_INTERRUPT_MACHINE
#define HPMCOUNTER23 0xC17  // FORWARD_JUMP_RETIRED
#define HPMCOUNTER24 0xC18  // BACKWARD_JUMP_RETIRED
#define HPMCOUNTER25 0xC19  // FORWARD_JUMP_PREDICTED_CORRECT
#define HPMCOUNTER26 0xC1A  // BACKWARD_JUMP_PREDICTED_CORRECT
#define HPMCOUNTER27 0xC1B  // FORWARD_JUMP_PREDICTED_INCORRECT
#define HPMCOUNTER28 0xC1C  // BACKWARD_JUMP_PREDICTED_INCORRECT
#define HPMCOUNTER29 0xC1D  // Unimplemented
#define HPMCOUNTER30 0xC1E  // Unimplemented
#define HPMCOUNTER31 0xC1F  // Unimplemented

#ifndef __ASSEMBLER__

#include <attributes.h>

typedef struct {
    unsigned ignored1 : 3;
    // MIE: 1 => Enabled
    unsigned machineInterruptsEnabled : 1;
    unsigned ignored2 : 3;
    // MPIE: Holds MIE prior to interrupt
    unsigned machinePriorInterruptsEnabled : 1;
    unsigned ignored3 : 3;
    // 3 if previously running in machine mode, 0 if previously running in user
    // mode
    unsigned machinePreviousPriviledge : 2;
    unsigned ignored4 : 19;
} PACKED MStatus_t;

uint64_t csr_getCycles(void);
uint64_t csr_getInstructionsRetired(void);
uint64_t csr_getICacheHit(void);
uint64_t csr_getDCacheHit(void);
uint64_t csr_getICacheMiss(void);
uint64_t csr_getDCacheMiss(void);
uint64_t csr_getDVRAMHit(void);

#endif

#endif