#ifndef __INTERRUPT_CAUSE_H
#define __INTERRUPT_CAUSE_H

// Sadly these can't be an enum because enum doesn't like negative values
#define INSTRUCTION_MISALIGNED 0x0
#define INSTRUCTION_ILLEGAL 0x2
#define LOAD_MISALIGNED 0x4
#define STORE_MISALIGNED 0x6
#define USER_ECALL 0x8
#define INSTRUCTION_PAGE_FAULT 0xC
#define LOAD_PAGE_FAULT 0xD
#define STORE_PAGE_FAULT 0xF
#define TIMER_INTERRUPT 0x80000007
#define EXTERNAL_INTERRUPT 0x80000009

#endif