#include "console.h"
#include "errorCode.h"
#include "log.h"
#include <asm.h>
#include <attributes.h>
#include <compiler.h>
#include <contracts.h>
#include <environment.h>
#include <stdbool.h>
#include <string.h>

static const int32_t CREATED_MAGIC = 0x77770099;

#define COLOR_MASK 0xFF

Console_t console;

void console_init(void) {
    console.row     = 0;
    console.column  = 0;
    console.color   = DEFAULT_COLOR;
    console.created = CREATED_MAGIC;
    console_clear();
}

static bool console_isValidRow(int32_t row) {
    return 0 <= row && row < CONSOLE_HEIGHT;
}

static bool console_isValidColumn(int32_t column) {
    return 0 <= column && column < CONSOLE_WIDTH;
}

static bool console_isValidColor(uint8_t color) {
    return (color & COLOR_MASK) == color;
}

static bool console_isValid(Console_t *console) {
    REQUIRES(console != NULL);
    if (console->created != CREATED_MAGIC) {
        halt(-100);
        // log_fatalf("Console not created? %#x (%d)",
        //            (unsigned)console->created,
        //            (int)console->created);
        return false;
    }

    if (!console_isValidColor(console->color)) {
        // log_fatalf("Console color invalid: %#x (%d)",
        //            (unsigned)console->color,
        //            (int)console->color);
        halt(-101);
        return false;
    }

    if (!console_isValidRow(console->row)) {
        // log_fatalf("Console row invalid: %d", (int)console->row);
        halt(-102);
        return false;
    }

    if (!console_isValidColumn(console->column)) {
        halt(-103);
        // log_fatalf("Console column invalid: %d", (int)console->column);
        return false;
    }

    return true;
}

static uint16_t console_getIndex(int32_t row, int32_t column) {
    REQUIRES(console_isValidRow(row));
    REQUIRES(console_isValidColumn(column));
    int32_t index = (CONSOLE_WIDTH * row + column);
    ASSERT((index & 0xFFFF) == index);  // Should still be 16 bits
    STATIC_ASSERT(CONSOLE_SIZE < INT16_MAX);
    ENSURES(0 <= index && index < CONSOLE_SIZE);
    return (uint16_t)index;
}

static void console_scroll(void) {
    Pixel_t *base = (Pixel_t *)CONSOLE_MEM_BASE;
    for (int32_t row = 0; row < CONSOLE_HEIGHT - 1; row++) {
        uint16_t previousIndex = console_getIndex(row, 0);
        uint16_t nextIndex     = console_getIndex(row + 1, 0);
        memcpy(&base[previousIndex],
               &base[nextIndex],
               CONSOLE_WIDTH * sizeof(Pixel_t));
    }
    for (int32_t column = 0; column < CONSOLE_WIDTH; column++) {
        console_drawChar(CONSOLE_HEIGHT - 1, column, ' ', console.color);
    }
}


char console_putByte(char ch) {
    REQUIRES(console_isValid(&console));
    switch (ch) {
        case '\n':
            console.row++;
            FALL_THROUGH;
        case '\r':
            console.column = 0;
            break;
        case '\b':
            if (console.column == 0 && console.row == 0) {
                log_warn("Can't backspace off first row");
                break;
            } else if (console.column == 0) {
                ASSERT(console.row > 0);
                console.row--;
                ASSERT(console_isValidRow(console.row));
                console.column = CONSOLE_WIDTH - 1;
            } else {
                ASSERT(console.column > 0);
                console.column--;
            }
            ASSERT(console_isValidColumn(console.column));
            console_drawChar(console.row, console.column, ' ', console.color);
            break;
        default:
            console_drawChar(console.row, console.column, ch, console.color);
            console.column++;
            if (console.column >= CONSOLE_WIDTH) {
                ASSERT(console.column == CONSOLE_WIDTH);
                console.row++;
                console.column = 0;
            }
            break;
    }
    if (console.row >= CONSOLE_HEIGHT) {
        ASSERT(console.row == CONSOLE_HEIGHT);
        console.row--;
        ASSERT(console_isValidRow(console.row));
        console_scroll();
    }
    ENSURES(console_isValid(&console));
    return ch;
}

void console_putBytes(const char *s, size_t len) {
    REQUIRES(console_isValid(&console));

    for (size_t index = 0; index < len; index++) {
        console_putByte(s[index]);
    }
    ENSURES(console_isValid(&console));
}

ErrorCode_t console_setTermColor(int32_t color) {
    if (!console_isValidColor(color)) {
        return CONSOLE_BAD_COLOR;
    }
    console.color = color;

    return SUCCESS;
}


void console_getTermColor(uint8_t *color) {
    REQUIRES(color != NULL);
    *color = console.color;
}

ErrorCode_t console_setCursor(int32_t row, int32_t col) {
    REQUIRES(console_isValid(&console));
    if (!console_isValidRow(row)) {
        return CONSOLE_BAD_ROW;
    }
    if (!console_isValidColumn(col)) {
        return CONSOLE_BAD_COLUMN;
    }
    console.row    = row;
    console.column = col;

    ENSURES(console_isValid(&console));

    return SUCCESS;
}

void console_getCursor(int32_t *row, int32_t *col) {
    REQUIRES(row != NULL);
    REQUIRES(col != NULL);
    REQUIRES(console_isValid(&console));
    *row = console.row;
    *col = console.column;
}

void console_clear(void) {
    REQUIRES(console_isValid(&console));
    memset((Pixel_t *)CONSOLE_MEM_BASE, 0, CONSOLE_SIZE * sizeof(Pixel_t));
    console_setCursor(0, 0);
    ENSURES(console_isValid(&console));
}

void console_drawChar(int32_t row, int32_t col, char ch, uint8_t color) {
    REQUIRES(console_isValid(&console));
    REQUIRES(console_isValidRow(row));
    REQUIRES(console_isValidColumn(col));
    REQUIRES(console_isValidColor(color));

    uint16_t index = console_getIndex(row, col);
    STATIC_ASSERT(sizeof(Pixel_t) == 2);

    Pixel_t  pixel = {.color = color, .character = ch};
    Pixel_t *base  = (Pixel_t *)CONSOLE_MEM_BASE;
    base[index]    = pixel;
}

char console_getChar(int32_t row, int32_t col) {
    REQUIRES(console_isValid(&console));
    REQUIRES(console_isValidRow(row));
    REQUIRES(console_isValidColumn(col));

    uint16_t index = console_getIndex(row, col);
    Pixel_t *base  = (Pixel_t *)CONSOLE_MEM_BASE;
    return base[index].character;
}
