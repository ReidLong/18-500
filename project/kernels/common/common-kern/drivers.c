#include "drivers.h"
#include "console.h"
#include "interrupt.h"
#include "keyboard.h"
#include "timer.h"
#include <contracts.h>
#include <interrupt_cause.h>
#include <stdio.h>
#include <stdlib.h>

static void interrupt_dispatch(uint32_t cause, uint32_t value);

int drivers_install(Tickback_t tickback) {
    REQUIRES(tickback != NULL);
    console_init();
    int result = keyboard_init();
    if (result != 0) {
        printf("Keyboard initialization failed");
        return result;
    }
    timer_init(tickback);
    interrupt_install(interrupt_dispatch);
    return 0;
}


void interrupt_dispatch(uint32_t cause, uint32_t value) {
    switch (cause) {
        case TIMER_INTERRUPT: {
            timer_handler(value);
            break;
        }
        case EXTERNAL_INTERRUPT: {
            // TODO: If we ever wanted to support buttons, we would need another
            // cause here
            keyboard_handler(value);
            break;
        }
        default:
            panic("Unknown interrupt cause\n");
    }
}
