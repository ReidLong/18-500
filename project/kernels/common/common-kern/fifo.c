#include "fifo.h"
#include <malloc.h>
#include <stdlib.h>

int fifo_init(FIFO_t* fifo, size_t size) {
    fifo->buffer = calloc(sizeof(char), size);
    fifo->head   = 0;
    fifo->tail   = 0;
    fifo->size   = 0;
    return fifo->buffer == NULL;
}

static size_t fifo_increment(size_t current, size_t size) {
    return (current + 1) % size;
}

bool fifo_enqueue(FIFO_t* fifo, char data) {
    size_t nextTail = fifo_increment(fifo->tail, fifo->size);
    if (nextTail == fifo->head) {
        // Buffer is full
        return false;
    } else {
        fifo->buffer[fifo->tail] = data;
        fifo->tail               = nextTail;
        return true;
    }
}

bool fifo_dequeue(FIFO_t* fifo, char* data) {
    if (fifo->tail == fifo->head) {
        // Buffer is empty
        return false;
    } else {
        size_t nextHead = fifo_increment(fifo->head, fifo->size);
        *data           = fifo->buffer[fifo->head];
        fifo->head      = nextHead;
        return true;
    }
}
