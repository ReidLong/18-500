#ifndef __ERRORCODE_H
#define __ERRORCODE_H

#include <stdint.h>

typedef int32_t ErrorCode_t;

#define SUCCESS 0
#define UNKNOWN_ERROR -1

#define CONSOLE_BAD_COLOR -100
#define CONSOLE_BAD_ROW -101
#define CONSOLE_BAD_COLUMN -102

#endif