#ifndef __FIFO_H
#define __FIFO_H

#include <stdbool.h>
#include <stddef.h>

typedef struct {
    char*  buffer;
    size_t head, tail;
    size_t size;
} FIFO_t;

int fifo_init(FIFO_t* fifo, size_t size);

bool fifo_enqueue(FIFO_t* fifo, char data);
bool fifo_dequeue(FIFO_t* fifo, char* data);

#endif