#ifndef __FOOTER_H
#define __FOOTER_H

#include <stdint.h>
#include <video_defines.h>

#define DEFAULT_FOOTER (CONSOLE_HEIGHT - 3)

// The argument needs to have enough space for at least three rows of text
// A good value is (CONSOLE_HEIGHT - 4)
void footer_draw(int32_t row);

#endif