#ifndef __INTERRUPT_H
#define __INTERRUPT_H

#ifndef __ASSEMBLER__
#include <stdint.h>
#include <ureg.h>

typedef void (*CallBack_t)(uint32_t cause, uint32_t value);

void interrupt_install(CallBack_t callback);

void interrupt_handler(ureg_t* ureg);

extern void* interrupt_wrapper;

#endif

#endif