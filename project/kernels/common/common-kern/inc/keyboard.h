#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include <stdint.h>

int keyboard_init(void);

void keyboard_handler(uint32_t ascii);

int readchar(void);

#endif