#ifndef __TIMER_H
#define __TIMER_H

#include <stddef.h>
#include <stdint.h>

// 1 Tick = 5ms, 200 tick = 1000ms = 1s
#define TICK_PER_SECOND 200
#define SECOND_PER_MINUTE 60
#define MINUTE_PER_HOUR 60

typedef void (*Tickback_t)(uint32_t tick);

void timer_init(Tickback_t tickback);

void timer_handler(uint32_t tick);

void timer_toString(uint32_t ticks, char* buffer, size_t length);

uint32_t timer_getTicks(void);

#endif
