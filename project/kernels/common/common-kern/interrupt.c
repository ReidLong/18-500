#include "interrupt.h"
#include <asm.h>
#include <contracts.h>
#include <stdlib.h>

static CallBack_t interrupt_callback;

void interrupt_install(CallBack_t callback) {
    REQUIRES(callback != NULL);
    interrupt_callback = callback;

    set_mtvec(&interrupt_wrapper);
}

void interrupt_handler(ureg_t* ureg) {
    (void)ureg;
    uint32_t cause = get_mcause();
    uint32_t tval  = get_mtval();
    (interrupt_callback)(cause, tval);
}