#include "keyboard.h"
#include "fifo.h"
#include <stdlib.h>

#define BUFFER_SIZE 512
static FIFO_t fifo;

int keyboard_init(void) {
    return fifo_init(&fifo, BUFFER_SIZE);
}

void keyboard_handler(uint32_t ascii) {
    bool result = fifo_enqueue(&fifo, (char)ascii);
    if (!result) {
        // Not actually a panic, but for now this is fine
        panic("Unable to insert character: %c", (char)ascii);
    }
}

int readchar(void) {
    char data;

    bool foundData = fifo_dequeue(&fifo, &data);

    if (foundData) {
        return data;
    } else {
        return -1;
    }
}