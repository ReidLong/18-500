
#include "timer.h"
#include <contracts.h>
#include <stddef.h>
#include <stdio.h>

static Tickback_t timer_tickback;
static uint32_t   totalTicks = 0;

void timer_init(Tickback_t tickback) {
    REQUIRES(tickback != NULL);
    timer_tickback = tickback;
    totalTicks     = 0;
}

void timer_handler(uint32_t tick) {
    (void)tick;
    // Turns out it isn't true, I guess we'll use kernel ticks?
    // Actually it probably is true, the issue is that memory wasn't initialized
    // in init due to hot-clear
    // But to be safe, let's just leave it out
    // ASSERT(tick == totalTicks);  // I think this should be true
    ++totalTicks;
    timer_tickback(totalTicks);
}

void timer_toString(uint32_t ticks, char* buffer, size_t length) {
    uint32_t seconds = ticks / TICK_PER_SECOND;
    uint32_t minutes = seconds / SECOND_PER_MINUTE;
    uint32_t hours   = minutes / MINUTE_PER_HOUR;

    uint32_t s = seconds % SECOND_PER_MINUTE;
    uint32_t m = minutes % MINUTE_PER_HOUR;

    snprintf(buffer, length, "%01ld:%02ld:%02ld", hours, m, s);
}

uint32_t timer_getTicks(void) {
    return totalTicks;
}