#ifndef __ALIGNMENT_H
#define __ALIGNMENT_H

#define ALIGN_TO(x, multiple) ((x) & ~((multiple)-1))
#define IS_ALIGNED_TO(x, multiple) ((x) == ALIGN_TO(x, multiple))

#endif