#ifndef __ATTRIBUTES_H
#define __ATTRIBUTES_H

#ifndef NO_RETURN
#define NO_RETURN void __attribute__((__noreturn__))
#endif

#ifndef PACKED
#define PACKED __attribute__((packed))
#endif

#ifndef MUST_CHECK
#define MUST_CHECK(type) type __attribute__((warn_unused_result))
#endif

#ifndef FALL_THROUGH
#define FALL_THROUGH __attribute__((fallthrough))
#endif

#endif