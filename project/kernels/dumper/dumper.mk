410KDIR = 410kern
410SDIR = spec
STUKDIR = kern

SELF_DIR = $(dir $(lastword $(MAKEFILE_LIST)))

include $(SELF_DIR)/$(410KDIR)/common_kern.mk

STUDENT_SOURCES = dumper.c common/console.c

KERNEL_SOURCES = $(COMMON_KERNEL_SOURCES) $(addprefix $(STUKDIR)/,$(STUDENT_SOURCES))

KERNEL_INCLUDE_PATHS = $(STUKDIR) $(STUKDIR)/inc $(STUKDIR)/common/inc $(COMMON_KERNEL_INCLUDE_PATHS)
