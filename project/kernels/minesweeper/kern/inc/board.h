/** @file board.h
 * @brief Abstraction for the game board
 *
 * @author Reid Long (relong)
 */

#ifndef __BOARD_H
#define __BOARD_H

/* REQUIRES: 0 <= level < numLevels */
void board_init(int level);


#define DRAW_MODE_COUNT 3
typedef enum { BLUR = 0, REVEAL = 1, NORMAL = 2 } DrawMode_t;

#define MINE_CHARACTER 'X'
#define MARK_CHARACTER '*'
#define ZERO_CHARACTER '0'

void board_draw(DrawMode_t mode);

void board_blinkCursor(void);

/* REQUIRES: key == {UP, LEFT, RIGHT, DOWN} */
void board_moveCursor(int key);

typedef enum { MINE, GOOD_PLAY, ALL_CLEAR } ActionResult_t;

/* REQUIRES: key == OPEN_KEY || key == MARK_KEY */
ActionResult_t board_applyAction(int key);

int board_getRunningMineCount(void);

#endif