#ifndef __GAME_H
#define __GAME_H

typedef enum { TITLE = 0, HELP = 1, PLAY = 2, END_GAME = 3 } GameState_t;

// Should be big enough
#define TIME_BUFFER_SIZE 20

#endif