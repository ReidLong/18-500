#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "help.h"
#include "keyboard.h"
#include "over.h"
#include "play.h"
#include "title.h"
#include <asm.h>
#include <drivers.h>
#include <interrupt_cause.h>
#include <string.h>
#include <video_defines.h>

#define STATE_HANDLER_COUNT 4

static void (*outputLogic[STATE_HANDLER_COUNT])(void) = {
    [TITLE]    = title_output,
    [HELP]     = help_output,
    [PLAY]     = play_output,
    [END_GAME] = over_output,
};

static GameState_t (*nextStateLogic[STATE_HANDLER_COUNT])(int key) = {
    [TITLE]    = title_nextState,
    [HELP]     = help_nextState,
    [PLAY]     = play_nextState,
    [END_GAME] = over_nextState,
};

static GameState_t currentState = TITLE;

void game_tick(uint32_t numTicks) {
    if (currentState == PLAY) {
        play_tick(numTicks);
    }
}

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    drivers_install(game_tick);

    enable_interrupts();

    currentState = TITLE;

    play_reset(0);  // Setup game

    while (1) {  // Have fun

        // do output logic
        (*outputLogic[currentState])();

        // Read input
        int key;
        // Spin until we find a key press
        while ((key = readchar()) == -1) continue;

        // do next state logic
        GameState_t nextState = (*nextStateLogic[currentState])(key);

        // Transition
        currentState = nextState;
    }

    panic("Game isn't suppose to stop!");

    environment_exit(911);

    panic("Exit failed?");
}
