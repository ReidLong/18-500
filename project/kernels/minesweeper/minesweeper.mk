410KDIR = 410kern
410SDIR = spec
STUKDIR = kern

SELF_DIR = $(dir $(lastword $(MAKEFILE_LIST)))

include $(SELF_DIR)/$(410KDIR)/common_kern.mk

STUDENT_SOURCES = main.c common/console.c common/interrupt_asm.S common/interrupt.c\
common/fifo.c common/drivers.c common/timer.c common/keyboard.c common/malloc_wrappers.c\
play.c over.c board.c title.c help.c score.c minesweeper_levels.c common/footer.c

KERNEL_SOURCES = $(COMMON_KERNEL_SOURCES) $(addprefix $(STUKDIR)/,$(STUDENT_SOURCES))

KERNEL_INCLUDE_PATHS = $(STUKDIR) $(STUKDIR)/inc $(STUKDIR)/common/inc $(COMMON_KERNEL_INCLUDE_PATHS)
