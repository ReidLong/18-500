#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <interrupt_cause.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 5

static volatile int interrupts = 0;

static void interrupt_dispatch(uint32_t cause, uint32_t value);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    printf("Hello World\n");

    interrupt_install(&interrupt_dispatch);

    enable_interrupts();

    volatile int steps = 0;
    while (1) {
        steps++;
    }

    environment_exit(200);

    panic("Exit failed?");
}

static int ticks = 0;

static void interrupt_dispatch(uint32_t cause, uint32_t value) {
    interrupts++;
    if (cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, value, value);
        }
    } else {
        printf("Tock: %c (%lx)\n", (char)value, value);
    }
}