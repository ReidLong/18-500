#!/usr/bin/python

_447TestsPath = 'tests/447'
_447Tests = [
    "additest.S",
    "addtest.S",
    "arithtest.S",
    "brtest0.S",
    "brtest1.S",
    "brtest2.S",
    "depend.S",
    "dependLow.S",
    "fib.c",
    "matrix_mult.c",
    "memtest0.S",
    "memtest1.S",
    "shifttest.S",
    # "syscalltest.S",
]

handWrittenPath = 'tests/hand_written'

handWrittenTests = [
    "additest-nop.S",
    "additest-safe.S",
    "additest-stall.S",
    "bad-store.S",
    "jumptest-2.S",
    "jumptest-basic.S",
    "predictTest.S",
    "small-arithtest.S",
    "test-branch.S",
    "test-branch2.S",
    "test-branch3.S",
    "test-branch5.S",
    "test-jump.S",
    "test-loop-small.S",
    "test-loop.S",
    "test-loop2.S",
    "test-loop3.S",
    "test-loop4.S",
    "test-tricky-branch.S",
    "atomic_store.S",
    "atomic_load.S",
    # "mul-test.S",
    # "mulh-test.S",
    # "mulhu-test.S",
    # "mulhsu-test.S",
    # "div-test.S",
    # "divu-test.S",
    # "rem-test.S",
    # "remu-test.S",
    "test-40-debug.S",
    "csr_test.S",
    "csr_test2.S",
    "csr_test3.S",
    'timer_tick.S',
    'timer_tick2.S',
]

handWrittenTests += ['B/test-B-{}.S'.format(i) for i in xrange(1, 21)]

benchmarkPath = "tests/benchmarks"

benchmarks = [
    "fibi.c",
    "fibm.c",
    "fibr.c",
]

generatedPath = 'tests/generated'

generatedTests = ['test-{}.S'.format(i) for i in xrange(1,69)]

kernels = [
    'hello',
    'dumper',
    'console',
    'tick_tock',
    'rainbow',
    'station',
]

kernelTests = ['kernels/{}/{}.mk'.format(k, k) for k in kernels]


import subprocess
import re
import sys


def executeTest(test, script):
    bashCommand = 'make assemble TEST={}'.format(test)
    exitStatus = subprocess.call(bashCommand.split())
    if exitStatus != 0:
        print bashCommand
        exit(exitStatus)
    bashCommand = 'make {} TEST={}'.format(script, test)
    print bashCommand
    output = subprocess.check_output(bashCommand.split())
    match = re.search('Correct!', output)
    if match == None:
        print('Test: {} failed!'.format(test))
        exit(1)

def generateGolden(test, ignored):
    bashCommand = './generateGolden.py {}'.format(test)
    exitStatus = subprocess.call(bashCommand.split())
    if exitStatus != 0:
        print bashCommand
        exit(exitStatus)

def runTestSet(testArray, path, script):
    for file in testArray:
        if path:
            executeTest('{}/{}'.format(path, file), script)
        else:
            executeTest(file, script)
        # generateGolden('{}/{}'.format(path, file), script)


if len(sys.argv) < 2:
    print('USAGE: {} [arch-verify/verify]'.format(sys.argv[0]))
    exit(2)

RUN_MODES = ['arch-verify', 'verify', 'arch-run']

if sys.argv[1] in RUN_MODES:
    runTestSet(_447Tests, _447TestsPath, sys.argv[1])
    runTestSet(handWrittenTests, handWrittenPath, sys.argv[1])
    runTestSet(benchmarks, benchmarkPath, sys.argv[1])
    runTestSet(generatedTests, generatedPath, sys.argv[1])
    runTestSet(kernelTests, None, sys.argv[1])
    print "All tests passed"






