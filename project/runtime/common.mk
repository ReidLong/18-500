
COMMON_CFLAGS = -DDEBUG -Wall -Wextra -Werror -std=gnu99 -pedantic -g -Werror=implicit-function-declaration

# The compiler for test programs, and its flags. Even though integer
# multiplication and floating point instructions aren't supported by the
# processor, we can use libgcc's software implementation of these instructions.
RISCV_CC = riscv64-unknown-elf-gcc
RISCV_CFLAGS = $(COMMON_CFLAGS) -static -nostdlib -nostartfiles -march=rv32i -mabi=ilp32 -Wa,-march=rv32ia -mstrict-align
# -save-temps
# RISCV_CFLAGS = $(COMMON_CFLAGS) -static -nostartfiles -march=rv32i -mabi=ilp32

RISCV_LD = riscv64-unknown-elf-ld
RISCV_AR = riscv64-unknown-elf-ar
RISCV_OBJCOPY = riscv64-unknown-elf-objcopy