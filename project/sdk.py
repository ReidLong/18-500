#!/bin/python3

import sys
import os
import shutil

if len(sys.argv) < 3:
    print("Usage: {} <version> <impl>".format(sys.argv[0]))
    exit(2)

# Delete SDK folder
# Copy hdf
# Write sdk.tcl to versionFolder/sdk.tcl

versionFolder = sys.argv[1]
implementation = sys.argv[2]
# This is so sad, but Windows sucks
projectRoot = os.getcwd()
# This will convert /mnt/c to c:
windowsRoot = 'c:{}'.format(projectRoot[6:])

vivadoRoot = os.path.join(projectRoot, 'vivado')
versionPath = os.path.join(vivadoRoot, versionFolder)
tclScriptPath = os.path.join(versionPath, 'sdk.tcl')
zedboardPath = os.path.join(versionPath, 'zedboard') 
sdkPath = os.path.join(zedboardPath, 'zedboard.sdk')
vivadoHDF = os.path.join(zedboardPath, 'zedboard.runs', implementation, 'design_1_wrapper.sysdef')
sdkHDF = os.path.join(sdkPath, 'design_1_wrapper.hdf')

print('Setting up file system for {}'.format(sdkPath))

if os.path.isdir(sdkPath):
	shutil.rmtree(sdkPath)
os.mkdir(sdkPath)
shutil.copyfile(vivadoHDF, sdkHDF)

print('Generating TCL Script: {}'.format(tclScriptPath))

template = """
# Set Workspace
setws {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.sdk
createhw -name design_1_wrapper_hw_platform_0 -hwspec {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.sdk/design_1_wrapper.hdf

createapp -name riscv_bootloader -app {{Hello World}} -proc ps7_cortexa9_0 -hwproject design_1_wrapper_hw_platform_0 -os standalone

#projects -build

"""

tclScript = template.format(projectRoot=windowsRoot, versionFolder=versionFolder)

scriptFile = open(tclScriptPath, 'w')
scriptFile.write(tclScript)
scriptFile.close()

print('All done')
