#!/bin/python3

import sys
import os
import shutil

if len(sys.argv) < 2:
	print('Usage: {} <version>'.format(sys.argv[0]))
	exit(2)

# Copy files over
# Update .cproject

versionFolder = sys.argv[1]
projectRoot = os.getcwd()

sdkRoot = os.path.join(projectRoot, 'vivado', versionFolder, 'zedboard', 'zedboard.sdk')
testPath = os.path.join(projectRoot, 'output', 'sdk', 'sdk.test.o')
bootloaderPath = os.path.join(sdkRoot, 'riscv_bootloader')
bootloaderSrc = os.path.join(bootloaderPath, 'src')

if not os.path.exists(testPath):
	print('Test missing: {}'.format(testPath))
	exit(3)

if not os.path.exists(bootloaderSrc):
	print('SDK Missing: {}'.format(sdkRoot))
	exit(4)

sourceFiles = [
	'helloworld.c',
	'main.c',
	'lscript.ld',	
]

print('Copying files')
for fileName in sourceFiles:
	filePath = os.path.join(projectRoot, 'sdk', fileName)
	srcPath = os.path.join(bootloaderSrc, fileName)
	shutil.copyfile(filePath, srcPath)

testSrcPath = os.path.join(bootloaderSrc, 'sdk.test.o')
shutil.copyfile(testPath, testSrcPath)


import xml.etree.ElementTree as ET
import random

projectPath = os.path.join(bootloaderPath, '.cproject')

print('Reading cproject: {}'.format(projectPath))
tree = ET.parse(projectPath)
root = tree.getroot()

linkers = [element for element in root.iter('tool') if element.attrib['superClass'] == 'xilinx.gnu.armv7.c.toolchain.linker.debug']

if len(linkers) != 1:
	print('No linker in cproject {}'.format(projectPath))
	exit(5)

linker = linkers[0]

userObjectsOption = ET.SubElement(linker, 'option')
userObjectsOption.set('valueType', 'userObjs')
userObjectsOption.set('id', 'xilinx.gnu.c.link.option.userobjs.{}'.format(random.randint(0, 2**30)))
userObjectsOption.set('superClass', 'xilinx.gnu.c.link.option.userobjs')

listOptionValue = ET.SubElement(userObjectsOption, 'listOptionValue')
listOptionValue.set('builtIn', 'false')
listOptionValue.set('value', '"${workspace_loc:/${ProjName}/src/sdk.test.o}"')

print('Writing cproject: {}'.format(projectPath))

cprojectFile = open(projectPath, 'w')
cprojectFile.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?><?fileVersion 4.0.0?>')

tempPath = '{}.tmp'.format(projectPath)
tree.write(tempPath)
tempFile = open(tempPath, 'r')

for line in tempFile:
	cprojectFile.write(line)

tempFile.close()
cprojectFile.close()
os.remove(tempPath)

print('Patching completed')