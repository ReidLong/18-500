#include <compiler.h>
#include <control_status_register.h>
#include <stdio.h>

#define MACHINE_STATUS_POINTER ((MachineStatus_t*)&cpu_state->csrs[MSTATUS])

uint32_t csr_read(cpu_state_t* cpu_state, ControlStatusRegisterName_t name) {
    return cpu_state->csrs[name];
}

void csr_update(cpu_state_t* cpu_state) {
    cpu_state->csrs[CYCLE]++;
    cpu_state->csrs[INSTRET]++;
    cpu_state->csrs[INSTRUCTION_CACHE_HIT]++;
    cpu_state->csrs[DATA_CACHE_HIT]++;
}

void csr_write(cpu_state_t*                cpu_state,
               ControlStatusRegisterName_t name,
               uint32_t                    value) {
    switch (name) {
        case SATP:
        case MTVEC:
        case MSCRATCH:
        case MEPC:
        case MCAUSE:
        case MTVAL:
            cpu_state->csrs[name] = value;
            break;
        case MSTATUS:
            cpu_state->csrs[name] = value & MSTATUS_WRITE_MASK;
            break;
        default:
            fprintf(stderr,
                    "Invalid CSR Write: %d [%x] = %d (%x)\n",
                    name,
                    name,
                    value,
                    value);
            cpu_state->halted = true;
            break;
    }
}

bool csr_interruptsEnabled(cpu_state_t* cpu_state) {
    STATIC_ASSERT(sizeof(MachineStatus_t) == sizeof(uint32_t));

    MachineStatus_t* status = MACHINE_STATUS_POINTER;

    return status->machineInterruptsEnabled == 1;
}

void csr_deliverInterrupt(cpu_state_t* cpu_state,
                          uint32_t     cause,
                          uint32_t     val,
                          uint32_t     epc) {
    cpu_state->csrs[MCAUSE] = cause;
    cpu_state->csrs[MEPC]   = epc;
    cpu_state->csrs[MTVAL]  = val;

    MachineStatus_t* status = MACHINE_STATUS_POINTER;

    MachineStatus_t nextStatus = {
        .zero1                         = 0,
        .machineInterruptsEnabled      = INTERRUPT_DISABLED,
        .zero2                         = 0,
        .machinePriorInterruptsEnabled = status->machineInterruptsEnabled,
        .zero3                         = 0,
        .machinePreviousPriviledge     = cpu_state->currentMode,
        .zero4                         = 0,
    };
    *status = nextStatus;

    cpu_state->currentMode = MACHINE_MODE;
    cpu_state->pc          = cpu_state->csrs[MTVEC];
}

void csr_interruptReturn(cpu_state_t* cpu_state) {
    MachineStatus_t* status = MACHINE_STATUS_POINTER;

    MachineStatus_t nextStatus = {
        .zero1                         = 0,
        .machineInterruptsEnabled      = status->machinePriorInterruptsEnabled,
        .zero2                         = 0,
        .machinePriorInterruptsEnabled = INTERRUPT_ENABLED,  // Enabled
        .zero3                         = 0,
        .machinePreviousPriviledge     = MACHINE_MODE,
        .zero4                         = 0,
    };

    cpu_state->currentMode = status->machinePreviousPriviledge;
    *status                = nextStatus;

    cpu_state->pc = cpu_state->csrs[MEPC];
}