#ifndef __CONTROL_STATUS_REGISTER_H
#define __CONTROL_STATUS_REGISTER_H

#include <riscv_isa.h>
#include <sim.h>
#include <stdbool.h>

uint32_t csr_read(cpu_state_t* cpu_state, ControlStatusRegisterName_t name);

void csr_write(cpu_state_t*                cpu_state,
               ControlStatusRegisterName_t name,
               uint32_t                    value);

bool csr_interruptsEnabled(cpu_state_t* cpu_state);

void csr_deliverInterrupt(cpu_state_t* cpu_state,
                          uint32_t     cause,
                          uint32_t     val,
                          uint32_t     epc);

void csr_interruptReturn(cpu_state_t* cpu_state);

void csr_update(cpu_state_t* cpu_state);

#endif