#ifndef __INTERRUPT_CONTROLLER_H
#define __INTERRUPT_CONTROLLER_H

#include <sim.h>

#define CYCLES_PER_TICK 250000

void interruptController_check(cpu_state_t* cpu_state);

#endif