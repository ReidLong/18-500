/**
 * riscv_isa.h
 *
 * RISC-V 32-bit Instruction Level Simulator
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This contains the definitions for the RISC-V opcodes and function codes for
 * the instructions that must be implemented by the simulator.
 *
 * Note that the names of the enumerations are based on the names given in
 * chapter 2 of the RISC-V 2.2 ISA manual.
 *
 * Authors:
 *  - 2016 - 2017: Brandon Perez
 **/

#ifndef RISCV_ISA_H_
#define RISCV_ISA_H_

/*----------------------------------------------------------------------------
 * Definitions
 *----------------------------------------------------------------------------*/

#include <attributes.h>

// The number of registers in the register file
#define RISCV_NUM_REGS 32

#define CSR_BITS 12

/*----------------------------------------------------------------------------
 * Opcodes (All Instruction Types)
 *----------------------------------------------------------------------------*/

// Opcodes for the RISC-V ISA, in the lowest 7 bits of the instruction
typedef enum opcode {
    // Opcode that indicates an integer R-type instruction (register)
    OP_OP = 0x33,

    // Opcode that indicates an integer I-type instruction (immediate)
    OP_IMM = 0x13,

    // Opcodes for load and store instructions (I-type and S-type)
    OP_LOAD  = 0x03,
    OP_STORE = 0x23,

    // Opcode that indicates an atomic instruction
    OP_AMO = 0x2F,

    // Opcodes for U-type instructions (unsigned immediate)
    OP_LUI   = 0x37,
    OP_AUIPC = 0x17,

    // Opcodes for jump instructions (UJ-type and I-type)
    OP_JAL  = 0x6F,
    OP_JALR = 0x67,

    // Opcode that indicates a general SB-type instruction (branch)
    OP_BRANCH = 0x63,

    // Opcode that indicates a special system instruction (I-type)
    OP_SYSTEM = 0x73,
} opcode_t;

/*----------------------------------------------------------------------------
 * 7-bit Function Codes (R-type and I-type Instructions)
 *----------------------------------------------------------------------------*/

// 7-bit function codes, the highest 7-bits of the instruction
typedef enum riscv_funct7 {
    FUNCT7_INT     = 0x00,  // Typical integer instruction
    FUNCT7_ALT_INT = 0x20,  // Alternate instruction (sub/sra/srai)
    FUNCT7_MULDIV  = 0x01,  // Multiplication and Division Extension
} funct7_t;

/*----------------------------------------------------------------------------
 * R-type Function Codes
 *----------------------------------------------------------------------------*/

// 3-bit function codes for integer R-type instructions
typedef enum riscv_rtype_funct3 {
    FUNCT3_ADD_SUB = 0x0,  // Add/subtract
    FUNCT3_SLL     = 0x1,  // Shift left logical
    FUNCT3_SLT     = 0x2,  // Set on less than signed
    FUNCT3_SLTU    = 0x3,  // Set on less than unsigned
    FUNCT3_XOR     = 0x4,  // Bit-wise xor
    FUNCT3_SRL_SRA = 0x5,  // Shift right logical/arithmetic
    FUNCT3_OR      = 0x6,  // Bit-wise or
    FUNCT3_AND     = 0x7,  // Bit-wise and
} rtype_funct3_t;

typedef enum {
    FUNCT3_MUL    = 0x0,  // Multiplication
    FUNCT3_MULH   = 0x1,  // Multiplication (signed x signed)
    FUNCT3_MULHSU = 0x2,  // Multiplication (signed x unsigned)
    FUNCT3_MULHU  = 0x3,  // Multiplication (unsigned x unsigned)
    FUNCT3_DIV    = 0x4,  // Division
    FUNCT3_DIVU   = 0x5,  // Division (Unsigned)
    FUNCT3_REM    = 0x6,  // Remainder
    FUNCT3_REMU   = 0x7,  // Remainder (Unsigned)
} rtype_m_funct3_t;

/*----------------------------------------------------------------------------
 * I-type Function Codes
 *----------------------------------------------------------------------------*/

// 3-bit function codes for integer I-type instructions
typedef enum riscv_itype_int_funct3 {
    FUNCT3_ADDI      = 0x0,  // Add immediate
    FUNCT3_SLTI      = 0x2,  // Set on less than signed
    FUNCT3_SLTIU     = 0x3,  // Set on less than unsigned
    FUNCT3_XORI      = 0x4,  // Bit-wise xor immediate
    FUNCT3_ORI       = 0x6,  // Bit-wise or immediate
    FUNCT3_ANDI      = 0x7,  // Bit-wise and immediate
    FUNCT3_SLLI      = 0x1,  // Shift left logical immediate
    FUNCT3_SRLI_SRAI = 0x5,  // Shift right logical/arithmetic
} itype_int_funct3_t;

// 3-bit function codes for load instructions (I-type)
typedef enum riscv_itype_load_funct3 {
    FUNCT3_LB  = 0x0,  // Load byte (1 byte) signed
    FUNCT3_LH  = 0x1,  // Load halfword (2 bytes) signed
    FUNCT3_LW  = 0x2,  // Load word (4 bytes)
    FUNCT3_LBU = 0x4,  // Load byte (1 byte) unsigned
    FUNCT3_LHU = 0x5,  // Load halfword (2 bytes) unsigned
} itype_load_funct3_t;

// 12-bit function codes for special system instructions (I-type)
typedef enum riscv_itype_funct12 {
    FUNCT12_ECALL = 0x000,  // Environment call
    FUNCT12_MRET  = 0x302,
} itype_funct12_t;


typedef enum {
    FUNCT3_PRIV   = 0x0,
    FUNCT3_CSRRW  = 0x1,
    FUNCT3_CSRRS  = 0x2,
    FUNCT3_CSRRC  = 0x3,
    FUNCT3_CSRRWI = 0x5,
    FUNCT3_CSRRSI = 0x6,
    FUNCT3_CSRRCI = 0x7,
} itype_system_funct3_t;


/*----------------------------------------------------------------------------
 * S-type Function Codes
 *----------------------------------------------------------------------------*/

// 3-bit function codes for S-type instructions (store)
typedef enum riscv_stype_funct3 {
    FUNCT3_SB = 0x0,  // Store byte (1 byte)
    FUNCT3_SH = 0x1,  // Store halfword (2 bytes)
    FUNCT3_SW = 0x2,  // Store word (4 bytes)
} stype_funct3_t;


// 5-bit function codes for AMO instructions
typedef enum {
    FUNCT5_LR = 0x2,  // Load Reserved
    FUNCT5_SC = 0x3,  // Store Conditional
} amo_funct5_t;

/*----------------------------------------------------------------------------
 * SB-type Function Codes
 *----------------------------------------------------------------------------*/

// 3-bit function codes for SB-type instructions (branch)
typedef enum riscv_sbtype_funct3 {
    FUNCT3_BEQ  = 0x0,  // Branch if equal
    FUNCT3_BNE  = 0x1,  // Branch if not equal
    FUNCT3_BLT  = 0x4,  // Branch if less than (signed)
    FUNCT3_BGE  = 0x5,  // Branch if greater than or equal
    FUNCT3_BLTU = 0x6,  // Branch if less than (unsigned)
    FUNCT3_BGEU = 0x7,  // Branch if greater than or equal
} sbtype_funct3_t;

/*----------------------------------------------------------------------------
 * ISA Register Names
 *----------------------------------------------------------------------------*/

// Enumeration of the registers in the ISA
typedef enum logic {
    REG_X0  = 0,   // ISA Register 0, hardwired to 0
    REG_X1  = 1,   // ISA Register 1
    REG_X2  = 2,   // ISA Register 2
    REG_X3  = 3,   // ISA Register 3
    REG_X4  = 4,   // ISA Register 4
    REG_X5  = 5,   // ISA Register 5
    REG_X6  = 6,   // ISA Register 6
    REG_X7  = 7,   // ISA Register 7
    REG_X8  = 8,   // ISA Register 8
    REG_X9  = 9,   // ISA Register 9
    REG_X10 = 10,  // ISA Register 10
    REG_X11 = 11,  // ISA Register 11
    REG_X12 = 12,  // ISA Register 12
    REG_X13 = 13,  // ISA Register 13
    REG_X14 = 14,  // ISA Register 14
    REG_X15 = 15,  // ISA Register 15
    REG_X16 = 16,  // ISA Register 16
    REG_X17 = 17,  // ISA Register 17
    REG_X18 = 18,  // ISA Register 18
    REG_X19 = 19,  // ISA Register 19
    REG_X20 = 20,  // ISA Register 20
    REG_X21 = 21,  // ISA Register 21
    REG_X22 = 22,  // ISA Register 22
    REG_X23 = 23,  // ISA Register 23
    REG_X24 = 24,  // ISA Register 24
    REG_X25 = 25,  // ISA Register 25
    REG_X26 = 26,  // ISA Register 26
    REG_X27 = 27,  // ISA Register 27
    REG_X28 = 28,  // ISA Register 28
    REG_X29 = 29,  // ISA Register 29
    REG_X30 = 30,  // ISA Register 30
    REG_X31 = 31,  // ISA Register 31
} riscv_isa_reg_t;


// CSRs

typedef enum {
    SATP     = 0x180,  // Virtual Memory Page Directory
    MSTATUS  = 0x300,  // Enables/Disables interrupts
    MTVEC    = 0x305,  // Holds the address of the interrupt handler
    MSCRATCH = 0x340,  // R/W scratch register (holds kernel SP)
    MEPC     = 0x341,  // Faulting instruction address
    MCAUSE   = 0x342,  // Holds the cause of the exception
    MTVAL    = 0x343,  // Faulting address (for page faults)

    CYCLE        = 0xC00,
    INSTRET      = 0xC02,
    HPMCOUNTER3  = 0xC03,  // INSTRUCTIONS_FETCHED
    HPMCOUNTER4  = 0xC04,  // FORWARD_BRANCH_RETIRED
    HPMCOUNTER5  = 0xC05,  // BACKWARD_BRANCH_RETIRED
    HPMCOUNTER6  = 0xC06,  // FORWARD_BRANCH_PREDICTED_CORRECT
    HPMCOUNTER7  = 0xC07,  // BACKWARD_BRANCH_PREDICTED_CORRECT
    HPMCOUNTER8  = 0xC08,  // FORWARD_BRANCH_PREDICTED_INCORRECT
    HPMCOUNTER9  = 0xC09,  // BACKWARD_BRANCH_PREDICTED_INCORRECT
    HPMCOUNTER10 = 0xC0A,  // INSTRUCTION_CACHE_HIT
    HPMCOUNTER11 = 0xC0B,  // INSTRUCTION_CACHE_MISS
    HPMCOUNTER12 = 0xC0C,  // DATA_CACHE_HIT
    HPMCOUNTER13 = 0xC0D,  // DATA_CACHE_MISS
    HPMCOUNTER14 = 0xC0E,  // DATA_VRAM_HIT
    HPMCOUNTER15 = 0xC0F,  // STORE_CONDITIONAL_SUCCESS
    HPMCOUNTER16 = 0xC10,  // STORE_CONDITIONAL_REJECT
    HPMCOUNTER17 = 0xC11,  // KEYBOARD_INTERRUPT_USER
    HPMCOUNTER18 = 0xC12,  // KEYBOARD_INTERRUPT_MACHINE
    HPMCOUNTER19 = 0xC13,  // EXECEPTION_USER
    HPMCOUNTER20 = 0xC14,  // EXCEPTION_MACHINE
    HPMCOUNTER21 = 0xC15,  // TIMER_INTERRUPT_USER
    HPMCOUNTER22 = 0xC16,  // TIMER_INTERRUPT_MACHINE
    HPMCOUNTER23 = 0xC17,  // FORWARD_JUMP_RETIRED
    HPMCOUNTER24 = 0xC18,  // BACKWARD_JUMP_RETIRED
    HPMCOUNTER25 = 0xC19,  // FORWARD_JUMP_PREDICTED_CORRECT
    HPMCOUNTER26 = 0xC1A,  // BACKWARD_JUMP_PREDICTED_CORRECT
    HPMCOUNTER27 = 0xC1B,  // FORWARD_JUMP_PREDICTED_INCORRECT
    HPMCOUNTER28 = 0xC1C,  // BACKWARD_JUMP_PREDICTED_INCORRECT
    HPMCOUNTER29 = 0xC1D,  // Unimplemented
    HPMCOUNTER30 = 0xC1E,  // Unimplemented
    HPMCOUNTER31 = 0xC1F,  // Unimplemented

    CYCLEH        = 0xC80,
    INSTRETH      = 0xC82,
    HPMCOUNTER3H  = 0xC83,  // INSTRUCTIONS_FETCHED
    HPMCOUNTER4H  = 0xC84,  // FORWARD_BRANCH_RETIRED
    HPMCOUNTER5H  = 0xC85,  // BACKWARD_BRANCH_RETIRED
    HPMCOUNTER6H  = 0xC86,  // FORWARD_BRANCH_PREDICTED_CORRECT
    HPMCOUNTER7H  = 0xC87,  // BACKWARD_BRANCH_PREDICTED_CORRECT
    HPMCOUNTER8H  = 0xC88,  // FORWARD_BRANCH_PREDICTED_INCORRECT
    HPMCOUNTER9H  = 0xC89,  // BACKWARD_BRANCH_PREDICTED_INCORRECT
    HPMCOUNTER10H = 0xC8A,  // INSTRUCTION_CACHE_HIT
    HPMCOUNTER11H = 0xC8B,  // INSTRUCTION_CACHE_MISS
    HPMCOUNTER12H = 0xC8C,  // DATA_CACHE_HIT
    HPMCOUNTER13H = 0xC8D,  // DATA_CACHE_MISS
    HPMCOUNTER14H = 0xC8E,  // DATA_VRAM_HIT
    HPMCOUNTER15H = 0xC8F,  // STORE_CONDITIONAL_SUCCESS
    HPMCOUNTER16H = 0xC90,  // STORE_CONDITIONAL_REJECT
    HPMCOUNTER17H = 0xC91,  // KEYBOARD_INTERRUPT_USER
    HPMCOUNTER18H = 0xC92,  // KEYBOARD_INTERRUPT_MACHINE
    HPMCOUNTER19H = 0xC93,  // EXECEPTION_USER
    HPMCOUNTER20H = 0xC94,  // EXCEPTION_MACHINE
    HPMCOUNTER21H = 0xC95,  // TIMER_INTERRUPT_USER
    HPMCOUNTER22H = 0xC96,  // TIMER_INTERRUPT_MACHINE
    HPMCOUNTER23H = 0xC97,  // FORWARD_JUMP_RETIRED
    HPMCOUNTER24H = 0xC98,  // BACKWARD_JUMP_RETIRED
    HPMCOUNTER25H = 0xC99,  // FORWARD_JUMP_PREDICTED_CORRECT
    HPMCOUNTER26H = 0xC9A,  // BACKWARD_JUMP_PREDICTED_CORRECT
    HPMCOUNTER27H = 0xC9B,  // FORWARD_JUMP_PREDICTED_INCORRECT
    HPMCOUNTER28H = 0xC9C,  // BACKWARD_JUMP_PREDICTED_INCORRECT
    HPMCOUNTER29H = 0xC9D,  // Unimplemented
    HPMCOUNTER30H = 0xC9E,  // Unimplemented
    HPMCOUNTER31H = 0xC9F,  // Unimplemented
} ControlStatusRegisterName_t;

typedef enum {
    INSTRUCTIONS_FETCHED                = HPMCOUNTER3,
    FORWARD_BRANCH_RETIRED              = HPMCOUNTER4,
    BACKWARD_BRANCH_RETIRED             = HPMCOUNTER5,
    FORWARD_BRANCH_PREDICTED_CORRECT    = HPMCOUNTER6,
    BACKWARD_BRANCH_PREDICTED_CORRECT   = HPMCOUNTER7,
    FORWARD_BRANCH_PREDICTED_INCORRECT  = HPMCOUNTER8,
    BACKWARD_BRANCH_PREDICTED_INCORRECT = HPMCOUNTER9,
    INSTRUCTION_CACHE_HIT               = HPMCOUNTER10,
    INSTRUCTION_CACHE_MISS              = HPMCOUNTER11,
    DATA_CACHE_HIT                      = HPMCOUNTER12,
    DATA_CACHE_MISS                     = HPMCOUNTER13,
    DATA_VRAM_HIT                       = HPMCOUNTER14,
    STORE_CONDITIONAL_SUCCESS           = HPMCOUNTER15,
    STORE_CONDITIONAL_REJECT            = HPMCOUNTER16,
    KEYBOARD_INTERRUPT_USER             = HPMCOUNTER17,
    KEYBOARD_INTERRUPT_MACHINE          = HPMCOUNTER18,
    EXECEPTION_USER                     = HPMCOUNTER19,
    EXCEPTION_MACHINE                   = HPMCOUNTER20,
    TIMER_INTERRUPT_USER                = HPMCOUNTER21,
    TIMER_INTERRUPT_MACHINE             = HPMCOUNTER22,
    FORWARD_JUMP_RETIRED                = HPMCOUNTER23,
    BACKWARD_JUMP_RETIRED               = HPMCOUNTER24,
    FORWARD_JUMP_PREDICTED_CORRECT      = HPMCOUNTER25,
    BACKWARD_JUMP_PREDICTED_CORRECT     = HPMCOUNTER26,
    FORWARD_JUMP_PREDICTED_INCORRECT    = HPMCOUNTER27,
    BACKWARD_JUMP_PREDICTED_INCORRECT   = HPMCOUNTER28,
} PeformanceCounter_t;


#define MSTATUS_WRITE_MASK 0x1888

#define INTERRUPT_ENABLED 1
#define INTERRUPT_DISABLED 0

typedef struct {
    unsigned zero1 : 3;
    // MIE: 1 => Enabled
    unsigned machineInterruptsEnabled : 1;
    unsigned zero2 : 3;
    // MPIE: Holds MIE prior to interrupt
    unsigned machinePriorInterruptsEnabled : 1;
    unsigned zero3 : 3;
    // 3 if previously running in machine mode, 0 if previously running in user
    // mode
    unsigned machinePreviousPriviledge : 2;
    unsigned zero4 : 19;
} PACKED MachineStatus_t;

typedef enum {
    MACHINE_MODE = 0,
    USER_MODE    = 3,
} ProcessorMode_t;


// Sadly these can't be an enum because enum doesn't like negative values
#define INSTRUCTION_MISALIGNED 0x0
#define INSTRUCTION_ILLEGAL 0x2
#define LOAD_MISALIGNED 0x4
#define STORE_MISALIGNED 0x6
#define USER_ECALL 0x8
#define INSTRUCTION_PAGE_FAULT 0xC
#define LOAD_PAGE_FAULT 0xD
#define STORE_PAGE_FAULT 0xF
#define TIMER_INTERRUPT 0x80000007
#define EXTERNAL_INTERRUPT 0x80000009

typedef struct {
    unsigned rootPageTablePageNumber : 22;
    unsigned asid : 9;
    unsigned virtualMemoryEnabled : 1;
} PACKED SupervisorAddressTranslation_t;


#endif /* RISCV_ISA_H_ */
