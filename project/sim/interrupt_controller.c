#include <control_status_register.h>
#include <interrupt_controller.h>

void interruptController_check(cpu_state_t* cpu_state) {
    if (!csr_interruptsEnabled(cpu_state)) {
        // Nothing to do if interrupts disabled
        return;
    }

    // Deliver timer interupt
    if (cpu_state->cycle % CYCLES_PER_TICK == 0) {
        static int ticks = 0;
        // This PC value is pointing to the next instruction (the instruction
        // that should be executed after the interrupt is handled)
        csr_deliverInterrupt(
            cpu_state, TIMER_INTERRUPT, ticks++, cpu_state->pc);
    }
}