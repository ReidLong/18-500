/**
 * sim.c
 *
 * RISC-V 32-bit Instruction Level Simulator
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This is the core part of the simulator. The `process_instruction` function
 * will be invoked by the simulator each time it needs to simulate a single
 * processor cycle. This is responsible for simulating the next processor cycle.
 * This corresponds to simulating the next instruction, and updating the
 * register file, memory, and PC register appropriately as required by the next
 * instruction.
 *
 * This is where you can start add code and make modifications to implement the
 * rest of the instructions. You can add any additional files or change and
 * delete files as you need to implement the simulator, provided that they are
 * under the src directory. You may not change any files outside the src
 * directory. The only requirement is that you define a `process_instruction`
 * function with the same interface as below.
 *
 * The Makefile will automatically find any files you add, provided they are
 * under the src directory and have either a *.c or *.h extension. The files may
 * be nested in subdirectories under the src directory as well. Additionally,
 * the build system sets up the include paths so that you can place header files
 * in any subdirectory under the src directory, and include them from anywhere
 * else inside the src directory.
 **/

/*----------------------------------------------------------------------------*
 *  You may edit this file and add or change any files in the src directory.  *
 *----------------------------------------------------------------------------*/

// Standard Includes
#include <limits.h>
#include <stdbool.h>  // Boolean type and definitions
#include <stdint.h>
#include <stdio.h>  // Printf and related functions

// 18-447 Simulator Includes
#include <control_status_register.h>
#include <memory.h>         // Interface to the processor memory
#include <register_file.h>  // Interface to the register file
#include <riscv_abi.h>      // ABI registers and definitions
#include <riscv_isa.h>      // Definition of RISC-V opcodes, ISA registers
#include <sim.h>            // Definitions for the simulator

#include <alignment.h>
#include <compiler.h>
#include <contracts.h>
#include <interrupt_controller.h>

#define NEXT_INSTRUCTION()                                                     \
    do {                                                                       \
        cpu_state->pc = cpu_state->pc + sizeof(instr);                         \
    } while (0)

#define WRITE_BACK()                                                           \
    do {                                                                       \
        register_write(cpu_state, rd, result);                                 \
    } while (0)

typedef union {
    struct {
        unsigned opcode : 7;
        unsigned dest : 5;
        unsigned imm_19_12 : 8;
        unsigned imm_11 : 1;
        unsigned imm_10_1 : 10;
        unsigned imm_20 : 1;
    } fields;
    uint32_t instr;
} UJ_Inst_t;

typedef union {
    struct {
        unsigned zero : 1;
        unsigned imm_10_1 : 10;
        unsigned imm_11 : 1;
        unsigned imm_19_12 : 8;
        unsigned sign : 12;
    } fields;
    uint32_t value;
} UJ_Imm_t;

typedef union {
    struct {
        unsigned opcode : 7;
        unsigned imm_11 : 1;
        unsigned imm_4_1 : 4;
        unsigned funct3 : 3;
        unsigned rs1 : 5;
        unsigned rs2 : 5;
        unsigned imm_10_5 : 6;
        unsigned imm_12 : 1;
    } fields;
    uint32_t instr;
} SB_Inst_t;

typedef union {
    struct {
        unsigned zero : 1;
        unsigned imm_4_1 : 4;
        unsigned imm_10_5 : 6;
        unsigned imm_11 : 1;
        unsigned sign : 20;
    } fields;
    uint32_t value;
} SB_Imm_t;

typedef union {
    struct {
        unsigned opcode : 7;
        unsigned imm_4_0 : 5;
        unsigned funct3 : 3;
        unsigned rs1 : 5;
        unsigned rs2 : 5;
        unsigned imm_11_5 : 7;
    } fields;
    uint32_t instr;
} S_Inst_t;


uint32_t sim_readPartial(cpu_state_t* cpu_state, uint32_t address) {
    uint32_t offset = address & 0x3;
    ASSERT(offset < sizeof(uint32_t));
    uint32_t wordAlignedAddress = ALIGN_TO(address, sizeof(uint32_t));
    ASSERT(IS_ALIGNED_TO(wordAlignedAddress, sizeof(uint32_t)));

    uint32_t word = mem_read32(cpu_state, wordAlignedAddress);
    return word >> (offset * CHAR_BIT);
}

void sim_writePartial(cpu_state_t* cpu_state,
                      uint32_t     address,
                      uint32_t     value,
                      uint32_t     mask) {
    uint32_t offset = address & 0x3;
    ASSERT(offset < sizeof(uint32_t));
    uint32_t wordAlignedAddress = ALIGN_TO(address, sizeof(uint32_t));
    ASSERT(IS_ALIGNED_TO(wordAlignedAddress, sizeof(uint32_t)));

    uint32_t keepMask    = mask << (offset * CHAR_BIT);
    uint32_t maskedValue = (value & mask) << (offset * CHAR_BIT);

    uint32_t word = mem_read32(cpu_state, wordAlignedAddress);
    word          = (word & ~keepMask) | (maskedValue & keepMask);
    mem_write32(cpu_state, wordAlignedAddress, word);
}

/**
 * Simulates a single cycle on the CPU, updating the CPU's state as needed.
 *
 * This is the core part of the simulator. This simulates the current
 * instruction pointed to by the PC. This performs the necessary actions for the
 * instruction, and updates the CPU state appropriately.
 *
 * You implement this function.
 *
 * Inputs:
 *  - cpu_state     The current state of the CPU being simulated.
 *
 * Outputs:
 *  - cpu_state     The next state of the CPU being simulated. This function
 *                  updates the fields of the state as needed by the current
 *                  instruction to simulate it.
 **/
void process_instruction(cpu_state_t* cpu_state) {
    // Fetch the 4-bytes for the current instruction
    uint32_t instr = mem_read32(cpu_state, cpu_state->pc);

    // Decode the opcode, 7-bit function code, and registers
    opcode_t        opcode = instr & 0x7F;
    funct7_t        funct7 = (instr >> 25) & 0x7F;
    riscv_isa_reg_t rs1    = (instr >> 15) & 0x1F;
    riscv_isa_reg_t rs2    = (instr >> 20) & 0x1F;
    riscv_isa_reg_t rd     = (instr >> 7) & 0x1F;

    /* Decode the instruction as an I-type instruction, sign extending the
     * immediate value. */
    itype_int_funct3_t itype_funct3 = (instr >> 12) & 0x7;
    int32_t            itype_imm    = ((int32_t)instr) >> 20;

    // Decode the instruction as an R-type instruction
    rtype_funct3_t rtype_funct3 = (instr >> 12) & 0x7;

    itype_system_funct3_t       itype_system_funct3 = (instr >> 12) & 0x7;
    ControlStatusRegisterName_t csr_name = itype_imm & ((1 << CSR_BITS) - 1);
    ASSERT(csr_name < (1 << CSR_BITS));
    uint32_t csr_imm = (instr >> 15) & 0x1F;

    // Decode the instruction as a MUL_DIV instruction
    rtype_m_funct3_t rtype_m_funct3 = (instr >> 12) & 0x7;

    // Decode the 12-bit function code for system instructions
    itype_funct12_t sys_funct12 = (instr >> 20) & 0xFFF;

    // Decode the U-type instruction
    // Lowest 12 bits are zero
    uint32_t utype_imm = (uint32_t)instr & 0xFFFFF000;

    // Decode UJ-type instruction
    UJ_Inst_t jal_inst = {.instr = instr};

    STATIC_ASSERT(sizeof(UJ_Inst_t) == sizeof(instr));
    STATIC_ASSERT(sizeof(UJ_Imm_t) == sizeof(uint32_t));
    int32_t jal_sign = (((int32_t)instr >> 11) & ~0xFFFFF);
    ASSERT((jal_sign < 0 && (int32_t)instr < 0)
           || (jal_sign >= 0 && (int32_t)instr > 0));
    UJ_Imm_t jal_base = {.fields = {
                             .zero      = 0,
                             .imm_10_1  = jal_inst.fields.imm_10_1,
                             .imm_11    = jal_inst.fields.imm_11,
                             .imm_19_12 = jal_inst.fields.imm_19_12,
                             .sign      = 0,
                         }};
    uint32_t jal_imm  = ((uint32_t)jal_sign | (jal_base.value & 0xFFFFF));

    uint32_t jalr_imm = (uint32_t)((int32_t)instr >> 20);
    ASSERT(((int32_t)jalr_imm < 0 && (int32_t)instr < 0)
           || ((int32_t)jalr_imm >= 0 && (int32_t)instr > 0));

    // Decode SB-Type instruction
    sbtype_funct3_t sbtype_funct3 = (instr >> 12) & 0x7;

    SB_Inst_t sb_inst = {.instr = instr};
    STATIC_ASSERT(sizeof(SB_Inst_t) == sizeof(instr));
    STATIC_ASSERT(sizeof(SB_Imm_t) == sizeof(uint32_t));
    int32_t sb_sign = (((int32_t)instr >> 19) & ~0xFFF);
    ASSERT((sb_sign < 0 && (int32_t)instr < 0)
           || (sb_sign >= 0 && (int32_t)instr > 0));
    SB_Imm_t sb_base = {.fields = {
                            .zero     = 0,
                            .imm_4_1  = sb_inst.fields.imm_4_1,
                            .imm_10_5 = sb_inst.fields.imm_10_5,
                            .imm_11   = sb_inst.fields.imm_11,
                            .sign     = 0,
                        }};
    uint32_t sb_imm  = ((uint32_t)sb_sign | (sb_base.value & 0xFFF));
    ASSERT(((int32_t)sb_imm < 0 && (int32_t)instr < 0)
           || ((int32_t)sb_imm >= 0 && (int32_t)instr > 0));

    // Load Type Funct3
    itype_load_funct3_t load_funct3 = (instr >> 12) & 0x7;

    // S-Type Funct3
    stype_funct3_t stype_funct3 = (instr >> 12) & 0x7;

    STATIC_ASSERT(sizeof(S_Inst_t) == sizeof(instr));
    int32_t s_sign = (((int32_t)instr >> 20) & ~0x1F);
    ASSERT((s_sign < 0 && (int32_t)instr < 0)
           || (s_sign >= 0 && (int32_t)instr > 0));
    uint32_t stype_imm = ((uint32_t)s_sign | ((instr >> 7) & 0x1F));

    // AMO-Type Funct5

    amo_funct5_t amo_funct5 = (instr >> 27) & 0x1F;


    switch (opcode) {
        // General R-Type arithmetic operation
        case OP_OP: {
            switch (funct7) {
                case FUNCT7_INT: {
                    switch (rtype_funct3) {
                        // 3-bit function code for add or subtract
                        case FUNCT3_ADD_SUB: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              + register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_SLL: {
                            uint32_t result =
                                register_read(cpu_state, rs1)
                                << (register_read(cpu_state, rs2) & 0x1F);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_SLT: {
                            uint32_t result =
                                (int32_t)register_read(cpu_state, rs1)
                                < (int32_t)register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_SLTU: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              < register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_XOR: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              ^ register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_SRL_SRA: {
                            uint32_t result =
                                register_read(cpu_state, rs1)
                                >> (register_read(cpu_state, rs2) & 0x1F);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_OR: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              | register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_AND: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              & register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }

                        default: {
                            fprintf(stderr,
                                    "Encountered unknown/unimplemented "
                                    "3-bit rtype function code 0x%01x. Halting "
                                    "simulation.\n",
                                    rtype_funct3);
                            cpu_state->halted = true;
                            break;
                        }
                    }
                    break;
                }
                case FUNCT7_ALT_INT: {
                    switch (rtype_funct3) {
                        case FUNCT3_ADD_SUB: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              - register_read(cpu_state, rs2);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_SRL_SRA: {
                            uint32_t result =
                                (int32_t)register_read(cpu_state, rs1)
                                >> (register_read(cpu_state, rs2) & 0x1F);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        default: {
                            fprintf(stderr,
                                    "Encountered unknown/unimplemented "
                                    "3-bit rtype function code 0x%01x. Halting "
                                    "simulation.\n",
                                    rtype_funct3);
                            cpu_state->halted = true;
                            break;
                        }
                    }
                    break;
                }
                case FUNCT7_MULDIV: {
                    switch (rtype_m_funct3) {
                        case FUNCT3_MUL: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              * register_read(cpu_state, rs2);
                            fprintf(stderr,
                                    "Multiplication: %d * %d = %d\n",
                                    register_read(cpu_state, rs1),
                                    register_read(cpu_state, rs2),
                                    result);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_MULH: {
                            uint64_t temp =
                                (int64_t)(int32_t)register_read(cpu_state, rs1)
                                * (int64_t)(int32_t)register_read(cpu_state,
                                                                  rs2);
                            uint32_t result = temp >> 32;
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_MULHSU: {
                            uint64_t temp =
                                (int64_t)(int32_t)register_read(cpu_state, rs1)
                                * (int64_t)(uint64_t)register_read(cpu_state,
                                                                   rs2);
                            uint32_t result = temp >> 32;
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_MULHU: {
                            uint64_t temp =
                                (uint64_t)register_read(cpu_state, rs1)
                                * (uint64_t)register_read(cpu_state, rs2);
                            uint32_t result = temp >> 32;
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_DIV: {
                            uint32_t result;
                            if (register_read(cpu_state, rs2) == 0) {
                                // Divide by zero
                                result = (uint32_t)-1;
                            } else if ((int32_t)register_read(cpu_state, rs1)
                                           == INT32_MIN
                                       && (int32_t)register_read(cpu_state, rs2)
                                              == -1) {
                                // INT_MIN / -1 = INT_MIN
                                result = INT32_MIN;
                            } else {
                                result = (uint32_t)(
                                    (int32_t)register_read(cpu_state, rs1)
                                    / (int32_t)register_read(cpu_state, rs2));
                            }
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_DIVU: {
                            uint32_t result;

                            if (register_read(cpu_state, rs2) == 0) {
                                // Divide by zero
                                result = UINT32_MAX;
                            } else {
                                result = register_read(cpu_state, rs1)
                                         / register_read(cpu_state, rs2);
                            }
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_REM: {
                            uint32_t result;
                            if (register_read(cpu_state, rs2) == 0) {
                                // Divide by zero
                                result = register_read(cpu_state, rs1);
                            } else if ((int32_t)register_read(cpu_state, rs1)
                                           == INT32_MIN
                                       && (int32_t)register_read(cpu_state, rs2)
                                              == -1) {
                                // INT_MIN % -1 = INT_MIN
                                result = 0;
                            } else {
                                result =
                                    (int32_t)register_read(cpu_state, rs1)
                                    % (int32_t)register_read(cpu_state, rs2);
                            }
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT3_REMU: {
                            uint32_t result;
                            if (register_read(cpu_state, rs2) == 0) {
                                result = register_read(cpu_state, rs1);
                            } else {
                                result = register_read(cpu_state, rs1)
                                         % register_read(cpu_state, rs2);
                            }
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        default: {
                            fprintf(stderr,
                                    "Encountered unknown/unimplemented "
                                    "3-bit rtype MUL_DIV function code 0x%01x. "
                                    "Halting "
                                    "simulation.\n",
                                    rtype_m_funct3);
                            cpu_state->halted = true;
                            break;
                        }
                    }
                    break;
                }
                default: {
                    fprintf(stderr,
                            "Encountered unknown/unimplemented "
                            "7-bit function code 0x%01x. Halting "
                            "simulation.\n",
                            funct7);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }

        // General I-type arithmetic operation
        case OP_IMM: {
            switch (itype_funct3) {
                // 3-bit function code for ADDI
                case FUNCT3_ADDI: {
                    uint32_t result =
                        register_read(cpu_state, rs1) + (uint32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_SLTI: {
                    uint32_t result = (int32_t)register_read(cpu_state, rs1)
                                      < (int32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_SLTIU: {
                    uint32_t result =
                        register_read(cpu_state, rs1) < (uint32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_XORI: {
                    uint32_t result =
                        register_read(cpu_state, rs1) ^ (uint32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_ORI: {
                    uint32_t result =
                        register_read(cpu_state, rs1) | (uint32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_ANDI: {
                    uint32_t result =
                        register_read(cpu_state, rs1) & (uint32_t)itype_imm;
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_SLLI: {
                    uint32_t result = register_read(cpu_state, rs1)
                                      << ((uint32_t)itype_imm & 0x1F);
                    WRITE_BACK();
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_SRLI_SRAI: {
                    switch (funct7) {
                        case FUNCT7_INT: {
                            uint32_t result = register_read(cpu_state, rs1)
                                              >> ((uint32_t)itype_imm & 0x1F);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        case FUNCT7_ALT_INT: {
                            uint32_t result =
                                (int32_t)register_read(cpu_state, rs1)
                                >> ((uint32_t)itype_imm & 0x1F);
                            WRITE_BACK();
                            NEXT_INSTRUCTION();
                            break;
                        }
                        default: {
                            fprintf(stderr,
                                    "FUNCT3_SRLI_SRAI: Encountered "
                                    "unknown/unimplemented "
                                    "7-bit function code 0x%01x. Halting "
                                    "simulation.\n",
                                    funct7);
                            cpu_state->halted = true;
                            break;
                        }
                    }
                    break;
                }
                default: {
                    fprintf(stderr,
                            "Encountered unknown/unimplemented 3-bit "
                            "itype function code 0x%01x. Halting simulation.\n",
                            rtype_funct3);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }

        case OP_LOAD: {
            cpu_state->reservation.isValid = false;
            switch (load_funct3) {
                case FUNCT3_LB: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + itype_imm;
                    uint32_t word = sim_readPartial(cpu_state, address);
                    register_write(cpu_state,
                                   rd,
                                   (uint32_t)(int32_t)(int8_t)(word & 0xFF));
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_LH: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + itype_imm;
                    if (IS_ALIGNED_TO(address, sizeof(int16_t))) {
                        uint32_t word = sim_readPartial(cpu_state, address);
                        register_write(
                            cpu_state,
                            rd,
                            (uint32_t)(int32_t)(int16_t)(word & 0xFFFF));
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                load_funct3,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                case FUNCT3_LW: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + itype_imm;
                    if (IS_ALIGNED_TO(address, sizeof(int32_t))) {
                        uint32_t word = mem_read32(cpu_state, address);
                        register_write(cpu_state, rd, word);
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                load_funct3,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                case FUNCT3_LBU: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + itype_imm;
                    uint32_t word = sim_readPartial(cpu_state, address);
                    register_write(cpu_state, rd, word & 0xFF);
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_LHU: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + itype_imm;
                    if (IS_ALIGNED_TO(address, sizeof(uint16_t))) {
                        uint32_t word = sim_readPartial(cpu_state, address);
                        register_write(cpu_state, rd, word & 0xFFFF);
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                load_funct3,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                default: {
                    fprintf(
                        stderr,
                        "Encountered unknown/unimplemented 3-bit "
                        "load itype function code 0x%01x. Halting simulation\n",
                        load_funct3);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }
        case OP_STORE: {
            cpu_state->reservation.isValid = false;
            switch (stype_funct3) {
                case FUNCT3_SB: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + stype_imm;

                    sim_writePartial(cpu_state,
                                     address,
                                     register_read(cpu_state, rs2),
                                     0xFF);
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_SH: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + stype_imm;
                    if (IS_ALIGNED_TO(address, sizeof(uint16_t))) {
                        sim_writePartial(cpu_state,
                                         address,
                                         register_read(cpu_state, rs2),
                                         0xFFFF);
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                stype_funct3,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                case FUNCT3_SW: {
                    uint32_t address =
                        register_read(cpu_state, rs1) + stype_imm;
                    if (IS_ALIGNED_TO(address, sizeof(uint32_t))) {
                        mem_write32(
                            cpu_state, address, register_read(cpu_state, rs2));
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                stype_funct3,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                default: {
                    fprintf(stderr,
                            "Encountered unknown/unimplemented 3-bit "
                            "store function code 0x%01x. Halting simulation\n",
                            stype_funct3);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }
        case OP_AMO: {
            switch (amo_funct5) {
                case FUNCT5_LR: {
                    uint32_t address = register_read(cpu_state, rs1);
                    if (IS_ALIGNED_TO(address, sizeof(int32_t))) {
                        uint32_t word = mem_read32(cpu_state, address);
                        register_write(cpu_state, rd, word);
                        cpu_state->reservation.address = address;
                        cpu_state->reservation.isValid = true;
                        NEXT_INSTRUCTION();
                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                amo_funct5,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                case FUNCT5_SC: {
                    uint32_t address = register_read(cpu_state, rs1);
                    if (IS_ALIGNED_TO(address, sizeof(int32_t))) {
                        if (cpu_state->reservation.address == address
                            && cpu_state->reservation.isValid) {
                            mem_write32(cpu_state,
                                        address,
                                        register_read(cpu_state, rs2));
                            register_write(cpu_state, rd, 0);  // Success
                        } else {
                            register_write(
                                cpu_state, rd, 1);  // No valid reservation
                        }
                        cpu_state->reservation.isValid = false;
                        NEXT_INSTRUCTION();

                    } else {
                        fprintf(stderr,
                                "Unaligned memory operation %#01x Address: "
                                "%#x. Halting simulation\n",
                                amo_funct5,
                                address);
                        cpu_state->halted = true;
                    }
                    break;
                }
                default: {
                    fprintf(stderr,
                            "Encountered unknown/unimplemented 5-bit "
                            "amo function code 0x%01x. Halting simulation\n",
                            amo_funct5);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }
        case OP_LUI: {
            register_write(cpu_state, rd, utype_imm);
            NEXT_INSTRUCTION();
            break;
        }
        case OP_AUIPC: {
            uint32_t result = utype_imm + cpu_state->pc;
            WRITE_BACK();
            NEXT_INSTRUCTION();
            break;
        }
        case OP_JAL: {
            uint32_t nextPC = cpu_state->pc + jal_imm;
            register_write(cpu_state, rd, cpu_state->pc + 4);
            cpu_state->pc = nextPC;
            break;
        }
        case OP_JALR: {
            // The lowest bit is ignored
            uint32_t nextPC = (register_read(cpu_state, rs1) + jalr_imm) & ~0x1;
            register_write(cpu_state, rd, cpu_state->pc + 4);
            cpu_state->pc = nextPC;
            break;
        }
        case OP_BRANCH: {
            switch (sbtype_funct3) {
                case FUNCT3_BEQ: {
                    if (register_read(cpu_state, rs1)
                        == register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                case FUNCT3_BNE: {
                    if (register_read(cpu_state, rs1)
                        != register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                case FUNCT3_BLT: {
                    if ((int32_t)register_read(cpu_state, rs1)
                        < (int32_t)register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                case FUNCT3_BGE: {
                    if ((int32_t)register_read(cpu_state, rs1)
                        >= (int32_t)register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                case FUNCT3_BLTU: {
                    if (register_read(cpu_state, rs1)
                        < register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                case FUNCT3_BGEU: {
                    if (register_read(cpu_state, rs1)
                        >= register_read(cpu_state, rs2)) {
                        cpu_state->pc = cpu_state->pc + sb_imm;
                    } else {
                        NEXT_INSTRUCTION();
                    }
                    break;
                }
                default: {
                    fprintf(
                        stderr,
                        "Encountered unknown/unimplemented 3-bit "
                        "sbtype function code 0x%01x. Halting simulation.\n",
                        rtype_funct3);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }

        // General system operation
        case OP_SYSTEM: {
            switch (itype_system_funct3) {
                case FUNCT3_PRIV: {
                    switch (sys_funct12) {
                        // 12-bit function code for ECALL
                        case FUNCT12_ECALL: {
                            /* FIXME: To actually support syscalls, the argument
                             * register a0 must be read from the register value,
                             * and its value must equal 0xa to terminate
                             * simulation. */
                            uint32_t a0_value =
                                register_read(cpu_state, REG_X10);
                            if (a0_value == ECALL_ARG_HALT) {
                                fprintf(stdout,
                                        "ECALL invoked with halt argument, "
                                        "halting the simulator.\n");
                                cpu_state->halted = true;
                            } else {
                                fprintf(stderr,
                                        "Unknown ECALL triggered: %#x. "
                                        "Treating as NOP\n",
                                        a0_value);
                                NEXT_INSTRUCTION();
                            }

                            break;
                        }
                        case FUNCT12_MRET: {
                            csr_interruptReturn(cpu_state);
                            break;
                        }
                        default: {
                            fprintf(stderr,
                                    "Encountered unknown/unimplemented 12-bit "
                                    "system function code 0x%03x. Halting "
                                    "simulation.\n",
                                    sys_funct12);
                            cpu_state->halted = true;
                            break;
                        }
                    }
                    break;
                }
                case FUNCT3_CSRRW: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    uint32_t rs1_value = register_read(cpu_state, rs1);
                    register_write(cpu_state, rd, csr_value);
                    csr_write(cpu_state, csr_name, rs1_value);
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_CSRRS: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    uint32_t rs1_value = register_read(cpu_state, rs1);
                    uint32_t next_csr  = csr_value | rs1_value;
                    register_write(cpu_state, rd, csr_value);
                    if (rs1 != 0) {
                        csr_write(cpu_state, csr_name, next_csr);
                    }
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_CSRRC: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    uint32_t rs1_value = register_read(cpu_state, rs1);
                    uint32_t next_csr  = csr_value & ~rs1_value;
                    register_write(cpu_state, rd, csr_value);
                    if (rs1 != 0) {
                        csr_write(cpu_state, csr_name, next_csr);
                    }
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_CSRRWI: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    register_write(cpu_state, rd, csr_value);
                    csr_write(cpu_state, csr_name, csr_imm);
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_CSRRSI: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    uint32_t next_csr  = csr_value | csr_imm;
                    register_write(cpu_state, rd, csr_value);
                    if (rs1 != 0) {
                        csr_write(cpu_state, csr_name, next_csr);
                    }
                    NEXT_INSTRUCTION();
                    break;
                }
                case FUNCT3_CSRRCI: {
                    uint32_t csr_value = csr_read(cpu_state, csr_name);
                    uint32_t next_csr  = csr_value & ~csr_imm;
                    register_write(cpu_state, rd, csr_value);
                    if (rs1 != 0) {
                        csr_write(cpu_state, csr_name, next_csr);
                    }
                    NEXT_INSTRUCTION();
                    break;
                }
                default: {
                    fprintf(stderr,
                            "Encountered unknown/unimplemented 3-bit itype"
                            "system function code 0x%03x. Halting "
                            "simulation.\n",
                            itype_system_funct3);
                    cpu_state->halted = true;
                    break;
                }
            }
            break;
        }

        default: {
            fprintf(stderr,
                    "Encountered unknown opcode 0x%02x. Halting "
                    "simulation.\n",
                    opcode);
            cpu_state->halted = true;
            break;
        }
    }

    if (!cpu_state->halted) {
        interruptController_check(cpu_state);
        csr_update(cpu_state);
    }

    return;
}