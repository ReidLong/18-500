`default_nettype none

`include "riscv_console.svh"

module kbd_mock(
    KBD_Interface.Device keyboard,
    input logic clock, clear
);

    // TODO: It would be super cool if we could read a .inp file and then deliver keyboard interrupts in simulation
    assign keyboard.readch = 8'b0;
    assign keyboard.scancode = 8'b0;
    assign keyboard.ascii_ready = 1'b0;

endmodule