
`ifdef SIMULATION_18447

`include "riscv_uarch.svh"

module VGA_mock(
    VGA_Interface.Device vga,
    input logic clock, clear
);

    import RISCV_UArch::CLOCK_HALF_PERIOD;

    Clock #(.HALF_PERIOD(4*CLOCK_HALF_PERIOD)) vga_clockGenerator(.clock(vga.vgaClock));
    Clock #(.HALF_PERIOD(CLOCK_HALF_PERIOD/2)) vram_clockGenerator(.clock(vga.vramClock));

endmodule

`endif