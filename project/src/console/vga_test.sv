`include "riscv_console.svh"
`include "riscv_memory.svh"
`include "chip.svh"

// This will copy data from address 0 to VRAM_START

module VGA_test(
	input logic clock, clear,
	output MemoryRequest_t dataRequest,
	input MemoryResponse_t dataResponse,
	input logic dataReady,
	BasicIO_Interface.Master basicIO
);

	assign dataRequest = '{
		readEnable: 1'b0,
		writeEnable: 4'hf,
		address: `VRAM_START,
		addressType: PHYSICAL_ADDRESS,
		dataIn: {`FGND_WHITE, 8'h49, `FGND_BLUE, 8'h48}
	};

	assign basicIO.led = {4'hA, clear, dataResponse.isValid, dataReady, 1'b1};

	initial begin
		#10000;
		$finish;
	end
	// Address_t savedAddress, nextAddress, address;
	// logic loadNextAddress;
	// Register #(.WIDTH($bits(savedAddress))) address_register(.q(savedAddress), .d(nextAddress), .clock, .enable(loadNextAddress), .clear);
	// always_comb begin
	// 	if(savedAddress + 32'd2 >= `VRAM_SIZE)
	// 		nextAddress = 32'd0;
	// 	else
	// 		nextAddress = savedAddress + 32'd2;
	// end

	// enum {IDLE, READ_MEMORY, WRITE_VRAM} state, nextState;

	// always_ff @(posedge clock)
	// 	if(clear) state <= IDLE;
	// 	else state <= nextState;

	// Word_t savedData;
	// logic loadSavedData;
	// Register #(.WIDTH($bits(savedData))) data_register(.q(savedData), .d(dataResponse.dataOut), .clock, .enable(loadSavedData), .clear);

	// logic readEnable;
	// logic[3:0] writeEnable;
	// assign dataRequest = '{
	// 	readEnable: readEnable,
	// 	writeEnable: writeEnable,
	// 	address: address,
	// 	addressType: PHYSICAL_ADDRESS,
	// 	dataIn: savedData
	// };

	// always_comb begin
	// 	nextState = state;
	// 	loadSavedData = 1'b0;
	// 	readEnable = 1'b0;
	// 	writeEnable = 4'h0;
	// 	address = 32'b0;
	// 	unique case(state)
	// 		IDLE:
	// 			if(button[`BUTTON_UP]) begin
	// 				nextState = READ_MEMORY;
	// 			end
	// 		READ_MEMORY: begin
	// 			address = savedAddress;
	// 			readEnable = 1'b1;
	// 			if(dataResponse)
	// 		end
	// 		WRITE_VRAM:
	// 	endcase
	// end

endmodule