`default_nettype none

`include "config.svh"
`include "interrupt_controller.svh"
`include "control_status_register.svh"
`include "riscv_memory.svh"

module InterruptController(

    ControlFlowInterface.InterruptController controlFlow,
    ControlStatusRegisterInterface.InterruptController csrInterface,
    InterruptControllerInterface.InterruptController interruptControl,

    // Devices
    InterruptDevice.InterruptController timer, keyboard,

    input logic clock, clear, halted
);

    acceptPending: assert property (@(posedge clock) disable iff(clear) controlFlow.interruptAccepted |-> controlFlow.interruptPending);

    // Interrupt Controller
    assign csrInterface.interruptValid = controlFlow.interruptAccepted;
    assign csrInterface.epc = interruptControl.expectedNextInstruction_WB;
    assign controlFlow.interruptPC = csrInterface.mtvec;

    always_comb begin
        controlFlow.interruptPending = 1'b0;
        csrInterface.cause = 32'b0;
        csrInterface.tval = 32'b0;

        timer.interruptAccepted = 1'b0;
        keyboard.interruptAccepted = 1'b0;

        if((csrInterface.mstatus.machineInterruptsEnabled == INTERRUPTS_ENABLED) && !halted) begin

            // Perhaps we should have a better priority system, but this seems to work for now
            if(keyboard.interruptPending) begin
                controlFlow.interruptPending = 1'b1;
                csrInterface.cause = EXTERNAL_INTERRUPT;
                csrInterface.tval = keyboard.tval;
                if(controlFlow.interruptAccepted) begin
                    keyboard.interruptAccepted = 1'b1;
                end
            end else if(timer.interruptPending) begin
                controlFlow.interruptPending = 1'b1;
                csrInterface.cause = TIMER_INTERRUPT;
                csrInterface.tval = timer.tval;
                if(controlFlow.interruptAccepted) begin
                    timer.interruptAccepted = 1'b1;
                end
            end
        end
    end

endmodule
