`ifndef __CHIP_SVH
`define __CHIP_SVH

`define BUTTON_UP 3
`define BUTTON_DOWN 0
`define BUTTON_LEFT 1
`define BUTTON_RIGHT 2
`define BUTTON_CENTER 4

interface BasicIO_Interface();
	logic [7:0] switch, led;
	logic [4:0] button;
	modport Master(
		input switch, button,
		output led
	);
	
endinterface

`endif