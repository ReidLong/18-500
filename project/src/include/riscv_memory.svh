`ifndef __RISCV_MEMORY_SVH
`define __RISCV_MEMORY_SVH

`define PAGE_SIZE 32'd4096
`define WORD_POISON 32'hBADF00D
`define ADDRESS_POISON 32'HBADADD
`define CACHE_TAG_POISON 20'h2BAD

`define BLOCK_SIZE 16

// `define PHYSICAL_MEMORY_SIZE (256 * 1024 * 1024)
`define PHYSICAL_MEMORY_SIZE (64 * 1024 * 1024)


`define TEXT_START 32'h100000

// Even though RISC-V technically supports 34 bits of physical address space, our system is explicitly limited to 32 bits (and it uses much less). This allows us to have a unified address type that is always 32 bits

typedef logic [31:0] Address_t;
typedef logic [31:0] Word_t;

typedef logic [5:0] CacheIndex_t;
typedef logic [3:0] CacheBlockOffset_t;
typedef logic [19:0] CacheTag_t;

typedef enum {TLB_HIT, VRAM_DIRECT_MAP, TLB_MISS, VM_FAULT} TranslationType_t;

typedef struct packed {
    Address_t virtualAddress;
    logic            isValid       ;
} AddressTranslationRequest_t;

typedef struct packed {
    AddressTranslationRequest_t request        ;
    Address_t           physicalAddress;
    logic                       isValid        ;
    TranslationType_t           translationType;
} AddressTranslationResponse_t;


typedef enum {VIRTUAL_ADDRESS, PHYSICAL_ADDRESS} AddressType_t;

typedef struct packed {
    logic               readEnable ;
    logic         [3:0] writeEnable; // Byte write
    Address_t           address    ;
    AddressType_t       addressType;
    Word_t              dataIn     ;
} MemoryRequest_t;

typedef enum {INVALID_RESPONSE, CACHE_HIT, CACHE_MISS, VRAM} ResponseType_t;

typedef struct packed {
    MemoryRequest_t              request    ;
    AddressTranslationResponse_t translation;
    Word_t                       dataOut    ;
    logic                        isValid    ;
    ResponseType_t responseType;
} MemoryResponse_t;

typedef enum {CACHE_READ, CACHE_WRITE, CACHE_DRAM_FILL} CacheRequestType_t;

typedef struct packed {
    CacheIndex_t             index      ;
    CacheBlockOffset_t       blockOffset;
    // This field is only valid if requestType is a write
    CacheTag_t               tag        ;
    Word_t                   dataIn     ;
    CacheRequestType_t       requestType;
    logic                    isValid    ;
    logic              [3:0] writeEnable;
} CacheRequest_t;

typedef struct packed {
    logic      isValid;
    logic      isDirty;
    CacheTag_t tag    ;
} CacheTagEntry_t;

typedef struct packed {
    CacheRequest_t  request ;
    Word_t          dataOut ;
    CacheTagEntry_t tagEntry;
} CacheResponse_t;

typedef struct packed {
    Address_t physicalAddress;
    logic readEnable;
    logic writeEnable;
    Word_t dataIn;
} DRAMRequest_t;

typedef struct packed {
    Address_t physicalAddress;
    logic     isValid        ;
    Word_t    dataOut        ;
} DRAMResponse_t;

interface DRAM_Interface();
    logic [`BLOCK_SIZE-1:0][31:0] writeData;
    logic [`BLOCK_SIZE-1:0][31:0] readData;
    Address_t addressBase;
    logic readEnable, writeEnable;
    logic requestAccepted, requestCompleted, requestError;
    modport Requester(
        output writeData,
        input readData,
        output addressBase,
        output readEnable, writeEnable,
        input requestAccepted, requestCompleted, requestError
    );
    modport Device(
        input writeData,
        output readData,
        input addressBase,
        input readEnable, writeEnable,
        output requestAccepted, requestCompleted, requestError
    );
endinterface


`endif