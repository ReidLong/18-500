
//  Xilinx True Dual Port RAM Byte Write Read First Single Clock RAM
//  This code implements a parameterizable true dual port memory (both ports can read and write).
//  The behavior of this RAM is when data is written, the prior memory contents at the write
//  address are presented on the output port.

module xilinx_bram_byteWrite #(
  parameter NB_COL    = 4   , // Specify number of columns (number of bytes)
  parameter COL_WIDTH = 9   , // Specify column width (byte width, typically 8 or 9)
  parameter RAM_DEPTH = 1024, // Specify RAM depth (number of entries)
  parameter INIT_FILE = ""                        // Specify name/location of RAM initialization file if using one (leave blank if not)
) (
  input  [ $clog2(RAM_DEPTH)-1:0] addra, // Port A address bus, width determined from RAM_DEPTH
  input  [ $clog2(RAM_DEPTH)-1:0] addrb, // Port B address bus, width determined from RAM_DEPTH
  input  [(NB_COL*COL_WIDTH)-1:0] dina , // Port A RAM input data
  input  [(NB_COL*COL_WIDTH)-1:0] dinb , // Port B RAM input data
  input                           clka , // Clock
  input  [            NB_COL-1:0] wea  , // Port A write enable
  input  [            NB_COL-1:0] web  , // Port B write enable
  output [(NB_COL*COL_WIDTH)-1:0] douta, // Port A RAM output data
  output [(NB_COL*COL_WIDTH)-1:0] doutb  // Port B RAM output data
);

`ifdef SIMULATION_18447
  localparam WORD_SIZE = NB_COL * COL_WIDTH;

  logic [NB_COL-1:0][COL_WIDTH-1:0] BRAM [RAM_DEPTH-1:0];
  logic [WORD_SIZE-1:0] ram_data_a = {WORD_SIZE{1'b0}};
  logic [WORD_SIZE-1:0] ram_data_b = {WORD_SIZE{1'b0}};

  always @(posedge clka)
    ram_data_a <= BRAM[addra];

  always @(posedge clka)
    ram_data_b <= BRAM[addrb];

  `STATIC_ASSERT(NB_COL == 4);
  `STATIC_ASSERT(COL_WIDTH == 8);

      always_ff @(posedge clka) begin
        if (wea[0])
            BRAM[addra][0] <= dina[7:0];
        if (web[0])
            BRAM[addrb][0] <= dinb[7:0];
        if (wea[1])
            BRAM[addra][1] <= dina[15:8];
        if (web[1])
            BRAM[addrb][1] <= dinb[15:8];
        if (wea[2])
            BRAM[addra][2] <= dina[23:16];
        if (web[2])
            BRAM[addrb][2] <= dinb[23:16];
        if (wea[3])
            BRAM[addra][3] <= dina[31:24];
        if (web[3])
            BRAM[addrb][3] <= dinb[31:24];
      end



`else
  reg [(NB_COL*COL_WIDTH)-1:0] BRAM [RAM_DEPTH-1:0];
  reg [(NB_COL*COL_WIDTH)-1:0] ram_data_a = {(NB_COL*COL_WIDTH){1'b0}};
  reg [(NB_COL*COL_WIDTH)-1:0] ram_data_b = {(NB_COL*COL_WIDTH){1'b0}};

  // The following code either initializes the memory values to a specified file or to all zeros to match hardware
  generate
    if (INIT_FILE != "") begin: use_init_file
      initial
        $readmemh(INIT_FILE, BRAM, 0, RAM_DEPTH-1);
    end else begin: init_bram_to_zero
      integer ram_index;
      initial
        for (ram_index = 0; ram_index < RAM_DEPTH; ram_index = ram_index + 1)
          BRAM[ram_index] = {(NB_COL*COL_WIDTH){1'b0}};
    end
  endgenerate

  always @(posedge clka)
    ram_data_a <= BRAM[addra];

  always @(posedge clka)
    ram_data_b <= BRAM[addrb];

  generate
    genvar i;
    for (i = 0; i < NB_COL; i = i+1) begin: byte_write
      always_ff @(posedge clka)
        if (wea[i])
          BRAM[addra][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dina[(i+1)*COL_WIDTH-1:i*COL_WIDTH];

      always_ff @(posedge clka)
        if (web[i])
          BRAM[addrb][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dinb[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
        end
      endgenerate
`endif


  // The following is a 1 clock cycle read latency at the cost of a longer clock-to-out timing
  assign douta = ram_data_a;
  assign doutb = ram_data_b;



endmodule


