/**
 * testbench.sv
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This is the testbench (top module) used for processor simulation.
 *
 * This top module is intended only for simulation, and is not synthesizable. It
 * is responsible for running and managing the riscv_core module. See top.sv for
 * the synthesizable top module.
 *
 * The top module handles connecting the memory simulation model to the
 * processor core, and terminating simulation when requested by the core. It
 * also generates the clock, and keeps track of basic information, such as
 * the cycle count and PC value.
 *
 * Authors:
 *  - 2016 - 2017: Brandon Perez
 **/

/*----------------------------------------------------------------------------*
 *                          DO NOT MODIFY THIS FILE!                          *
 *          You should only add or change files in the src directory!         *
 *----------------------------------------------------------------------------*/

// This module is only included when we are running simulation
`ifdef SIMULATION_18447

// Force the compiler to throw an error if any variables are undeclared
`default_nettype none

// RISC-V Includes
`include "riscv_uarch.svh"           // Definition of main memory parameters

/*----------------------------------------------------------------------------
 * Simulation Top Module
 *----------------------------------------------------------------------------*/

/**
 * The top module for the RISC-V core used for simulation.
 *
 * For simulation, a non-synthesizable, but behaviorally accurate model is
 * used for memory. Additionally, the top module for simulation handles
 * clock generation, resetting the processor, keeping track of cycles, and
 * terminating the simulation when prompted.
 **/
module top;

    // Import the clock to use for the processor in simulation
    import RISCV_UArch::CLOCK_HALF_PERIOD;

    // Internal variables
    int                                     cycle_count;

    // Processor and memory interface signals
    logic                                   clock, clear_l, clear;
    logic halted, fatalError;

    // Handle resetting the processor when simulation begins
    initial begin
        clear_l = 0;
        repeat(5) @(posedge clock);
        clear_l = 1;

    end

    assign clear = ~clear_l;

    // The global clock for the design
    Clock #(.HALF_PERIOD(CLOCK_HALF_PERIOD)) clockGenerator(.clock);

    VGA_Interface vga();
    DRAM_Interface dram();
    BasicIO_Interface basicIO();
    KBD_Interface keyboard();

    assign basicIO.button = 5'h0;
    assign basicIO.switch = 8'h0;

    RISCV_Chip chip(.clock, .clear, .vga(vga.Controller), .halted, .fatalError, .dram(dram.Requester), .basicIO(basicIO.Master), .kbd(keyboard.Controller));

    DRAM_mock dram_mock(.dram(dram.Device), .clock, .clear);

    VGA_mock vga_mock(.vga(vga.Device), .clock, .clear);

    kbd_mock kbd_mock(.keyboard(keyboard.Device), .clock, .clear);

    // Handle terminating simulation whenever halted is asserted
    always @(posedge clock) begin
        #1;                 // Allow all other tasks to finish
        if (clear_l && halted) begin
            // Delay hack to make sure all of the counters print....
            #100 $finish;
        end
    end

endmodule: top

`endif /* SIMULATION_18447 */
