#!/bin/python3

import sys
import os
import shutil

if len(sys.argv) < 2:
    print("Usage: {} <version>".format(sys.argv[0]))
    exit(2)

# Delete directory for versionFolder
# Make directory for versionFolder
# Make directory for versionFolder/full_chip
# Copy dummy.v to versionFolder/full_chip
# Write synth.tcl to versionFolder/synth.tcl

versionFolder = sys.argv[1]
# This is so sad, but Windows sucks
projectRoot = os.getcwd()
# This will convert /mnt/c to c:
windowsRoot = 'c:{}'.format(projectRoot[6:])

dummyName = 'dummy.v'
vivadoRoot = os.path.join(projectRoot, 'vivado')
versionPath = os.path.join(vivadoRoot, versionFolder)
dummyPath = os.path.join(vivadoRoot, dummyName)
chipPath = os.path.join(versionPath, 'full_chip')
tclScriptPath = os.path.join(versionPath, 'synth.tcl')

print('Setting up file system for {}'.format(versionFolder))
if os.path.isdir(versionPath):
    shutil.rmtree(versionPath)
os.mkdir(versionPath)
os.mkdir(chipPath)
shutil.copyfile(dummyPath, os.path.join(chipPath, dummyName))

print('Generating TCL Script: {}'.format(tclScriptPath))

# Setup for tcl script

topLevelFiles = [
    'ChipInterface.sv',
    'ChipInterfaceWrapper.v',
    'riscv_chip.sv',
]
topLevelPaths = ' '.join(['{projectRoot}/src/{file}'.format(projectRoot=windowsRoot, file=file) for file in topLevelFiles])

includeFiles = [
    'register_file.svh',
    'riscv_memory.svh',
    'chip.svh',
    'control_flow.svh',
    'riscv_isa.svh',
    'riscv_uarch.svh',
    'riscv_register_names.svh',
    'log.svh',
    'riscv_console.svh',
    'compiler.svh',
    'pipeline.svh',
    'config.svh',
    'control_status_register.svh',
    'interrupt_controller.svh',
]
includePaths = ' '.join(['{projectRoot}/src/include/{file}'.format(projectRoot=windowsRoot, file=file) for file in includeFiles])

libraryFiles = [
    'sram.sv',
    'library.sv',
    'bram_byteWrite.sv',
    'bram.sv',
    'bram_2clock_bytewrite.sv',
    'fifo.sv',
]
libraryPaths = ' '.join(['{projectRoot}/src/library/{file}'.format(projectRoot=windowsRoot, file=file) for file in libraryFiles])

coreFiles = [
    'decode_stage.sv',
    'fetch_stage.sv',
    'write_back_stage.sv',
    'control_flow.sv',
    'memory_stage.sv',
    'riscv_core.sv',
    'forwarding_register_file.sv',
    'execute_stage.sv',
    'performance_counters.sv',
    'register_file.sv',
    'riscv_decode.sv',
    'control_status_register.sv',
    'interrupt_controller.sv',
]
corePaths = ' '.join(['{projectRoot}/src/core/{file}'.format(projectRoot=windowsRoot, file=file) for file in coreFiles])

memoryFiles = [
    'main_memory.sv',
    'translation.sv',
    'dram_wrapper.sv',
    'dram_controller.sv',
    'cache.sv',
    'vram.sv',
    'riscv_memory.sv',
]
memoryPaths = ' '.join(['{projectRoot}/src/memory/{file}'.format(projectRoot=windowsRoot, file=file) for file in memoryFiles])

consoleFiles = [
    'vga_controller.sv',
    'charmap.sv',
    'vga_test.sv',
    'ascii_code.v',
    'ps2.v',
    'ps2_defines.v',
    'ps2_keyboard.v',
    'ps2_translation_table.v',
    'kbd_controller.sv',
    'keyboard_driver.sv',
    'timer_controller.sv',
]
consolePaths = ' '.join(['{projectRoot}/src/console/{file}'.format(projectRoot=windowsRoot, file=file) for file in consoleFiles])

template = """
# Create Project
create_project zedboard {projectRoot}/vivado/{versionFolder}/zedboard -part xc7z020clg484-1
set_property board_part em.avnet.com:zed:part0:1.3 [current_project]
add_files -fileset constrs_1 -norecurse {projectRoot}/vivado_config/io.xdc

# Create full_chip IP
ipx::infer_core -vendor xilinx.com -library user -taxonomy /UserIP {projectRoot}/vivado/{versionFolder}/full_chip
ipx::edit_ip_in_project -upgrade true -name edit_ip_project -directory {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.tmp {projectRoot}/vivado/{versionFolder}/full_chip/component.xml
ipx::current_core {projectRoot}/vivado/{versionFolder}/full_chip/component.xml
update_compile_order -fileset sources_1
add_files -norecurse {{{topLevelPaths}}}
update_compile_order -fileset sources_1
add_files -norecurse {{{includePaths}}}
update_compile_order -fileset sources_1
add_files -norecurse {{{libraryPaths}}}
update_compile_order -fileset sources_1
add_files -norecurse {{{corePaths}}}
update_compile_order -fileset sources_1
add_files -norecurse {{{memoryPaths}}}
update_compile_order -fileset sources_1
add_files -norecurse {{{consolePaths}}}
update_compile_order -fileset sources_1
set_property top top [current_fileset]
update_compile_order -fileset sources_1
export_ip_user_files -of_objects  [get_files {projectRoot}/vivado/{versionFolder}/full_chip/dummy.v] -no_script -reset -force -quiet
remove_files  {projectRoot}/vivado/{versionFolder}/full_chip/dummy.v
set_property name full_chip [ipx::current_core]
set_property display_name full_chip_v1 [ipx::current_core]
set_property description full_chip_v1 [ipx::current_core]
ipx::merge_project_changes files [ipx::current_core]
ipx::merge_project_changes hdl_parameters [ipx::current_core]
ipx::associate_bus_interfaces -clock m00_axi_aclk -reset m00_axi_aresetn -clear [ipx::current_core]
ipx::add_bus_interface M00_AXI [ipx::current_core]
set_property abstraction_type_vlnv xilinx.com:interface:aximm_rtl:1.0 [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property bus_type_vlnv xilinx.com:interface:aximm:1.0 [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
ipx::add_port_map BVALID [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_bvalid [ipx::get_port_maps BVALID -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map RREADY [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_rready [ipx::get_port_maps RREADY -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map BREADY [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_bready [ipx::get_port_maps BREADY -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map AWVALID [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_awvalid [ipx::get_port_maps AWVALID -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map AWREADY [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_awready [ipx::get_port_maps AWREADY -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map AWPROT [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_awprot [ipx::get_port_maps AWPROT -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map WDATA [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_wdata [ipx::get_port_maps WDATA -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map RRESP [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_rresp [ipx::get_port_maps RRESP -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map ARPROT [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_arprot [ipx::get_port_maps ARPROT -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map RVALID [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_rvalid [ipx::get_port_maps RVALID -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map ARADDR [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_araddr [ipx::get_port_maps ARADDR -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map AWADDR [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_awaddr [ipx::get_port_maps AWADDR -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map ARREADY [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_arready [ipx::get_port_maps ARREADY -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map WVALID [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_wvalid [ipx::get_port_maps WVALID -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map WREADY [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_wready [ipx::get_port_maps WREADY -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map ARVALID [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_arvalid [ipx::get_port_maps ARVALID -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map WSTRB [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_wstrb [ipx::get_port_maps WSTRB -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map BRESP [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_bresp [ipx::get_port_maps BRESP -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::add_port_map RDATA [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]
set_property physical_name m00_axi_rdata [ipx::get_port_maps RDATA -of_objects [ipx::get_bus_interfaces M00_AXI -of_objects [ipx::current_core]]]
ipx::associate_bus_interfaces -busif M00_AXI -clock m00_axi_aclk [ipx::current_core]
ipx::infer_bus_interface areset_n xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]
ipx::associate_bus_interfaces -clock clock -reset areset_n -clear [ipx::current_core]
ipx::associate_bus_interfaces -clock m00_axi_aclk -reset m00_axi_aresetn [ipx::current_core]
ipx::associate_bus_interfaces -clock clock -reset areset_n [ipx::current_core]
ipx::infer_bus_interface vgaClock xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
ipx::infer_bus_interface vgaClear_n xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]
ipx::associate_bus_interfaces -clock clock -reset vgaClear_n -clear [ipx::current_core]
ipx::associate_bus_interfaces -clock vgaClock -reset vgaClear_n [ipx::current_core]
ipx::infer_bus_interface vramClock xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
set_property previous_version_for_upgrade xilinx.com:user:dummy_top:1.0 [ipx::current_core]
set_property core_revision 1 [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
close_project -delete

# Update IP Repo
set_property  ip_repo_paths  {projectRoot}/vivado/{versionFolder}/full_chip [current_project]
update_ip_catalog

# Create block diagram
create_bd_design "design_1"
#update_compile_order -fileset sources_1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
endgroup
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {{make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }}  [get_bd_cells processing_system7_0]
startgroup
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {{1}} CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {{32}}] [get_bd_cells processing_system7_0]
endgroup
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
endgroup
set_property -dict [list CONFIG.NUM_MI {{1}}] [get_bd_cells axi_interconnect_0]
apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {{Clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }}  [get_bd_pins axi_interconnect_0/ACLK]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {{Clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }}  [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK]
endgroup
connect_bd_net [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/FCLK_CLK0]
startgroup
create_bd_cell -type ip -vlnv xilinx.com:user:full_chip:1.0 full_chip_0
endgroup
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {{Slave "/processing_system7_0/S_AXI_HP0" intc_ip "/axi_interconnect_0" Clk_xbar "Auto" Clk_master "Auto" Clk_slave "Auto" }}  [get_bd_intf_pins full_chip_0/M00_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {{Clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }}  [get_bd_pins full_chip_0/clock]
endgroup
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0
endgroup
set_property -dict [list CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {{25}} CONFIG.USE_LOCKED {{false}} CONFIG.USE_RESET {{false}} CONFIG.MMCM_DIVCLK_DIVIDE {{1}} CONFIG.MMCM_CLKFBOUT_MULT_F {{9.125}} CONFIG.MMCM_CLKOUT0_DIVIDE_F {{36.500}} CONFIG.CLKOUT1_JITTER {{181.828}} CONFIG.CLKOUT1_PHASE_ERROR {{104.359}}] [get_bd_cells clk_wiz_0]
set_property -dict [list CONFIG.USE_LOCKED {{true}}] [get_bd_cells clk_wiz_0]
set_property -dict [list CONFIG.USE_RESET {{true}}] [get_bd_cells clk_wiz_0]
connect_bd_net [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins full_chip_0/vgaClock]
connect_bd_net [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins processing_system7_0/FCLK_CLK0]
save_bd_design
# connect_bd_net [get_bd_pins clk_wiz_0/locked] [get_bd_pins full_chip_0/vgaClear_n]
connect_bd_net [get_bd_pins rst_ps7_0_100M/peripheral_reset] [get_bd_pins clk_wiz_0/reset]
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/switch]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/button]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/led]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/vga_r]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/vga_g]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/vga_b]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/vga_vs]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/vga_hs]
endgroup
set_property name button [get_bd_ports button_0]
set_property name led [get_bd_ports led_0]
set_property name switch [get_bd_ports switch_0]
set_property name vga_b [get_bd_ports vga_b_0]
set_property name vga_g [get_bd_ports vga_g_0]
set_property name vga_hs [get_bd_ports vga_hs_0]
set_property name vga_r [get_bd_ports vga_r_0]
set_property name vga_vs [get_bd_ports vga_vs_0]

startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/ps2_clk]
endgroup
startgroup
make_bd_pins_external  [get_bd_pins full_chip_0/ps2_data]
endgroup
set_property name ps2_clk [get_bd_ports ps2_clk_0]
set_property name ps2_data [get_bd_ports ps2_data_0]

startgroup
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {{50}} CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {{100}} CONFIG.PCW_EN_CLK1_PORT {{1}}] [get_bd_cells processing_system7_0]
endgroup
disconnect_bd_net /processing_system7_0_FCLK_CLK0 [get_bd_pins clk_wiz_0/clk_in1]
connect_bd_net [get_bd_pins processing_system7_0/FCLK_CLK1] [get_bd_pins clk_wiz_0/clk_in1]
connect_bd_net [get_bd_pins processing_system7_0/FCLK_CLK1] [get_bd_pins full_chip_0/vramClock]

regenerate_bd_layout
validate_bd_design

# Export Block Diagram
make_wrapper -files [get_files {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/design_1.bd] -top
add_files -norecurse {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v
generate_target all [get_files  {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/design_1.bd]
catch {{ config_ip_cache -export [get_ips -all design_1_processing_system7_0_0] }}
catch {{ config_ip_cache -export [get_ips -all design_1_rst_ps7_0_100M_0] }}
catch {{ config_ip_cache -export [get_ips -all design_1_full_chip_0_0] }}
catch {{ config_ip_cache -export [get_ips -all design_1_clk_wiz_0_0] }}
catch {{ config_ip_cache -export [get_ips -all design_1_auto_pc_0] }}
export_ip_user_files -of_objects [get_files {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/design_1.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/design_1.bd]
launch_runs -jobs 4 {{design_1_processing_system7_0_0_synth_1 design_1_rst_ps7_0_100M_0_synth_1 design_1_full_chip_0_0_synth_1 design_1_clk_wiz_0_0_synth_1 design_1_auto_pc_0_synth_1}}
export_simulation -of_objects [get_files {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.srcs/sources_1/bd/design_1/design_1.bd] -directory {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.ip_user_files/sim_scripts -ip_user_files_dir {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.ip_user_files -ipstatic_source_dir {projectRoot}/vivado/{versionFolder}/zedboard/zedboard.ip_user_files/ipstatic -lib_map_path [list {{modelsim={projectRoot}/vivado/{versionFolder}/zedboard/zedboard.cache/compile_simlib/modelsim}} {{questa={projectRoot}/vivado/{versionFolder}/zedboard/zedboard.cache/compile_simlib/questa}} {{riviera={projectRoot}/vivado/{versionFolder}/zedboard/zedboard.cache/compile_simlib/riviera}} {{activehdl={projectRoot}/vivado/{versionFolder}/zedboard/zedboard.cache/compile_simlib/activehdl}}] -use_ip_compiled_libs -force -quiet

create_run synth_2 -flow {{Vivado Synthesis 2017}} -strategy Flow_AlternateRoutability
# create_run synth_3 -flow {{Vivado Synthesis 2017}} -strategy Flow_PerfThresholdCarry
create_run impl_2_1 -parent_run synth_2 -flow {{Vivado Implementation 2017}} -strategy Performance_ExtraTimingOpt
# create_run impl_3_1 -parent_run synth_3 -flow {{Vivado Implementation 2017}} -strategy Performance_ExtraTimingOpt
# create_run impl_2_2 -parent_run synth_2 -flow {{Vivado Implementation 2017}} -strategy Flow_RunPostRoutePhysOpt
# create_run impl_3_2 -parent_run synth_3 -flow {{Vivado Implementation 2017}} -strategy Flow_RunPostRoutePhysOpt
# launch_runs impl_2_1 impl_3_1 impl_2_2 impl_3_2 -jobs 4
launch_runs impl_2_1 -jobs 4

wait_on_run impl_2_1
# wait_on_run impl_3_1
# wait_on_run impl_1

current_run [get_runs impl_2_1]

# Generate Bitstream
launch_runs impl_2_1 -to_step write_bitstream -jobs 4
wait_on_run impl_2_1
"""

tclScript = template.format(projectRoot=windowsRoot, versionFolder=versionFolder, topLevelPaths=topLevelPaths, includePaths=includePaths, libraryPaths=libraryPaths, corePaths=corePaths, memoryPaths=memoryPaths, consolePaths=consolePaths)

scriptFile = open(tclScriptPath, 'w')
scriptFile.write(tclScript)
scriptFile.close()

print('All done')

