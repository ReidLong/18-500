# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x3f75c057
	li x2, 0x70e8e27e
	li x3, 0xed57a6a7
	li x4, 0x187d7168
	li x5, 0x104f7e09
	li x6, 0x363b4682
	li x7, 0x82dbb8e1
	li x8, 0xd2010f35
	li x9, 0x3fa567c4
	li x10, 0x9e5e81
	li x11, 0xd750b321
	li x12, 0x2721e3d5
	li x13, 0x68795956
	li x14, 0xc85c05bd
	li x15, 0x2a1248c2
	li x16, 0x70911ad9
	li x17, 0xf1f91bf3
	li x18, 0x23700293
	li x19, 0x525a0fc
	li x20, 0x1cd0104b
	li x21, 0x143405af
	li x22, 0x93116bb2
	li x23, 0x9e174f0b
	li x24, 0xdf934a11
	li x25, 0xe49768b8
	li x26, 0x6c4dcaec
	li x27, 0xf5cd5b5e
	li x28, 0x854c49a3
	li x29, 0xa6237e60
	li x30, 0x6f68b188
	li x31, 0x17fcbfcc

# Seeding Simple Instructions
	or	x0, x29, x4
	or	x31, x19, x11
	sub	x22, x24, x25
	and	x7, x28, x29
	xor	x28, x17, x14
	sll	x30, x28, x31
	sltu	x0, x27, x15
	add	x16, x22, x26
	and	x29, x25, x19
	slt	x15, x1, x5
	or	x6, x20, x6
	or	x12, x29, x25
	sltu	x31, x29, x3
	add	x13, x24, x14
	and	x20, x29, x26
	and	x17, x31, x0
	and	x26, x30, x17
	add	x27, x29, x26
	xor	x25, x1, x18
	slt	x30, x15, x28
	sub	x8, x16, x21
	or	x23, x10, x12
	or	x15, x4, x4
	sll	x22, x8, x7
	sub	x4, x0, x20
	slt	x27, x26, x0
	sub	x18, x15, x13
	slt	x14, x8, x30
	xor	x28, x27, x7
	sll	x11, x22, x21
	add	x27, x24, x26
	xor	x12, x9, x14
	sll	x28, x19, x3
	srl	x3, x16, x5
	and	x27, x4, x18
	srl	x12, x20, x2
	sll	x18, x11, x6
	sra	x11, x7, x18
	and	x25, x13, x0
	srl	x4, x13, x16
	and	x0, x14, x7
	or	x29, x29, x22
	and	x26, x25, x29
	sll	x16, x7, x5
	slt	x16, x16, x7
	sltu	x26, x5, x26
	srl	x15, x13, x11
	sll	x27, x1, x18
	sra	x21, x25, x2
	xor	x20, x30, x22
	or	x20, x0, x11
	sltu	x17, x29, x25
	and	x2, x1, x27

    li a0, 0xa
    ecall
