# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x42b482d4
	li x2, 0x73bd3f8d
	li x3, 0xd81f4430
	li x4, 0x5e15965a
	li x5, 0x7624c16e
	li x6, 0x8ed7150a
	li x7, 0x8ff1e58e
	li x8, 0x8e07fc21
	li x9, 0xfc0b2667
	li x10, 0x6f4299ed
	li x11, 0xa6a542c8
	li x12, 0x14e86983
	li x13, 0x340ec490
	li x14, 0xd4d28853
	li x15, 0x50f135ae
	li x16, 0x120ea37e
	li x17, 0x5e9fe461
	li x18, 0xb3703dc
	li x19, 0x363db043
	li x20, 0x9fbe568e
	li x21, 0xa444a623
	li x22, 0x3a206139
	li x23, 0x4707b5f1
	li x24, 0x3208b349
	li x25, 0x8b3f829c
	li x26, 0xb733b563
	li x27, 0x5f0695a8
	li x28, 0x73fda562
	li x29, 0x624d0496
	li x30, 0x5844fa7b
	li x31, 0x9dbe7625

    la x1, mydata

# Generating Store Sequence
	li	x12,	2093
	add	x12,	x1,	x12
	sb	x10,	1672(x12)
	li	x25,	3657
	add	x25,	x1,	x25
	sb	x20,	-1672(x25)
	li	x30,	2745
	add	x30,	x1,	x30
	sh	x23,	269(x30)
	li	x16,	301
	add	x16,	x1,	x16
	sw	x28,	-269(x16)
	li	x9,	-490
	add	x9,	x1,	x9
	sh	x17,	1682(x9)
	li	x6,	4940
	add	x6,	x1,	x6
	sh	x8,	-1682(x6)
	li	x21,	-1153
	add	x21,	x1,	x21
	sb	x14,	1435(x21)
	li	x9,	4657
	add	x9,	x1,	x9
	sb	x13,	-1435(x9)
	li	x18,	-282
	add	x18,	x1,	x18
	sh	x24,	1440(x18)
	li	x31,	2266
	add	x31,	x1,	x31
	sh	x9,	-1440(x31)
	li	x3,	713
	add	x3,	x1,	x3
	sb	x31,	1381(x3)
	li	x21,	2769
	add	x21,	x1,	x21
	sw	x16,	-1381(x21)
	li	x31,	46
	add	x31,	x1,	x31
	sh	x0,	820(x31)
	li	x17,	4898
	add	x17,	x1,	x17
	sh	x20,	-820(x17)
	li	x10,	2009
	add	x10,	x1,	x10
	sw	x28,	1911(x10)
	li	x9,	2859
	add	x9,	x1,	x9
	sb	x18,	-1911(x9)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

    li a0, 0xa
    ecall
