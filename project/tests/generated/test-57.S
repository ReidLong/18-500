# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0xbb4f0018
	li x2, 0x76ad315a
	li x3, 0x4a494a64
	li x4, 0x25bdb96b
	li x5, 0x1153c3d
	li x6, 0x70c8ec14
	li x7, 0x11eb5a9a
	li x8, 0x74a875aa
	li x9, 0xa313e78a
	li x10, 0xa61a9059
	li x11, 0x46da549b
	li x12, 0xdc96469b
	li x13, 0xabac56ee
	li x14, 0x32d01446
	li x15, 0x45cec135
	li x16, 0xd1a1eeff
	li x17, 0x8fecb27a
	li x18, 0xadb8be56
	li x19, 0xe7a32373
	li x20, 0xc6ece6a
	li x21, 0xa12e87ad
	li x22, 0x767bc923
	li x23, 0x4cc74940
	li x24, 0x5d1b9e84
	li x25, 0xcfd775a9
	li x26, 0x54bec104
	li x27, 0x9127f75b
	li x28, 0x62853895
	li x29, 0xa5088e8d
	li x30, 0x6160792a
	li x31, 0x9a3cbec4

        la x1, mydata

# Generating Store Sequence
	li	x29,	2102
	add	x29,	x1,	x29
	sw	x2,	66(x29)
	li	x29,	1740
	add	x29,	x1,	x29
	sh	x18,	-66(x29)
	li	x8,	2583
	add	x8,	x1,	x8
	sh	x15,	647(x8)
	li	x20,	4434
	add	x20,	x1,	x20
	sb	x15,	-647(x20)
	li	x23,	-548
	add	x23,	x1,	x23
	sh	x11,	1562(x23)
	li	x20,	2795
	add	x20,	x1,	x20
	sb	x12,	-1562(x20)
	li	x3,	1693
	add	x3,	x1,	x3
	sh	x22,	543(x3)
	li	x6,	2885
	add	x6,	x1,	x6
	sh	x2,	-543(x6)
	li	x22,	1158
	add	x22,	x1,	x22
	sb	x5,	940(x22)
	li	x25,	1636
	add	x25,	x1,	x25
	sh	x15,	-940(x25)
	li	x27,	1781
	add	x27,	x1,	x27
	sh	x25,	483(x27)
	li	x11,	2087
	add	x11,	x1,	x11
	sh	x31,	-483(x11)
	li	x5,	48
	add	x5,	x1,	x5
	sw	x2,	1140(x5)
	li	x6,	3325
	add	x6,	x1,	x6
	sb	x14,	-1140(x6)
	li	x25,	391
	add	x25,	x1,	x25
	sh	x2,	697(x25)
	li	x19,	4263
	add	x19,	x1,	x19
	sh	x27,	-697(x19)

# Generating Load/Store Sequence
	li	x15,	867
	add	x15,	x1,	x15
	sw	x11,	-7(x15)
	li	x29,	2941
	add	x29,	x1,	x29
	lw	x26,	-1377(x29)
	li	x25,	2700
	add	x25,	x1,	x25
	sw	x20,	-1672(x25)
	li	x24,	2347
	add	x24,	x1,	x24
	lb	x2,	1304(x24)
	li	x2,	1967
	add	x2,	x1,	x2
	sb	x2,	-1020(x2)
	li	x10,	4689
	add	x10,	x1,	x10
	lb	x0,	-1484(x10)
	li	x17,	4212
	add	x17,	x1,	x17
	sh	x0,	-828(x17)
	li	x27,	3001
	add	x27,	x1,	x27
	lhu	x5,	-195(x27)
	li	x4,	-753
	add	x4,	x1,	x4
	lhu	x10,	1253(x4)
	li	x2,	1966
	add	x2,	x1,	x2
	sb	x2,	-202(x2)
	li	x2,	2264
	add	x2,	x1,	x2
	lw	x16,	-2044(x2)
	li	x13,	3207
	add	x13,	x1,	x13
	sw	x24,	-155(x13)
	li	x11,	424
	add	x11,	x1,	x11
	sw	x26,	220(x11)
	li	x21,	3555
	add	x21,	x1,	x21
	lhu	x26,	-115(x21)
	li	x10,	4605
	add	x10,	x1,	x10
	lw	x30,	-725(x10)
	li	x30,	2247
	add	x30,	x1,	x30
	lhu	x22,	-1439(x30)
	li	x27,	2058
	add	x27,	x1,	x27
	lw	x24,	-1510(x27)
	li	x20,	1116
	add	x20,	x1,	x20
	lw	x22,	648(x20)
	li	x31,	1865
	add	x31,	x1,	x31
	sw	x18,	939(x31)
	li	x4,	2342
	add	x4,	x1,	x4
	lw	x16,	-150(x4)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
