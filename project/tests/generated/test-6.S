# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0xc7d80d66
	li x2, 0x60662b51
	li x3, 0x81b4530c
	li x4, 0x80e1b957
	li x5, 0xf42ce905
	li x6, 0x96b295ac
	li x7, 0x9f67508
	li x8, 0x8c82282d
	li x9, 0x66c4113c
	li x10, 0x4a7f8c60
	li x11, 0x9a487748
	li x12, 0x981869ba
	li x13, 0xd99fd732
	li x14, 0x5b2486c3
	li x15, 0x61650335
	li x16, 0x7c954df1
	li x17, 0xa54636bd
	li x18, 0x1fae2faa
	li x19, 0xce97dd55
	li x20, 0x922ef42e
	li x21, 0x737cb948
	li x22, 0xaac10ad7
	li x23, 0x24e43db1
	li x24, 0x73d3521e
	li x25, 0x9e7b0e3e
	li x26, 0x26d4d4c1
	li x27, 0xe517a7e9
	li x28, 0xf8c717c7
	li x29, 0x420abbdf
	li x30, 0x6f16389d
	li x31, 0xe24813f0

# Seeding Simple Instructions
	sltu	x11, x10, x10
	sltu	x12, x23, x14
	or	x23, x4, x2
	sub	x4, x11, x30
	or	x19, x14, x21
	add	x14, x18, x5
	add	x5, x23, x10
	add	x6, x22, x28

    li a0, 0xa
    ecall
