#add test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    add x5, x1, x2
    add x6, x2, x1
    add x7, x1, x1
    add x8, x1, x2
    add x9, x1, x3
    add x10, x1, x4
    add x11, x2, x1
    add x12, x2, x2
    add x13, x2, x3
    add x14, x2, x4
    add x15, x3, x1
    add x16, x3, x2
    add x17, x3, x3
    add x18, x3, x4
    add x19, x4, x1
    add x20, x4, x2
    add x21, x4, x3
    add x22, x4, x4
    add x23, x0, x1
    add x24, x0, x2
    add x25, x0, x3
    add x26, x0, x4
    add x27, x1, x0
    add x28, x2, x0
    add x29, x3, x0
    add x30, x4, x0


    li a0, 0xa
    ecall