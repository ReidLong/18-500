#slt test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    slt x5, x1, x2
    slt x6, x2, x1
    slt x7, x1, x1
    slt x8, x1, x2
    slt x9, x1, x3
    slt x10, x1, x4
    slt x11, x2, x1
    slt x12, x2, x2
    slt x13, x2, x3
    slt x14, x2, x4
    slt x15, x3, x1
    slt x16, x3, x2
    slt x17, x3, x3
    slt x18, x3, x4
    slt x19, x4, x1
    slt x20, x4, x2
    slt x21, x4, x3
    slt x22, x4, x4
    slt x23, x0, x1
    slt x24, x0, x2
    slt x25, x0, x3
    slt x26, x0, x4
    slt x27, x1, x0
    slt x28, x2, x0
    slt x29, x3, x0
    slt x30, x4, x0


    li a0, 0xa
    ecall