# subi test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    // subi x4, x1, -1
    // subi x5, x2, -1
    // subi x6, x1, 1
    // subi x7, x2, 1
    // subi x8, x3, 2147483647
    // subi x9, x4, 2147483647
    // subi x10, x3, -2147483648
    // subi x11, x4, -2147483648
    // subi x12, x1, 2147483647
    // subi x13, x2, 2147483647
    // subi x14, x1, -2147483648
    // subi x15, x2, -2147483648
    // subi x16, x1, 0
    // subi x17, x2, 0
    // subi x18, x3, 0
    // subi x19, x4, 0
    // subi x20, x3, 1
    // subi x21, x4, 1
    // subi x22, x3, -1
    // subi x23, x4, -1

    li a0, 0xa
    ecall
