# additest.s
#
# Addi Instruction test
#
# This test simply tests that the addi instruction is working as expected.

    .text                       # Declare the code to be in the .text segment
    .global main                # Make main visible to the linker
main:
    addi    t0, zero, 10        # t0 (x5) = 10
    addi    t1, zero, 5         # t1 (x6) = 5
    addi    a0, zero, 0xa       # a0 (x10) = 0xa
    addi    t2, t0,   300       # t2 (x7) = t1 + 300
    addi    t3, zero, 500       # t3 (x28) = 500
    jal     ra, skipSection
    ecall                       # Terminate the simulation by passing 0xa to
    addi    a2, zero, -1107
    addi    a3, zero, -1107
    addi    a4, zero, -1107
    addi    a5, zero, -1107

skipSection:
    jalr    t6, ra, 0
    addi    a6, zero, -1107
    addi    a7, zero, -1107
