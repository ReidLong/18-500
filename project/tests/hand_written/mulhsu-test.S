    .text
    .global main
main:
    li x1, 0x1
    li x2, 0x7FFFFFFF
    li x31, -0x1
    li x30, 0x80000000
    MULHSU x3, x1, x1 # 1 * 1 = [1]
    MULHSU x4, x1, x31 # 1 * -1 = [0xFFFF_FFFF]
    MULHSU x5, x31, x1 # -1 * 1 = [0xFFFF_FFFF_FFFF_FFFF]
    MULHSU x6, x31, x31 # -1 * -1 = [0xFFFF_FFFF_0000_0001]
    MULHSU x7, x1, x30 # 1 * 0x8000_0000  = [0x8000_0000]
    MULHSU x8, x31, x30 # -1 * 0x8000_0000 = [0xFFFF_FFFF_8000_0000]
    MULHSU x9, x1, x2 # 1 * 0x7FFF_FFFF = 0x7FFF_FFFF [0]
    MULHSU x11, x30, x30 # 0x8000_0000 * 0x8000_0000 = [0xC000_0000_0000_0000]
    MULHSU x12, x2, x2 # 0x7FFF_FFFF * 0x7FFF_FFFF = [0x3FFF_FFFF] 1
    MULHSU x13, x30, x2 # 0x8000_0000 * 0x7FFF_FFFF = [0xC000_0000] 0x8000_0000
    MULHSU x14, x1, x0 # 1 * 0 = 0 [0]
    MULHSU x15, x31, x0 # -1 * 0 = 0 [0]
    MULHSU x16, x30, x0 # 0x8000_0000 * 0 = 0 [0]
    MULHSU x17, x2, x0 # 0x7FFF_FFFF * 0 = 0 [0]
    MULHSU x18, x31, x2 # -1 * 0x7FFF_FFFF = 0x8000_0001 [0xFFFF_FFFF]
    li x19, 0x5
    li x20, 0x7
    MULHSU x19, x20, x19 # 5 * 7 = 35 (Sanity Check)
    li x20, 0x7FFF0000
    li x21, 0x7000FFFF
    MULHSU x20, x20, x21 # 0x7FFF0000 * 0x7000FFFF = 0x3800_0FFE_8001_0000

    MULHSU x21, x30, x1 # 0x8000_0000 * 1 = 0x8000_0000 [0xFFFF_FFFF]
    MULHSU x22, x30, x31 # 0x8000_0000 * -1 =[0x8000_0000_8000_0000]
    MULHSU x23, x2, x30 # 0x7FFF_FFFF * 0x8000_0000 = [0x3FFF_FFFF] 0x8000_0000
    MULHSU x24, x0, x31 # 0 * -1 = 0 [0]
    MULHSU x25, x0, x30 # 0 * 0x8000_0000 = 0 [0]
    MULHSU x26, x2, x31 # 0x7FFF_FFFF * -1  = [0x7FFF_FFFE_8000_0001]
    li x27, 0x8000FFFF
    li x28, 0x7FFF0000
    MULHSU x29, x27, x28 # 0x8000FFFF * 0x7FFF0000 = [0xC000_FFFE_8001_0000]
    MULHSU x30, x28, x27 # 0x7FFF0000 * 0x8000FFFF = [0x3FFF_FFFE_8001_0000]

    li a0, 0xa
    ecall