# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:


# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	addi	x5,	x2, 0x8
	li	x1,	0x0

    li x10, 0xDEAD
    sw x10, 0(x2)
    li x10, 0xBEEF
    sw x10, 4(x2)


loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

    li a0, 0xa
    ecall
