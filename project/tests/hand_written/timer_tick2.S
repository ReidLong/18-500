
.text
.global main
main:


    la a0, interuptHandler
    jal set_mtvec
    li a0, 0x5
    jal set_mscratch

    li s0, 0
    jal enable_interrupts

loop:
    addi s0, s0, 1
    j loop
endloop:

# This shouldn't be reachable
    li a0, 0xa
    ecall


interuptHandler:
    CSRR t1, mscratch
    CSRR t2, mcause
    CSRR t3, 0x343 # MTVAL
    CSRR t4, mstatus
    CSRR t5, mepc

    beqz t1, done

    ADDI t1, t1, -1
    CSRRW zero, mscratch, t1
    MRET

done:

    li x11, 0xB7000 # ~750,000
    SLT s0, x11, s0
    # li x11, 0x16e300 # ~1,500,000
    # SLT t3, x11, t3

    # Check that EPC is between loop and endloop
    la x11, loop
    ADDI x11, x11, -4
    SLT x12, x11, t5 # x12 = loop <= t5
    la x11, endloop
    SLT x13, t5, x11 # x13 = t5 < endloop
    AND t5, x12, x13

    li a0, 0xa
    ecall


# From 410kern/asm.S

enable_interrupts:
    li a0, 0x8
    csrrs zero, mstatus, a0
    ret

set_mtvec:
    csrrw zero, mtvec, a0
    ret

set_mscratch:
    csrrw a0, mscratch, a0
    ret

