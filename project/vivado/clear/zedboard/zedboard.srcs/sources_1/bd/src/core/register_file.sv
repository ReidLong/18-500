/**
 * register_file.sv
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This is the register file used by the main processor.
 *
 * The register file is a standard register file, with synchronous writes and
 * combinational reads. It has two read ports and a single write port for
 * instructions to access it.
 *
 * The register file also handles producing register dumps for simulation.
 * Whenever simulation finishes (indicated by the halted signal), the register
 * file dumps the values of all registers out to stdout and to file. This file
 * is then used by the build system to run verification of the results against
 * the reference register dump.
 *
 * Authors:
 *  - 2016 - 2017: Brandon Perez
 **/

// // RISC-V Includes
`include "riscv_isa.svh"                 // Default number of registers and width

// // Local Includes
`include "riscv_register_names.svh"      // Names for the RISC-V registers

// Force the compiler to throw an error if any variables are undeclared
`default_nettype none

/*------------------------------------------------------------------------------
 * Register File Module
 *----------------------------------------------------------------------------*/

/**
 * The register file used by the RISC-V processor.
 *
 * This is a synchronous write, combinational (asynchronous) read register file.
 * The register file has two read ports and a single write port for each way, or
 * pipeline. The value of register 0 is guaranteed to always be 0.  The register
 * file does not have internal forwarding. Thus, if a read and write occur to
 * the same location on a clock cycle, the read will get the value of the
 * register from the previous clock cycle.
 *
 * The register file is parameterized by the number of superscalar ways
 * (or pipelines) in the design, the number of registers, the width of the
 * registers. If there is a write conflict between the different ways, then the
 * highest numbered way will be the one to update the register, which
 * should correspond to the youngest instruction in the processor.
 *
 * Parameters:
 *  - WAYS      The number of superscalar ways, or pipelines, that access the
 *              register file.
 *  - NUM_REGS  The number of registers in the register file.
 *  - WIDTH     The number of bits that each register holds.
 *
 * Inputs:
 *  - clk       The clock to use for the registers in the register file.
 *  - rst_l     The asynchronous, active-low reset for the registers.
 *  - halted    Indicates that the processor has stopped due to a syscall or an
 *              exception. This is used by the register file to trigger a dump
 *              of the registers to stdout and to file.
 *  - rd_we     Indicates that the rd_data should be written to register(s) rd.
 *  - rs1       The first source register(s) to read from the register file.
 *  - rs2       The second source register(s) to read from the register file.
 *  - rd        The destination register(s) to write to in the register file.
 *  - rd_data   The data to write into register rd if rd_we is asserted.
 *
 * Outputs:
 *  - rs1_data  The data read from the rs1 register(s).
 *  - rs2_data  The data read from the rs2 register(s).
 **/
module register_file
    #(parameter NUM_REGS=`NUM_REGS, WIDTH=`XLEN)
    (input  logic                               clock, clear_l, halted,
     input  logic                     rd_we,
     input  logic [$clog2(NUM_REGS)-1:0] rs1, rs2, rd,
     input  logic [WIDTH-1:0]         rd_data,
     output logic [WIDTH-1:0]         rs1_data, rs2_data);

    // The file handle number for stdout
    localparam STDOUT = 32'h8000_0002;

    // The registers in the register file
    logic [NUM_REGS-1:0][WIDTH-1:0] registers;

    // Handle initialization and writing to the registers
    always_ff @(posedge clock) begin
       if (!clear_l) begin
           registers <= 'b0;
       end else begin
            if(rd_we && (rd != 'd0))
                registers[rd] <= rd_data;
       end
    end

    // Handle reading from the registers
    always_comb begin
        rs1_data = registers[rs1];
        rs2_data = registers[rs2];
    end

`ifdef SIMULATION_18447

    // Import the names of all the registers
    import RISCV_RegisterNames::*;

    // When simulation finishes, dump the register state to stdout and file
    int fd;
    always_ff @(posedge clock) begin
        if (clear_l && halted) begin
            $display("\n18-447 Register File Dump at Cycle %0d", $time);
            $display("---------------------------------------------\n");
            print_cpu_state(STDOUT, registers);

            fd = $fopen("simulation.reg");
            print_cpu_state(fd, registers);
            $display();
            $fclose(fd);
        end
    end

    // Prints out the information for a single register to the given file.
    function void print_register(int fd, int i, register_name_t reg_name,
            const ref logic [NUM_REGS-1:0][WIDTH-1:0] registers);

        // Format the ABI alias name for the register
        string abi_name, reg_uint_value, reg_int_value;
        abi_name = {"(", reg_name.abi_name, ")"};

        // Format the signed and unsigned views of the register
        $sformat(reg_uint_value, "(%0d)", registers[i]);
        $sformat(reg_int_value, "(%0d)", signed'(registers[i]));

        // Print out the register's names and values
        $fdisplay(fd, "%-8s %-8s = 0x%08x %-12s %-13s", reg_name.isa_name,
                abi_name, registers[i], reg_uint_value, reg_int_value);
    endfunction: print_register

    // Prints the CPU state to the given file.
    function void print_cpu_state(int fd,
            const ref logic [NUM_REGS-1:0][WIDTH-1:0] registers);

        /* Print out the instructions fetched and the current pc value. Don't
         * print this to the register dump file. */
        if (fd == STDOUT) begin
            $fdisplay(fd, "Current CPU State and Register Values:");
            $fdisplay(fd, "--------------------------------------");
            $fdisplay(fd, "%-20s = %0d", "Cycle Count",
                    $root.top.cycle_count);
            // $fdisplay(fd, "%-20s = 0x%08x\n", "Program Counter (PC)",
            //         $root.top.chip.core.memoryExport_WB_START.fetch.fetchState.pc);
        end

        // Display the header for the table of register values
        $fdisplay(fd, "%-8s %-8s   %-10s %-12s %-13s", "ISA Name", "ABI Name",
                "Hex Value", "Uint Value", "Int Value");
        $fdisplay(fd, {(8+1+8+3+10+1+12+1+13){"-"}});

        // Display the register and its values for each register
        foreach (REGISTER_NAMES[i]) begin
            print_register(fd, i, REGISTER_NAMES[i], registers);
        end
    endfunction: print_cpu_state

`endif /* SIMULATION_18447 */

endmodule: register_file
