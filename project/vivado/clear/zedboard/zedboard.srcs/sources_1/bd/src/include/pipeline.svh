`ifndef __PIPELINE_SVH
`define __PIPELINE_SVH

`include "riscv_isa.svh"
`include "register_file.svh"
`include "riscv_memory.svh"
`include "control_flow.svh"
`include "config.svh"

typedef struct packed {
    Word_t pc;
    logic isValid;
    Prediction_t prediction;
} FetchPartialState_t;

typedef struct packed {
    logic isValid;
    FetchPartialState_t fetchState;
    MemoryResponse_t instructionResponse;
} FetchExport_t;

// Decode Stage Signals

typedef enum {NO_IMMEDIATE, I_TYPE, U_TYPE, SB_TYPE, UJ_TYPE, S_TYPE, CSR_TYPE} ImmediateType_t;

// Constants that specify which operation the ALU should perform
typedef enum {
    ALU_ADD,                // Addition operation
    ALU_SHIFT_LEFT,
    ALU_LESS_THAN,
    ALU_LESS_THAN_UNSIGNED,
    ALU_XOR,
    ALU_SHIFT_RIGHT_LOGICAL,
    ALU_OR,
    ALU_AND,
    ALU_CLEAR_MASK,
    ALU_SUBTRACT,
    ALU_SHIFT_RIGHT_ARITHMATIC,
    `ifdef HARDWARE_MULT
    ALU_MUL_LOW,
    ALU_MUL_HIGH_SIGNED_SIGNED,
    ALU_MUL_HIGH_SIGNED_UNSIGNED,
    ALU_MUL_HIGH_UNSIGNED_UNSIGNED,
    ALU_DIV_SIGNED,
    ALU_DIV_UNSIGNED,
    ALU_REM_SIGNED,
    ALU_REM_UNSIGNED,
    `endif
    ALU_EQUAL,
    ALU_NOT_EQUAL,
    ALU_GREATER_THAN_OR_EQUAL,
    ALU_GREATER_THAN_OR_EQUAL_UNSIGNED
} ArithmeticOperation_t;

typedef enum {ALU_SELECT_ZERO, ALU_SELECT_REGISTER, ALU_SELECT_PC, ALU_SELECT_IMMEDIATE, ALU_SELECT_CSR} ALUInputSelect_t;

typedef struct packed {
    ArithmeticOperation_t aluOp;
    ALUInputSelect_t aluSelect1, aluSelect2;
} ALUControl_t;

typedef enum {NO_WRITE_BACK='d0, ALU_RESULT, IF_NEXT_INSTRUCTION, MEM_RESULT, CSR_RESULT} WritebackSelect_t;

typedef enum logic[3:0] {NO_MEMORY=4'd0, LOAD_BYTE, LOAD_HALF_WORD, LOAD_WORD, LOAD_BYTE_UNSIGNED, LOAD_HALF_WORD_UNSIGNED, STORE_BYTE, STORE_HALF_WORD, STORE_WORD, STORE_CONDITIONAL, LOAD_RESERVED} MemoryMode_t;

typedef enum {JUMP_BASE_ZERO, JUMP_BASE_PC, JUMP_BASE_RS1} JumpBaseSelect_t;

typedef enum {NO_CSR_UPDATE, CSR_RETURN, CSR_WRITE} UpdateCSRMode_t;

typedef struct packed {
    ImmediateType_t immediateType;
    ALUControl_t aluControl;
    Register_t rs1, rs2, rd;
    logic isIllegal;
    WritebackSelect_t writeBackSelect;
    MemoryMode_t memoryMode;
    JumpBaseSelect_t jumpBaseSelect;
    InstructionType_t instructionType;
    UpdateCSRMode_t csrMode;
} DecodeControlSignals_t;


typedef struct packed {
    logic isValid;
    DecodeControlSignals_t controlSignals;
    Word_t immediate;
    ControlStatusRegisterName_t csrName;
    Word_t csrValue;
    Word_t rs1Value, rs2Value;
} DecodeState_t;

typedef struct packed {
    FetchExport_t fetch;
    DecodeState_t decode;
} DecodeExport_t;

// Execute Signals

typedef struct packed {
    Word_t aluSrc1, aluSrc2, aluOut;
    Word_t executeResult;
    logic executeResultValid;
    Word_t jumpTarget;
    PredictionSummary_t predictionSummary;
    Word_t actualTarget;
} ExecuteState_t;

typedef struct packed {
    FetchExport_t fetch;
    DecodeState_t decode;
    ExecuteState_t execute;
} ExecuteExport_t;

// Memory signals

typedef struct packed {
    logic isValid;
    logic storeConditionalSuccess;
    MemoryRequest_t dataRequest;
    logic [1:0] offset;
    logic [4:0] shiftAmount;
    MemoryResponse_t dataResponse;
    Word_t memoryResult;
} MemoryState_t;

typedef struct packed {
    FetchExport_t fetch;
    DecodeState_t decode;
    ExecuteState_t execute;
    MemoryState_t memory;
} MemoryExport_t;


`endif