
`ifndef __REGISTER_FILE_SVH
`define __REGISTER_FILE_SVH

`include "riscv_memory.svh"

typedef struct packed {
    logic [4:0] number;
    logic isValid;
} Register_t;

typedef struct packed {
    Register_t register;
    Word_t result;
    logic resultValid;
} RegisterResponse_t;

interface RegisterFileInterface();
    // Signals from decode stage
    Register_t rs1_ID, rs2_ID;
    Word_t rs1Value_ID, rs2Value_ID;
    // Signals from write back stage
    RegisterResponse_t response_WB;
    // Execute forwarding
    RegisterResponse_t response_EX1, response_EX2;
    // Memory forwarding
    RegisterResponse_t response_MEM1, response_MEM2;

    modport RegisterFile(
        input rs1_ID, rs2_ID,
        output rs1Value_ID, rs2Value_ID,
        input response_WB,
        input response_EX1,
        input response_EX2,
        input response_MEM1,
        input response_MEM2
    );
    modport DecodeStage(
        output rs1_ID, rs2_ID,
        input rs1Value_ID, rs2Value_ID
    );
    modport ExecuteStage(
        output response_EX1,
        output response_EX2
    );
    modport MemoryStage(
        output response_MEM1,
        output response_MEM2
    );
    modport WriteBackStage(
        output response_WB
    );
endinterface : RegisterFileInterface

`endif