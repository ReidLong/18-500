`ifndef __RISCV_CONSOLE_SVH
`define __RISCV_CONSOLE_SVH

`include "config.svh"

`define VRAM_START 32'h1000
`define VRAM_END 32'h2000
`define VRAM_SIZE ((`VRAM_END) - (`VRAM_START))

`define CONSOLE_WIDTH 80
`define CONSOLE_HEIGHT 25

/* Bits 3:0 are the foreground color, bits 6:4 are the
   background color */
`define FGND_BLACK 8'h0
`define FGND_BLUE  8'h1
`define FGND_GREEN 8'h2
`define FGND_CYAN  8'h3
`define FGND_RED   8'h4
`define FGND_MAG   8'h5
`define FGND_BRWN  8'h6
`define FGND_LGRAY 8'h7 /* Light gray. */
`define FGND_DGRAY 8'h8 /* Dark gray. */
`define FGND_BBLUE 8'h9 /* Bright blue. */
`define FGND_BGRN  8'hA /* Bright green. */
`define FGND_BCYAN 8'hB /* Bright cyan. */
`define FGND_PINK  8'hC
`define FGND_BMAG  8'hD /* Bright magenta. */
`define FGND_YLLW  8'hE
`define FGND_WHITE 8'hF

`define BGND_BLACK 8'h00
`define BGND_BLUE  8'h10
`define BGND_GREEN 8'h20
`define BGND_CYAN  8'h30
`define BGND_RED   8'h40
`define BGND_MAG   8'h50
`define BGND_BRWN  8'h60
`define BGND_LGRAY 8'h70 /* Light gray. */

interface VGA_Interface();
    logic [3:0] vga_r, vga_g, vga_b;
    logic vga_vs, vga_hs;
    logic vgaClock;
    logic vramClock;
    modport Controller(
        output vga_r, vga_g, vga_b,
        output vga_vs, vga_hs,
        input vgaClock,
        input vramClock
    );

    modport Device(
        input vga_r, vga_g, vga_b,
        input vga_vs, vga_hs,
        output vgaClock,
        output vramClock
    );
endinterface

`define KEYBOARD_QUEUE_SIZE 64

interface KBD_Interface();
    logic pollClock;
    logic [7:0] readch, scancode;
    logic ascii_ready;

    modport Controller(
        output pollClock,
        input readch, scancode, ascii_ready
    );

    modport Device(
        input pollClock,
        output readch, scancode, ascii_ready
    );
endinterface

`endif