/**
 * riscv_uarch.vh
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This file contains definitions for the RISC-V microarchitecture
 * implemented by this processor. Namely, this defines parameters that determine
 * the processor's underlying architecture.
 *
 * Authors:
 *  - 2017: Brandon Perez
 **/

`ifndef RISCV_UARCH_VH_
`define RISCV_UARCH_VH_

/*----------------------------------------------------------------------------
 * Definitions
 *----------------------------------------------------------------------------*/

package RISCV_UArch;

    // Half of the period of the clock for the processor (simulation only)
    parameter CLOCK_HALF_PERIOD         = 50;

    // The width of words in the SRAM used for the branch target buffer (BTB)
    parameter BTB_WORD_WIDTH            = 62;

    // The number of words (entries) in the SRAM used for the BTB, by default
    parameter BTB_NUM_WORDS             = 128;

endpackage: RISCV_UArch

`endif /* RISCV_UARCH_VH_ */
