
`ifndef SIMULATION_18447

`include "chip.svh"
`include "riscv_console.svh"
`include "riscv_memory.svh"

`default_nettype none

module ChipInterface
    #(  parameter integer C_M00_AXI_ADDR_WIDTH  = 32,
        parameter integer C_M00_AXI_DATA_WIDTH  = 32,
        parameter integer C_M00_AXI_TRANSACTIONS_NUM    = 16,
        parameter integer C_M00_PHYSICAL_ADDRESS_OFFSET = 'h8000000
    )(
    // Basic IO
    input logic [7:0] switch,
    output logic [7:0] led,
    input logic [4:0] button,

    // VGA
    output logic[3:0] vga_r, vga_g, vga_b,
    output logic vga_vs, vga_hs,
    input logic vgaClock, vgaClear_n,
    input logic vramClock,

    // PS/2
    inout wire ps2_clk, ps2_data,

    // Ports of Axi Master Bus Interface M00_AXI
    input logic  m00_axi_aclk,
    input logic  m00_axi_aresetn,
    output logic [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr,
    output logic [2 : 0] m00_axi_awprot,
    output logic  m00_axi_awvalid,
    input logic  m00_axi_awready,
    output logic [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata,
    output logic [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb,
    output logic  m00_axi_wvalid,
    input logic  m00_axi_wready,
    input logic [1 : 0] m00_axi_bresp,
    input logic  m00_axi_bvalid,
    output logic  m00_axi_bready,
    output logic [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr,
    output logic [2 : 0] m00_axi_arprot,
    output logic  m00_axi_arvalid,
    input logic  m00_axi_arready,
    input logic [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata,
    input logic [1 : 0] m00_axi_rresp,
    input logic  m00_axi_rvalid,
    output logic  m00_axi_rready,

    input logic clock, areset_n
);
    logic [7:0] switch_s;
    logic [4:0] button_s, button_vga_s, button_vram_s;
    logic clear_s;

    Sync #(.WIDTH(8)) switch_sync (.clock(clock), .sig(switch), .sig_s(switch_s));
    Sync #(.WIDTH(5)) button_sync (.clock(clock), .sig(button), .sig_s(button_s));
    Sync #(.WIDTH(5)) button_vgaSync(.clock(vgaClock), .sig(button), .sig_s(button_vga_s));
    Sync #(.WIDTH(1)) clear_sync(.clock(vgaClock), .sig(~areset_n), .sig_s(clear_s));

    VGA_Interface vga();
    KBD_Interface kbd();
    DRAM_Interface dram();
    BasicIO_Interface basicIO();

    logic halted, fatalError, clear;

    assign basicIO.switch = switch_s;
    assign led = basicIO.led;
    assign basicIO.button = button_s;

    assign clear = clear_s || button_vga_s[`BUTTON_CENTER] || ~vgaClear_n;

    RISCV_Chip chip(
        .clock, .clear, .vga(vga.Controller), .kbd(kbd.Controller),
        .halted, .fatalError, .dram(dram.Requester), .basicIO(basicIO.Master)
    );

    DRAM_Controller #(
        .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M_TRANSACTIONS_NUM(C_M00_AXI_TRANSACTIONS_NUM),
        .PHYSICAL_ADDRESS_OFFSET(C_M00_PHYSICAL_ADDRESS_OFFSET)
        ) dramController(
        .dram(dram.Device),
        .M_AXI_ACLK(m00_axi_aclk),
        .M_AXI_ARESETN(m00_axi_aresetn),
        .M_AXI_AWADDR(m00_axi_awaddr),
        .M_AXI_AWPROT(m00_axi_awprot),
        .M_AXI_AWVALID(m00_axi_awvalid),
        .M_AXI_AWREADY(m00_axi_awready),
        .M_AXI_WDATA(m00_axi_wdata),
        .M_AXI_WSTRB(m00_axi_wstrb),
        .M_AXI_WVALID(m00_axi_wvalid),
        .M_AXI_WREADY(m00_axi_wready),
        .M_AXI_BRESP(m00_axi_bresp),
        .M_AXI_BVALID(m00_axi_bvalid),
        .M_AXI_BREADY(m00_axi_bready),
        .M_AXI_ARADDR(m00_axi_araddr),
        .M_AXI_ARPROT(m00_axi_arprot),
        .M_AXI_ARVALID(m00_axi_arvalid),
        .M_AXI_ARREADY(m00_axi_arready),
        .M_AXI_RDATA(m00_axi_rdata),
        .M_AXI_RRESP(m00_axi_rresp),
        .M_AXI_RVALID(m00_axi_rvalid),
        .M_AXI_RREADY(m00_axi_rready)
    );


    always_comb begin
        vga.vgaClock = vgaClock;
        vga.vramClock = vramClock;
        vga_r = vga.vga_r;
        vga_g = vga.vga_g;
        vga_b = vga.vga_b;
        vga_vs = vga.vga_vs;
        vga_hs = vga.vga_hs;
    end


    keyboard_driver keyboard(
        .ps2_clk, .ps2_data, .kbd_interface(kbd.Device), .clock, .clear
    );

endmodule

`endif
