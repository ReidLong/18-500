`ifndef SIMULATION_18447

module top
    #(  parameter integer C_M00_AXI_ADDR_WIDTH  = 32,
        parameter integer C_M00_AXI_DATA_WIDTH  = 32,
        parameter integer C_M00_AXI_TRANSACTIONS_NUM    = 16,
        parameter integer C_M00_PHYSICAL_ADDRESS_OFFSET = 'h8000000
    )(
    // Basic IO
    input wire [7:0] switch,
    output wire [7:0] led,
    input wire [4:0] button,

    // VGA
    output wire [3:0] vga_r, vga_g, vga_b,
    output wire vga_vs, vga_hs,
    input wire vgaClock, vgaClear_n,
    input wire vramClock,

    // PS/2
    inout wire ps2_clk, ps2_data,

    // Ports of Axi Master Bus Interface M00_AXI
    input wire  m00_axi_aclk,
    input wire  m00_axi_aresetn,
    output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr,
    output wire [2 : 0] m00_axi_awprot,
    output wire  m00_axi_awvalid,
    input wire  m00_axi_awready,
    output wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata,
    output wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb,
    output wire  m00_axi_wvalid,
    input wire  m00_axi_wready,
    input wire [1 : 0] m00_axi_bresp,
    input wire  m00_axi_bvalid,
    output wire  m00_axi_bready,
    output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr,
    output wire [2 : 0] m00_axi_arprot,
    output wire  m00_axi_arvalid,
    input wire  m00_axi_arready,
    input wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata,
    input wire [1 : 0] m00_axi_rresp,
    input wire  m00_axi_rvalid,
    output wire  m00_axi_rready,

    input wire clock, areset_n
);

    ChipInterface #(
        .C_M00_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M00_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M00_AXI_TRANSACTIONS_NUM(C_M00_AXI_TRANSACTIONS_NUM),
        .C_M00_PHYSICAL_ADDRESS_OFFSET(C_M00_PHYSICAL_ADDRESS_OFFSET)
        ) chipInterface(
        .switch(switch), .led(led), .button(button),
        .vga_r(vga_r), .vga_g(vga_g), .vga_b(vga_b),
        .vga_vs(vga_vs), .vga_hs(vga_hs),
        .vgaClock(vgaClock),
        .vgaClear_n(vgaClear_n),
        .ps2_clk(ps2_clk), .ps2_data(ps2_data),
        .vramClock(vramClock),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_awaddr(m00_axi_awaddr),
        .m00_axi_awprot(m00_axi_awprot),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wstrb(m00_axi_wstrb),
        .m00_axi_wvalid(m00_axi_wvalid),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_bresp(m00_axi_bresp),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_bready(m00_axi_bready),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_arprot(m00_axi_arprot),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_rdata(m00_axi_rdata),
        .m00_axi_rresp(m00_axi_rresp),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_rready(m00_axi_rready),
        .clock(clock),
        .areset_n(areset_n));

endmodule

`endif