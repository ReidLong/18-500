`default_nettype none

`include "riscv_memory.svh"
`include "riscv_console.svh"
`include "compiler.svh"

module VGA_Controller(
    input MemoryResponse_t vgaResponse,
    output MemoryRequest_t vgaRequest,
    VGA_Interface.Controller vga_interface,
    input logic clear
    );

    function MemoryRequest_t createRequest(logic readEnable, Address_t vramIndex);
        return '{
            readEnable: readEnable,
            writeEnable: 4'b0,
            // This is going to clear the lowest 2 bits to ensure all address requests are word aligned
            address: (vramIndex << 1) + `VRAM_START,
            addressType: PHYSICAL_ADDRESS,
            dataIn: `WORD_POISON
        };
    endfunction

    logic [11:0] index;

    localparam CONSOLE_SIZE = `CONSOLE_WIDTH * `CONSOLE_HEIGHT;
    logic [$clog2(CONSOLE_SIZE)-1:0] char_idx;

    `STATIC_ASSERT(2**$bits(index) >= CONSOLE_SIZE);

    assign index = char_idx & 12'hFFE;

    responseInRange: assert property (@(posedge vga_interface.vramClock) disable iff(clear) vgaResponse.isValid |->
        (`VRAM_START <= vgaResponse.request.address
            && vgaResponse.request.address < `VRAM_END));


    requstAligned: assert property (@(posedge vga_interface.vramClock) disable iff(clear) (index & 32'b1) == 32'b0);
    assign vgaRequest = createRequest(1'b1, index);


    // This state is from the previous cycles request (1 cycle latency on request to vga data)
    logic [1:0][7:0] character, color;

    assign character[0] = vgaResponse.dataOut[7:0];
    assign color[0] = vgaResponse.dataOut[15:8];
    assign character[1] = vgaResponse.dataOut[23:16];
    assign color[1] = vgaResponse.dataOut[31:24];

    logic char_offset;
    always_ff @(posedge vga_interface.vramClock) begin
        char_offset <= char_idx[0];
    end

    vga vga(.clk(vga_interface.vramClock), .rst(clear), .vgaClock(vga_interface.vgaClock),
        .cur_color(color[char_offset]), .ascii_code(character[char_offset][6:0]),
        .char_idx, .r(vga_interface.vga_r), .g(vga_interface.vga_g), .b(vga_interface.vga_b), .hs(vga_interface.vga_hs), .vs(vga_interface.vga_vs));


endmodule

module vga
   #(
     parameter CONSOLE_SIZE = `CONSOLE_WIDTH * `CONSOLE_HEIGHT
     )
   (
    input logic                             clk, rst, vgaClock,
    input logic [7:0]                       cur_color,
    input logic [6:0]                       ascii_code,
    output logic [$clog2(CONSOLE_SIZE)-1:0] char_idx,
    output logic [3:0]                      r, g, b,
    output logic                            hs, vs
    );

   // Handle VGA row and column generation here probably
   localparam VGA_WIDTH  = 640;
   localparam VGA_HEIGHT = 400;

   logic [9:0] vga_row, vga_col;
   logic vga_hs, vga_vs, vga_valid;
   vga_data datagen(.vgaClock, .rst, .HDATA(vga_col), .VDATA(vga_row), .VGA_HS(vga_hs), .VGA_VS(vga_vs), .valid(vga_valid));

   // Increment counters for characters
   localparam CHAR_WIDTH  = 8;
   localparam CHAR_HEIGHT = 16;
   logic [$clog2(`CONSOLE_HEIGHT)-1:0]      char_row;
   logic [$clog2(`CONSOLE_WIDTH)-1:0]       char_col;

   always_comb begin
      char_row  = vga_row / CHAR_HEIGHT;
      char_col  = vga_col / CHAR_WIDTH;
      char_idx  = char_row * `CONSOLE_WIDTH + char_col;
   end

   // Increment counter within characters
   logic [3:0]                              ascii_row;
   logic [2:0]                              ascii_col;

   always_comb begin
      ascii_row  = vga_row % CHAR_HEIGHT;
      ascii_col  = vga_col % CHAR_WIDTH;
   end

   logic                                    ascii_out;
   charmap charmap(clk, ascii_code, ascii_row, ascii_col, ascii_out);

   logic disp;
   `ifdef REID_MONITOR
      assign disp = ascii_out && vga_valid;
   `else
   `ifdef TEGUH_PROJECTOR
      always_ff @(posedge clk) begin
        disp <= ascii_out && vga_valid;
      end
    `else
      $fatal("Hardware mode not selected");
    `endif
    `endif

   logic [3:0]  fg_index, bg_index;
   logic [11:0] fg_color, bg_color;
   assign { bg_index, fg_index } = cur_color;
   cga_colors fg_cga(fg_index, fg_color);
   cga_colors bg_cga(bg_index, bg_color);

   always_ff @(posedge clk) begin
      { r, g, b } <= disp ? fg_color : bg_color;
      hs <= vga_hs;
      vs <= vga_vs;
   end

endmodule

module cga_colors (
                   input logic [3:0]   index,
                   output logic [11:0] color
                   );

   // TODO: these values might need to be changed
   always_comb begin
      case (index)
         'h0: color = { 4'b0000, 4'b0000, 4'b0000 };
         'h1: color = { 4'b0000, 4'b0000, 4'b1000 };
         'h2: color = { 4'b0000, 4'b1000, 4'b0000 };
         'h3: color = { 4'b0000, 4'b1000, 4'b1000 };
         'h4: color = { 4'b1000, 4'b0000, 4'b0000 };
         'h5: color = { 4'b1000, 4'b0000, 4'b1000 };
         'h6: color = { 4'b1000, 4'b0100, 4'b0000 };
         'h7: color = { 4'b1000, 4'b1000, 4'b1000 };
         'h8: color = { 4'b0100, 4'b0100, 4'b0100 };
         'h9: color = { 4'b0100, 4'b0100, 4'b1100 };
         'ha: color = { 4'b0100, 4'b1100, 4'b0100 };
         'hb: color = { 4'b0100, 4'b1100, 4'b1100 };
         'hc: color = { 4'b1100, 4'b0100, 4'b0100 };
         'hd: color = { 4'b1100, 4'b0100, 4'b1100 };
         'he: color = { 4'b1100, 4'b1100, 4'b0100 };
         'hf: color = { 4'b1100, 4'b1100, 4'b1100 };
      endcase
   end

endmodule

module vga_data (
    input  logic       vgaClock, rst,
    output logic [9:0] HDATA, VDATA,
    output logic       VGA_HS, VGA_VS,
    output logic       valid
    );

    // Timing information taken from http://tinyvga.com/vga-timing/640x400@70Hz
    // Clock divider from 100 MHZ to 25.175 MHz

    // Row state
    parameter ROWS = 400;
    parameter V_FP_LINES   = 12;
    parameter V_SYNC_LINES = 2;
    parameter V_BP_LINES   = 35;
    parameter ROW_LINES = ROWS + V_FP_LINES + V_SYNC_LINES + V_BP_LINES;

    logic row_rst, row_en;
    logic [$clog2(ROW_LINES)-1:0] row;
    Counter #(.WIDTH($clog2(ROW_LINES))) row_counter(.clock(vgaClock), .clear(row_rst|rst), .enable(row_en), .q(row));

    typedef enum { ROW_VALID, ROW_FP, ROW_SYNC, ROW_BP } row_state_t;
    row_state_t row_state, next_row_state;
    always_ff @(posedge vgaClock) begin
        if (rst) row_state <= ROW_VALID;
        else row_state <= next_row_state;
    end

    // Column state
    parameter COLS = 640;
    parameter H_FP_PIXELS   = 16;
    parameter H_SYNC_PIXELS = 96;
    parameter H_BP_PIXELS   = 48;
    parameter COL_PIXELS = COLS + H_FP_PIXELS + H_SYNC_PIXELS + H_BP_PIXELS;

    logic col_rst, col_en;
    logic [$clog2(COL_PIXELS)-1:0] col;
    Counter #(.WIDTH($clog2(COL_PIXELS))) col_counter(.clock(vgaClock), .clear(col_rst|rst), .enable(col_en), .q( col));

    typedef enum { COL_VALID, COL_FP, COL_SYNC, COL_BP } col_state_t;
    col_state_t col_state, next_col_state;
    always_ff @(posedge vgaClock) begin
        if (rst) col_state <= COL_VALID;
        else col_state <= next_col_state;
    end

    // State logic
    always_comb begin
        unique case (row_state)
            ROW_VALID: next_row_state = row == (ROWS-1) ? ROW_FP : ROW_VALID;
            ROW_FP:    next_row_state = row == (ROWS+V_FP_LINES-1) ? ROW_SYNC : ROW_FP;
            ROW_SYNC:  next_row_state = row == (ROWS+V_FP_LINES+V_SYNC_LINES-1) ? ROW_BP : ROW_SYNC;
            ROW_BP:    next_row_state = row == (ROWS+V_FP_LINES+V_SYNC_LINES+V_BP_LINES-1) ? ROW_VALID : ROW_BP;
        endcase // row_state

        unique case (col_state)
            COL_VALID: next_col_state = col == (COLS-1) ? COL_FP : COL_VALID;
            COL_FP:    next_col_state = col == (COLS+H_FP_PIXELS-1) ? COL_SYNC : COL_FP;
            COL_SYNC:  next_col_state = col == (COLS+H_FP_PIXELS+H_SYNC_PIXELS-1) ? COL_BP : COL_SYNC;
            COL_BP:    next_col_state = col == (COLS+H_FP_PIXELS+H_SYNC_PIXELS+H_BP_PIXELS-1) ? COL_VALID : COL_BP;
        endcase // col_state
    end

    // Control logic
    always_comb begin
        row_rst = 0;
        row_en = 0;

        col_rst = 0;
        col_en = 1;

        // Increment row and reset columns
        if (col == (COLS + H_FP_PIXELS + H_SYNC_PIXELS + H_BP_PIXELS) - 1) begin
            col_rst = 1;
            row_en = 1;
        end

        // Reset row
        if (row == (ROWS + V_FP_LINES + V_SYNC_LINES + V_BP_LINES) - 1) begin
            row_rst = 1;
        end
    end

    // Output logic
    always_comb begin
        HDATA = col;
        VDATA = row;
        valid = (col_state == COL_VALID && row_state == ROW_VALID);

        VGA_VS = ~(row_state == ROW_SYNC);
        VGA_HS = ~(col_state == COL_SYNC);
    end

endmodule