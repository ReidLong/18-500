
`default_nettype none

`include "control_status_register.svh"
`include "compiler.svh"
`include "riscv_memory.svh"
`include "log.svh"
`include "riscv_isa.svh"

module ControlStatusRegister(
    ControlStatusRegisterInterface.ControlStatusRegister csrInterface,

    input logic clock, clear, halted
);

    `STATIC_ASSERT($bits(MachineStatus_t) == `XLEN);
    `STATIC_ASSERT($bits(SupervisorAddressTranslation_t) == `XLEN);


    PerformanceCounterSummary_t summary;

    function automatic logic shouldEnable(ControlStatusRegisterName_t name);
        return csrInterface.writeEnable_WB && (csrInterface.name_WB == name);
    endfunction

    SupervisorAddressTranslation_t satp;
    logic satp_enable, mstatus_enable;
    logic mtvec_enable, mscratch_enable, mepc_enable, mcause_enable, mtval_enable;
    Word_t nextCause, nextEPC, nextTVAL;

    assign satp_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == SATP);


    Register #(.WIDTH(`XLEN)) satp_reg(.q(satp), .d(csrInterface.csrValue_WB), .clock, .enable(satp_enable), .clear);

    MachineStatus_t mstatus, returnStatus, nextStatus;

    noReturnWrite: assert property (@(posedge clock) disable iff(clear) csrInterface.returnMSTATUS_WB |-> !csrInterface.writeEnable_WB && !csrInterface.interruptValid);
    noInterruptWrite: assert property (@(posedge clock) disable iff(clear) csrInterface.interruptValid |-> !csrInterface.returnMSTATUS_WB && !csrInterface.writeEnable_WB);

    PriviledgeLevel_t currentLevel, nextLevel;
    logic priviledgeLevel_enable;

    Register #(.WIDTH($bits(currentLevel))) priviledgeLevel_register(.q(currentLevel), .d(nextLevel), .clock, .enable(priviledgeLevel_enable), .clear);

    // This should be removed once we support user mode
    alwaysMachineMode: assert property (@(posedge clock) disable iff(clear) currentLevel == MACHINE_MODE);

    always_comb begin
        nextStatus = csrInterface.csrValue_WB & `MSTATUS_WRITE_MASK;
        mstatus_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MSTATUS);
        priviledgeLevel_enable = 1'b0;
        nextLevel = currentLevel;
        nextCause = csrInterface.csrValue_WB;
        nextEPC = csrInterface.csrValue_WB;
        nextTVAL = csrInterface.csrValue_WB;
        mepc_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MEPC);
        mcause_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MCAUSE);
        mtval_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MTVAL);
        if(csrInterface.returnMSTATUS_WB) begin
            nextStatus = '{
                zero1: 'd0,
                machineInterruptsEnabled: mstatus.machinePriorInterruptsEnabled,
                zero2: 'd0,
                machinePriorInterruptsEnabled: INTERRUPTS_ENABLED,
                zero3: 'd0,
                machinePreviousPriviledge: MACHINE_MODE,
                zero4: 'd0
            };
            mstatus_enable = 1'b1;
            priviledgeLevel_enable = 1'b1;
            nextLevel = mstatus.machinePreviousPriviledge;
        end else if(csrInterface.interruptValid) begin
            nextStatus = '{
                zero1: 'd0,
                machineInterruptsEnabled: INTERRUPTS_DISABLED,
                zero2: 'd0,
                machinePriorInterruptsEnabled: mstatus.machineInterruptsEnabled,
                zero3: 'd0,
                machinePreviousPriviledge: currentLevel,
                zero4: 'd0
            };
            mstatus_enable = 1'b1;
            nextLevel = MACHINE_MODE;
            priviledgeLevel_enable = 1'b1;
            mepc_enable = 1'b1;
            nextEPC = csrInterface.epc;
            mcause_enable = 1'b1;
            nextCause = csrInterface.cause;
            mtval_enable = 1'b1;
            nextTVAL = csrInterface.tval;
        end
    end

    zeroZero: assert property (@(posedge clock) disable iff(clear) mstatus.zero1 == 'd0 && mstatus.zero2 == 'd0 && mstatus.zero3 == 'd0 && mstatus.zero4 == 'd0);

    Register #(.WIDTH(`XLEN)) mstatus_reg(.q(mstatus), .d(nextStatus), .clock, .enable(mstatus_enable), .clear);

    Word_t mtvec, mscratch, mepc, mcause, mtval;

    assign mtvec_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MTVEC);
    assign mscratch_enable = csrInterface.writeEnable_WB && (csrInterface.name_WB == MSCRATCH);

    `STATIC_ASSERT($bits(Word_t) == `XLEN);
    Register #(.WIDTH(`XLEN)) mtvec_reg(.q(mtvec), .d(csrInterface.csrValue_WB), .clock, .enable(mtvec_enable), .clear);
    Register #(.WIDTH(`XLEN)) mscratch_reg(.q(mscratch), .d(csrInterface.csrValue_WB), .clock, .enable(mscratch_enable), .clear);
    Register #(.WIDTH(`XLEN)) mepc_reg(.q(mepc), .d(nextEPC), .clock, .enable(mepc_enable), .clear);
    Register #(.WIDTH(`XLEN)) mcause_reg(.q(mcause), .d(nextCause), .clock, .enable(mcause_enable), .clear);
    Register #(.WIDTH(`XLEN)) mtval_reg(.q(mtval), .d(nextTVAL), .clock, .enable(mtval_enable), .clear);

    assign csrInterface.mepc_EX2 = mepc;
    assign csrInterface.mtvec = mtvec;
    assign csrInterface.mstatus = mstatus;

    PerformanceCounters performanceCounters(
        .instructionFetched(csrInterface.instructionFetched),
        .instructionRetired(csrInterface.memoryExport_WB.fetch.isValid),
        .predictionSummary(csrInterface.memoryExport_WB.execute.predictionSummary),
        .instructionResponse(csrInterface.memoryExport_WB.fetch.instructionResponse),
        .memoryState(csrInterface.memoryExport_WB.memory),
        .memoryMode(csrInterface.memoryExport_WB.decode.controlSignals.memoryMode),
        .summary,
        .clock, .clear, .halted
    );


    always_comb begin
        unique case(csrInterface.name_ID)
            NONE: csrInterface.csrValue_ID = 32'b0;
            SATP: csrInterface.csrValue_ID = satp;
            MSTATUS: csrInterface.csrValue_ID = mstatus;
            MTVEC: csrInterface.csrValue_ID = mtvec;
            MSCRATCH: csrInterface.csrValue_ID = mscratch;
            MEPC: csrInterface.csrValue_ID = mepc;
            MCAUSE: csrInterface.csrValue_ID = mcause;
            MTVAL: csrInterface.csrValue_ID = mtval;
            CYCLE: csrInterface.csrValue_ID = summary.cycleCount[31:0];
            INSTRET: csrInterface.csrValue_ID = summary.instructionsRetired[31:0];
            HPMCOUNTER3: csrInterface.csrValue_ID = summary.instructionsFetched[31:0];
            HPMCOUNTER4: csrInterface.csrValue_ID = summary.forwardBranchRetired[31:0];
            HPMCOUNTER5: csrInterface.csrValue_ID = summary.backwardBranchRetired[31:0];
            HPMCOUNTER6: csrInterface.csrValue_ID = summary.forwardBranchPredictedCorrect[31:0];
            HPMCOUNTER7: csrInterface.csrValue_ID = summary.backwardBranchPredictedCorrect[31:0];
            HPMCOUNTER8: csrInterface.csrValue_ID = summary.forwardBranchPredictedIncorrect[31:0];
            HPMCOUNTER9: csrInterface.csrValue_ID = summary.backwardBranchPredictedIncorrect[31:0];
            HPMCOUNTER10: csrInterface.csrValue_ID = summary.instructionCacheHit[31:0];
            HPMCOUNTER11: csrInterface.csrValue_ID = summary.instructionCacheMiss[31:0];
            HPMCOUNTER12: csrInterface.csrValue_ID = summary.dataCacheHit[31:0];
            HPMCOUNTER13: csrInterface.csrValue_ID = summary.dataCacheMiss[31:0];
            HPMCOUNTER14: csrInterface.csrValue_ID = summary.dataVramHit[31:0];
            HPMCOUNTER15: csrInterface.csrValue_ID = summary.storeConditionalSuccess[31:0];
            HPMCOUNTER16: csrInterface.csrValue_ID = summary.storeConditionalReject[31:0];
            HPMCOUNTER17: csrInterface.csrValue_ID = summary.keyboardInterruptUser[31:0];
            HPMCOUNTER18: csrInterface.csrValue_ID = summary.keyboardInterruptMachine[31:0];
            HPMCOUNTER19: csrInterface.csrValue_ID = summary.execeptionUser[31:0];
            HPMCOUNTER20: csrInterface.csrValue_ID = summary.exceptionMachine[31:0];
            HPMCOUNTER21: csrInterface.csrValue_ID = summary.timerInterruptUser[31:0];
            HPMCOUNTER22: csrInterface.csrValue_ID = summary.timerInterruptMachine[31:0];
            HPMCOUNTER23: csrInterface.csrValue_ID = summary.forwardJumpRetired[31:0];
            HPMCOUNTER24: csrInterface.csrValue_ID = summary.backwardJumpRetired[31:0];
            HPMCOUNTER25: csrInterface.csrValue_ID = summary.forwardJumpPredictedCorrect[31:0];
            HPMCOUNTER26: csrInterface.csrValue_ID = summary.backwardJumpPredictedCorrect[31:0];
            HPMCOUNTER27: csrInterface.csrValue_ID = summary.forwardJumpPredictedIncorrect[31:0];
            HPMCOUNTER28: csrInterface.csrValue_ID = summary.backwardJumpPredictedIncorrect[31:0];
            HPMCOUNTER29: csrInterface.csrValue_ID = 32'b0;
            HPMCOUNTER30: csrInterface.csrValue_ID = 32'b0;
            HPMCOUNTER31: csrInterface.csrValue_ID = 32'b0;
            CYCLEH: csrInterface.csrValue_ID =         summary.cycleCount[63:32];
            INSTRETH: csrInterface.csrValue_ID =       summary.instructionsRetired[63:32];
            HPMCOUNTER3H: csrInterface.csrValue_ID =   summary.instructionsFetched[63:32];
            HPMCOUNTER4H: csrInterface.csrValue_ID =   summary.forwardBranchRetired[63:32];
            HPMCOUNTER5H: csrInterface.csrValue_ID =   summary.backwardBranchRetired[63:32];
            HPMCOUNTER6H: csrInterface.csrValue_ID =   summary.forwardBranchPredictedCorrect[63:32];
            HPMCOUNTER7H: csrInterface.csrValue_ID =   summary.backwardBranchPredictedCorrect[63:32];
            HPMCOUNTER8H: csrInterface.csrValue_ID =   summary.forwardBranchPredictedIncorrect[63:32];
            HPMCOUNTER9H: csrInterface.csrValue_ID =   summary.backwardBranchPredictedIncorrect[63:32];
            HPMCOUNTER10H: csrInterface.csrValue_ID =  summary.instructionCacheHit[63:32];
            HPMCOUNTER11H: csrInterface.csrValue_ID =  summary.instructionCacheMiss[63:32];
            HPMCOUNTER12H: csrInterface.csrValue_ID =  summary.dataCacheHit[63:32];
            HPMCOUNTER13H: csrInterface.csrValue_ID =  summary.dataCacheMiss[63:32];
            HPMCOUNTER14H: csrInterface.csrValue_ID =  summary.dataVramHit[63:32];
            HPMCOUNTER15H: csrInterface.csrValue_ID =  summary.storeConditionalSuccess[63:32];
            HPMCOUNTER16H: csrInterface.csrValue_ID =  summary.storeConditionalReject[63:32];
            HPMCOUNTER17H: csrInterface.csrValue_ID =  summary.keyboardInterruptUser[63:32];
            HPMCOUNTER18H: csrInterface.csrValue_ID =  summary.keyboardInterruptMachine[63:32];
            HPMCOUNTER19H: csrInterface.csrValue_ID =  summary.execeptionUser[63:32];
            HPMCOUNTER20H: csrInterface.csrValue_ID =  summary.exceptionMachine[63:32];
            HPMCOUNTER21H: csrInterface.csrValue_ID =  summary.timerInterruptUser[63:32];
            HPMCOUNTER22H: csrInterface.csrValue_ID =  summary.timerInterruptMachine[63:32];
            HPMCOUNTER23H: csrInterface.csrValue_ID =  summary.forwardJumpRetired[63:32];
            HPMCOUNTER24H: csrInterface.csrValue_ID =  summary.backwardJumpRetired[63:32];
            HPMCOUNTER25H: csrInterface.csrValue_ID =  summary.forwardJumpPredictedCorrect[63:32];
            HPMCOUNTER26H: csrInterface.csrValue_ID =  summary.backwardJumpPredictedCorrect[63:32];
            HPMCOUNTER27H: csrInterface.csrValue_ID =  summary.forwardJumpPredictedIncorrect[63:32];
            HPMCOUNTER28H: csrInterface.csrValue_ID =  summary.backwardJumpPredictedIncorrect[63:32];
            HPMCOUNTER29H: csrInterface.csrValue_ID =  32'b0;
            HPMCOUNTER30H: csrInterface.csrValue_ID =  32'b0;
            HPMCOUNTER31H: csrInterface.csrValue_ID =  32'b0;
            default: csrInterface.csrValue_ID = 32'b0;
        endcase
    end


endmodule