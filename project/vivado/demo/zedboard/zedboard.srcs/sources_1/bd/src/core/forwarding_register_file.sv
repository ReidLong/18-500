`include "register_file.svh"
`include "control_flow.svh"
`include "riscv_memory.svh"
`include "compiler.svh"
`include "riscv_isa.svh"

`default_nettype none

typedef enum {EX_FORWARD, EX_STALL, MEM_STALL, MEM_FORWARD, WB_FORWARD, RF_FILL} ForwardCause_t;

module ForwardingRegisterFile(
    RegisterFileInterface.RegisterFile registerFileInterface,
    ControlFlowInterface.RegisterFile controlFlow,
    input logic clock, clear, halted
);

    logic writeEnable;
    assign writeEnable = registerFileInterface.response_WB.resultValid;

    writebackResultValid: assert property (@(posedge clock) disable iff(clear) registerFileInterface.response_WB.resultValid == registerFileInterface.response_WB.register.isValid);

    `STATIC_ASSERT($bits(registerFileInterface.rs1_ID.number) == $clog2(`NUM_REGS));

    Word_t rs1RegisterFile, rs2RegisterFile;

    register_file registerFile(
        .clock, .clear_l(~clear), .halted,

        .rd_we(writeEnable), .rd(registerFileInterface.response_WB.register.number), .rd_data(registerFileInterface.response_WB.result),
        .rs1(registerFileInterface.rs1_ID.number), .rs1_data(rs1RegisterFile),
        .rs2(registerFileInterface.rs2_ID.number), .rs2_data(rs2RegisterFile)
    );

    logic rs1NeedStall, rs2NeedStall;
    Word_t rs1Data, rs2Data;

    assign controlFlow.registerFileStall_ID = rs1NeedStall || rs2NeedStall;
    assign registerFileInterface.rs1Value_ID = rs1Data;
    assign registerFileInterface.rs2Value_ID = rs2Data;

    function automatic logic isMatch(Register_t key, Register_t test);
        // If our register isn't valid or we are the zero register, then we don't match anything
        if(~key.isValid || key.number == 5'd0) return 1'b0;
        return test.isValid && (test.number == key.number);
    endfunction

    ForwardCause_t rs1Cause, rs2Cause;

    assign rs1NeedStall = (rs1Cause == EX_STALL || rs1Cause == MEM_STALL);
    assign rs2NeedStall = (rs2Cause == EX_STALL || rs2Cause == MEM_STALL);

`ifdef SIMULATION_18447
    function automatic void displayForwardingInformation();
        if(rs1Cause != RF_FILL) $display("Forwarding RS1 (%d) because %s", registerFileInterface.rs1_ID, rs1Cause);
        if(rs2Cause != RF_FILL) $display("Forwarding RS2 (%d) because %s", registerFileInterface.rs2_ID, rs2Cause);
    endfunction
`endif

    function automatic ForwardCause_t forwardRegister(input Register_t register, input Word_t rfValue, output Word_t value);
        if(isMatch(register, registerFileInterface.response_EX1.register)) begin
            if(registerFileInterface.response_EX1.resultValid) begin
                value = registerFileInterface.response_EX1.result;
                return EX_FORWARD;
            end else begin
                // This must be a memory operation
                value = `WORD_POISON;
                return EX_STALL;
            end
        end else if(isMatch(register, registerFileInterface.response_EX2.register)) begin
            if(registerFileInterface.response_EX2.resultValid) begin
                value = registerFileInterface.response_EX2.result;
                return EX_FORWARD;
            end else begin
                // This must be a memory operation
                value = `WORD_POISON;
                return EX_STALL;
            end
        end else if(isMatch(register, registerFileInterface.response_MEM1.register)) begin
            if(registerFileInterface.response_MEM1.resultValid) begin
                value = registerFileInterface.response_MEM1.result;
                return MEM_FORWARD;
            end else begin
                // Since the memory is sequential there is no forwarding possible. The data will be visible in the write back stage and forwarded there.
                value = `WORD_POISON;
                return MEM_STALL;
            end
        end else if(isMatch(register, registerFileInterface.response_MEM2.register)) begin
            if(registerFileInterface.response_MEM2.resultValid) begin
                value = registerFileInterface.response_MEM2.result;
                return MEM_FORWARD;
            end else begin
                // Technically we should be able to forward the result here, but this is on the critical path, so we will wait until the next cycle.
                value = `WORD_POISON;
                return MEM_STALL;
            end
        end else if(isMatch(register, registerFileInterface.response_WB.register)) begin
            assert(registerFileInterface.response_WB.resultValid);
            value = registerFileInterface.response_WB.result;
            return WB_FORWARD;
        end else begin
            // Nothing in the pipeline has the right value
            value = rfValue;
            return RF_FILL;
        end
    endfunction

    always_comb begin
        rs1Cause = forwardRegister(registerFileInterface.rs1_ID, rs1RegisterFile, rs1Data);
        rs2Cause = forwardRegister(registerFileInterface.rs2_ID, rs2RegisterFile, rs2Data);
    end

endmodule