
`ifndef __CONFIG_SVH
`define __CONFIG_SVH

/* A quick switch to enable/disable tracing. Comment out to disable. Please
 * comment this out before submitting your code. You'll also want to comment
 * this out for longer tests, as it will make them run much faster. */
// `define TRACE

// Enable the M extension
// `define HARDWARE_MULT

// Which VGA configuration do you want? (Adds extra registers because VGA is hard)
`define REID_MONITOR
// `define TEGUH_PROJECTOR

`define TIMER_TICK_CYCLES 32'd250000
// `define TIMER_TICK_CYCLES 32'd25000


`endif