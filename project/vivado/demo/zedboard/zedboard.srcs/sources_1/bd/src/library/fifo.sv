`default_nettype none

module FIFO #(parameter SIZE=64, WIDTH=32)(
    // Producer
    input logic [WIDTH-1:0] producerData,
    input logic enqueueValid,
    // Consumer
    output logic [WIDTH-1:0] consumerData,
    input logic dequeueValid,
    // State
    output logic isEmpty, isFull,
    input logic clock, clear
);

    // Since we are asserting these properties, we aren't going to handle the cases where you enqueue when full or dequeue when empty. This is actually pretty dangerous in the general case, but should be safe enough for us
    noEnqueueFull: assert property (@(posedge clock) disable iff(clear) isFull |-> !enqueueValid);
    noDequeueEmpty: assert property (@(posedge clock) disable iff(clear) isEmpty |-> !dequeueValid);

    logic [$clog2(SIZE)-1:0] producerIndex, consumerIndex;

    xilinx_bram #(.RAM_WIDTH(WIDTH), .RAM_DEPTH(SIZE)) bram(
        .addressA(producerIndex),
        .dataInA(producerData),
        .writeEnableA(enqueueValid),
        .dataOutA(),

        .addressB(consumerIndex),
        .dataInB({WIDTH{1'b0}}),
        .writeEnableB(1'b0),
        .dataOutB(consumerData),

        .clock
    );

    Counter #(.WIDTH($bits(producerIndex))) producerIndex_register(.clock, .clear, .enable(enqueueValid), .q(producerIndex));
    Counter #(.WIDTH($bits(consumerIndex))) consumerIndex_register(.clock, .clear, .enable(dequeueValid), .q(consumerIndex));

    assign isEmpty = (producerIndex == consumerIndex);
    assign isFull = (((producerIndex + 32'd1) % SIZE) == consumerIndex);

endmodule