`default_nettype none

module Sync #(parameter WIDTH = 1) (
  input  logic             clock ,
  input  logic [WIDTH-1:0] sig  ,
  output logic [WIDTH-1:0] sig_s
);

  logic [WIDTH-1:0] sig_i;

  always_ff @(posedge clock) begin
    sig_i <= sig;
    sig_s <= sig_i;
  end

endmodule

module Register #(parameter WIDTH=32, CLEAR_VALUE = 0) (
  output logic [WIDTH-1:0] q    ,
  input  logic [WIDTH-1:0] d    ,
  input  logic             clock, enable, clear
);

  always_ff @(posedge clock)
    if(clear)
      q <= CLEAR_VALUE;
    else if (enable)
      q <= d;

endmodule // register

module Array #(parameter ELEMENTS=16, WIDTH=32)(
  input logic [$clog2(ELEMENTS)-1:0] index,
  input logic [WIDTH-1:0] element,
  output logic [ELEMENTS-1:0][WIDTH-1:0] array,
  input logic clock, clear, enable
  );

  localparam ZERO_ELEMENT = {WIDTH{1'b0}};

  always_ff @(posedge clock)
    if(clear)
      array <= {ELEMENTS{ZERO_ELEMENT}};
    else if(enable)
      array[index] <= element;

endmodule

module Counter #(parameter WIDTH = 8, INCREMENT=1) (
   input  logic         clock  ,
   input  logic         clear  ,
   input  logic         enable ,
   output logic [WIDTH-1:0] q
);

   always_ff @(posedge clock)
      if(clear)
         q <= 0;
      else if (enable)
         q <= q + INCREMENT;

endmodule: Counter

module EdgeTrigger(
    input logic signal,
    output logic isEdge,
    input logic clock, clear);

    logic current, next;

    assign isEdge = !next && current;

    always_ff @(posedge clock) begin
        if(clear) begin
            current <= 1'b0;
            next <= 1'b0;
        end else begin
            current <= signal;
            next <= current;
        end
    end

endmodule


`ifdef SIMULATION_18447
/*----------------------------------------------------------------------------
 * Clock Module
 *----------------------------------------------------------------------------*/

/**
 * The generator for the global clock used for the processor.
 *
 * This outputs the global clock for the design, and is parameterized by
 * the clock's half period, so the actual period is double that.
 *
 * Parameters:
 *  - HALF_PERIOD   Half of the generated clock's period.
 *
 * Outputs:
 *  - clk           The global clock for the design, with a period of
 *                  2*HALF_PERIOD.
 **/
module Clock
    #(parameter HALF_PERIOD=0)
    (output logic clock);

    initial begin
        clock = 1;

        forever #HALF_PERIOD clock = ~clock;
    end

endmodule: Clock

`endif