`default_nettype none

module keyboard_driver(
    inout wire ps2_clk, ps2_data,
    KBD_Interface.Device kbd_interface,
    input logic clock, clear
);

    kbd kbd(.clk(kbd_interface.pollClock), .rst(clear),
      .ps2_clk, .ps2_data,
      .readch(kbd_interface.readch), .scancode(kbd_interface.scancode),
      .ascii_ready(kbd_interface.ascii_ready));

endmodule


module kbd
   (
    input logic                             clk, rst,
    inout wire                              ps2_clk, ps2_data,
    output logic [7:0]                      readch, scancode,
    output logic                            ascii_ready
    );

  ps2 keyb_mouse(
    .clk(clk),
    .rst(rst),
    .PS2_K_CLK_IO(ps2_clk),
    .PS2_K_DATA_IO(ps2_data),
    .PS2_M_CLK_IO(),
    .PS2_M_DATA_IO(),
    .ascii_code(readch[6:0]),
    .ascii_data_ready_out(ascii_ready),
    .rx_translated_scan_code_out(scancode),
    .rx_ascii_read(ascii_ready));

endmodule