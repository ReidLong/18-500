
`include "pipeline.svh"
`include "register_file.svh"
`include "control_flow.svh"
`include "riscv_memory.svh"

`default_nettype none

module DecodeStage(
    input FetchExport_t fetchExport_ID_START,
    output DecodeExport_t decodeExport_EX_START,

    ControlFlowInterface.DecodeControl controlFlow,
    RegisterFileInterface.DecodeStage registerFile,
    ControlStatusRegisterInterface.DecodeStage csrInterface,

    input logic clock, clear, halted);

    DecodeControlSignals_t controlSignals;

    writebackSelectValid: assert property (@(posedge clock) disable iff(clear) controlSignals.writeBackSelect == NO_WRITE_BACK |-> !controlSignals.rd.isValid);
    writebackValidSelect: assert property (@(posedge clock) disable iff(clear) controlSignals.rd.isValid |-> controlSignals.writeBackSelect != NO_WRITE_BACK);

    validPCRelationships: assert property (@(posedge clock) disable iff(clear) fetchExport_ID_START.isValid |-> fetchExport_ID_START.fetchState.prediction.currentPC == fetchExport_ID_START.fetchState.pc);
    validNextPCRelationship: assert property (@(posedge clock) disable iff(clear) fetchExport_ID_START.isValid |-> fetchExport_ID_START.fetchState.prediction.nextInstruction == fetchExport_ID_START.fetchState.pc + 32'd4);

    Word_t instruction;

    assign instruction = fetchExport_ID_START.isValid ? fetchExport_ID_START.instructionResponse.dataOut : 32'b0;

    RISCV_Decode decoder(.valid(~clear & fetchExport_ID_START.isValid), .instruction, .controlSignals);

    Word_t immediate;

    ImmediateDecoder immediateDecoder(.instruction(fetchExport_ID_START.instructionResponse.dataOut), .immediate, .immediateType(controlSignals.immediateType));

    assign registerFile.rs1_ID = controlSignals.rs1;
    assign registerFile.rs2_ID = controlSignals.rs2;

    ControlStatusRegisterName_t csrName;
    assign csrName = ControlStatusRegisterName_t'(fetchExport_ID_START.instructionResponse.dataOut[31:20]);
    assign csrInterface.name_ID = csrName;

    DecodeState_t newDecodeState;

    assign newDecodeState = '{
        controlSignals: controlSignals,
        immediate: immediate,
        rs1Value: registerFile.rs1Value_ID,
        rs2Value: registerFile.rs2Value_ID,
        isValid: fetchExport_ID_START.isValid,
        csrName: csrName,
        csrValue: csrInterface.csrValue_ID
    };

    DecodeExport_t decodeExport_ID_END;

    assign decodeExport_ID_END = '{
        decode: newDecodeState,
        fetch: fetchExport_ID_START
    };

    assign controlFlow.instructionType_ID = controlSignals.instructionType;

    Register #(.WIDTH($bits(decodeExport_ID_END))) decode_pipelineRegister(.clock, .clear(clear | controlFlow.decodeControl.flush), .enable(~controlFlow.decodeControl.stall & ~halted), .d(decodeExport_ID_END), .q(decodeExport_EX_START));

endmodule

module ImmediateDecoder(
    input Word_t instruction,
    output Word_t immediate,
    input ImmediateType_t immediateType
    );

    always_comb begin
        unique case(immediateType)
            NO_IMMEDIATE: immediate = 32'd0;
            I_TYPE: immediate = {{21{instruction[31]}}, instruction[30:20]};
            U_TYPE: immediate = {instruction[31:12], 12'd0};
            SB_TYPE: immediate = {{20{instruction[31]}}, instruction[7], instruction[30:25], instruction[11:8], 1'b0};
            UJ_TYPE: immediate = {{12{instruction[31]}}, instruction[19:12], instruction[20], instruction[30:21], 1'b0};
            S_TYPE: immediate = {{21{instruction[31]}}, instruction[30:25], instruction[11:7]};
            CSR_TYPE: immediate = {27'd0, instruction[19:15]};
        endcase
    end

endmodule