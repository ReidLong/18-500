`default_nettype none

`include "pipeline.svh"
`include "control_flow.svh"
`include "register_file.svh"
`include "riscv_memory.svh"

module ExecuteStage(
    input DecodeExport_t decodeExport_EX_START,
    output ExecuteExport_t executeExport_MEM_START,
    ControlFlowInterface.ExecuteControl controlFlow,
    RegisterFileInterface.ExecuteStage registerFile,
    ControlStatusRegisterInterface.ExecuteStage csrInterface,
    input logic clock, clear, halted
);

    Word_t alu_src1, alu_src2, alu_out;

    always_comb begin
        alu_src1 = 32'b0;
        unique case(decodeExport_EX_START.decode.controlSignals.aluControl.aluSelect1)
            ALU_SELECT_REGISTER: alu_src1 = decodeExport_EX_START.decode.rs1Value;
            ALU_SELECT_PC: alu_src1 = decodeExport_EX_START.fetch.fetchState.pc;
            ALU_SELECT_IMMEDIATE: alu_src1 = decodeExport_EX_START.decode.immediate;
            ALU_SELECT_ZERO: alu_src1 = 32'b0;
            ALU_SELECT_CSR: alu_src1 = decodeExport_EX_START.decode.csrValue;
        endcase
    end

    always_comb begin
        alu_src2 = 32'b0;
        unique case(decodeExport_EX_START.decode.controlSignals.aluControl.aluSelect2)
            ALU_SELECT_REGISTER: alu_src2 = decodeExport_EX_START.decode.rs2Value;
            ALU_SELECT_PC: alu_src2 = decodeExport_EX_START.fetch.fetchState.pc;
            ALU_SELECT_IMMEDIATE: alu_src2 = decodeExport_EX_START.decode.immediate;
            ALU_SELECT_ZERO: alu_src2 = 32'b0;
            ALU_SELECT_CSR: alu_src2 = decodeExport_EX_START.decode.csrValue;
        endcase
    end

    RISCV_ALU alu(.alu_src1, .alu_src2, .alu_op(decodeExport_EX_START.decode.controlSignals.aluControl.aluOp), .alu_out);

    Word_t jumpBase, jumpTarget;
    always_comb begin
        jumpBase = decodeExport_EX_START.fetch.fetchState.pc;
        unique case(decodeExport_EX_START.decode.controlSignals.jumpBaseSelect)
            JUMP_BASE_PC: jumpBase = decodeExport_EX_START.fetch.fetchState.pc;
            JUMP_BASE_RS1: jumpBase = decodeExport_EX_START.decode.rs1Value;
            JUMP_BASE_ZERO: jumpBase = 32'b0;
        endcase
    end

    assign jumpTarget = jumpBase + decodeExport_EX_START.decode.immediate;

    branchTypeALUOut: assert property (@(posedge clock) disable iff(clear) decodeExport_EX_START.decode.controlSignals.instructionType == BRANCH |-> alu_out[31:1] == 31'b0);

    ExecuteState_t executeState_EX1, executeState_EX2;
    Word_t executeResult;
    logic resultValid;

    always_comb begin
        executeResult = alu_out;
        resultValid = 1'b0;
        unique case(decodeExport_EX_START.decode.controlSignals.writeBackSelect)
            ALU_RESULT: begin
                executeResult = alu_out;
                resultValid = 1'b1;
            end
            IF_NEXT_INSTRUCTION: begin
                executeResult = decodeExport_EX_START.fetch.fetchState.prediction.nextInstruction;
                resultValid = 1'b1;
            end
            CSR_RESULT: begin
                executeResult = decodeExport_EX_START.decode.csrValue;
                resultValid = 1'b1;
            end
            MEM_RESULT: executeResult = `WORD_POISON;
            NO_WRITE_BACK: executeResult = ~`WORD_POISON;
        endcase
    end


    illegalInvalid: assert property (@(posedge clock) disable iff(clear) decodeExport_EX_START.fetch.isValid |-> (decodeExport_EX_START.decode.controlSignals.instructionType == INVALID_INSTRUCTION) == (decodeExport_EX_START.decode.controlSignals.isIllegal));

    assign registerFile.response_EX1 = '{
        register: decodeExport_EX_START.decode.controlSignals.rd,
        result: executeResult,
        resultValid: resultValid
    };

    validDecode: assert property (@(posedge clock) disable iff(clear) decodeExport_EX_START.fetch.isValid |-> decodeExport_EX_START.decode.isValid);

    assign executeState_EX1 = '{
        aluSrc1: alu_src1,
        aluSrc2: alu_src2,
        aluOut: alu_out,
        executeResult: executeResult,
        executeResultValid: resultValid,
        jumpTarget: jumpTarget,
        predictionSummary: '{default:0}, // controlFlow.predictionSummary_EX
        actualTarget: `WORD_POISON
    };

    ExecuteExport_t executeExport_EX1_END, executeExport_EX2_END, executeExport_EX2_START;

    assign executeExport_EX1_END = '{
        decode: decodeExport_EX_START.decode,
        fetch: decodeExport_EX_START.fetch,
        execute: executeState_EX1
    };

    assign controlFlow.instructionType_EX1 = decodeExport_EX_START.decode.controlSignals.instructionType;

    Register #(.WIDTH($bits(executeExport_EX1_END))) execute1_pipelineRegister(.clock, .clear(clear | controlFlow.execute1Control.flush), .enable(~controlFlow.execute1Control.stall & ~halted), .d(executeExport_EX1_END), .q(executeExport_EX2_START));

    assign registerFile.response_EX2 = '{
        register: executeExport_EX2_START.decode.controlSignals.rd,
        result: executeExport_EX2_START.execute.executeResult,
        resultValid: executeExport_EX2_START.execute.executeResultValid
    };

    Word_t actualTarget;
    always_comb begin
        actualTarget = `ADDRESS_POISON;
        unique case(executeExport_EX2_START.decode.controlSignals.instructionType)
            NORMAL: actualTarget = executeExport_EX2_START.fetch.fetchState.prediction.nextInstruction;
            JUMP: actualTarget = executeExport_EX2_START.execute.jumpTarget;
            BRANCH: actualTarget = executeExport_EX2_START.execute.aluOut[0] ? executeExport_EX2_START.execute.jumpTarget : executeExport_EX2_START.fetch.fetchState.prediction.nextInstruction;
            // TODO: ECALL is probably going to need to be something weird here
            ECALL: actualTarget = executeExport_EX2_START.fetch.fetchState.prediction.nextInstruction;
            INVALID_INSTRUCTION: actualTarget = `ADDRESS_POISON;
            SYSTEM: begin
                if(executeExport_EX2_START.decode.controlSignals.csrMode == CSR_RETURN) begin
                    actualTarget = csrInterface.mepc_EX2;
                end else begin
                    actualTarget = executeExport_EX2_START.fetch.fetchState.prediction.nextInstruction;
                end
            end
        endcase
    end

    assign controlFlow.prediction_EX = executeExport_EX2_START.fetch.fetchState.prediction;
    assign controlFlow.actualTarget_EX = actualTarget;
    assign controlFlow.isValid_EX = executeExport_EX2_START.fetch.isValid && (executeExport_EX2_START.decode.controlSignals.instructionType != INVALID_INSTRUCTION);
    assign controlFlow.instructionType_EX2 = executeExport_EX2_START.decode.controlSignals.instructionType;

    always_comb begin
        executeState_EX2 = executeExport_EX2_START.execute;
        executeState_EX2.predictionSummary = controlFlow.predictionSummary_EX;
        executeState_EX2.actualTarget = actualTarget;
    end

    always_comb begin
        executeExport_EX2_END = executeExport_EX2_START;
        executeExport_EX2_END.execute = executeState_EX2;
    end

    Register #(.WIDTH($bits(executeExport_MEM_START))) execute_pipelineRegister(.clock, .clear(clear | controlFlow.execute2Control.flush), .enable(~controlFlow.execute2Control.stall & ~halted), .d(executeExport_EX2_END), .q(executeExport_MEM_START));

endmodule

/**
 * The arithmetic-logic unit (ALU) for the RISC-V processor.
 *
 * The ALU handles executing the current instruction, producing the
 * appropriate output based on the ALU operation specified to it by the
 * decoder.
 *
 * Inputs:
 *  - alu_src1      The first operand to the ALU.
 *  - alu_src2      The second operand to the ALU.
 *  - alu_op        The ALU operation to perform.
 * Outputs:
 *  - alu_out       The result of the ALU operation on the two sources.
 **/
module RISCV_ALU
    (input  Word_t    alu_src1, alu_src2,
     input  ArithmeticOperation_t    alu_op,
     output Word_t alu_out);

    Word_t shiftAmount;
    assign shiftAmount = alu_src2 & 32'h1F;
    logic [63:0] multiplicationResult;
    logic sign;

    function automatic Word_t absoluteValue(Word_t word);
        return word < 0 ? -word : word;
    endfunction

    always_comb begin
        multiplicationResult = 64'b0;
        sign = 1'b0;
        unique case (alu_op)
            ALU_ADD: alu_out = alu_src1 + alu_src2;
            ALU_SHIFT_LEFT: alu_out = alu_src1 << shiftAmount;
            ALU_LESS_THAN: alu_out = $signed(alu_src1) < $signed(alu_src2);
            ALU_LESS_THAN_UNSIGNED: alu_out =$unsigned(alu_src1) < $unsigned(alu_src2);
            ALU_XOR: alu_out = alu_src1 ^ alu_src2;
            ALU_SHIFT_RIGHT_LOGICAL: alu_out = $unsigned(alu_src1) >> shiftAmount;
            ALU_OR: alu_out = alu_src1 | alu_src2;
            ALU_AND: alu_out = alu_src1 & alu_src2;
            ALU_CLEAR_MASK: alu_out = ~alu_src1 & alu_src2;
            ALU_SUBTRACT: alu_out = alu_src1 - alu_src2;
            ALU_SHIFT_RIGHT_ARITHMATIC: alu_out = $signed(alu_src1) >>> shiftAmount;
            `ifdef HARDWARE_MULT
            ALU_MUL_LOW: alu_out = alu_src1 * alu_src2;
            ALU_MUL_HIGH_SIGNED_SIGNED: begin
                multiplicationResult = $signed({{32{alu_src1[31]}}, alu_src1}) * $signed({{32{alu_src2[31]}}, alu_src2});
                alu_out = multiplicationResult[63:32];
            end
            ALU_MUL_HIGH_SIGNED_UNSIGNED: begin
                multiplicationResult = $signed({{32{alu_src1[31]}}, alu_src1}) * $unsigned({32'b0, alu_src2});
                alu_out = multiplicationResult[63:32];
            end
            ALU_MUL_HIGH_UNSIGNED_UNSIGNED: begin
                multiplicationResult = $unsigned({32'b0, alu_src1}) * $unsigned({32'b0, alu_src2});
                alu_out = multiplicationResult[63:32];
            end
            ALU_DIV_SIGNED: begin
                if(alu_src2 == 32'b0) begin
                    // Divide by zero
                    alu_out = 32'hFFFFFFFF; // -1
                end else if(alu_src1 == 32'h80000000 && alu_src2 == 32'hFFFFFFFF) begin
                    // INT_MIN / -1 = INT_MIN
                    alu_out = alu_src1;
                end else begin
                    alu_out = $signed(alu_src1) / $signed(alu_src2);
                end
            end
            ALU_DIV_UNSIGNED: begin
                if(alu_src2 == 32'b0) begin
                    // Divide by zero
                    alu_out = 32'hFFFFFFFF;
                end else begin
                    alu_out = $unsigned(alu_src1) / $unsigned(alu_src2);
                end
            end
            ALU_REM_SIGNED: begin
                if(alu_src2 == 32'b0) begin
                    // Divide by zero
                    alu_out = alu_src1;
                end else if(alu_src1 == 32'h80000000 && alu_src2 == 32'hFFFFFFFF) begin
                    // INT_MIN % -1 = INT_MIN
                    alu_out = 32'b0;
                end else begin
                    alu_out = $signed(alu_src1) % $signed(alu_src2);
                end
            end
            ALU_REM_UNSIGNED: begin
                if(alu_src2 == 32'b0) begin
                    // Divide by zero
                    alu_out = alu_src1;
                end else begin
                    alu_out = $unsigned(alu_src1) % $unsigned(alu_src2);
                end
            end
            `endif
            ALU_EQUAL: alu_out = (alu_src1 == alu_src2);
            ALU_NOT_EQUAL: alu_out = (alu_src1 != alu_src2);
            ALU_GREATER_THAN_OR_EQUAL: alu_out = ($signed(alu_src1) >= $signed(alu_src2));
            ALU_GREATER_THAN_OR_EQUAL_UNSIGNED: alu_out = ($unsigned(alu_src1) >= $unsigned(alu_src2));
            default: alu_out = 'dx;
        endcase
    end

endmodule: RISCV_ALU