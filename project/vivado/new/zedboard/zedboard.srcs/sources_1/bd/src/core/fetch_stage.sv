
`default_nettype none

`include "riscv_memory.svh"
`include "pipeline.svh"
`include "control_flow.svh"



module FetchStage(
    // Memory Interface
    output MemoryRequest_t instructionRequest,
    input MemoryResponse_t instructionResponse,
    input logic instructionReady,

    ControlFlowInterface.FetchControl controlFlow,

    output FetchExport_t fetchExport_ID_START,

    // Infrastructure
    input logic clock, clear, halted
);

    Word_t pc, nextPC;

    assign nextPC = controlFlow.overrideValid_IF ? controlFlow.overridePC_IF : controlFlow.currentPrediction_IF.predictedPC;
    assign controlFlow.pc_IF = pc;

    logic loadPC, loadSavedPC;

    assign loadPC = loadSavedPC | controlFlow.overrideValid_IF;
    assign loadSavedPC = instructionReady & ~controlFlow.fetchControl.stall;

    Register #(.WIDTH($bits(pc)), .CLEAR_VALUE(`TEXT_START)) pc_register(.clock, .enable(loadPC & ~halted), .clear, .d(nextPC), .q(pc));

    assign instructionRequest = '{
        readEnable: 1'b1,
        writeEnable: 4'h0,
        address: pc,
        addressType: PHYSICAL_ADDRESS,
        dataIn: `WORD_POISON
    };

    FetchPartialState_t nextFetchState, fetchState;

    assign nextFetchState = '{
        pc: pc,
        isValid: 1'b1,
        prediction: controlFlow.currentPrediction_IF
    };


    // For some reason this assertion breaks a the program sometimes. VCS sucks
    // overrideFlush: assert property (@(posedge clock) disable iff(clear) controlFlow.overrideValid_IF |-> (controlFlow.fetchControl.flush) or (controlFlow.fetchControl.stall [*1:100] ##1 controlFlow.fetchControl.flush));
    overrideFlush: assert property (@(posedge clock) disable iff(clear) controlFlow.overrideValid_IF |-> controlFlow.fetchControl.stall [*0:100] ##1 controlFlow.fetchControl.flush);

    Register #(.WIDTH($bits(fetchState))) fetchState_register(.clock, .enable(loadSavedPC), .clear(clear | controlFlow.fetchControl.flush), .d(nextFetchState), .q(fetchState));

    logic isValidFetch;

    assign isValidFetch = instructionResponse.isValid & fetchState.isValid & (instructionResponse.request.address == fetchState.pc);

    FetchExport_t newFetchExport, savedFetchExport, selectedFetchExport;

    assign newFetchExport = '{
        fetchState: fetchState,
        isValid: isValidFetch,
        instructionResponse: instructionResponse
    };

    samePC: assert property (@(posedge clock) disable iff(clear) isValidFetch |-> newFetchExport.fetchState.pc == newFetchExport.fetchState.prediction.currentPC);
    validNextPC: assert property (@(posedge clock) disable iff(clear) isValidFetch |-> newFetchExport.fetchState.prediction.nextInstruction == newFetchExport.fetchState.pc + 32'd4);
    noHitNextInstruction: assert property (@(posedge clock) disable iff(clear) isValidFetch && !newFetchExport.fetchState.prediction.predictionHit |-> newFetchExport.fetchState.prediction.predictedPC == newFetchExport.fetchState.pc + 32'd4);

    logic savedStall;

    Register #(.WIDTH($bits(savedFetchExport))) fetchExport_register(.clock, .enable((controlFlow.fetchControl.stall & ~savedStall) | isValidFetch), .clear(clear | controlFlow.fetchControl.flush), .d(newFetchExport), .q(savedFetchExport));

    Register #(.WIDTH($bits(savedStall))) stall_register(.clock, .enable(1'b1), .clear, .d(controlFlow.fetchControl.stall), .q(savedStall));

    assign selectedFetchExport = (savedStall && !isValidFetch) ? savedFetchExport : newFetchExport;

    Register #(.WIDTH($bits(newFetchExport))) fetch_pipelineRegister(.clock, .clear(clear | controlFlow.fetchControl.flush), .enable(~controlFlow.fetchControl.stall & ~halted), .d(selectedFetchExport), .q(fetchExport_ID_START));

endmodule

