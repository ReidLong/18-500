/**
 * riscv_core.sv
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This is the core part of the processor, and is responsible for executing the
 * instructions and updating the CPU state appropriately.
 *
 * This is where you can start to add code and make modifications to fully
 * implement the processor. You can add any additional files or change and
 * delete files as you need to implement the processor, provided that they are
 * under the src directory. You may not change any files outside the src
 * directory. The only requirement is that there is a riscv_core module with the
 * interface defined below, with the same port names as below.
 *
 * The Makefile will automatically find any files you add, provided they are
 * under the src directory and have either a *.v, *.vh, or *.sv extension. The
 * files may be nested in subdirectories under the src directory as well.
 * Additionally, the build system sets up the include paths so that you can
 * place header files (*.vh) in any subdirectory in the src directory, and
 * include them from anywhere else inside the src directory.
 *
 * The compiler and synthesis tools support both Verilog and System Verilog
 * constructs and syntax, so you can write either Verilog or System Verilog
 * code, or mix both as you please.
 **/

// RISC-V Includes
// `include "riscv_abi.svh"             // ABI registers and definitions
`include "riscv_isa.svh"             // RISC-V ISA definitions
`include "riscv_memory.svh"

// Local Includes
`include "pipeline.svh"
`include "control_flow.svh"
`include "register_file.svh"
`include "config.svh"

// Force the compiler to throw an error if any variables are undeclared
`default_nettype none

/**
 * The core of the RISC-V processor, everything except main memory.
 *
 * This is the RISC-V processor, which, each cycle, fetches the next
 * instruction, executes it, and then updates the register file, memory,
 * and register file appropriately.
 *
 * The memory that the processor interacts with is dual-ported with a
 * single-cycle synchronous write and combinational read. One port is used to
 * fetch instructions, while the other is for loading and storing data.
 *
 * Inputs:
 *  - clk               The global clock for the processor.
 *  - rst_l             The asynchronous, active low reset for the processor.
 *  - instr_mem_excpt   Indicates that an invalid instruction address was given
 *                      to memory.
 *  - data_mem_excpt    Indicates that an invalid address was given to the data
 *                      memory during a load and/or store operation.
 *  - instr             The instruction loaded loaded from the instr_addr
 *                      address in memory.
 *  - data_load         The data loaded from the data_addr address in memory.
 *
 * Outputs:
 *  - data_load_en      Indicates that data from the data_addr address in
 *                      memory should be loaded.
 *  - halted            Indicates that the processor has stopped because of a
 *                      syscall or exception. Used to indicate to the testbench
 *                      to end simulation. Must be held until next clock cycle.
 *  - data_store_mask   A one-hot signal indicating which bytes of data_store
 *                      should be written to the data_addr address in memory.
 *  - instr_addr        The address of the instruction to load from memory.
 *  - data_addr         The address of the data to load or store from memory.
 *  - data_store        The data to store to the data_addr address in memory.
 **/
module RISCV_Core(
    input  logic           clock, clear,
    // Memory operations
    output  MemoryRequest_t dataRequest, instructionRequest,
    input   MemoryResponse_t dataResponse, instructionResponse,
    input logic dataReady, instructionReady,

    // Interrupt Devices
    InterruptDevice.InterruptController timer, keyboard,

    // Control Signals
    output logic           halted
    );

    RegisterFileInterface rfInterface();
    ControlFlowInterface controlFlowInterface();
    ControlStatusRegisterInterface csrInterface();
    InterruptControllerInterface interruptControllerInterface();

    assign csrInterface.instructionFetched = instructionResponse.isValid;

    ForwardingRegisterFile registerFile(
        .clock, .clear, .halted,
        .registerFileInterface(rfInterface.RegisterFile),
        .controlFlow(controlFlowInterface.RegisterFile)
    );

    ControlFlow controlFlow(
        .clock, .clear, .halted,
        .controlFlowInterface(controlFlowInterface.ControlManager)
    );

    ControlStatusRegister controlStatusRegisterState(
        .clock, .clear, .halted,
        .csrInterface(csrInterface.ControlStatusRegister)
    );

    InterruptController interruptController(
        .controlFlow(controlFlowInterface.InterruptController),
        .csrInterface(csrInterface.InterruptController),
        .interruptControl(interruptControllerInterface.InterruptController),
        .timer, .keyboard,
        .clock, .clear, .halted
    );

    FetchExport_t fetchExport_ID_START;

    FetchStage fetch_stage(
        // Memory Interface
        .instructionRequest, .instructionResponse, .instructionReady,
        // Control flow
        .controlFlow(controlFlowInterface.FetchControl),
        // Pipeline
        .fetchExport_ID_START,
        // Infrastructure
        .clock, .clear, .halted
    );

    DecodeExport_t decodeExport_EX_START;

    DecodeStage decode_stage(
        // Pipeline
        .fetchExport_ID_START,
        .decodeExport_EX_START,
        // Control Flow
        .controlFlow(controlFlowInterface.DecodeControl),
        // Register File
        .registerFile(rfInterface.DecodeStage),
        .csrInterface(csrInterface.DecodeStage),
        .clock, .clear, .halted
    );

    ExecuteExport_t executeExport_MEM_START;

    ExecuteStage execute_stage(
        // Pipeline
        .decodeExport_EX_START,
        .executeExport_MEM_START,
        .controlFlow(controlFlowInterface.ExecuteControl),
        .registerFile(rfInterface.ExecuteStage),
        .csrInterface(csrInterface.ExecuteStage),
        .clock, .clear, .halted
    );

    MemoryExport_t memoryExport_WB_START;

    MemoryStage memory_stage(
        // Pipeline
        .executeExport_MEM_START,
        .memoryExport_WB_START,
        .controlFlow(controlFlowInterface.MemoryControl),
        .registerFile(rfInterface.MemoryStage),
        .dataRequest, .dataResponse, .dataReady,
        .clock, .clear, .halted
    );

    WriteBackStage writeBack_stage(
        .memoryExport_WB_START,
        .controlFlow(controlFlowInterface.WriteBackControl),
        .registerFile(rfInterface.WriteBackStage),
        .csrInterface(csrInterface.WriteBackStage),
        .interruptController(interruptControllerInterface.WriteBackStage),
        .clock, .clear, .halted
    );

`ifdef SIMULATION_18447
`ifdef TRACE

    always_ff @(posedge clock) begin
        if (~clear) begin
            $display({"\n", {80{"-"}}});
            $display("- Simulation Cycle %0d", $time);
            $display({{80{"-"}}, "\n"});
            // $display("Fetch->Decode: %p\n", fetchExport_ID_START);
            // $display("Decode->Execute: %p\n", decodeExport_EX_START);
            // $display("Execute->Memory: %p\n", executeExport_MEM_START);
            // $display("Memory->WriteBack: %p\n", memoryExport_WB_START);
            controlFlow.displayControlFlowInformation();
            registerFile.displayForwardingInformation();
        end
    end
`endif
`endif


endmodule: RISCV_Core


