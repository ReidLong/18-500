
`include "pipeline.svh"
`include "control_flow.svh"
`include "register_file.svh"
`include "riscv_memory.svh"

`default_nettype none

module WriteBackStage(
    input MemoryExport_t memoryExport_WB_START,
    ControlFlowInterface.WriteBackControl controlFlow,
    RegisterFileInterface.WriteBackStage registerFile,
    ControlStatusRegisterInterface.WriteBackStage csrInterface,
    InterruptControllerInterface.WriteBackStage interruptController,

    input logic clock, clear, halted
);

    noIllegalInstructions: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.fetch.isValid |-> !memoryExport_WB_START.decode.controlSignals.isIllegal);

    assign controlFlow.instructionType_WB = memoryExport_WB_START.decode.controlSignals.instructionType;

    Word_t writeBackResult;
    logic instructionValid;

    assign instructionValid = memoryExport_WB_START.fetch.isValid;

    assign registerFile.response_WB = '{
        register: memoryExport_WB_START.decode.controlSignals.rd,
        result: writeBackResult,
        resultValid: instructionValid && memoryExport_WB_START.decode.controlSignals.rd.isValid
    };

    aluSanity: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.decode.controlSignals.writeBackSelect == ALU_RESULT && instructionValid |-> memoryExport_WB_START.execute.executeResult == memoryExport_WB_START.execute.aluOut);
    validSanity: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.decode.controlSignals.writeBackSelect != NO_WRITE_BACK && instructionValid |-> memoryExport_WB_START.decode.controlSignals.rd.isValid);
    selectSanity: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.decode.controlSignals.rd.isValid && instructionValid |-> memoryExport_WB_START.decode.controlSignals.writeBackSelect != NO_WRITE_BACK);
    nextInstructionSanity: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.decode.controlSignals.writeBackSelect == IF_NEXT_INSTRUCTION && instructionValid |-> memoryExport_WB_START.execute.executeResult == memoryExport_WB_START.fetch.fetchState.prediction.nextInstruction);
    memorySanity: assert property (@(posedge clock) disable iff(clear) memoryExport_WB_START.decode.controlSignals.writeBackSelect == MEM_RESULT && instructionValid |-> memoryExport_WB_START.memory.isValid);

    always_comb begin
        unique case(memoryExport_WB_START.decode.controlSignals.writeBackSelect)
            ALU_RESULT: writeBackResult = memoryExport_WB_START.execute.executeResult;
            IF_NEXT_INSTRUCTION: writeBackResult = memoryExport_WB_START.execute.executeResult;
            MEM_RESULT: writeBackResult = memoryExport_WB_START.memory.memoryResult;
            NO_WRITE_BACK: writeBackResult = `WORD_POISON;
            CSR_RESULT: writeBackResult = memoryExport_WB_START.decode.csrValue;
        endcase
    end

    dataResponseMemory: assert property (@(posedge clock) disable iff(clear) instructionValid |-> memoryExport_WB_START.memory.dataResponse.isValid == (memoryExport_WB_START.decode.controlSignals.memoryMode != NO_MEMORY));
    sameType: assert property (@(posedge clock) disable iff(clear) instructionValid |-> memoryExport_WB_START.execute.predictionSummary.instructionType == memoryExport_WB_START.decode.controlSignals.instructionType && memoryExport_WB_START.decode.controlSignals.instructionType != INVALID_INSTRUCTION);

    always_comb begin
        csrInterface.name_WB = memoryExport_WB_START.decode.csrName;
        csrInterface.csrValue_WB = memoryExport_WB_START.execute.aluOut;
        csrInterface.writeEnable_WB =  memoryExport_WB_START.decode.controlSignals.csrMode == CSR_WRITE;

        csrInterface.memoryExport_WB = memoryExport_WB_START;
        csrInterface.returnMSTATUS_WB = memoryExport_WB_START.decode.controlSignals.csrMode == CSR_RETURN;

    end

    Word_t nextExpectedPC;
    Register #(.WIDTH($bits(nextExpectedPC))) nextPC_register(.q(nextExpectedPC), .d(memoryExport_WB_START.execute.actualTarget), .enable(memoryExport_WB_START.fetch.isValid), .clear, .clock);
    assign interruptController.expectedNextInstruction_WB = nextExpectedPC;

endmodule