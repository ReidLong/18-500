`ifndef __CONTROL_STATUS_REGISTER_SVH
`define __CONTROL_STATUS_REGISTER_SVH

`include "riscv_isa.svh"
`include "riscv_memory.svh"
`include "pipeline.svh"

typedef enum logic [`CSR_BITS-1:0] {
    CYCLE_COUNT = CYCLE,
    INSTRUCTIONS_RETIRED = INSTRET,
    INSTRUCTIONS_FETCHED                = HPMCOUNTER3,
    FORWARD_BRANCH_RETIRED              = HPMCOUNTER4,
    BACKWARD_BRANCH_RETIRED             = HPMCOUNTER5,
    FORWARD_BRANCH_PREDICTED_CORRECT    = HPMCOUNTER6,
    BACKWARD_BRANCH_PREDICTED_CORRECT   = HPMCOUNTER7,
    FORWARD_BRANCH_PREDICTED_INCORRECT  = HPMCOUNTER8,
    BACKWARD_BRANCH_PREDICTED_INCORRECT = HPMCOUNTER9,
    INSTRUCTION_CACHE_HIT               = HPMCOUNTER10,
    INSTRUCTION_CACHE_MISS              = HPMCOUNTER11,
    DATA_CACHE_HIT                      = HPMCOUNTER12,
    DATA_CACHE_MISS                     = HPMCOUNTER13,
    DATA_VRAM_HIT                       = HPMCOUNTER14,
    STORE_CONDITIONAL_SUCCESS           = HPMCOUNTER15,
    STORE_CONDITIONAL_REJECT            = HPMCOUNTER16,
    KEYBOARD_INTERRUPT_USER             = HPMCOUNTER17,
    KEYBOARD_INTERRUPT_MACHINE          = HPMCOUNTER18,
    EXECEPTION_USER                     = HPMCOUNTER19,
    EXCEPTION_MACHINE                   = HPMCOUNTER20,
    TIMER_INTERRUPT_USER                = HPMCOUNTER21,
    TIMER_INTERRUPT_MACHINE             = HPMCOUNTER22,
    FORWARD_JUMP_RETIRED                = HPMCOUNTER23,
    BACKWARD_JUMP_RETIRED               = HPMCOUNTER24,
    FORWARD_JUMP_PREDICTED_CORRECT      = HPMCOUNTER25,
    BACKWARD_JUMP_PREDICTED_CORRECT     = HPMCOUNTER26,
    FORWARD_JUMP_PREDICTED_INCORRECT    = HPMCOUNTER27,
    BACKWARD_JUMP_PREDICTED_INCORRECT   = HPMCOUNTER28
} PeformanceCounter_t;

`define MSTATUS_WRITE_MASK 32'h1888

typedef enum logic {INTERRUPTS_DISABLED = 1'b0, INTERRUPTS_ENABLED = 1'b1} InterruptsEnabled_t;

// System Verilog is crazy. Apparently this needs to be opposite what it looks like in C
typedef struct packed {
    // MSB
    logic [18:0] zero4;
    // 3 if previously running in machine mode, 0 if previously running in user
    // mode
    PriviledgeLevel_t machinePreviousPriviledge;
    logic [2:0] zero3;
    // MPIE: Holds MIE prior to interrupt
    InterruptsEnabled_t machinePriorInterruptsEnabled;
    logic [2:0] zero2;
    // MIE: 1 => Enabled
    InterruptsEnabled_t machineInterruptsEnabled;
    logic [2:0] zero1;
    // LCB
} MachineStatus_t;

// TODO: Verify bit ordering
typedef struct packed {
    logic [21:0] rootPageTablePageNumber;
    logic [8:0] asid;
    logic virtualMemoryEnabled;
} SupervisorAddressTranslation_t;

typedef logic[63:0] CounterValue_t;

typedef struct packed {
    CounterValue_t cycleCount;
    CounterValue_t instructionsRetired;
    CounterValue_t instructionsFetched;
    CounterValue_t forwardBranchRetired;
    CounterValue_t backwardBranchRetired;
    CounterValue_t forwardBranchPredictedCorrect;
    CounterValue_t backwardBranchPredictedCorrect;
    CounterValue_t forwardBranchPredictedIncorrect;
    CounterValue_t backwardBranchPredictedIncorrect;
    CounterValue_t instructionCacheHit;
    CounterValue_t instructionCacheMiss;
    CounterValue_t dataCacheHit;
    CounterValue_t dataCacheMiss;
    CounterValue_t dataVramHit;
    CounterValue_t storeConditionalSuccess;
    CounterValue_t storeConditionalReject;
    CounterValue_t keyboardInterruptUser;
    CounterValue_t keyboardInterruptMachine;
    CounterValue_t execeptionUser;
    CounterValue_t exceptionMachine;
    CounterValue_t timerInterruptUser;
    CounterValue_t timerInterruptMachine;
    CounterValue_t forwardJumpRetired;
    CounterValue_t backwardJumpRetired;
    CounterValue_t forwardJumpPredictedCorrect;
    CounterValue_t backwardJumpPredictedCorrect;
    CounterValue_t forwardJumpPredictedIncorrect;
    CounterValue_t backwardJumpPredictedIncorrect;
} PerformanceCounterSummary_t;


interface ControlStatusRegisterInterface();

    logic instructionFetched;

    ControlStatusRegisterName_t name_ID;
    Word_t csrValue_ID;

    Word_t mepc_EX2;

    ControlStatusRegisterName_t name_WB;
    Word_t csrValue_WB;
    logic writeEnable_WB;
    logic returnMSTATUS_WB;

    MemoryExport_t memoryExport_WB;

    Word_t cause, epc, tval, mtvec;
    MachineStatus_t mstatus;
    logic interruptValid;

    modport ControlStatusRegister(
        input instructionFetched,
        input name_ID,
        output csrValue_ID,
        input name_WB, csrValue_WB, writeEnable_WB,
        input memoryExport_WB,
        output mepc_EX2,
        input returnMSTATUS_WB,
        input interruptValid,
        input cause, epc, tval,
        output mtvec, mstatus
    );

    modport InterruptController(
        output interruptValid,
        output cause, epc, tval,
        input mtvec, mstatus
    );

    modport DecodeStage(
        output name_ID,
        input csrValue_ID
    );

    modport ExecuteStage(
        input mepc_EX2
    );

    modport WriteBackStage(
        output name_WB, csrValue_WB, writeEnable_WB,
        output memoryExport_WB,
        output returnMSTATUS_WB
    );

endinterface

`endif