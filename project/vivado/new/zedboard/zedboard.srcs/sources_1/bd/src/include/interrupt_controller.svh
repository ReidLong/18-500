`ifndef __INTERRUPT_CONTROLLER_SVH
`define __INTERRUPT_CONTROLLER_SVH

interface InterruptControllerInterface();

    Word_t expectedNextInstruction_WB;

    modport InterruptController(
        input expectedNextInstruction_WB
    );

    modport WriteBackStage(
        output expectedNextInstruction_WB
    );

endinterface

interface InterruptDevice();

    Word_t tval;
    logic interruptAccepted, interruptPending;

    modport InterruptController(
        input tval,
        input interruptPending,
        output interruptAccepted
    );

    modport Device(
        output tval,
        output interruptPending,
        input interruptAccepted
    );

endinterface

`endif