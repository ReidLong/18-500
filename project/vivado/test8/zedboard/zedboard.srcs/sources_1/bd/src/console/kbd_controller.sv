`default_nettype none

`include "riscv_memory.svh"
`include "riscv_console.svh"
`include "compiler.svh"


module KBD_Controller(
    KBD_Interface.Controller kbd_interface,
    InterruptDevice.Device interruptController,
    input logic clock, clear
);

    logic [7:0] keyboardData, interruptData;
    logic isFull, isEmpty;
    FIFO #(.SIZE(`KEYBOARD_QUEUE_SIZE), .WIDTH(8)) keyboardQueue(
        .producerData(keyboardData), .enqueueValid(kbd_interface.ascii_ready & ~isFull),
        .consumerData(interruptData), .dequeueValid(interruptController.interruptAccepted),
        .isEmpty, .isFull, .clock, .clear);

    assign keyboardData = kbd_interface.readch; // TODO: This should probably be scancode

    assign kbd_interface.pollClock = clock;

    // If there is something in the queue, we have an interrupt ready to go
    assign interruptController.interruptPending = !isEmpty;
    assign interruptController.tval = {24'b0, interruptData};

endmodule


