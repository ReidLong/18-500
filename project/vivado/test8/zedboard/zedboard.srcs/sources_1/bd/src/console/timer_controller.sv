`default_nettype none

module TimerController(
    InterruptDevice.Device interruptController,
    input logic clock, clear
);

    // TODO: This should probably be a CSR for the next tick configuration
    logic[63:0] cycleCount, nextTimerInterrupt;

    Counter #(.WIDTH($bits(cycleCount))) timer_counter(.clock, .clear, .enable(1'b1), .q(cycleCount));
    Register #(.WIDTH($bits(nextTimerInterrupt)), .CLEAR_VALUE(`TIMER_TICK_CYCLES)) nextTick_register(.q(nextTimerInterrupt), .d(nextTimerInterrupt + `TIMER_TICK_CYCLES), .clock, .enable(interruptController.interruptAccepted), .clear);

    Word_t tickCount;
    Counter #(.WIDTH($bits(tickCount))) tick_counter(.clock, .clear, .enable(interruptController.interruptAccepted), .q(tickCount));

    assign interruptController.interruptPending = cycleCount >= nextTimerInterrupt;
    assign interruptController.tval = tickCount;


endmodule