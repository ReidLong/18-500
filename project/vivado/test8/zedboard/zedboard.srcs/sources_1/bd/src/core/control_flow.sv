`default_nettype none

`include "control_flow.svh"
`include "riscv_memory.svh"
`include "riscv_uarch.svh"

module ControlFlow(
    ControlFlowInterface.ControlManager controlFlowInterface,
    input logic clock, clear,
    output logic halted
);

    logic misprediction;
    logic halt_WB, halt_MEM1, halt_MEM2, halt_EX1, halt_EX2, halt_ID;
    assign halt_WB = (controlFlowInterface.instructionType_WB == ECALL);
    assign halt_MEM2 = halt_WB || (controlFlowInterface.instructionType_MEM2 == ECALL);
    assign halt_MEM1 = halt_MEM2 || (controlFlowInterface.instructionType_MEM1 == ECALL);
    assign halt_EX2 = halt_MEM1 || (controlFlowInterface.instructionType_EX2 == ECALL);
    assign halt_EX1 = halt_EX2 || (controlFlowInterface.instructionType_EX1 == ECALL);
    assign halt_ID = halt_EX1 || (controlFlowInterface.instructionType_ID == ECALL);

    logic system_WB, system_MEM1, system_MEM2, system_EX1, system_EX2;
    assign system_WB = (controlFlowInterface.instructionType_WB == SYSTEM);
    assign system_MEM2 = system_WB || (controlFlowInterface.instructionType_MEM2 == SYSTEM);
    assign system_MEM1 = system_MEM2 || (controlFlowInterface.instructionType_MEM1 == SYSTEM);
    assign system_EX2 = system_MEM1 || (controlFlowInterface.instructionType_EX2 == SYSTEM);
    assign system_EX1 = system_EX2 || (controlFlowInterface.instructionType_EX1 == SYSTEM);

    // Since CSR instructions are not the common case, we will stall whenever there is a system instruction in ID while another one is still in flight downstream. This prevents us from needing to implement a complex forwarding mechanism for CSRs.
    logic systemStall;
    assign systemStall = (controlFlowInterface.instructionType_ID == SYSTEM) && system_EX1;

    PipelineControl_t fetchControl, decodeControl, execute1Control, execute2Control, control_MEM1, control_MEM2;

    // We can accept the interrupt whenever the entire pipeline is idle and we have an interrupt to accept
    assign controlFlowInterface.interruptAccepted =
        (controlFlowInterface.instructionType_WB == INVALID_INSTRUCTION) &&
        (controlFlowInterface.instructionType_MEM2 == INVALID_INSTRUCTION) &&
        (controlFlowInterface.instructionType_MEM1 == INVALID_INSTRUCTION) &&
        (controlFlowInterface.instructionType_EX2 == INVALID_INSTRUCTION) &&
        (controlFlowInterface.instructionType_EX1 == INVALID_INSTRUCTION) &&
        (controlFlowInterface.instructionType_ID == INVALID_INSTRUCTION) && controlFlowInterface.interruptPending;

    // Control Flow Operations need to be done in the order of the instruction age
    // 1. Memory Stall (MEM)
    // 2. Misprediction (EX)
    // 3. Register File Stall or System Stall (ID)
    // 4. System HALT

    always_comb begin
        fetchControl.flush = 1'b0;
        fetchControl.stall = 1'b0;
        if(controlFlowInterface.interruptPending) begin
            // We want to start flushing the pipeline from IF whenever we see an interrupt pending
            fetchControl.flush = 1'b1;
        end else if(controlFlowInterface.memoryStall_MEM1 || controlFlowInterface.memoryStall_MEM2) begin
            fetchControl.stall = 1'b1;
        end else if(misprediction) begin
            // If there is a misprediction, everything is invalid
            fetchControl.flush = 1'b1;
        end else if(controlFlowInterface.registerFileStall_ID || systemStall) begin
            fetchControl.stall = 1'b1;
        end else if(halt_ID) begin
            // If we are draining the pipeline, continue flushing
            fetchControl.flush = 1'b1;
        end
    end

    always_comb begin
        decodeControl.flush = 1'b0;
        decodeControl.stall = 1'b0;
        if(controlFlowInterface.memoryStall_MEM1 || controlFlowInterface.memoryStall_MEM2) begin
            // The memory controller wants us to slow down, stall
            decodeControl.stall = 1'b1;
        end else if(misprediction) begin
            // If there is a misprediction, everything is invalid
            decodeControl.flush = 1'b1;
        end else if(controlFlowInterface.registerFileStall_ID || systemStall) begin
            // This ordering after the memory stall is important since if memory wants a stall, we want to defer the flushing associated with our generated stall
            decodeControl.flush = 1'b1;
        end else if(halt_EX1) begin
            // If we are draining the pipeline, continue flushing
            decodeControl.flush = 1'b1;
        end
    end

    always_comb begin
        execute1Control.flush = 1'b0;
        execute1Control.stall = 1'b0;
        if (controlFlowInterface.memoryStall_MEM1 || controlFlowInterface.memoryStall_MEM2) begin
            execute1Control.stall = 1'b1;
        end else if(misprediction) begin
            execute1Control.flush = 1'b1;
        end else if(halt_EX2) begin
            execute1Control.flush = 1'b1;
        end
    end

    always_comb begin
        execute2Control.flush = 1'b0;
        execute2Control.stall = 1'b0;
        if(controlFlowInterface.memoryStall_MEM1 || controlFlowInterface.memoryStall_MEM2) begin
            // If the memory controller wants us to slow down, stall
            execute2Control.stall = 1'b1;
        end else if(halt_MEM1) begin
            // If we are draining the pipeline, continue flushing
            execute2Control.flush = 1'b1;
        end
    end

    always_comb begin
        control_MEM1.flush = 1'b0;
        control_MEM1.stall = 1'b0;
        if(controlFlowInterface.memoryStall_MEM2) begin
            control_MEM1.stall = 1'b1;
        end else if (controlFlowInterface.memoryStall_MEM1) begin
            control_MEM1.flush = 1'b1;
        end else if(halt_MEM2) begin
            control_MEM1.flush = 1'b1;
        end
    end

    always_comb begin
        control_MEM2.flush = 1'b0;
        control_MEM2.stall = 1'b0;
        if(controlFlowInterface.memoryStall_MEM2) begin
            control_MEM2.flush = 1'b1;
        end else if(halt_WB) begin
            // If we are draining the pipeline continue flushing
            control_MEM2.flush = 1'b1;
        end
    end

    assign controlFlowInterface.fetchControl = fetchControl;
    assign controlFlowInterface.decodeControl = decodeControl;
    assign controlFlowInterface.execute1Control = execute1Control;
    assign controlFlowInterface.execute2Control = execute2Control;
    assign controlFlowInterface.control_MEM1 = control_MEM1;
    assign controlFlowInterface.control_MEM2 = control_MEM2;

    assign controlFlowInterface.predictionSummary_EX = '{
        direction: controlFlowInterface.actualTarget_EX >= controlFlowInterface.prediction_EX.currentPC ? FORWARD : BACKWARD,
        predictionCorrect: ~misprediction,
        instructionType: controlFlowInterface.instructionType_EX2
    };

    // Very bad things would happen if we asserted both flush and stall since the stall would be overridden. This is actually the opposite of the desired behavior.
    fetchSafety: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.fetchControl.stall |-> !controlFlowInterface.fetchControl.flush);
    decodeSafety: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.decodeControl.stall |-> !controlFlowInterface.decodeControl.flush);
    execute1Safety: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.execute1Control.stall |-> !controlFlowInterface.execute1Control.flush);
    execute2Safety: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.execute2Control.stall |-> !controlFlowInterface.execute2Control.flush);
    safety_MEM1: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.control_MEM1.stall |-> !controlFlowInterface.control_MEM1.flush);
    safety_MEM2: assert property (@(posedge clock) disable iff(clear) controlFlowInterface.control_MEM2.stall |-> !controlFlowInterface.control_MEM2.flush);

    // The SYSTEM case is for MRET which we don't put through branch prediction (but we could)
    mispredictControlFlow: assert property (@(posedge clock) disable iff(clear) misprediction |-> isControlFlow(controlFlowInterface.instructionType_EX2) || controlFlowInterface.instructionType_EX2 == SYSTEM);

    assign misprediction = (controlFlowInterface.actualTarget_EX != controlFlowInterface.prediction_EX.predictedPC) && controlFlowInterface.isValid_EX;

    // TODO: I think it should be okay to assert the override signal when the pipeline is stalling and doing a misprediction at the same time. What should happen is that the PC register is updated, but the pipeline remains stalled. I hope the only thing that will actually happen is we will start fetching the correct instruction instead of another bad instruction.

    noMispredictInterrupt: assert property (@(posedge clock) disable iff(clear) misprediction |-> !controlFlowInterface.interruptAccepted);

    always_comb begin
        controlFlowInterface.overridePC_IF = controlFlowInterface.actualTarget_EX;
        controlFlowInterface.overrideValid_IF = misprediction;
        if(controlFlowInterface.interruptAccepted) begin
            controlFlowInterface.overridePC_IF = controlFlowInterface.interruptPC;
            controlFlowInterface.overrideValid_IF = 1'b1;
        end
    end


    Register #(.WIDTH($bits(halted))) halt_register(.q(halted), .d(1'b1), .clock,
        .enable(halted | halt_WB), .clear);

    function automatic logic isControlFlow(InstructionType_t instructionType);
        return instructionType == JUMP || instructionType == BRANCH;
    endfunction

    BranchPredictor branchPredictor(
        .pc_IF(controlFlowInterface.pc_IF),
        .prediction_IF(controlFlowInterface.currentPrediction_IF),
        .prediction_EX(controlFlowInterface.prediction_EX),
        .targetPC_EX(controlFlowInterface.actualTarget_EX),
        .isValid_EX(controlFlowInterface.isValid_EX),
        .instructionType_EX(controlFlowInterface.instructionType_EX2),
        .clock, .clear
    );

`ifdef SIMULATION_18447
    function automatic void displayControlFlowInformation();
        if(misprediction) $display("Misprediction! Overriding: PC: %h Valid: %b (Prediction: %p Actual: %h)\n", controlFlowInterface.overridePC_IF, controlFlowInterface.overrideValid_IF, controlFlowInterface.prediction_EX, controlFlowInterface.actualTarget_EX);
        if(controlFlowInterface.registerFileStall_ID) $display("!!!!!!!!!!!Register File Requests Stall\n");
        if(controlFlowInterface.memoryStall_MEM1) $display("!!!!!!!!! Memory1 requests stall\n");
        if(controlFlowInterface.memoryStall_MEM2) $display("!!!!!!!!! Memory2 requests stall\n");
        $display("Fetch Control: %p", controlFlowInterface.fetchControl);
        $display("Decode Control: %p", controlFlowInterface.decodeControl);
        $display("Execute1 Control: %p", controlFlowInterface.execute1Control);
        $display("Execute2 Control: %p", controlFlowInterface.execute2Control);
        $display("Memory1 Control: %p", controlFlowInterface.control_MEM1);
        $display("Memory2 Control: %p", controlFlowInterface.control_MEM2);
        if(halted | halt_WB) $display("!!!!!!!!!!!!!Halting: %b | %b", halted, halt_WB);

    endfunction
`endif

endmodule

module BranchPredictor(
    input Word_t pc_IF,
    output Prediction_t prediction_IF,

    input Prediction_t prediction_EX,
    input Word_t targetPC_EX,
    input logic isValid_EX,
    input InstructionType_t instructionType_EX,

    input logic clock, clear

);
    parameter ADDRESS_WIDTH = $clog2(RISCV_UArch::BTB_NUM_WORDS);
    logic [ADDRESS_WIDTH-1:0] readAddress, writeAddress;

    // The two lowest bits are 0
    assign readAddress = pc_IF[ADDRESS_WIDTH+1:2];
    assign writeAddress = prediction_EX.currentPC[ADDRESS_WIDTH+1:2];

    pcWordAligned: assert property (@(posedge clock) disable iff(clear) pc_IF[1:0] == 2'b0);
    pcEXWordAligned: assert property (@(posedge clock) disable iff(clear) isValid_EX |-> prediction_EX.currentPC[1:0] == 2'b0);

    function automatic logic predictedTaken(CounterState_t state);
        return state == STRONGLY_TAKEN || state == WEAKLY_TAKEN;
    endfunction

    BranchPredictionEntry_t predictionEntry_IF, predictionEntry_EX;
    logic writePrediction_EX;

    struct packed {
        logic writeEnable;
        logic [ADDRESS_WIDTH-1:0] writeAddress;
        BranchPredictionEntry_t writeData;
    } nextWrite, currentWrite;

    assign nextWrite = '{
        writeEnable: writePrediction_EX,
        writeAddress: writeAddress,
        writeData: predictionEntry_EX
    };

    Register #(.WIDTH($bits(nextWrite))) btb_preRegister(.clock, .clear, .enable(1'b1), .d(nextWrite), .q(currentWrite));

    xilinx_sram #(.NUM_WORDS(RISCV_UArch::BTB_NUM_WORDS), .WORD_WIDTH($bits(predictionEntry_IF))) btb(
        .clock, .clear,
        .writeEnable(currentWrite.writeEnable), .writeAddress(currentWrite.writeAddress), .writeData(currentWrite.writeData),
        .readAddress, .readData(predictionEntry_IF));

    // Fetch Stage prediction generator

    logic predictionHit_IF;
    Word_t predictedPC_IF;
    Word_t nextInstruction_IF;

    assign prediction_IF = '{
        currentPC: pc_IF,
        nextInstruction: nextInstruction_IF,
        predictedPC: predictedPC_IF,
        predictionHit: predictionHit_IF,
        predictionEntry: predictionEntry_IF
    };

    assign predictionHit_IF = (predictionEntry_IF.tagPC == pc_IF);
    assign predictedPC_IF = predictionHit_IF ? predictionEntry_IF.nextPC : nextInstruction_IF;
    assign nextInstruction_IF = pc_IF + 32'd4;

    predictionNotTakenConsistent: assert property (@(posedge clock) disable iff(clear) predictionHit_IF && (predictedPC_IF == nextInstruction_IF) |-> !predictedTaken(predictionEntry_IF.counterState));

    predictionTakenConsistent: assert property (@(posedge clock) disable iff(clear) predictionHit_IF && (predictedPC_IF != nextInstruction_IF) |-> predictedTaken(predictionEntry_IF.counterState));

    // Execute stage prediction correction

    CounterState_t updatedCounter_EX;
    Word_t nextPredictionPC_EX;

    assign predictionEntry_EX = '{
        tagPC: prediction_EX.currentPC,
        counterState: updatedCounter_EX,
        nextPC: nextPredictionPC_EX
    };

    logic branchTaken_EX;

    // Not strictly true, but good enough since we don't need to distinguish between a branch that is not taken and a branch with a target of the next instruction
    assign branchTaken_EX = prediction_EX.nextInstruction != targetPC_EX;

    function automatic CounterState_t transitionTaken(CounterState_t state);
        unique case(state)
            STRONGLY_NOT_TAKEN: return WEAKLY_NOT_TAKEN;
            WEAKLY_NOT_TAKEN: return WEAKLY_TAKEN;
            WEAKLY_TAKEN: return STRONGLY_TAKEN;
            STRONGLY_TAKEN: return STRONGLY_TAKEN;
        endcase
    endfunction

    function automatic CounterState_t transitionNotTaken(CounterState_t state);
        unique case(state)
            STRONGLY_NOT_TAKEN: return STRONGLY_NOT_TAKEN;
            WEAKLY_NOT_TAKEN: return STRONGLY_NOT_TAKEN;
            WEAKLY_TAKEN: return WEAKLY_NOT_TAKEN;
            STRONGLY_TAKEN: return WEAKLY_TAKEN;
        endcase
    endfunction

    writeTakenNonNext: assert property (@(posedge clock) disable iff(clear) writePrediction_EX && predictedTaken(predictionEntry_EX.counterState) |-> predictionEntry_EX.nextPC != prediction_EX.nextInstruction);

    always_comb begin
        updatedCounter_EX = WEAKLY_TAKEN;
        nextPredictionPC_EX = targetPC_EX;
        writePrediction_EX = 1'b0;
        if(isValid_EX) begin
            unique case(instructionType_EX)
                JUMP: begin
                    // Jumps are always taken, so we should make the prediction strongly taken to the latest target
                    updatedCounter_EX = STRONGLY_TAKEN;
                    writePrediction_EX = 1'b1;
                    nextPredictionPC_EX = targetPC_EX;
                end
                BRANCH: begin
                    if(branchTaken_EX) begin
                        // The branch was taken
                        if(prediction_EX.predictionHit) begin
                            updatedCounter_EX = transitionTaken(prediction_EX.predictionEntry.counterState);
                        end else begin
                            updatedCounter_EX = WEAKLY_TAKEN;
                        end

                        writePrediction_EX = 1'b1;
                        // We only want to change the prediction state if the prediction will generate future taken predictions
                        if(predictedTaken(updatedCounter_EX)) begin
                            nextPredictionPC_EX = targetPC_EX;
                        end else begin
                            nextPredictionPC_EX = prediction_EX.nextInstruction;
                        end
                    end else begin
                        // The branch was not taken
                        if(prediction_EX.predictionHit) begin
                            updatedCounter_EX = transitionNotTaken(prediction_EX.predictionEntry.counterState);
                        end else begin
                            updatedCounter_EX = STRONGLY_NOT_TAKEN;
                        end

                        writePrediction_EX = 1'b1;
                        // We only want to change the prediction state if the prediction will generate future not taken predictions
                        if(predictedTaken(updatedCounter_EX)) begin
                            nextPredictionPC_EX = prediction_EX.predictedPC;
                        end else begin
                            nextPredictionPC_EX = prediction_EX.nextInstruction;
                        end
                    end
                end
                // TODO: We could handle SYSTEM instructions here for MRET
                default: ;// Nothing to do
            endcase
        end
    end

endmodule