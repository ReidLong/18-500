
`default_nettype none

`include "pipeline.svh"
`include "control_flow.svh"
`include "register_file.svh"
`include "riscv_memory.svh"

typedef struct packed {
    logic isValid;
    Address_t address;
} Reservation_t;

module MemoryStage(
    input ExecuteExport_t executeExport_MEM_START,
    output MemoryExport_t memoryExport_WB_START,

    ControlFlowInterface.MemoryControl controlFlow,
    RegisterFileInterface.MemoryStage registerFile,

    output MemoryRequest_t dataRequest,
    input MemoryResponse_t dataResponse,
    input logic dataReady,

    input logic clock, clear, halted
);

    // MEM1 STAGE
    assign registerFile.response_MEM1 = '{
        register: executeExport_MEM_START.decode.controlSignals.rd,
        result: executeExport_MEM_START.execute.executeResult,
        resultValid: executeExport_MEM_START.execute.executeResultValid
    };

    logic memoryStall_MEM1;
    logic memoryStall_MEM2;

    assign controlFlow.memoryStall_MEM1 = memoryStall_MEM1;
    assign controlFlow.instructionType_MEM1 = executeExport_MEM_START.decode.controlSignals.instructionType;

    // Generate a stall upstream and flush MEM2 if we have a memory operation in MEM_START, but the memory controller is not ready
    assign memoryStall_MEM1 = !dataReady && executeExport_MEM_START.fetch.isValid && (executeExport_MEM_START.decode.controlSignals.memoryMode != NO_MEMORY);

    logic readEnable_MEM1;
    logic [3:0] writeEnable_MEM1;
    Address_t alignedAddress_MEM1;
    Word_t alignedDataIn_MEM1;

    assign dataRequest = '{
        readEnable: readEnable_MEM1,
        writeEnable: writeEnable_MEM1,
        address: alignedAddress_MEM1,
        dataIn: alignedDataIn_MEM1,
        addressType: PHYSICAL_ADDRESS
    };

    logic [4:0] shiftAmount_MEM1;
    logic [1:0] offset_MEM1;

    assign offset_MEM1 = executeExport_MEM_START.execute.aluOut[1:0];
    assign shiftAmount_MEM1 = offset_MEM1  * 5'd8;

    assign alignedAddress_MEM1 = executeExport_MEM_START.execute.aluOut & ~32'b11;
    assign alignedDataIn_MEM1 = executeExport_MEM_START.decode.rs2Value << shiftAmount_MEM1;


    Reservation_t currentReservation, nextReservation;
    logic clearReservation_MEM1, loadReservation_MEM1;
    logic reservationSuccess_MEM1, reservationMatch_MEM1;

    assign reservationMatch_MEM1 = (executeExport_MEM_START.execute.aluOut == currentReservation.address) && currentReservation.isValid;

    assign nextReservation = '{
        isValid: loadReservation_MEM1,
        address: executeExport_MEM_START.execute.aluOut
    };

    logic gatedClearReservation;
    // We only want to clear the reservation, if the instruction is being passed along to MEM2
    assign gatedClearReservation = clear | (clearReservation_MEM1 & (~controlFlow.control_MEM1.stall) & (~controlFlow.control_MEM1.flush));
    Register #(.WIDTH($bits(currentReservation))) reservation_register(.clock,
        .clear(gatedClearReservation),
        .enable(loadReservation_MEM1),
        .d(nextReservation), .q(currentReservation)
    );

    memoryModeValid: assert property (@(posedge clock) disable iff(clear) executeExport_MEM_START.decode.controlSignals.memoryMode != NO_MEMORY |-> executeExport_MEM_START.fetch.isValid);

    always_comb begin
        clearReservation_MEM1 = 1'b0;
        loadReservation_MEM1 = 1'b0;
        readEnable_MEM1 = 1'b0;
        writeEnable_MEM1 = 4'b0;
        reservationSuccess_MEM1 = 1'b0;
        unique case(executeExport_MEM_START.decode.controlSignals.memoryMode)
            LOAD_BYTE: begin
                readEnable_MEM1 = 1'b1;
                clearReservation_MEM1 = 1'b1;
            end
            LOAD_HALF_WORD: begin
                readEnable_MEM1 = 1'b1;
                clearReservation_MEM1 = 1'b1;
            end
            LOAD_WORD: begin
                readEnable_MEM1 = 1'b1;
                clearReservation_MEM1 = 1'b1;
            end
            LOAD_BYTE_UNSIGNED: begin
                readEnable_MEM1 = 1'b1;
                clearReservation_MEM1 = 1'b1;
            end
            LOAD_HALF_WORD_UNSIGNED: begin
                readEnable_MEM1 = 1'b1;
                clearReservation_MEM1 = 1'b1;
            end
            STORE_BYTE: begin
                writeEnable_MEM1 = 4'b0001 << offset_MEM1;
                clearReservation_MEM1 = 1'b1;
            end
            STORE_HALF_WORD: begin
                writeEnable_MEM1 = 4'b0011 << offset_MEM1;
                clearReservation_MEM1 = 1'b1;
            end
            STORE_WORD: begin
                writeEnable_MEM1 = 4'b1111;
                clearReservation_MEM1 = 1'b1;
            end
            STORE_CONDITIONAL: begin
                if(reservationMatch_MEM1) begin
                    writeEnable_MEM1 = 4'b1111;
                    reservationSuccess_MEM1 = 1'b1;
                end
                clearReservation_MEM1 = 1'b1;
            end
            LOAD_RESERVED: begin
                readEnable_MEM1 = 1'b1;
                loadReservation_MEM1 = 1'b1;
            end
            NO_MEMORY: begin
                // Nothing to do
            end
        endcase
    end

    MemoryExport_t memoryExport_MEM1_END, memoryExport_MEM2_START;
    MemoryState_t memoryState_MEM1_END;

    assign memoryState_MEM1_END = '{
        storeConditionalSuccess: reservationSuccess_MEM1,
        dataRequest: dataRequest,
        offset: offset_MEM1,
        shiftAmount: shiftAmount_MEM1,
        dataResponse: dataResponse, // This is invalid junk data
        isValid: 1'b0,
        memoryResult: `WORD_POISON
    };

    assign memoryExport_MEM1_END = '{
        fetch: executeExport_MEM_START.fetch,
        decode: executeExport_MEM_START.decode,
        execute: executeExport_MEM_START.execute,
        memory: memoryState_MEM1_END
    };

    Register #(.WIDTH($bits(memoryExport_MEM2_START))) memory1_pipelineRegister(.clock,
        .clear(clear | controlFlow.control_MEM1.flush),
        .enable(~halted & ~controlFlow.control_MEM1.stall),
        .d(memoryExport_MEM1_END), .q(memoryExport_MEM2_START));

    // MEM2 STAGE

    Word_t memoryResult_MEM2;
    logic memoryResultValid_MEM2;
    Word_t alignedDataOut_MEM2;

    assign alignedDataOut_MEM2 = dataResponse.dataOut >> memoryExport_MEM2_START.memory.shiftAmount;

    correctAddress: assert property (@(posedge clock) disable iff(clear) memoryExport_MEM2_START.fetch.isValid && (memoryExport_MEM2_START.decode.controlSignals.memoryMode != NO_MEMORY) && dataResponse.isValid |-> dataResponse.request.address == (memoryExport_MEM2_START.execute.aluOut & ~32'b11));

    always_comb begin
        memoryResult_MEM2 = `WORD_POISON;
        memoryResultValid_MEM2 = dataResponse.isValid;
        unique case(memoryExport_MEM2_START.decode.controlSignals.memoryMode)
            LOAD_BYTE: memoryResult_MEM2 = {{24{alignedDataOut_MEM2[7]}}, alignedDataOut_MEM2[7:0]};
            LOAD_HALF_WORD: memoryResult_MEM2 = {{16{alignedDataOut_MEM2[15]}}, alignedDataOut_MEM2[15:0]};
            LOAD_WORD: memoryResult_MEM2 = dataResponse.dataOut;
            LOAD_BYTE_UNSIGNED: memoryResult_MEM2 = {24'h0, alignedDataOut_MEM2[7:0]};
            LOAD_HALF_WORD_UNSIGNED: memoryResult_MEM2 = {16'h0, alignedDataOut_MEM2[15:0]};
            STORE_BYTE: begin
                memoryResult_MEM2 = `WORD_POISON;
                memoryResultValid_MEM2 = 1'b0;
            end
            STORE_HALF_WORD: begin
                memoryResult_MEM2 = `WORD_POISON;
                memoryResultValid_MEM2 = 1'b0;
            end
            STORE_WORD: begin
                memoryResult_MEM2 = `WORD_POISON;
                memoryResultValid_MEM2 = 1'b0;
            end
            STORE_CONDITIONAL: begin
                memoryResult_MEM2 = memoryExport_MEM2_START.memory.storeConditionalSuccess ? 32'b0 : 32'b1;
            end
            LOAD_RESERVED: memoryResult_MEM2 = dataResponse.dataOut;
            NO_MEMORY: begin
                memoryResult_MEM2 = `WORD_POISON;
                memoryResultValid_MEM2 = 1'b0;
            end
        endcase
    end

    always_comb begin
        registerFile.response_MEM2 = '{
            register: memoryExport_MEM2_START.decode.controlSignals.rd,
            result: memoryExport_MEM2_START.execute.executeResult,
            resultValid: memoryExport_MEM2_START.execute.executeResultValid
        };

        // This may be on the critical path
        if(memoryExport_MEM2_START.decode.controlSignals.writeBackSelect == MEM_RESULT && memoryResultValid_MEM2) begin
            registerFile.response_MEM2.result = memoryResult_MEM2;
            registerFile.response_MEM2.resultValid = memoryResultValid_MEM2;
        end
    end

    // Generate a stall upstream and flush WB if we have a memory operation in MEM2_START, but the memory controller response is not valid
    assign memoryStall_MEM2 = !dataResponse.isValid && memoryExport_MEM2_START.fetch.isValid && (memoryExport_MEM2_START.decode.controlSignals.memoryMode != NO_MEMORY);

    assign controlFlow.memoryStall_MEM2 = memoryStall_MEM2;
    assign controlFlow.instructionType_MEM2 = memoryExport_MEM2_START.decode.controlSignals.instructionType;

    flushOnInvalid: assert property (@(posedge clock) disable iff(clear) memoryExport_MEM2_START.decode.controlSignals.memoryMode != NO_MEMORY && !dataResponse.isValid |-> controlFlow.control_MEM2.flush);
    memoryModeValid2: assert property (@(posedge clock) disable iff(clear) memoryExport_MEM2_START.decode.controlSignals.memoryMode != NO_MEMORY |-> memoryExport_MEM2_START.fetch.isValid);

    MemoryExport_t memoryExport_MEM2_END;
    MemoryState_t memoryState_MEM2;

    assign memoryExport_MEM2_END = '{
        fetch: memoryExport_MEM2_START.fetch,
        decode: memoryExport_MEM2_START.decode,
        execute: memoryExport_MEM2_START.execute,
        memory: memoryState_MEM2
    };

    always_comb begin
        memoryState_MEM2 = memoryExport_MEM2_START.memory;
        // Patch stuff up
        memoryState_MEM2.dataResponse = dataResponse;
        memoryState_MEM2.isValid = dataResponse.isValid && (memoryExport_MEM2_START.decode.controlSignals.memoryMode != NO_MEMORY);
        memoryState_MEM2.memoryResult = memoryResult_MEM2;
    end

    noMemory2Stall: assert property (@(posedge clock) disable iff(clear) !controlFlow.control_MEM2.stall);

    Register #(.WIDTH($bits(memoryExport_WB_START))) memory2_pipelineRegister(.clock,
        .clear(clear | controlFlow.control_MEM2.flush),
        .enable(~halted & ~controlFlow.control_MEM2.stall),
        .d(memoryExport_MEM2_END), .q(memoryExport_WB_START));

endmodule