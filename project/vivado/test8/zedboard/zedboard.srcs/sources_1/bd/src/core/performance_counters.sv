
`default_nettype none
`include "pipeline.svh"
`include "control_flow.svh"
`include "riscv_memory.svh"
`include "control_status_register.svh"

`define INSTALL_COUNTER(name, increment) \
    generate \
        CounterValue_t name;\
        Counter #(.WIDTH($bits(name))) name``_counter(.clock, .clear, .enable((increment) & ~halted), .q(name)); \
        always @(posedge clock) begin \
            if(halted) begin \
                $display("Counter: %-35s Value: %d", "name", name);\
            end\
        end\
    endgenerate

module PerformanceCounters(
        // Fetch State
        input logic instructionFetched,
        // Write Back State
        input logic instructionRetired,
        input PredictionSummary_t predictionSummary,
        input MemoryResponse_t instructionResponse,
        input MemoryState_t memoryState,
        input MemoryMode_t memoryMode,

        output PerformanceCounterSummary_t summary,

        // Infrastructure
        input logic clock, clear, halted
);

    `INSTALL_COUNTER(cycleCount, 1'b1);
    `INSTALL_COUNTER(instructionsFetched, instructionFetched);

    // All other counters should be updated in the write back stage when the instruction is retired

    `INSTALL_COUNTER(instructionsRetired, instructionRetired);

    logic isBranch;
    assign isBranch = (predictionSummary.instructionType == BRANCH) && instructionRetired;

    `INSTALL_COUNTER(forwardBranchRetired, isBranch && (predictionSummary.direction == FORWARD));
    `INSTALL_COUNTER(backwardBranchRetired, isBranch && (predictionSummary.direction == BACKWARD));
    `INSTALL_COUNTER(forwardBranchPredictedCorrectly, isBranch && (predictionSummary.direction == FORWARD) && predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(backwardBranchPredictedCorrectly, isBranch && (predictionSummary.direction == BACKWARD) && predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(forwardBranchPredictedIncorrectly, isBranch && (predictionSummary.direction == FORWARD) && !predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(backwardBranchPredictedIncorrectly, isBranch && (predictionSummary.direction == BACKWARD) && !predictionSummary.predictionCorrect);

    forwardBranchCheck: assert property (@(posedge clock) disable iff(clear) forwardBranchRetired == (forwardBranchPredictedCorrectly + forwardBranchPredictedIncorrectly));
    backwardBranchCheck: assert property (@(posedge clock) disable iff(clear) backwardBranchRetired == (backwardBranchPredictedCorrectly + backwardBranchPredictedIncorrectly));

    noInstructionVRAM: assert property (@(posedge clock) disable iff(clear) instructionRetired |-> instructionResponse.responseType != VRAM && instructionResponse.responseType != INVALID_RESPONSE);

    `INSTALL_COUNTER(instructionCacheHits, instructionRetired && (instructionResponse.responseType == CACHE_HIT));
    `INSTALL_COUNTER(instructionCacheMisses, instructionRetired && (instructionResponse.responseType == CACHE_MISS));
    logic isDataMemory;
    MemoryResponse_t dataResponse;
    assign dataResponse = memoryState.dataResponse;
    assign isDataMemory = instructionRetired && memoryState.isValid;

    noInvalidData: assert property (@(posedge clock) disable iff(clear) isDataMemory |-> dataResponse.responseType != INVALID_RESPONSE && dataResponse.isValid);


    `INSTALL_COUNTER(dataCacheHits, isDataMemory && (dataResponse.responseType == CACHE_HIT));
    `INSTALL_COUNTER(dataCacheMisses, isDataMemory && (dataResponse.responseType == CACHE_MISS));
    `INSTALL_COUNTER(dataVRAMHit, isDataMemory && (dataResponse.responseType == VRAM));

    logic isStoreConditional;
    assign isStoreConditional =  (memoryMode == STORE_CONDITIONAL) && instructionRetired;
    `INSTALL_COUNTER(storeConditionalSuccesses, memoryState.storeConditionalSuccess && isStoreConditional);
    `INSTALL_COUNTER(storeConditionalRejections, !memoryState.storeConditionalSuccess && isStoreConditional);

    `INSTALL_COUNTER(keyboardInterruptsU, 1'b0);
    `INSTALL_COUNTER(keyboardInterruptsM, 1'b0);
    `INSTALL_COUNTER(exceptionsU, 1'b0);
    `INSTALL_COUNTER(exceptionsM, 1'b0);
    `INSTALL_COUNTER(timerTicksU, 1'b0);
    `INSTALL_COUNTER(timerTicksM, 1'b0);

    logic isJump;
    assign isJump = (predictionSummary.instructionType == JUMP) && instructionRetired;

    `INSTALL_COUNTER(forwardJumpRetired, isJump && (predictionSummary.direction == FORWARD));
    `INSTALL_COUNTER(backwardJumpRetired, isJump && (predictionSummary.direction == BACKWARD));
    `INSTALL_COUNTER(forwardJumpPredictedCorrectly, isJump && (predictionSummary.direction == FORWARD) && predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(backwardJumpPredictedCorrectly, isJump && (predictionSummary.direction == BACKWARD) && predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(forwardJumpPredictedIncorrectly, isJump && (predictionSummary.direction == FORWARD) && !predictionSummary.predictionCorrect);
    `INSTALL_COUNTER(backwardJumpPredictedIncorrectly, isJump && (predictionSummary.direction == BACKWARD) && !predictionSummary.predictionCorrect);

    always @(posedge clock) begin
        if(halted) begin
            $display("%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d,%0d",
                cycleCount,
                instructionsFetched,
                instructionsRetired,
                forwardBranchPredictedCorrectly,
                forwardBranchPredictedIncorrectly,
                backwardBranchPredictedCorrectly,
                backwardBranchPredictedIncorrectly,
                instructionCacheHits,
                instructionCacheMisses,
                dataCacheHits,
                dataCacheMisses,
                dataVRAMHit,
                forwardJumpPredictedCorrectly,
                forwardJumpPredictedIncorrectly,
                backwardJumpPredictedCorrectly,
                backwardJumpPredictedIncorrectly
            );
        end
    end

    forwardJumpCheck: assert property (@(posedge clock) disable iff(clear) forwardJumpRetired == (forwardJumpPredictedCorrectly + forwardJumpPredictedIncorrectly));
    backwardJumpCheck: assert property (@(posedge clock) disable iff(clear) backwardJumpRetired == (backwardJumpPredictedCorrectly + backwardJumpPredictedIncorrectly));


    assign summary = '{
        cycleCount: cycleCount,
        instructionsRetired: instructionsRetired,
        instructionsFetched: instructionsFetched,
        forwardBranchRetired: forwardBranchRetired,
        backwardBranchRetired: backwardBranchRetired,
        forwardBranchPredictedCorrect: forwardBranchPredictedCorrectly,
        backwardBranchPredictedCorrect: backwardBranchPredictedCorrectly,
        forwardBranchPredictedIncorrect: forwardBranchPredictedIncorrectly,
        backwardBranchPredictedIncorrect: backwardBranchPredictedIncorrectly,
        instructionCacheHit: instructionCacheHits,
        instructionCacheMiss: instructionCacheMisses,
        dataCacheHit: dataCacheHits,
        dataCacheMiss: dataCacheMisses,
        dataVramHit: dataVRAMHit,
        storeConditionalSuccess: storeConditionalSuccesses,
        storeConditionalReject: storeConditionalRejections,
        keyboardInterruptUser: keyboardInterruptsU,
        keyboardInterruptMachine: keyboardInterruptsM,
        execeptionUser: exceptionsU,
        exceptionMachine: exceptionsM,
        timerInterruptUser: timerTicksU,
        timerInterruptMachine: timerTicksM,
        forwardJumpRetired: forwardJumpRetired,
        backwardJumpRetired: backwardJumpRetired,
        forwardJumpPredictedCorrect: forwardJumpPredictedCorrectly,
        backwardJumpPredictedCorrect: backwardJumpPredictedCorrectly,
        forwardJumpPredictedIncorrect: forwardJumpPredictedIncorrectly,
        backwardJumpPredictedIncorrect: backwardJumpPredictedIncorrectly
    };




endmodule