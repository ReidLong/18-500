/**
 * riscv_decode.sv
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * This file contains the implementation of the RISC-V decoder.
 *
 * This takes in information about the current RISC-V instruction and produces
 * the appropriate control signals to get the processor to execute the current
 * instruction.
 **/

/*----------------------------------------------------------------------------*
 *  You may edit this file and add or change any files in the src directory.  *
 *----------------------------------------------------------------------------*/

// RISC-V Includes
`include "riscv_isa.svh"             // RISC-V ISA definitions

// Local Includes
`include "control_flow.svh"
`include "pipeline.svh"
`include "log.svh"
`include "riscv_memory.svh"
`include "register_file.svh"

// Force the compiler to throw an error if any variables are undeclared
`default_nettype none



/**
 * The instruction decoder for the RISC-V processor.
 *
 * This module processes the current instruction, determines what instruction it
 * is, and sets the control signals for the processor appropriately.
 *
 * Inputs:
 *  - rst_l             The asynchronous, active low reset for the processor.
 *  - instr             The current instruction being executed by the processor.
 *
 * Outputs:
 *  - ctrl_signals      The control signals needed to execute the given
 *                      instruction correctly.
 **/
module RISCV_Decode
    (input  logic           valid,
     input  Word_t instruction,
     output DecodeControlSignals_t  controlSignals
     );

    // The various fields of an instruction
    opcode_t            opcode;
    funct7_t            funct7;
    rtype_funct3_t      rtype_funct3;
    rtype_m_funct3_t rtype_m_funct3;
    itype_int_funct3_t  itype_int_funct3;
    itype_load_funct3_t itype_load_funct3;
    itype_funct12_t     itype_funct12;
    stype_funct3_t stype_funct3;
    amo_funct5_t amo_funct5;
    sbtype_funct3_t sbtype_funct3;
    itype_system_funct3_t itype_system_funct3;

    // Decode the opcode and various function codes for the instruction
    assign opcode           = opcode_t'(instruction[6:0]);
    assign funct7           = funct7_t'(instruction[31:25]);
    assign rtype_funct3     = rtype_funct3_t'(instruction[14:12]);
    assign rtype_m_funct3 = rtype_m_funct3_t'(instruction[14:12]);
    assign itype_int_funct3 = itype_int_funct3_t'(instruction[14:12]);
    assign itype_load_funct3 = itype_load_funct3_t'(instruction[14:12]);
    assign itype_funct12    = itype_funct12_t'(instruction[31:20]);
    assign stype_funct3 = stype_funct3_t'(instruction[14:12]);
    assign amo_funct5 = amo_funct5_t'(instruction[31:27]);
    assign sbtype_funct3 = sbtype_funct3_t'(instruction[14:12]);
    assign itype_system_funct3 = itype_system_funct3_t'(instruction[14:12]);

    logic rdValid;
    assign rdValid = controlSignals.writeBackSelect != NO_WRITE_BACK;

    Register_t rs1, rs2, rd;

    always_comb begin
        rs1 = '{
            number: instruction[19:15],
            isValid: 1'b0
        };
        rs2 = '{
            number: instruction[24:20],
            isValid: 1'b0
        };
        rd = '{
            number: instruction[11:7],
            isValid: rdValid
        };
        controlSignals = '{
            rs1: rs1,
            rs2: rs2,
            rd: rd,
            immediateType: NO_IMMEDIATE,
            isIllegal: 1'b0,
            aluControl: '{
                aluOp: ALU_ADD,
                aluSelect1: ALU_SELECT_ZERO,
                aluSelect2: ALU_SELECT_ZERO
            },
            writeBackSelect: NO_WRITE_BACK,
            memoryMode: NO_MEMORY,
            instructionType: NORMAL,
            jumpBaseSelect: JUMP_BASE_ZERO,
            csrMode: NO_CSR_UPDATE
        };

        unique case (opcode)
            OP_OP: begin
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_REGISTER;
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.rs2.isValid = 1'b1;
                controlSignals.writeBackSelect = ALU_RESULT;
                unique case(funct7)
                    FUNCT7_INT: begin
                        unique case(rtype_funct3)
                            FUNCT3_ADD_SUB: controlSignals.aluControl.aluOp = ALU_ADD;
                            FUNCT3_SLL: controlSignals.aluControl.aluOp = ALU_SHIFT_LEFT;
                            FUNCT3_SLT: controlSignals.aluControl.aluOp = ALU_LESS_THAN;
                            FUNCT3_SLTU: controlSignals.aluControl.aluOp = ALU_LESS_THAN_UNSIGNED;
                            FUNCT3_XOR: controlSignals.aluControl.aluOp = ALU_XOR;
                            FUNCT3_SRL_SRA: controlSignals.aluControl.aluOp = ALU_SHIFT_RIGHT_LOGICAL;
                            FUNCT3_OR: controlSignals.aluControl.aluOp = ALU_OR;
                            FUNCT3_AND:controlSignals.aluControl.aluOp = ALU_AND;
                            default: begin
                                `log_display(valid, "Encountered unknown/unimplemented 3-bit rtype integer function code 0x%01x.",
                                rtype_funct3);
                                controlSignals.isIllegal = 1'b1;
                            end
                        endcase
                    end
                    FUNCT7_ALT_INT: begin
                        unique case(rtype_funct3)
                            FUNCT3_ADD_SUB: controlSignals.aluControl.aluOp = ALU_SUBTRACT;
                            FUNCT3_SRL_SRA: controlSignals.aluControl.aluOp = ALU_SHIFT_RIGHT_ARITHMATIC;
                            default: begin
                                `log_display(valid, "Encountered unknown/unimplemented 3-bit rtype ALT integer function code 0x%01x.",rtype_funct3);
                                controlSignals.isIllegal = 1'b1;
                            end
                        endcase
                    end
                    `ifdef HARDWARE_MULT
                    FUNCT7_MULDIV: begin
                        unique case(rtype_m_funct3)
                            FUNCT3_MUL: controlSignals.aluControl.aluOp = ALU_MUL_LOW;
                            FUNCT3_MULH: controlSignals.aluControl.aluOp = ALU_MUL_HIGH_SIGNED_SIGNED;
                            FUNCT3_MULHSU: controlSignals.aluControl.aluOp = ALU_MUL_HIGH_SIGNED_UNSIGNED;
                            FUNCT3_MULHU: controlSignals.aluControl.aluOp = ALU_MUL_HIGH_UNSIGNED_UNSIGNED;
                            FUNCT3_DIV: controlSignals.aluControl.aluOp = ALU_DIV_SIGNED;
                            FUNCT3_DIVU: controlSignals.aluControl.aluOp = ALU_DIV_UNSIGNED;
                            FUNCT3_REM: controlSignals.aluControl.aluOp = ALU_REM_SIGNED;
                            FUNCT3_REMU: controlSignals.aluControl.aluOp = ALU_REM_UNSIGNED;
                            default: begin
                                `log_display(valid, "Encountered unknown/unimplemented 3-bit rtype multiplication integer function code 0x%01x.",
                                rtype_m_funct3);
                                controlSignals.isIllegal = 1'b1;
                            end
                        endcase
                    end
                    `endif
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 7-bit function code 0x%02x.", funct7);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end

            // General I-type arithmetic operation
            OP_IMM: begin
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_IMMEDIATE;
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.writeBackSelect = ALU_RESULT;
                controlSignals.immediateType = I_TYPE;
                unique case (itype_int_funct3)
                    FUNCT3_ADDI: controlSignals.aluControl.aluOp = ALU_ADD;
                    FUNCT3_SLTI: controlSignals.aluControl.aluOp = ALU_LESS_THAN;
                    FUNCT3_SLTIU: controlSignals.aluControl.aluOp = ALU_LESS_THAN_UNSIGNED;
                    FUNCT3_XORI: controlSignals.aluControl.aluOp = ALU_XOR;
                    FUNCT3_ORI: controlSignals.aluControl.aluOp = ALU_OR;
                    FUNCT3_ANDI: controlSignals.aluControl.aluOp = ALU_AND;
                    FUNCT3_SLLI: controlSignals.aluControl.aluOp = ALU_SHIFT_LEFT;
                    FUNCT3_SRLI_SRAI:
                        unique case(funct7)
                            FUNCT7_INT: controlSignals.aluControl.aluOp = ALU_SHIFT_RIGHT_LOGICAL;
                            FUNCT7_ALT_INT: controlSignals.aluControl.aluOp = ALU_SHIFT_RIGHT_ARITHMATIC;
                            default: begin
                                `log_display(valid, "Encountered unknown/unimplemented 3-bit itype integer funct7 function code 0x%02x.", funct7);
                                controlSignals.isIllegal = 1'b1;
                            end
                        endcase
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 3-bit itype integer function code 0x%01x.",
                                itype_int_funct3);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end
            OP_LOAD: begin
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_IMMEDIATE;
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.writeBackSelect = MEM_RESULT;
                controlSignals.immediateType = I_TYPE;
                unique case(itype_load_funct3)
                    FUNCT3_LB: controlSignals.memoryMode = LOAD_BYTE;
                    FUNCT3_LH: controlSignals.memoryMode = LOAD_HALF_WORD;
                    FUNCT3_LW: controlSignals.memoryMode = LOAD_WORD;
                    FUNCT3_LBU: controlSignals.memoryMode = LOAD_BYTE_UNSIGNED;
                    FUNCT3_LHU: controlSignals.memoryMode = LOAD_HALF_WORD_UNSIGNED;
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 3-bit itype load function code 0x%01x.", itype_load_funct3);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end

            OP_STORE: begin
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_IMMEDIATE;
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.rs2.isValid = 1'b1;
                controlSignals.immediateType = S_TYPE;
                unique case(stype_funct3)
                    FUNCT3_SB: controlSignals.memoryMode = STORE_BYTE;
                    FUNCT3_SH: controlSignals.memoryMode = STORE_HALF_WORD;
                    FUNCT3_SW: controlSignals.memoryMode = STORE_WORD;
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 3-bit store function code 0x%01x.", stype_funct3);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end

            OP_AMO: begin
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.writeBackSelect = MEM_RESULT;
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_ZERO;
                unique case(amo_funct5)
                    FUNCT5_LR: begin
                        controlSignals.memoryMode = LOAD_RESERVED;
                    end
                    FUNCT5_SC: begin
                        controlSignals.rs2.isValid = 1'b1;
                        controlSignals.memoryMode = STORE_CONDITIONAL;
                    end
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 5-bit amo function code 0x%02x.", amo_funct5);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase

            end

            OP_LUI: begin
                controlSignals.immediateType = U_TYPE;
                controlSignals.writeBackSelect = ALU_RESULT;
                controlSignals.aluControl.aluOp = ALU_ADD;
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_ZERO;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_IMMEDIATE;
            end

            OP_AUIPC: begin
                controlSignals.immediateType = U_TYPE;
                controlSignals.writeBackSelect = ALU_RESULT;
                controlSignals.aluControl.aluOp = ALU_ADD;
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_PC;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_IMMEDIATE;
            end

            OP_JAL: begin
                controlSignals.immediateType = UJ_TYPE;
                controlSignals.jumpBaseSelect = JUMP_BASE_PC;
                controlSignals.writeBackSelect = IF_NEXT_INSTRUCTION;
                controlSignals.instructionType = JUMP;
            end

            OP_JALR: begin
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.immediateType = I_TYPE;
                controlSignals.jumpBaseSelect = JUMP_BASE_RS1;
                controlSignals.writeBackSelect = IF_NEXT_INSTRUCTION;
                controlSignals.instructionType = JUMP;
            end

            OP_BRANCH: begin
                controlSignals.rs1.isValid = 1'b1;
                controlSignals.rs2.isValid = 1'b1;
                controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                controlSignals.aluControl.aluSelect2 = ALU_SELECT_REGISTER;
                controlSignals.immediateType = SB_TYPE;
                controlSignals.instructionType = BRANCH;
                controlSignals.jumpBaseSelect = JUMP_BASE_PC;
                unique case(sbtype_funct3)
                    FUNCT3_BEQ: controlSignals.aluControl.aluOp = ALU_EQUAL;
                    FUNCT3_BNE: controlSignals.aluControl.aluOp = ALU_NOT_EQUAL;
                    FUNCT3_BLT: controlSignals.aluControl.aluOp = ALU_LESS_THAN;
                    FUNCT3_BGE: controlSignals.aluControl.aluOp = ALU_GREATER_THAN_OR_EQUAL;
                    FUNCT3_BLTU: controlSignals.aluControl.aluOp = ALU_LESS_THAN_UNSIGNED;
                    FUNCT3_BGEU: controlSignals.aluControl.aluOp = ALU_GREATER_THAN_OR_EQUAL_UNSIGNED;
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 3-bit branch function code 0x%01x.", sbtype_funct3);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end


            // General system operation
            OP_SYSTEM: begin
                unique case(itype_system_funct3)
                    FUNCT3_PRIV: begin
                        unique case (itype_funct12)
                            FUNCT12_ECALL: begin
                                controlSignals.instructionType = ECALL;
                            end
                            FUNCT12_MRET: begin
                                controlSignals.instructionType = SYSTEM;
                                controlSignals.csrMode = CSR_RETURN;
                            end
                            default: begin
                                `log_display(valid, "Encountered unknown/unimplemented 12-bit itype function code 0x%03x.",
                                        itype_funct12);
                                controlSignals.isIllegal = 1'b1;
                            end
                        endcase
                    end
                    FUNCT3_CSRRW: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.csrMode = CSR_WRITE;
                        controlSignals.rs1.isValid = 1'b1;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_ZERO;
                        controlSignals.aluControl.aluOp = ALU_ADD;
                    end
                    FUNCT3_CSRRS: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.rs1.isValid = 1'b1;
                        controlSignals.csrMode = (controlSignals.rs1 != X0) ? CSR_WRITE : NO_CSR_UPDATE;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_CSR;
                        controlSignals.aluControl.aluOp = ALU_OR;
                    end
                    FUNCT3_CSRRC: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.rs1.isValid = 1'b1;
                        controlSignals.csrMode = (controlSignals.rs1 != X0) ? CSR_WRITE : NO_CSR_UPDATE;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_REGISTER;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_CSR;
                        controlSignals.aluControl.aluOp = ALU_CLEAR_MASK;
                    end
                    FUNCT3_CSRRWI: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.csrMode = CSR_WRITE;
                        controlSignals.immediateType = CSR_TYPE;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_IMMEDIATE;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_ZERO;
                        controlSignals.aluControl.aluOp = ALU_ADD;
                    end
                    FUNCT3_CSRRSI: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.immediateType = CSR_TYPE;
                        controlSignals.csrMode = (controlSignals.rs1 != X0) ? CSR_WRITE : NO_CSR_UPDATE;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_IMMEDIATE;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_CSR;
                        controlSignals.aluControl.aluOp = ALU_OR;
                    end
                    FUNCT3_CSRRCI: begin
                        controlSignals.instructionType = SYSTEM;
                        controlSignals.immediateType = CSR_TYPE;
                        controlSignals.csrMode = (controlSignals.rs1 != X0) ? CSR_WRITE : NO_CSR_UPDATE;

                        controlSignals.writeBackSelect = CSR_RESULT;

                        controlSignals.aluControl.aluSelect1 = ALU_SELECT_IMMEDIATE;
                        controlSignals.aluControl.aluSelect2 = ALU_SELECT_CSR;
                        controlSignals.aluControl.aluOp = ALU_CLEAR_MASK;
                    end
                    default: begin
                        `log_display(valid, "Encountered unknown/unimplemented 3-bit itype system 0x%02x.", itype_system_funct3);
                        controlSignals.isIllegal = 1'b1;
                    end
                endcase
            end

            default: begin
                `log_display(valid, "Encountered unknown/unimplemented opcode 0x%02x.", opcode);
                controlSignals.isIllegal = 1'b1;
            end
        endcase

        if(controlSignals.isIllegal) begin
            `log_display(valid, "Forbidding state changes on illegal instruction: 0x%08x", instruction);
            // Force all state changing signals to zero
            controlSignals.writeBackSelect = NO_WRITE_BACK;
            controlSignals.memoryMode = NO_MEMORY;
            controlSignals.instructionType = INVALID_INSTRUCTION;
            controlSignals.csrMode = NO_CSR_UPDATE;
        end

        if(~valid) begin
            controlSignals.instructionType = INVALID_INSTRUCTION;
        end
        // Only assert the illegal instruction exception after reset
        controlSignals.isIllegal &= valid;
    end

endmodule: RISCV_Decode
