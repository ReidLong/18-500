`ifndef __COMPILER
`define __COMPILER

`define STRINGIFY(x) `"x`"

`ifdef SIMULATION_18447
`define STATIC_ASSERT(CONDITION) \
generate\
    if(!(CONDITION)) begin\
        $error("%s.%d Condition Failed: %s", `__FILE__, `__LINE__, `STRINGIFY(CONDITION));\
        end\
        endgenerate
`else
`define STATIC_ASSERT(CONDITION) \
generate\
    if(!(CONDITION)) begin\
        DoesNotExist foo();\
        end\
        endgenerate
`endif


`endif