`ifndef __CONTROL_FLOW_SVH
`define __CONTROL_FLOW_SVH

`include "riscv_memory.svh"

typedef enum logic [1:0] {WEAKLY_NOT_TAKEN = 2'd0, STRONGLY_NOT_TAKEN = 2'd1, WEAKLY_TAKEN = 2'd2, STRONGLY_TAKEN = 2'd3} CounterState_t;

typedef struct packed {
    Word_t tagPC;
    CounterState_t counterState;
    Word_t nextPC;
} BranchPredictionEntry_t;

typedef struct packed {
    Word_t currentPC;
    Word_t nextInstruction;
    Word_t predictedPC;
    logic predictionHit;
    BranchPredictionEntry_t predictionEntry;
} Prediction_t;

typedef struct packed {
    logic flush, stall;
} PipelineControl_t;

typedef enum logic[2:0] {INVALID_INSTRUCTION = 3'd0, NORMAL, JUMP, BRANCH, ECALL, SYSTEM} InstructionType_t;
typedef enum {FORWARD, BACKWARD} Direction_t;

typedef struct packed {
    Direction_t direction;
    logic predictionCorrect;
    InstructionType_t instructionType;
} PredictionSummary_t;

interface ControlFlowInterface();
    Word_t overridePC_IF;
    logic  overrideValid_IF;
    Word_t pc_IF;
    Prediction_t currentPrediction_IF;

    Prediction_t prediction_EX;
    Word_t actualTarget_EX;
    PredictionSummary_t predictionSummary_EX;
    logic isValid_EX;

    logic registerFileStall_ID;
    logic memoryStall_MEM1, memoryStall_MEM2;

    InstructionType_t instructionType_ID, instructionType_EX1, instructionType_EX2, instructionType_MEM1, instructionType_MEM2, instructionType_WB;

    Word_t interruptPC;
    logic interruptPending, interruptAccepted;

    modport RegisterFile(
        output registerFileStall_ID
    );

    PipelineControl_t fetchControl, decodeControl, execute1Control, execute2Control, control_MEM1, control_MEM2;

    modport FetchControl(
        input fetchControl,
        output pc_IF,
        input currentPrediction_IF,
        input overridePC_IF,
        input overrideValid_IF
    );

    modport DecodeControl(
        input decodeControl,
        output instructionType_ID
    );

    modport ExecuteControl(
        input execute1Control,
        input execute2Control,
        output prediction_EX,
        output actualTarget_EX,
        input predictionSummary_EX,
        output isValid_EX,
        output instructionType_EX1,
        output instructionType_EX2
    );

    modport MemoryControl(
        input control_MEM1, control_MEM2,
        output memoryStall_MEM1, memoryStall_MEM2,
        output instructionType_MEM1, instructionType_MEM2
    );

    modport WriteBackControl(
        output instructionType_WB
    );

    modport ControlManager(
        input registerFileStall_ID,
        input memoryStall_MEM1, memoryStall_MEM2,

        input pc_IF,
        output currentPrediction_IF,
        output overridePC_IF, overrideValid_IF,

        input prediction_EX, actualTarget_EX,
        output predictionSummary_EX,
        input isValid_EX,

        output fetchControl,
        output decodeControl,
        output execute1Control, execute2Control,
        output control_MEM1, control_MEM2,
        input instructionType_ID, instructionType_EX1, instructionType_EX2, instructionType_MEM1, instructionType_MEM2, instructionType_WB,

        input interruptPC,
        input interruptPending,
        output interruptAccepted

    );

    modport InterruptController(
        output interruptPC,
        output interruptPending,
        input interruptAccepted
    );

endinterface


`endif