`ifndef _LOG_H
`define _LOG_H

// Define a macro that prints for simulation, does nothing for synthesis
`ifdef SIMULATION_18447
`define log_display(enabled, format, arg) \
    do begin \
        if (enabled) begin \
            $display($time,, format, arg); \
        end \
    end while (0)
`else
`define log_display(print, format, arg)
`endif /* SIMULATION_18447 */

`endif