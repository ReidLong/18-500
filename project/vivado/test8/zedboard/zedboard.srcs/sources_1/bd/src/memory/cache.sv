`default_nettype none

`include "riscv_memory.svh"
`include "compiler.svh"

module Cache (
    input  CacheRequest_t  instructionRequest, dataRequest,
    // Visible on the next cycle
    output CacheResponse_t instructionResponse, dataResponse,
    input  logic           clock, clear
);

    `STATIC_ASSERT($bits(CacheTagEntry_t) <= 36);
    `STATIC_ASSERT($bits(CacheIndex_t) + $bits(CacheBlockOffset_t) == 10);
    `STATIC_ASSERT($bits(CacheBlockOffset_t) == $clog2(`BLOCK_SIZE));

    logic [9:0] instructionBlockIndex, dataBlockIndex;

    assign instructionBlockIndex = {instructionRequest.index, instructionRequest.blockOffset};
    assign dataBlockIndex        = {dataRequest.index, dataRequest.blockOffset};

    CacheTagEntry_t instructionTagIn, instructionTagOut, dataTagIn, dataTagOut;

    function automatic logic isWriteEnabled(CacheRequest_t request);
        if(!request.isValid)
            return 1'b0;
        unique case(request.requestType)
            CACHE_READ      : return 1'b0;
            CACHE_WRITE     : return 1'b1;
            CACHE_DRAM_FILL : return 1'b1;
        endcase
    endfunction

    function automatic CacheTagEntry_t packTagEntry(CacheRequest_t request);
        return '{
            isValid : 1'b1, // If we are writing, it must be valid (I think this is true)
            isDirty : request.requestType == CACHE_WRITE,
            tag     : request.tag
        };
    endfunction

    assign instructionTagIn = packTagEntry(instructionRequest);
    assign dataTagIn        = packTagEntry(dataRequest);

    logic[3:0] instructionWriteEnable, dataWriteEnable;
    assign instructionWriteEnable = {4{instructionRequest.isValid}} & instructionRequest.writeEnable;
    assign dataWriteEnable        = {4{dataRequest.isValid}} & dataRequest.writeEnable;

    // This is a hack to flush the cache while clear is asserted
    CacheIndex_t clearIndex;
    Counter #(.WIDTH($bits(clearIndex))) clear_counter(.clock, .clear(1'b0), .enable(clear), .q(clearIndex));

    xilinx_bram #(.RAM_WIDTH($bits(CacheTagEntry_t)), .RAM_DEPTH(2**$bits(CacheIndex_t))) tag_bram (
        .addressA    (clear ? clearIndex : instructionRequest.index),
        .dataInA     (clear ? {$bits(CacheTagEntry_t){1'b0}} : instructionTagIn        ),
        .writeEnableA(clear ? 1'b1 : (|instructionWriteEnable) ),
        .dataOutA    (instructionTagOut       ),

        .addressB    (dataRequest.index       ),
        .dataInB     (dataTagIn               ),
        .writeEnableB(|dataWriteEnable        ),
        .dataOutB    (dataTagOut              ),

        .clock
    );

    `STATIC_ASSERT($bits(CacheIndex_t) + $bits(CacheBlockOffset_t) == 10);

    Word_t instructionWordOut, dataWordOut;

    xilinx_bram_byteWrite #(.NB_COL(4), .COL_WIDTH(8), .RAM_DEPTH(1024)) block_bram(
        .addra(instructionBlockIndex),
        .dina(instructionRequest.dataIn),
        .wea(instructionWriteEnable),
        .douta(instructionWordOut),

        .addrb(dataBlockIndex),
        .dinb(dataRequest.dataIn),
        .web(dataWriteEnable),
        .doutb(dataWordOut),

        .clka(clock)
        );

    CacheRequest_t savedInstructionRequest, savedDataRequest;

    Register #(.WIDTH($bits(CacheRequest_t))) instruction_reg(
        .q(savedInstructionRequest),
        .d(instructionRequest),
        .enable(1'b1), // No stalling in cache
        .clock,
        .clear
    );

    Register #(.WIDTH($bits(CacheRequest_t))) data_reg (
        .q  (savedDataRequest),
        .d   (dataRequest     ),
        .enable (1'b1            ),
        .clock                  ,
        .clear
    );


    // Visible on the next clock cycle

    function automatic CacheResponse_t packResponse(CacheRequest_t savedRequest, Word_t dataOut, CacheTagEntry_t tagOut);
        return '{
            request  : savedRequest,
            dataOut  : dataOut,
            tagEntry : tagOut
        };
    endfunction

    assign instructionResponse = packResponse(savedInstructionRequest, instructionWordOut, instructionTagOut);
    assign dataResponse = packResponse(savedDataRequest, dataWordOut, dataTagOut);

endmodule