`default_nettype none

`include "riscv_isa.svh"
`include "riscv_memory.svh"
`include "compiler.svh"
`include "riscv_console.svh"

`define MAX_MEMORY_LATENCY 100

// Port A will always be instruction or VGA
// Port B will always be data store/load

module RISCV_Memory (
    input  MemoryRequest_t  instructionRequest, dataRequest, vgaRequest,
    output logic            instructionReady, dataReady, vgaReady,
    // Visible on the clock edge after request asserted with ready valid
    output MemoryResponse_t instructionResponse, dataResponse, vgaResponse,
    // Infrastructure
    input DRAMResponse_t dramResponse,
    output DRAMRequest_t dramRequest,
    input  logic            coreClock, vramClock, clear, halted
);

    // If this isn't true then there are a lot of things that will need to be checked
    `STATIC_ASSERT(`XLEN == 32);

    function automatic logic isEqualRequest(MemoryRequest_t requestA, MemoryRequest_t requestB);
        // TODO: Does == work with structs?
        return requestA.readEnable == requestB.readEnable
            && requestA.writeEnable == requestB.writeEnable
            && requestA.address == requestB.address
            && requestA.addressType == requestB.addressType
            && requestA.dataIn == requestB.dataIn;
    endfunction

    function automatic logic isInVRAM(Address_t address);
        return `VRAM_START <= address && address < `VRAM_END;
    endfunction

    dataSingleEnable: assert property (@(posedge coreClock) disable iff(clear) !(dataRequest.readEnable && (|dataRequest.writeEnable)));
    vgaNeverWrite: assert property (@(posedge vramClock) disable iff(clear) !(|vgaRequest.writeEnable));
    instructionNeverWrite: assert property (@(posedge coreClock) disable iff(clear) !(|instructionRequest.writeEnable));
    vgaSingleCycle: assert property (@(posedge vramClock) disable iff(clear) vgaRequest.readEnable |=> vgaResponse.isValid);
    vgaAlwaysReady: assert property (@(posedge vramClock) disable iff(clear) vgaReady);

    // All assertions based on dataReady being asserted are very fragile since data ready may not be asserted if we transition directly to a cache miss
    writeVRAMResponse: assert property (@(posedge coreClock) disable iff(clear) |dataRequest.writeEnable && vram.isInVRAM(dataRequest.address) && dataReady |=> dataResponse.isValid && isEqualRequest($past(dataRequest,1), dataResponse.request));

    VRAMResponseReady: assert property (@(posedge coreClock) disable iff(clear) (dataRequest.readEnable || (!dataRequest.writeEnable)) && vram.isInVRAM(dataRequest.address) && dataReady |=> !dataReady [*0:`MAX_MEMORY_LATENCY] ##1 dataReady);

    readVRAMResponseValid: assert property (@(posedge coreClock) disable iff(clear) dataRequest.readEnable && vram.isInVRAM(dataRequest.address) && dataReady |=> dataResponse.isValid);
    readVRAMResponseRequest: assert property (@(posedge coreClock) disable iff(clear) dataRequest.readEnable && vram.isInVRAM(dataRequest.address) && dataReady |=> isEqualRequest($past(dataRequest,1), dataResponse.request));

    vgaInVRAM: assert property (@(posedge vramClock) disable iff(clear) vgaRequest.readEnable |-> memory.vram.isInVRAM(vgaRequest.address));

    // Valid and ready are not necessarily asserted at the same clock edge since DRAM may lock the interface
    readInstructionValid: assert property (@(posedge coreClock) disable iff(clear) instructionRequest.readEnable && instructionReady |=> (!instructionResponse.isValid && !instructionReady) [*0:`MAX_MEMORY_LATENCY] ##1 instructionResponse.isValid);
    readInstructionReady: assert property (@(posedge coreClock) disable iff(clear) instructionRequest.readEnable && instructionReady |=> !instructionReady [*0:`MAX_MEMORY_LATENCY] ##1 instructionReady);

    readDataValid: assert property (@(posedge coreClock) disable iff(clear) dataRequest.readEnable && dataReady |=> (!dataResponse.isValid && !dataReady) [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid);
    readDataReady: assert property (@(posedge coreClock) disable iff(clear) dataRequest.readEnable && dataReady |=> !dataReady [*0:`MAX_MEMORY_LATENCY] ##1 dataReady);

    writeDataValid: assert property (@(posedge coreClock) disable iff(clear) |dataRequest.writeEnable && dataReady |=> (!dataResponse.isValid && !dataReady) [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid);
    writeDataReady: assert property (@(posedge coreClock) disable iff(clear) |dataRequest.writeEnable && dataReady |=> !dataReady [*0:`MAX_MEMORY_LATENCY] ##1 dataReady);

    writeDataCacheValid: assert property (@(posedge coreClock) disable iff(clear) |dataRequest.writeEnable && dataReady && !vram.isInVRAM(dataRequest.address) |=> (!dataReady && !dataResponse.isValid) [*1:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid);
    writeDataCacheReady: assert property (@(posedge coreClock) disable iff(clear) |dataRequest.writeEnable && dataReady && !vram.isInVRAM(dataRequest.address) |=> !dataReady [*1:`MAX_MEMORY_LATENCY] ##1 dataReady);



    MemoryRequest_t  dataVRAMRequest, dataCacheRequest;
    MemoryResponse_t dataVRAMResponse, dataCacheResponse;
    logic memoryDataReady;

    function automatic MemoryRequest_t createEmptyRequest();
        return '{
            readEnable: 1'b0,
            writeEnable: 4'b0,
            address: `ADDRESS_POISON,
            addressType: PHYSICAL_ADDRESS,
            dataIn: `WORD_POISON
        };
    endfunction

    onlyValidDataAddress: assert property (@(posedge coreClock) disable iff(clear) dataRequest.readEnable || (|dataRequest.writeEnable) |-> dataRequest.address < `PHYSICAL_MEMORY_SIZE);
    onlyValidInstructionAddress: assert property (@(posedge coreClock) disable iff(clear) instructionRequest.readEnable |-> instructionRequest.address < `PHYSICAL_MEMORY_SIZE);
    onlyValidVGAAddress: assert property (@(posedge vramClock) disable iff(clear) vgaRequest.readEnable |-> vram.isInVRAM(vgaRequest.address));

    // We are always ready to take another VGA request since these can't fault
    assign vgaReady = 1'b1;

    VRAM vram (
        .dataRequest (dataVRAMRequest ),
        .dataResponse(dataVRAMResponse),
        .vgaRequest                    ,
        .vgaResponse                   ,
        .coreClock                     ,
        .vramClock                     ,
        .clear                         ,
        .halted
    );

    assign dataReady = memoryDataReady;

    // This will route the data request to only one of the modules (effectively a fancy mux)
    always_comb begin
        dataVRAMRequest  = createEmptyRequest();
        dataCacheRequest = createEmptyRequest();

        if(isInVRAM(dataRequest.address)) begin
            dataVRAMRequest  = dataRequest;
        end else begin
            // TODO: This should also check for completely invalid address and through an exception
            dataCacheRequest  = dataRequest;
        end
    end

    // We only need one signal for data ready which is controlled by the main memory subsystem because the main memory system should always assert ready unless it is busy. If the request is to VRAM, it will always be satisfied after a single cycle, so ready should continue to be asserted.
    MainMemory memory (
        .instructionRequest                    ,
        .instructionReady                      ,
        .instructionResponse                   ,
        .dataRequest        (dataCacheRequest ),
        .dataReady          (memoryDataReady)  ,
        .dataResponse       (dataCacheResponse),
        .dramRequest                           ,
        .dramResponse                          ,
        .clock              (coreClock)        ,
        .clear
    );

    MemoryRequest_t savedDataRequest;
    Register #(.WIDTH($bits(MemoryRequest_t))) dataRequest_reg (
        .q     (savedDataRequest),
        .d     (dataRequest     ),
        .enable(dataReady       ),
        .clock (coreClock       ),
        .clear
    );


    // This will route the data response from the appropriate source
    always_comb begin
        if(isInVRAM(savedDataRequest.address)) begin
            dataResponse = dataVRAMResponse;
        end else begin
            dataResponse = dataCacheResponse;
        end
    end

endmodule
