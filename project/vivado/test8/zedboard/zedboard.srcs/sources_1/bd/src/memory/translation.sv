`default_nettype none

`include "riscv_memory.svh"

module Translation (
    input  AddressTranslationRequest_t  instructionTranslationRequest, dataTranslationRequest,
    // Visible on the next clock cycle
    output AddressTranslationResponse_t instructionTranslationResponse, dataTranslationResponse,
    // Infrastructure Signals
    input  logic                        clock, clear
);

    // For now everything is direct mapped

    logic instructionTranslationReady, dataTranslationReady;

    AddressTranslationRequest_t savedInstructionTranslationRequest, savedDataTranslationRequest;

    Register #(.WIDTH($bits(AddressTranslationRequest_t))) instruction_reg (
        .q  (savedInstructionTranslationRequest),
        .d   (instructionTranslationRequest     ),
        .enable (instructionTranslationReady       ),
        .clock                                    ,
        .clear
    );

    Register #(.WIDTH($bits(AddressTranslationRequest_t))) data_reg (
        .q  (savedDataTranslationRequest),
        .d   (dataTranslationRequest     ),
        .enable (dataTranslationReady       ),
        .clock                             ,
        .clear
    );

    assign instructionTranslationReady = 1'b1; // Always ready in direct mapped scheme
    assign dataTranslationReady = 1'b1; // Always ready in direct mapped scheme

    function automatic AddressTranslationResponse_t buildTranslationResponse(AddressTranslationRequest_t request);
        return '{
            request: request,
            physicalAddress: request.virtualAddress, // direct Mapped
            isValid: request.isValid,
            translationType: TLB_HIT
        };
    endfunction

    assign instructionTranslationResponse = buildTranslationResponse(savedInstructionTranslationRequest);
    assign dataTranslationResponse        = buildTranslationResponse(savedDataTranslationRequest);

endmodule