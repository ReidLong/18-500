
`default_nettype none

`include "riscv_console.svh"
`include "riscv_memory.svh"

// The synthesizable top level interface

module RISCV_Chip(
    input logic clock, clear,

    VGA_Interface.Controller vga,
    KBD_Interface.Controller kbd,
    output logic halted, fatalError,
    DRAM_Interface.Requester dram,
    BasicIO_Interface.Master basicIO
);

    MemoryRequest_t instructionRequest, dataRequest, vgaRequest;
    logic instructionReady, dataReady, vgaReady;
    MemoryResponse_t instructionResponse, dataResponse, vgaResponse;

    DRAMRequest_t dramRequest;
    DRAMResponse_t dramResponse;

    InterruptDevice timer();
    InterruptDevice keyboard();

    vgaAlwaysReady: assert property (@(posedge clock) disable iff(clear) vgaReady == 1'b1);

    RISCV_Memory memory(.instructionRequest, .dataRequest, .vgaRequest, .instructionReady, .dataReady, .vgaReady, .instructionResponse, .dataResponse, .vgaResponse, .dramResponse, .dramRequest, .coreClock(clock), .vramClock(vga.vramClock), .clear, .halted);

    // assign basicIO.led = {4'h7, clear, halted, fatalError, 1'b0};
    assign basicIO.led = kbd.readch;
    RISCV_Core core(.clock, .clear, .dataRequest, .instructionRequest, .dataResponse, .instructionResponse, .dataReady, .instructionReady, .halted, .timer(timer.InterruptController), .keyboard(keyboard.InterruptController));

    DRAM_Wrapper dram_w(.request(dramRequest), .response(dramResponse), .dram, .clock, .clear, .fatalError);

    VGA_Controller display(.vgaResponse, .vgaRequest, .vga_interface(vga), .clear);

    KBD_Controller keyboardController(.kbd_interface(kbd), .clock, .clear, .interruptController(keyboard.Device));

    TimerController timerController(.clock, .clear, .interruptController(timer.Device));


endmodule