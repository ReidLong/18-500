
module register
    #(parameter width=32, reset_value = 0)(
    output logic [width-1:0] q,
    input logic [width-1:0] d,
    input logic clock, enable, clear);

    always_ff @(posedge clock)
        if(clear)
            q <= reset_value;
        else if (enable)
            q <= d;

endmodule // register

module top_tb;
    logic [7:0] switch;
    logic [7:0] led;
    logic up, down, left, right, center;
    logic clock;

    initial begin
        clock = 1'b0;
        forever #5 clock = ~clock;
    end

    top dut(.*);

    initial begin
        $monitor("%d: SWITCH: %h LED: %h UDLR: %b STATE: %s", $time, switch, led, {up, down, left, right}, dut.state);
        switch = 8'b0;
        up = 1'b0;
        down = 1'b0;
        left = 1'b0;
        right = 1'b0;
        center = 1'b1;
        // Let everything settle and initialize since we don't have a reset
        repeat (5) @(posedge clock);
        center = 1'b0;
        repeat(5) @(posedge clock);
        switch = 8'hAC;
        down = 1'b1;
        repeat (2) @(posedge clock);
        switch = 8'h88;
        down = 1'b0;
        left = 1'b1;
        repeat (2) @(posedge clock);
        left = 1'b0;
        right = 1'b1;
        switch = 8'h99;
        repeat (2) @(posedge clock);
        right = 1'b0;
        repeat (4) @(posedge clock);
        switch = 8'h88;
        left = 1'b1;
        repeat (2) @(posedge clock);
        left = 1'b0;
        switch = 8'h55;
        up = 1'b1;
        repeat (2) @(posedge clock);
        up = 1'b0;
        repeat (2) @(posedge clock);
        if(led != 8'hAC)
            $display("Bad LED: %h", led_l);
        @(posedge clock)
        $finish;
    end

endmodule

module top(
    input logic[7:0] switch,
    output logic[7:0] led,
    input logic up, down, left, right, center,
    input logic clock
    );

    logic [7:0] sw2, sw1;
    always_ff @(posedge clock) begin
        sw1 <= switch;
        sw2 <= sw1;
    end

    logic up1, doRead, down1, saveData, left1, saveAddress, right1, doWrite;
    logic center1, center2;
    always_ff @(posedge clock) begin
        up1 <= up;
        doRead <= up1;

        down1 <= down;
        saveData <= down1;

        left1 <= left;
        saveAddress <= left1;

        right1 <= right;
        doWrite <= right1;

        center1 <= center;
        center2 <= center1;

    end


    logic[9:0] addressA, addressB;
    logic[31:0] dataInA, dataInB;
    logic writeEnableA, writeEnableB;
    logic [31:0] dataOutA, dataOutB;
    bram memory(.*);

    logic [31:0] savedData;
    logic [9:0] savedAddress;
    logic loadOutput;
    logic [7:0] savedDataOut;
    logic bad;
    logic badEnable;

    register #(.width(10)) addressRegister(.q(savedAddress), .d({2'b0, sw2}),
        .clock, .enable(saveAddress), .clear(center2));
    register dataRegister(.q(savedData), .d({24'b0, sw2}), .clock,
        .enable(saveData), .clear(center2));
    register #(.width(8)) readData(.q(savedDataOut), .d(dataOutA[7:0]), .clock, .enable(loadOutput),
        .clear(center2));
    register #(.width(1)) badRegister(.q(bad), .d(1'b1), .clock, .enable(badEnable), .clear(center2));

    enum {IDLE, READ_REQUEST, WRITE_REQUEST, WRITE_CHECK, WRITE_RESPONSE}
        state, nextState;

    always_ff @(posedge clock)
        if(center2)
            state <= IDLE;
        else
            state <= nextState;

    assign led = bad ? 8'hBD : savedDataOut;

    always_comb begin
        addressA = savedAddress;
        addressB = ~savedAddress;
        dataInA = savedData;
        dataInB = ~savedData;
        writeEnableA = 1'b0;
        writeEnableB = 1'b0;
        loadOutput = 1'b0;
        badEnable = 1'b0;
        nextState = state;
        unique case(state)
            IDLE:
                if(doRead)
                    nextState = READ_REQUEST;
                else if(doWrite)
                    nextState = WRITE_REQUEST;
            READ_REQUEST: begin
                // This is assuming that on the previous cycle we set
                // the address properly and performed the read.
                // This is the cycle where we see the response.
                loadOutput = 1'b1;
                nextState = IDLE;
            end
            WRITE_REQUEST: begin
                writeEnableA = 1'b1;
                writeEnableB = 1'b1;
                nextState = WRITE_RESPONSE;
            end
            WRITE_RESPONSE:
                // What we are effectively doing here is starting a read request to the bram
                nextState = WRITE_CHECK;
            WRITE_CHECK: begin
                // Now we should have the data we just wrote on the output
                if(dataOutA != savedData)
                    badEnable = 1'b1; // This will hold forever
                nextState = IDLE;
            end
        endcase
    end

endmodule