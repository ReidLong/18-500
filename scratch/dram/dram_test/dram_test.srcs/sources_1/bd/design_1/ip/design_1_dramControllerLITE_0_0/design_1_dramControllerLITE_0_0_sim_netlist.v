// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Tue Mar 13 13:26:25 2018
// Host        : DESKTOP-QVPG904 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/Users/Reid/Documents/18-500/dram_test/dram_test.srcs/sources_1/bd/design_1/ip/design_1_dramControllerLITE_0_0/design_1_dramControllerLITE_0_0_sim_netlist.v
// Design      : design_1_dramControllerLITE_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_dramControllerLITE_0_0,dramControllerLITE_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "dramControllerLITE_v1_0,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module design_1_dramControllerLITE_0_0
   (dataIn,
    dataOut,
    addressBase,
    doRead,
    doWrite,
    requestAccepted,
    requestDone,
    m00_axi_awaddr,
    m00_axi_awprot,
    m00_axi_awvalid,
    m00_axi_awready,
    m00_axi_wdata,
    m00_axi_wstrb,
    m00_axi_wvalid,
    m00_axi_wready,
    m00_axi_bresp,
    m00_axi_bvalid,
    m00_axi_bready,
    m00_axi_araddr,
    m00_axi_arprot,
    m00_axi_arvalid,
    m00_axi_arready,
    m00_axi_rdata,
    m00_axi_rresp,
    m00_axi_rvalid,
    m00_axi_rready,
    m00_axi_aclk,
    m00_axi_aresetn,
    m00_axi_error);
  input [31:0]dataIn;
  output [31:0]dataOut;
  input [31:0]addressBase;
  input doRead;
  input doWrite;
  output requestAccepted;
  output requestDone;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI AWADDR" *) output [31:0]m00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI AWPROT" *) output [2:0]m00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI AWVALID" *) output m00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI AWREADY" *) input m00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI WDATA" *) output [31:0]m00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI WSTRB" *) output [3:0]m00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI WVALID" *) output m00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI WREADY" *) input m00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI BRESP" *) input [1:0]m00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI BVALID" *) input m00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI BREADY" *) output m00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI ARADDR" *) output [31:0]m00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI ARPROT" *) output [2:0]m00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI ARVALID" *) output m00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI ARREADY" *) input m00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI RDATA" *) input [31:0]m00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI RRESP" *) input [1:0]m00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI RVALID" *) input m00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M00_AXI RREADY" *) output m00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXI_CLK CLK, xilinx.com:signal:clock:1.0 m00_axi_aclk CLK" *) input m00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXI_RST RST, xilinx.com:signal:reset:1.0 m00_axi_aresetn RST" *) input m00_axi_aresetn;
  output m00_axi_error;

  wire \<const0> ;
  wire \<const1> ;
  wire [31:0]addressBase;
  wire [31:0]dataIn;
  wire [31:0]dataOut;
  wire doRead;
  wire doWrite;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [31:2]\^m00_axi_awaddr ;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire m00_axi_bready;
  wire [1:0]m00_axi_bresp;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire [31:0]m00_axi_rdata;
  wire m00_axi_rready;
  wire [1:0]m00_axi_rresp;
  wire m00_axi_rvalid;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  wire requestAccepted;
  wire requestDone;

  assign m00_axi_arprot[2] = \<const0> ;
  assign m00_axi_arprot[1] = \<const0> ;
  assign m00_axi_arprot[0] = \<const1> ;
  assign m00_axi_awaddr[31:2] = \^m00_axi_awaddr [31:2];
  assign m00_axi_awaddr[1:0] = m00_axi_araddr[1:0];
  assign m00_axi_awprot[2] = \<const0> ;
  assign m00_axi_awprot[1] = \<const0> ;
  assign m00_axi_awprot[0] = \<const0> ;
  assign m00_axi_wstrb[3] = \<const1> ;
  assign m00_axi_wstrb[2] = \<const1> ;
  assign m00_axi_wstrb[1] = \<const1> ;
  assign m00_axi_wstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  design_1_dramControllerLITE_0_0_dramControllerLITE_v1_0 inst
       (.M_AXI_BREADY(m00_axi_bready),
        .addressBase(addressBase),
        .dataIn(dataIn),
        .dataOut(dataOut),
        .doRead(doRead),
        .doWrite(doWrite),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_awaddr(\^m00_axi_awaddr ),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_bresp(m00_axi_bresp[1]),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_error(m00_axi_error),
        .m00_axi_rdata(m00_axi_rdata),
        .m00_axi_rready(m00_axi_rready),
        .m00_axi_rresp(m00_axi_rresp[1]),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_wvalid(m00_axi_wvalid),
        .requestAccepted(requestAccepted),
        .requestDone(requestDone));
endmodule

(* ORIG_REF_NAME = "EdgeTrigger" *) 
module design_1_dramControllerLITE_0_0_EdgeTrigger
   (current,
    next,
    \state_reg[0] ,
    \state_reg[1] ,
    \state_reg[1]_0 ,
    ERROR_reg,
    SR,
    doRead,
    m00_axi_aclk,
    \state_reg[1]_1 ,
    \state_reg[0]_0 ,
    writes_done,
    reads_done,
    next_0,
    current_1);
  output current;
  output next;
  output \state_reg[0] ;
  output \state_reg[1] ;
  output \state_reg[1]_0 ;
  output ERROR_reg;
  input [0:0]SR;
  input doRead;
  input m00_axi_aclk;
  input \state_reg[1]_1 ;
  input \state_reg[0]_0 ;
  input writes_done;
  input reads_done;
  input next_0;
  input current_1;

  wire ERROR_reg;
  wire [0:0]SR;
  wire current;
  wire current_1;
  wire doRead;
  wire m00_axi_aclk;
  wire next;
  wire next_0;
  wire reads_done;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[1] ;
  wire \state_reg[1]_0 ;
  wire \state_reg[1]_1 ;
  wire writes_done;

  LUT5 #(
    .INIT(32'h30BB3088)) 
    ERROR_i_2
       (.I0(writes_done),
        .I1(\state_reg[0]_0 ),
        .I2(reads_done),
        .I3(\state_reg[1]_1 ),
        .I4(\state_reg[1] ),
        .O(ERROR_reg));
  FDRE current_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(doRead),
        .Q(current),
        .R(SR));
  FDRE next_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(current),
        .Q(next),
        .R(SR));
  LUT4 #(
    .INIT(16'h4F44)) 
    requestAccepted_i_3
       (.I0(next),
        .I1(current),
        .I2(next_0),
        .I3(current_1),
        .O(\state_reg[1] ));
  LUT6 #(
    .INIT(64'h00000D000F0F0D00)) 
    \state[0]_i_1 
       (.I0(current),
        .I1(next),
        .I2(\state_reg[1]_1 ),
        .I3(\state_reg[1] ),
        .I4(\state_reg[0]_0 ),
        .I5(writes_done),
        .O(\state_reg[0] ));
  LUT6 #(
    .INIT(64'h000000000F220F00)) 
    \state[1]_i_1 
       (.I0(current),
        .I1(next),
        .I2(reads_done),
        .I3(\state_reg[1]_1 ),
        .I4(\state_reg[1] ),
        .I5(\state_reg[0]_0 ),
        .O(\state_reg[1]_0 ));
endmodule

(* ORIG_REF_NAME = "EdgeTrigger" *) 
module design_1_dramControllerLITE_0_0_EdgeTrigger_2
   (current,
    next,
    \q_reg[0] ,
    doSetup,
    E,
    out,
    SR,
    doWrite,
    m00_axi_aclk,
    m00_axi_aresetn,
    \write_index_reg[2] ,
    \write_index_reg[1] ,
    \write_index_reg[0] ,
    m00_axi_wready,
    axi_wvalid_reg,
    dataIn,
    Q,
    state,
    current_0,
    next_1);
  output current;
  output next;
  output \q_reg[0] ;
  output doSetup;
  output [0:0]E;
  output [31:0]out;
  input [0:0]SR;
  input doWrite;
  input m00_axi_aclk;
  input m00_axi_aresetn;
  input \write_index_reg[2] ;
  input \write_index_reg[1] ;
  input \write_index_reg[0] ;
  input m00_axi_wready;
  input axi_wvalid_reg;
  input [31:0]dataIn;
  input [31:0]Q;
  input [1:0]state;
  input current_0;
  input next_1;

  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;
  wire \axi_wdata[11]_i_2_n_0 ;
  wire \axi_wdata[11]_i_3_n_0 ;
  wire \axi_wdata[11]_i_4_n_0 ;
  wire \axi_wdata[11]_i_5_n_0 ;
  wire \axi_wdata[15]_i_2_n_0 ;
  wire \axi_wdata[15]_i_3_n_0 ;
  wire \axi_wdata[15]_i_4_n_0 ;
  wire \axi_wdata[15]_i_5_n_0 ;
  wire \axi_wdata[19]_i_2_n_0 ;
  wire \axi_wdata[19]_i_3_n_0 ;
  wire \axi_wdata[19]_i_4_n_0 ;
  wire \axi_wdata[19]_i_5_n_0 ;
  wire \axi_wdata[23]_i_2_n_0 ;
  wire \axi_wdata[23]_i_3_n_0 ;
  wire \axi_wdata[23]_i_4_n_0 ;
  wire \axi_wdata[23]_i_5_n_0 ;
  wire \axi_wdata[27]_i_2_n_0 ;
  wire \axi_wdata[27]_i_3_n_0 ;
  wire \axi_wdata[27]_i_4_n_0 ;
  wire \axi_wdata[27]_i_5_n_0 ;
  wire \axi_wdata[31]_i_3_n_0 ;
  wire \axi_wdata[31]_i_4_n_0 ;
  wire \axi_wdata[31]_i_5_n_0 ;
  wire \axi_wdata[31]_i_6_n_0 ;
  wire \axi_wdata[3]_i_2_n_0 ;
  wire \axi_wdata[3]_i_3_n_0 ;
  wire \axi_wdata[3]_i_4_n_0 ;
  wire \axi_wdata[3]_i_5_n_0 ;
  wire \axi_wdata[3]_i_6_n_0 ;
  wire \axi_wdata[3]_i_7_n_0 ;
  wire \axi_wdata[3]_i_8_n_0 ;
  wire \axi_wdata[7]_i_2_n_0 ;
  wire \axi_wdata[7]_i_3_n_0 ;
  wire \axi_wdata[7]_i_4_n_0 ;
  wire \axi_wdata[7]_i_5_n_0 ;
  wire \axi_wdata_reg[11]_i_1_n_0 ;
  wire \axi_wdata_reg[11]_i_1_n_1 ;
  wire \axi_wdata_reg[11]_i_1_n_2 ;
  wire \axi_wdata_reg[11]_i_1_n_3 ;
  wire \axi_wdata_reg[15]_i_1_n_0 ;
  wire \axi_wdata_reg[15]_i_1_n_1 ;
  wire \axi_wdata_reg[15]_i_1_n_2 ;
  wire \axi_wdata_reg[15]_i_1_n_3 ;
  wire \axi_wdata_reg[19]_i_1_n_0 ;
  wire \axi_wdata_reg[19]_i_1_n_1 ;
  wire \axi_wdata_reg[19]_i_1_n_2 ;
  wire \axi_wdata_reg[19]_i_1_n_3 ;
  wire \axi_wdata_reg[23]_i_1_n_0 ;
  wire \axi_wdata_reg[23]_i_1_n_1 ;
  wire \axi_wdata_reg[23]_i_1_n_2 ;
  wire \axi_wdata_reg[23]_i_1_n_3 ;
  wire \axi_wdata_reg[27]_i_1_n_0 ;
  wire \axi_wdata_reg[27]_i_1_n_1 ;
  wire \axi_wdata_reg[27]_i_1_n_2 ;
  wire \axi_wdata_reg[27]_i_1_n_3 ;
  wire \axi_wdata_reg[31]_i_2_n_1 ;
  wire \axi_wdata_reg[31]_i_2_n_2 ;
  wire \axi_wdata_reg[31]_i_2_n_3 ;
  wire \axi_wdata_reg[3]_i_1_n_0 ;
  wire \axi_wdata_reg[3]_i_1_n_1 ;
  wire \axi_wdata_reg[3]_i_1_n_2 ;
  wire \axi_wdata_reg[3]_i_1_n_3 ;
  wire \axi_wdata_reg[7]_i_1_n_0 ;
  wire \axi_wdata_reg[7]_i_1_n_1 ;
  wire \axi_wdata_reg[7]_i_1_n_2 ;
  wire \axi_wdata_reg[7]_i_1_n_3 ;
  wire axi_wvalid_reg;
  wire current;
  wire current_0;
  wire [31:0]dataIn;
  wire doSetup;
  wire doWrite;
  wire m00_axi_aclk;
  wire m00_axi_aresetn;
  wire m00_axi_wready;
  wire next;
  wire next_1;
  wire [31:0]out;
  wire \q_reg[0] ;
  wire [1:0]state;
  wire \write_index_reg[0] ;
  wire \write_index_reg[1] ;
  wire \write_index_reg[2] ;
  wire [3:3]\NLW_axi_wdata_reg[31]_i_2_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[11]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[11]),
        .I3(Q[11]),
        .O(\axi_wdata[11]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[11]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[10]),
        .I3(Q[10]),
        .O(\axi_wdata[11]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[11]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[9]),
        .I3(Q[9]),
        .O(\axi_wdata[11]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[11]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[8]),
        .I3(Q[8]),
        .O(\axi_wdata[11]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[15]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[15]),
        .I3(Q[15]),
        .O(\axi_wdata[15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[15]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[14]),
        .I3(Q[14]),
        .O(\axi_wdata[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[15]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[13]),
        .I3(Q[13]),
        .O(\axi_wdata[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[15]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[12]),
        .I3(Q[12]),
        .O(\axi_wdata[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[19]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[19]),
        .I3(Q[19]),
        .O(\axi_wdata[19]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[19]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[18]),
        .I3(Q[18]),
        .O(\axi_wdata[19]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[19]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[17]),
        .I3(Q[17]),
        .O(\axi_wdata[19]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[19]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[16]),
        .I3(Q[16]),
        .O(\axi_wdata[19]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[23]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[23]),
        .I3(Q[23]),
        .O(\axi_wdata[23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[23]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[22]),
        .I3(Q[22]),
        .O(\axi_wdata[23]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[23]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[21]),
        .I3(Q[21]),
        .O(\axi_wdata[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[23]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[20]),
        .I3(Q[20]),
        .O(\axi_wdata[23]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[27]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[27]),
        .I3(Q[27]),
        .O(\axi_wdata[27]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[27]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[26]),
        .I3(Q[26]),
        .O(\axi_wdata[27]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[27]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[25]),
        .I3(Q[25]),
        .O(\axi_wdata[27]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[27]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[24]),
        .I3(Q[24]),
        .O(\axi_wdata[27]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFBBB)) 
    \axi_wdata[31]_i_1 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(m00_axi_wready),
        .I3(axi_wvalid_reg),
        .O(E));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[31]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[31]),
        .I3(Q[31]),
        .O(\axi_wdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[31]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[30]),
        .I3(Q[30]),
        .O(\axi_wdata[31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[31]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[29]),
        .I3(Q[29]),
        .O(\axi_wdata[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[31]_i_6 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[28]),
        .I3(Q[28]),
        .O(\axi_wdata[31]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \axi_wdata[3]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[2] ),
        .O(\axi_wdata[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \axi_wdata[3]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[1] ),
        .O(\axi_wdata[3]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \axi_wdata[3]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[0] ),
        .O(\axi_wdata[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[3]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[3]),
        .I3(Q[3]),
        .O(\axi_wdata[3]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hBFFB0440)) 
    \axi_wdata[3]_i_6 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[2] ),
        .I3(Q[2]),
        .I4(dataIn[2]),
        .O(\axi_wdata[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hBFFB0440)) 
    \axi_wdata[3]_i_7 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[1] ),
        .I3(Q[1]),
        .I4(dataIn[1]),
        .O(\axi_wdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hBFFB0440)) 
    \axi_wdata[3]_i_8 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(\write_index_reg[0] ),
        .I3(Q[0]),
        .I4(dataIn[0]),
        .O(\axi_wdata[3]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[7]_i_2 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[7]),
        .I3(Q[7]),
        .O(\axi_wdata[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[7]_i_3 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[6]),
        .I3(Q[6]),
        .O(\axi_wdata[7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[7]_i_4 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[5]),
        .I3(Q[5]),
        .O(\axi_wdata[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    \axi_wdata[7]_i_5 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .I2(dataIn[4]),
        .I3(Q[4]),
        .O(\axi_wdata[7]_i_5_n_0 ));
  CARRY4 \axi_wdata_reg[11]_i_1 
       (.CI(\axi_wdata_reg[7]_i_1_n_0 ),
        .CO({\axi_wdata_reg[11]_i_1_n_0 ,\axi_wdata_reg[11]_i_1_n_1 ,\axi_wdata_reg[11]_i_1_n_2 ,\axi_wdata_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[11:8]),
        .S({\axi_wdata[11]_i_2_n_0 ,\axi_wdata[11]_i_3_n_0 ,\axi_wdata[11]_i_4_n_0 ,\axi_wdata[11]_i_5_n_0 }));
  CARRY4 \axi_wdata_reg[15]_i_1 
       (.CI(\axi_wdata_reg[11]_i_1_n_0 ),
        .CO({\axi_wdata_reg[15]_i_1_n_0 ,\axi_wdata_reg[15]_i_1_n_1 ,\axi_wdata_reg[15]_i_1_n_2 ,\axi_wdata_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[15:12]),
        .S({\axi_wdata[15]_i_2_n_0 ,\axi_wdata[15]_i_3_n_0 ,\axi_wdata[15]_i_4_n_0 ,\axi_wdata[15]_i_5_n_0 }));
  CARRY4 \axi_wdata_reg[19]_i_1 
       (.CI(\axi_wdata_reg[15]_i_1_n_0 ),
        .CO({\axi_wdata_reg[19]_i_1_n_0 ,\axi_wdata_reg[19]_i_1_n_1 ,\axi_wdata_reg[19]_i_1_n_2 ,\axi_wdata_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[19:16]),
        .S({\axi_wdata[19]_i_2_n_0 ,\axi_wdata[19]_i_3_n_0 ,\axi_wdata[19]_i_4_n_0 ,\axi_wdata[19]_i_5_n_0 }));
  CARRY4 \axi_wdata_reg[23]_i_1 
       (.CI(\axi_wdata_reg[19]_i_1_n_0 ),
        .CO({\axi_wdata_reg[23]_i_1_n_0 ,\axi_wdata_reg[23]_i_1_n_1 ,\axi_wdata_reg[23]_i_1_n_2 ,\axi_wdata_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[23:20]),
        .S({\axi_wdata[23]_i_2_n_0 ,\axi_wdata[23]_i_3_n_0 ,\axi_wdata[23]_i_4_n_0 ,\axi_wdata[23]_i_5_n_0 }));
  CARRY4 \axi_wdata_reg[27]_i_1 
       (.CI(\axi_wdata_reg[23]_i_1_n_0 ),
        .CO({\axi_wdata_reg[27]_i_1_n_0 ,\axi_wdata_reg[27]_i_1_n_1 ,\axi_wdata_reg[27]_i_1_n_2 ,\axi_wdata_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[27:24]),
        .S({\axi_wdata[27]_i_2_n_0 ,\axi_wdata[27]_i_3_n_0 ,\axi_wdata[27]_i_4_n_0 ,\axi_wdata[27]_i_5_n_0 }));
  CARRY4 \axi_wdata_reg[31]_i_2 
       (.CI(\axi_wdata_reg[27]_i_1_n_0 ),
        .CO({\NLW_axi_wdata_reg[31]_i_2_CO_UNCONNECTED [3],\axi_wdata_reg[31]_i_2_n_1 ,\axi_wdata_reg[31]_i_2_n_2 ,\axi_wdata_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[31:28]),
        .S({\axi_wdata[31]_i_3_n_0 ,\axi_wdata[31]_i_4_n_0 ,\axi_wdata[31]_i_5_n_0 ,\axi_wdata[31]_i_6_n_0 }));
  CARRY4 \axi_wdata_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\axi_wdata_reg[3]_i_1_n_0 ,\axi_wdata_reg[3]_i_1_n_1 ,\axi_wdata_reg[3]_i_1_n_2 ,\axi_wdata_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\axi_wdata[3]_i_2_n_0 ,\axi_wdata[3]_i_3_n_0 ,\axi_wdata[3]_i_4_n_0 }),
        .O(out[3:0]),
        .S({\axi_wdata[3]_i_5_n_0 ,\axi_wdata[3]_i_6_n_0 ,\axi_wdata[3]_i_7_n_0 ,\axi_wdata[3]_i_8_n_0 }));
  CARRY4 \axi_wdata_reg[7]_i_1 
       (.CI(\axi_wdata_reg[3]_i_1_n_0 ),
        .CO({\axi_wdata_reg[7]_i_1_n_0 ,\axi_wdata_reg[7]_i_1_n_1 ,\axi_wdata_reg[7]_i_1_n_2 ,\axi_wdata_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(out[7:4]),
        .S({\axi_wdata[7]_i_2_n_0 ,\axi_wdata[7]_i_3_n_0 ,\axi_wdata[7]_i_4_n_0 ,\axi_wdata[7]_i_5_n_0 }));
  FDRE current_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(doWrite),
        .Q(current),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \dataReadArray[0][31]_i_1 
       (.I0(doSetup),
        .I1(m00_axi_aresetn),
        .O(\q_reg[0] ));
  FDRE next_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(current),
        .Q(next),
        .R(SR));
  LUT6 #(
    .INIT(64'h0010001011110010)) 
    \q[1]_i_1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(current),
        .I3(next),
        .I4(current_0),
        .I5(next_1),
        .O(doSetup));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramControllerLITE_0_0_Register
   (S,
    \m00_axi_araddr[30] ,
    m00_axi_awaddr,
    Q,
    \m00_axi_awaddr[30] ,
    \m00_axi_awaddr[30]_0 ,
    \m00_axi_awaddr[30]_1 ,
    \m00_axi_awaddr[30]_2 ,
    \m00_axi_awaddr[30]_3 ,
    \m00_axi_awaddr[30]_4 ,
    \m00_axi_awaddr[30]_5 ,
    m00_axi_araddr,
    \m00_axi_araddr[30]_0 ,
    \m00_axi_araddr[30]_1 ,
    \m00_axi_araddr[30]_2 ,
    \m00_axi_araddr[30]_3 ,
    \m00_axi_araddr[30]_4 ,
    \m00_axi_araddr[30]_5 ,
    \m00_axi_araddr[30]_6 ,
    axi_awaddr_reg,
    axi_araddr_reg,
    SR,
    doSetup,
    addressBase,
    m00_axi_aclk);
  output [1:0]S;
  output [1:0]\m00_axi_araddr[30] ;
  output [0:0]m00_axi_awaddr;
  output [30:0]Q;
  output [3:0]\m00_axi_awaddr[30] ;
  output [3:0]\m00_axi_awaddr[30]_0 ;
  output [3:0]\m00_axi_awaddr[30]_1 ;
  output [3:0]\m00_axi_awaddr[30]_2 ;
  output [3:0]\m00_axi_awaddr[30]_3 ;
  output [3:0]\m00_axi_awaddr[30]_4 ;
  output [3:0]\m00_axi_awaddr[30]_5 ;
  output [0:0]m00_axi_araddr;
  output [3:0]\m00_axi_araddr[30]_0 ;
  output [3:0]\m00_axi_araddr[30]_1 ;
  output [3:0]\m00_axi_araddr[30]_2 ;
  output [3:0]\m00_axi_araddr[30]_3 ;
  output [3:0]\m00_axi_araddr[30]_4 ;
  output [3:0]\m00_axi_araddr[30]_5 ;
  output [3:0]\m00_axi_araddr[30]_6 ;
  input [29:0]axi_awaddr_reg;
  input [29:0]axi_araddr_reg;
  input [0:0]SR;
  input doSetup;
  input [31:0]addressBase;
  input m00_axi_aclk;

  wire [30:0]Q;
  wire [1:0]S;
  wire [0:0]SR;
  wire [31:0]addressBase;
  wire [29:0]axi_araddr_reg;
  wire [29:0]axi_awaddr_reg;
  wire doSetup;
  wire m00_axi_aclk;
  wire [0:0]m00_axi_araddr;
  wire [1:0]\m00_axi_araddr[30] ;
  wire [3:0]\m00_axi_araddr[30]_0 ;
  wire [3:0]\m00_axi_araddr[30]_1 ;
  wire [3:0]\m00_axi_araddr[30]_2 ;
  wire [3:0]\m00_axi_araddr[30]_3 ;
  wire [3:0]\m00_axi_araddr[30]_4 ;
  wire [3:0]\m00_axi_araddr[30]_5 ;
  wire [3:0]\m00_axi_araddr[30]_6 ;
  wire [0:0]m00_axi_awaddr;
  wire [3:0]\m00_axi_awaddr[30] ;
  wire [3:0]\m00_axi_awaddr[30]_0 ;
  wire [3:0]\m00_axi_awaddr[30]_1 ;
  wire [3:0]\m00_axi_awaddr[30]_2 ;
  wire [3:0]\m00_axi_awaddr[30]_3 ;
  wire [3:0]\m00_axi_awaddr[30]_4 ;
  wire [3:0]\m00_axi_awaddr[30]_5 ;
  wire \q_reg_n_0_[31] ;

  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__0_i_1
       (.I0(Q[9]),
        .I1(axi_araddr_reg[7]),
        .O(\m00_axi_araddr[30]_1 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__0_i_2
       (.I0(Q[8]),
        .I1(axi_araddr_reg[6]),
        .O(\m00_axi_araddr[30]_1 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__0_i_3
       (.I0(Q[7]),
        .I1(axi_araddr_reg[5]),
        .O(\m00_axi_araddr[30]_1 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__0_i_4
       (.I0(Q[6]),
        .I1(axi_araddr_reg[4]),
        .O(\m00_axi_araddr[30]_1 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__1_i_1
       (.I0(Q[13]),
        .I1(axi_araddr_reg[11]),
        .O(\m00_axi_araddr[30]_2 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__1_i_2
       (.I0(Q[12]),
        .I1(axi_araddr_reg[10]),
        .O(\m00_axi_araddr[30]_2 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__1_i_3
       (.I0(Q[11]),
        .I1(axi_araddr_reg[9]),
        .O(\m00_axi_araddr[30]_2 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__1_i_4
       (.I0(Q[10]),
        .I1(axi_araddr_reg[8]),
        .O(\m00_axi_araddr[30]_2 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__2_i_1
       (.I0(Q[17]),
        .I1(axi_araddr_reg[15]),
        .O(\m00_axi_araddr[30]_3 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__2_i_2
       (.I0(Q[16]),
        .I1(axi_araddr_reg[14]),
        .O(\m00_axi_araddr[30]_3 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__2_i_3
       (.I0(Q[15]),
        .I1(axi_araddr_reg[13]),
        .O(\m00_axi_araddr[30]_3 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__2_i_4
       (.I0(Q[14]),
        .I1(axi_araddr_reg[12]),
        .O(\m00_axi_araddr[30]_3 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__3_i_1
       (.I0(Q[21]),
        .I1(axi_araddr_reg[19]),
        .O(\m00_axi_araddr[30]_4 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__3_i_2
       (.I0(Q[20]),
        .I1(axi_araddr_reg[18]),
        .O(\m00_axi_araddr[30]_4 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__3_i_3
       (.I0(Q[19]),
        .I1(axi_araddr_reg[17]),
        .O(\m00_axi_araddr[30]_4 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__3_i_4
       (.I0(Q[18]),
        .I1(axi_araddr_reg[16]),
        .O(\m00_axi_araddr[30]_4 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__4_i_1
       (.I0(Q[25]),
        .I1(axi_araddr_reg[23]),
        .O(\m00_axi_araddr[30]_5 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__4_i_2
       (.I0(Q[24]),
        .I1(axi_araddr_reg[22]),
        .O(\m00_axi_araddr[30]_5 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__4_i_3
       (.I0(Q[23]),
        .I1(axi_araddr_reg[21]),
        .O(\m00_axi_araddr[30]_5 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__4_i_4
       (.I0(Q[22]),
        .I1(axi_araddr_reg[20]),
        .O(\m00_axi_araddr[30]_5 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__5_i_1
       (.I0(Q[29]),
        .I1(axi_araddr_reg[27]),
        .O(\m00_axi_araddr[30]_6 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__5_i_2
       (.I0(Q[28]),
        .I1(axi_araddr_reg[26]),
        .O(\m00_axi_araddr[30]_6 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__5_i_3
       (.I0(Q[27]),
        .I1(axi_araddr_reg[25]),
        .O(\m00_axi_araddr[30]_6 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__5_i_4
       (.I0(Q[26]),
        .I1(axi_araddr_reg[24]),
        .O(\m00_axi_araddr[30]_6 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__6_i_1
       (.I0(\q_reg_n_0_[31] ),
        .I1(axi_araddr_reg[29]),
        .O(\m00_axi_araddr[30] [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry__6_i_2
       (.I0(Q[30]),
        .I1(axi_araddr_reg[28]),
        .O(\m00_axi_araddr[30] [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry_i_1
       (.I0(Q[5]),
        .I1(axi_araddr_reg[3]),
        .O(\m00_axi_araddr[30]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry_i_2
       (.I0(Q[4]),
        .I1(axi_araddr_reg[2]),
        .O(\m00_axi_araddr[30]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry_i_3
       (.I0(Q[3]),
        .I1(axi_araddr_reg[1]),
        .O(\m00_axi_araddr[30]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_ARADDR_carry_i_4
       (.I0(Q[2]),
        .I1(axi_araddr_reg[0]),
        .O(\m00_axi_araddr[30]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__0_i_1
       (.I0(Q[9]),
        .I1(axi_awaddr_reg[7]),
        .O(\m00_axi_awaddr[30]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__0_i_2
       (.I0(Q[8]),
        .I1(axi_awaddr_reg[6]),
        .O(\m00_axi_awaddr[30]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__0_i_3
       (.I0(Q[7]),
        .I1(axi_awaddr_reg[5]),
        .O(\m00_axi_awaddr[30]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__0_i_4
       (.I0(Q[6]),
        .I1(axi_awaddr_reg[4]),
        .O(\m00_axi_awaddr[30]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__1_i_1
       (.I0(Q[13]),
        .I1(axi_awaddr_reg[11]),
        .O(\m00_axi_awaddr[30]_1 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__1_i_2
       (.I0(Q[12]),
        .I1(axi_awaddr_reg[10]),
        .O(\m00_axi_awaddr[30]_1 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__1_i_3
       (.I0(Q[11]),
        .I1(axi_awaddr_reg[9]),
        .O(\m00_axi_awaddr[30]_1 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__1_i_4
       (.I0(Q[10]),
        .I1(axi_awaddr_reg[8]),
        .O(\m00_axi_awaddr[30]_1 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__2_i_1
       (.I0(Q[17]),
        .I1(axi_awaddr_reg[15]),
        .O(\m00_axi_awaddr[30]_2 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__2_i_2
       (.I0(Q[16]),
        .I1(axi_awaddr_reg[14]),
        .O(\m00_axi_awaddr[30]_2 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__2_i_3
       (.I0(Q[15]),
        .I1(axi_awaddr_reg[13]),
        .O(\m00_axi_awaddr[30]_2 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__2_i_4
       (.I0(Q[14]),
        .I1(axi_awaddr_reg[12]),
        .O(\m00_axi_awaddr[30]_2 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__3_i_1
       (.I0(Q[21]),
        .I1(axi_awaddr_reg[19]),
        .O(\m00_axi_awaddr[30]_3 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__3_i_2
       (.I0(Q[20]),
        .I1(axi_awaddr_reg[18]),
        .O(\m00_axi_awaddr[30]_3 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__3_i_3
       (.I0(Q[19]),
        .I1(axi_awaddr_reg[17]),
        .O(\m00_axi_awaddr[30]_3 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__3_i_4
       (.I0(Q[18]),
        .I1(axi_awaddr_reg[16]),
        .O(\m00_axi_awaddr[30]_3 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__4_i_1
       (.I0(Q[25]),
        .I1(axi_awaddr_reg[23]),
        .O(\m00_axi_awaddr[30]_4 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__4_i_2
       (.I0(Q[24]),
        .I1(axi_awaddr_reg[22]),
        .O(\m00_axi_awaddr[30]_4 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__4_i_3
       (.I0(Q[23]),
        .I1(axi_awaddr_reg[21]),
        .O(\m00_axi_awaddr[30]_4 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__4_i_4
       (.I0(Q[22]),
        .I1(axi_awaddr_reg[20]),
        .O(\m00_axi_awaddr[30]_4 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__5_i_1
       (.I0(Q[29]),
        .I1(axi_awaddr_reg[27]),
        .O(\m00_axi_awaddr[30]_5 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__5_i_2
       (.I0(Q[28]),
        .I1(axi_awaddr_reg[26]),
        .O(\m00_axi_awaddr[30]_5 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__5_i_3
       (.I0(Q[27]),
        .I1(axi_awaddr_reg[25]),
        .O(\m00_axi_awaddr[30]_5 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__5_i_4
       (.I0(Q[26]),
        .I1(axi_awaddr_reg[24]),
        .O(\m00_axi_awaddr[30]_5 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__6_i_1
       (.I0(\q_reg_n_0_[31] ),
        .I1(axi_awaddr_reg[29]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry__6_i_2
       (.I0(Q[30]),
        .I1(axi_awaddr_reg[28]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry_i_1
       (.I0(Q[5]),
        .I1(axi_awaddr_reg[3]),
        .O(\m00_axi_awaddr[30] [3]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry_i_2
       (.I0(Q[4]),
        .I1(axi_awaddr_reg[2]),
        .O(\m00_axi_awaddr[30] [2]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry_i_3
       (.I0(Q[3]),
        .I1(axi_awaddr_reg[1]),
        .O(\m00_axi_awaddr[30] [1]));
  LUT2 #(
    .INIT(4'h6)) 
    M_AXI_AWADDR_carry_i_4
       (.I0(Q[2]),
        .I1(axi_awaddr_reg[0]),
        .O(\m00_axi_awaddr[30] [0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axi_araddr[2]_INST_0 
       (.I0(Q[2]),
        .I1(axi_araddr_reg[0]),
        .O(m00_axi_araddr));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \m00_axi_awaddr[2]_INST_0 
       (.I0(Q[2]),
        .I1(axi_awaddr_reg[0]),
        .O(m00_axi_awaddr));
  FDRE \q_reg[0] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \q_reg[10] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE \q_reg[11] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE \q_reg[12] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE \q_reg[13] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE \q_reg[14] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE \q_reg[15] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE \q_reg[16] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE \q_reg[17] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE \q_reg[18] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE \q_reg[19] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE \q_reg[1] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \q_reg[20] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE \q_reg[21] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE \q_reg[22] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE \q_reg[23] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE \q_reg[24] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE \q_reg[25] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE \q_reg[26] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE \q_reg[27] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE \q_reg[28] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE \q_reg[29] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE \q_reg[2] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \q_reg[30] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE \q_reg[31] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[31]),
        .Q(\q_reg_n_0_[31] ),
        .R(SR));
  FDRE \q_reg[3] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE \q_reg[4] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \q_reg[5] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \q_reg[6] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \q_reg[7] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE \q_reg[8] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE \q_reg[9] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(addressBase[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramControllerLITE_0_0_Register_0
   (SR,
    Q,
    m00_axi_aresetn,
    doSetup,
    dataIn,
    m00_axi_aclk);
  output [0:0]SR;
  output [31:0]Q;
  input m00_axi_aresetn;
  input doSetup;
  input [31:0]dataIn;
  input m00_axi_aclk;

  wire [31:0]Q;
  wire [0:0]SR;
  wire [31:0]dataIn;
  wire doSetup;
  wire m00_axi_aclk;
  wire m00_axi_aresetn;

  FDRE \q_reg[0] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \q_reg[10] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE \q_reg[11] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE \q_reg[12] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE \q_reg[13] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE \q_reg[14] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE \q_reg[15] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE \q_reg[16] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE \q_reg[17] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE \q_reg[18] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE \q_reg[19] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE \q_reg[1] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \q_reg[20] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE \q_reg[21] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE \q_reg[22] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE \q_reg[23] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE \q_reg[24] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE \q_reg[25] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE \q_reg[26] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE \q_reg[27] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE \q_reg[28] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE \q_reg[29] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE \q_reg[2] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \q_reg[30] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE \q_reg[31] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[31]),
        .Q(Q[31]),
        .R(SR));
  FDRE \q_reg[3] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE \q_reg[4] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \q_reg[5] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \q_reg[6] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \q_reg[7] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE \q_reg[8] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE \q_reg[9] 
       (.C(m00_axi_aclk),
        .CE(doSetup),
        .D(dataIn[9]),
        .Q(Q[9]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    requestAccepted_i_1
       (.I0(m00_axi_aresetn),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramControllerLITE_0_0_Register__parameterized0
   (read_error_cumulative,
    \state_reg[1] ,
    axi_rready_reg,
    m00_axi_aclk);
  output read_error_cumulative;
  input \state_reg[1] ;
  input axi_rready_reg;
  input m00_axi_aclk;

  wire axi_rready_reg;
  wire m00_axi_aclk;
  wire read_error_cumulative;
  wire \state_reg[1] ;

  FDRE \q_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_rready_reg),
        .Q(read_error_cumulative),
        .R(\state_reg[1] ));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramControllerLITE_0_0_Register__parameterized0_1
   (write_error_cumulative,
    \state_reg[1] ,
    axi_bready_reg,
    m00_axi_aclk);
  output write_error_cumulative;
  input \state_reg[1] ;
  input axi_bready_reg;
  input m00_axi_aclk;

  wire axi_bready_reg;
  wire m00_axi_aclk;
  wire \state_reg[1] ;
  wire write_error_cumulative;

  FDRE \q_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_bready_reg),
        .Q(write_error_cumulative),
        .R(\state_reg[1] ));
endmodule

(* ORIG_REF_NAME = "dramControllerLITE_v1_0" *) 
module design_1_dramControllerLITE_0_0_dramControllerLITE_v1_0
   (M_AXI_BREADY,
    m00_axi_wvalid,
    m00_axi_rready,
    dataOut,
    m00_axi_araddr,
    m00_axi_wdata,
    m00_axi_arvalid,
    m00_axi_awvalid,
    m00_axi_awaddr,
    requestAccepted,
    m00_axi_error,
    requestDone,
    m00_axi_aresetn,
    m00_axi_bvalid,
    m00_axi_wready,
    dataIn,
    m00_axi_aclk,
    doRead,
    doWrite,
    m00_axi_rvalid,
    m00_axi_rdata,
    addressBase,
    m00_axi_arready,
    m00_axi_awready,
    m00_axi_rresp,
    m00_axi_bresp);
  output M_AXI_BREADY;
  output m00_axi_wvalid;
  output m00_axi_rready;
  output [31:0]dataOut;
  output [31:0]m00_axi_araddr;
  output [31:0]m00_axi_wdata;
  output m00_axi_arvalid;
  output m00_axi_awvalid;
  output [29:0]m00_axi_awaddr;
  output requestAccepted;
  output m00_axi_error;
  output requestDone;
  input m00_axi_aresetn;
  input m00_axi_bvalid;
  input m00_axi_wready;
  input [31:0]dataIn;
  input m00_axi_aclk;
  input doRead;
  input doWrite;
  input m00_axi_rvalid;
  input [31:0]m00_axi_rdata;
  input [31:0]addressBase;
  input m00_axi_arready;
  input m00_axi_awready;
  input [0:0]m00_axi_rresp;
  input [0:0]m00_axi_bresp;

  wire ERROR_i_1_n_0;
  wire M_AXI_BREADY;
  wire [31:0]addressBase;
  wire axi_arvalid_i_1_n_0;
  wire axi_awvalid_i_1_n_0;
  wire axi_wvalid_i_1_n_0;
  wire [31:0]dataIn;
  wire [31:0]dataOut;
  wire doRead;
  wire doWrite;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_11;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_123;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_22;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_3;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_4;
  wire dramControllerLITE_v1_0_M00_AXI_inst_n_8;
  wire last_read;
  wire last_read_i_1_n_0;
  wire last_write;
  wire last_write_i_1_n_0;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [29:0]m00_axi_awaddr;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire [0:0]m00_axi_bresp;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire [31:0]m00_axi_rdata;
  wire m00_axi_rready;
  wire [0:0]m00_axi_rresp;
  wire m00_axi_rvalid;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  wire \q[0]_i_1__0_n_0 ;
  wire \q[0]_i_1_n_0 ;
  wire read_error_cumulative;
  wire [2:0]read_index;
  wire read_issued_i_1_n_0;
  wire reads_done;
  wire reads_done_i_1_n_0;
  wire requestAccepted;
  wire requestAccepted_i_2_n_0;
  wire requestDone;
  wire requestDone_i_1_n_0;
  wire start_single_read0;
  wire start_single_read_i_1_n_0;
  wire start_single_write0;
  wire start_single_write_i_1_n_0;
  wire [1:0]state;
  wire write_error_cumulative;
  wire [2:0]write_index;
  wire write_issued_i_1_n_0;
  wire writes_done;
  wire writes_done_i_1_n_0;

  LUT6 #(
    .INIT(64'hF888FFFFF8880000)) 
    ERROR_i_1
       (.I0(write_error_cumulative),
        .I1(state[0]),
        .I2(read_error_cumulative),
        .I3(state[1]),
        .I4(dramControllerLITE_v1_0_M00_AXI_inst_n_123),
        .I5(m00_axi_error),
        .O(ERROR_i_1_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    axi_arvalid_i_1
       (.I0(dramControllerLITE_v1_0_M00_AXI_inst_n_4),
        .I1(m00_axi_arready),
        .I2(m00_axi_arvalid),
        .O(axi_arvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    axi_awvalid_i_1
       (.I0(dramControllerLITE_v1_0_M00_AXI_inst_n_8),
        .I1(m00_axi_awvalid),
        .I2(m00_axi_awready),
        .O(axi_awvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    axi_wvalid_i_1
       (.I0(dramControllerLITE_v1_0_M00_AXI_inst_n_8),
        .I1(m00_axi_wready),
        .I2(m00_axi_wvalid),
        .O(axi_wvalid_i_1_n_0));
  design_1_dramControllerLITE_0_0_dramControllerLITE_v1_0_M00_AXI dramControllerLITE_v1_0_M00_AXI_inst
       (.ERROR_reg_0(dramControllerLITE_v1_0_M00_AXI_inst_n_123),
        .addressBase(addressBase),
        .axi_bready_reg_0(\q[0]_i_1__0_n_0 ),
        .axi_rready_reg_0(\q[0]_i_1_n_0 ),
        .dataIn(dataIn),
        .dataOut(dataOut),
        .doRead(doRead),
        .doWrite(doWrite),
        .last_read(last_read),
        .last_read_reg_0(reads_done_i_1_n_0),
        .last_write(last_write),
        .last_write_reg_0(writes_done_i_1_n_0),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_awaddr(m00_axi_awaddr),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_bready(M_AXI_BREADY),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_error(m00_axi_error),
        .m00_axi_rdata(m00_axi_rdata),
        .m00_axi_rready(m00_axi_rready),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_wvalid(m00_axi_wvalid),
        .\q_reg[0] (ERROR_i_1_n_0),
        .read_error_cumulative(read_error_cumulative),
        .read_index(read_index),
        .\read_index_reg[2]_0 (dramControllerLITE_v1_0_M00_AXI_inst_n_4),
        .\read_index_reg[2]_1 (last_read_i_1_n_0),
        .read_issued_reg_0(dramControllerLITE_v1_0_M00_AXI_inst_n_3),
        .reads_done(reads_done),
        .requestAccepted(requestAccepted),
        .requestDone(requestDone),
        .start_single_read0(start_single_read0),
        .start_single_read_reg_0(axi_arvalid_i_1_n_0),
        .start_single_write0(start_single_write0),
        .start_single_write_reg_0(axi_awvalid_i_1_n_0),
        .start_single_write_reg_1(axi_wvalid_i_1_n_0),
        .state(state),
        .\state_reg[0]_0 (read_issued_i_1_n_0),
        .\state_reg[0]_1 (start_single_read_i_1_n_0),
        .\state_reg[1]_0 (dramControllerLITE_v1_0_M00_AXI_inst_n_22),
        .\state_reg[1]_1 (requestAccepted_i_2_n_0),
        .\state_reg[1]_2 (start_single_write_i_1_n_0),
        .\state_reg[1]_3 (write_issued_i_1_n_0),
        .write_error_cumulative(write_error_cumulative),
        .write_index(write_index),
        .\write_index_reg[2]_0 (dramControllerLITE_v1_0_M00_AXI_inst_n_8),
        .\write_index_reg[2]_1 (last_write_i_1_n_0),
        .write_issued_reg_0(dramControllerLITE_v1_0_M00_AXI_inst_n_11),
        .writes_done(writes_done),
        .writes_done_reg_0(requestDone_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFF0008)) 
    last_read_i_1
       (.I0(read_index[2]),
        .I1(m00_axi_arready),
        .I2(read_index[1]),
        .I3(read_index[0]),
        .I4(last_read),
        .O(last_read_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFF0008)) 
    last_write_i_1
       (.I0(write_index[2]),
        .I1(m00_axi_awready),
        .I2(write_index[1]),
        .I3(write_index[0]),
        .I4(last_write),
        .O(last_write_i_1_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    \q[0]_i_1 
       (.I0(m00_axi_rvalid),
        .I1(m00_axi_rready),
        .I2(m00_axi_rresp),
        .I3(read_error_cumulative),
        .O(\q[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \q[0]_i_1__0 
       (.I0(m00_axi_bresp),
        .I1(M_AXI_BREADY),
        .I2(m00_axi_bvalid),
        .I3(write_error_cumulative),
        .O(\q[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFBFFFF00500000)) 
    read_issued_i_1
       (.I0(state[0]),
        .I1(m00_axi_rready),
        .I2(start_single_read0),
        .I3(reads_done),
        .I4(state[1]),
        .I5(dramControllerLITE_v1_0_M00_AXI_inst_n_3),
        .O(read_issued_i_1_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    reads_done_i_1
       (.I0(last_read),
        .I1(m00_axi_rvalid),
        .I2(m00_axi_rready),
        .I3(reads_done),
        .O(reads_done_i_1_n_0));
  LUT4 #(
    .INIT(16'hC302)) 
    requestAccepted_i_2
       (.I0(dramControllerLITE_v1_0_M00_AXI_inst_n_22),
        .I1(state[1]),
        .I2(state[0]),
        .I3(requestAccepted),
        .O(requestAccepted_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFCCFFFF30883088)) 
    requestDone_i_1
       (.I0(writes_done),
        .I1(state[0]),
        .I2(reads_done),
        .I3(state[1]),
        .I4(dramControllerLITE_v1_0_M00_AXI_inst_n_22),
        .I5(requestDone),
        .O(requestDone_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00500000)) 
    start_single_read_i_1
       (.I0(state[0]),
        .I1(m00_axi_rready),
        .I2(start_single_read0),
        .I3(reads_done),
        .I4(state[1]),
        .I5(dramControllerLITE_v1_0_M00_AXI_inst_n_4),
        .O(start_single_read_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB00004400)) 
    start_single_write_i_1
       (.I0(state[1]),
        .I1(state[0]),
        .I2(M_AXI_BREADY),
        .I3(start_single_write0),
        .I4(writes_done),
        .I5(dramControllerLITE_v1_0_M00_AXI_inst_n_8),
        .O(start_single_write_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBF00004400)) 
    write_issued_i_1
       (.I0(state[1]),
        .I1(state[0]),
        .I2(M_AXI_BREADY),
        .I3(start_single_write0),
        .I4(writes_done),
        .I5(dramControllerLITE_v1_0_M00_AXI_inst_n_11),
        .O(write_issued_i_1_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    writes_done_i_1
       (.I0(last_write),
        .I1(M_AXI_BREADY),
        .I2(m00_axi_bvalid),
        .I3(writes_done),
        .O(writes_done_i_1_n_0));
endmodule

(* ORIG_REF_NAME = "dramControllerLITE_v1_0_M00_AXI" *) 
module design_1_dramControllerLITE_0_0_dramControllerLITE_v1_0_M00_AXI
   (m00_axi_bready,
    last_read,
    reads_done,
    read_issued_reg_0,
    \read_index_reg[2]_0 ,
    m00_axi_arvalid,
    requestAccepted,
    writes_done,
    \write_index_reg[2]_0 ,
    m00_axi_awvalid,
    m00_axi_wvalid,
    write_issued_reg_0,
    last_write,
    requestDone,
    m00_axi_error,
    write_error_cumulative,
    read_error_cumulative,
    write_index,
    state,
    \state_reg[1]_0 ,
    m00_axi_rready,
    read_index,
    dataOut,
    m00_axi_araddr,
    m00_axi_wdata,
    ERROR_reg_0,
    start_single_read0,
    start_single_write0,
    m00_axi_awaddr,
    doRead,
    m00_axi_aclk,
    doWrite,
    \read_index_reg[2]_1 ,
    last_read_reg_0,
    \state_reg[0]_0 ,
    \state_reg[0]_1 ,
    start_single_read_reg_0,
    \state_reg[1]_1 ,
    last_write_reg_0,
    \state_reg[1]_2 ,
    start_single_write_reg_0,
    start_single_write_reg_1,
    \state_reg[1]_3 ,
    \write_index_reg[2]_1 ,
    writes_done_reg_0,
    \q_reg[0] ,
    axi_bready_reg_0,
    axi_rready_reg_0,
    m00_axi_aresetn,
    m00_axi_bvalid,
    m00_axi_wready,
    dataIn,
    m00_axi_rvalid,
    m00_axi_rdata,
    addressBase,
    m00_axi_arready,
    m00_axi_awready);
  output m00_axi_bready;
  output last_read;
  output reads_done;
  output read_issued_reg_0;
  output \read_index_reg[2]_0 ;
  output m00_axi_arvalid;
  output requestAccepted;
  output writes_done;
  output \write_index_reg[2]_0 ;
  output m00_axi_awvalid;
  output m00_axi_wvalid;
  output write_issued_reg_0;
  output last_write;
  output requestDone;
  output m00_axi_error;
  output write_error_cumulative;
  output read_error_cumulative;
  output [2:0]write_index;
  output [1:0]state;
  output \state_reg[1]_0 ;
  output m00_axi_rready;
  output [2:0]read_index;
  output [31:0]dataOut;
  output [31:0]m00_axi_araddr;
  output [31:0]m00_axi_wdata;
  output ERROR_reg_0;
  output start_single_read0;
  output start_single_write0;
  output [29:0]m00_axi_awaddr;
  input doRead;
  input m00_axi_aclk;
  input doWrite;
  input \read_index_reg[2]_1 ;
  input last_read_reg_0;
  input \state_reg[0]_0 ;
  input \state_reg[0]_1 ;
  input start_single_read_reg_0;
  input \state_reg[1]_1 ;
  input last_write_reg_0;
  input \state_reg[1]_2 ;
  input start_single_write_reg_0;
  input start_single_write_reg_1;
  input \state_reg[1]_3 ;
  input \write_index_reg[2]_1 ;
  input writes_done_reg_0;
  input \q_reg[0] ;
  input axi_bready_reg_0;
  input axi_rready_reg_0;
  input m00_axi_aresetn;
  input m00_axi_bvalid;
  input m00_axi_wready;
  input [31:0]dataIn;
  input m00_axi_rvalid;
  input [31:0]m00_axi_rdata;
  input [31:0]addressBase;
  input m00_axi_arready;
  input m00_axi_awready;

  wire ERROR_reg_0;
  wire M_AXI_ARADDR_carry__0_n_0;
  wire M_AXI_ARADDR_carry__0_n_1;
  wire M_AXI_ARADDR_carry__0_n_2;
  wire M_AXI_ARADDR_carry__0_n_3;
  wire M_AXI_ARADDR_carry__1_n_0;
  wire M_AXI_ARADDR_carry__1_n_1;
  wire M_AXI_ARADDR_carry__1_n_2;
  wire M_AXI_ARADDR_carry__1_n_3;
  wire M_AXI_ARADDR_carry__2_n_0;
  wire M_AXI_ARADDR_carry__2_n_1;
  wire M_AXI_ARADDR_carry__2_n_2;
  wire M_AXI_ARADDR_carry__2_n_3;
  wire M_AXI_ARADDR_carry__3_n_0;
  wire M_AXI_ARADDR_carry__3_n_1;
  wire M_AXI_ARADDR_carry__3_n_2;
  wire M_AXI_ARADDR_carry__3_n_3;
  wire M_AXI_ARADDR_carry__4_n_0;
  wire M_AXI_ARADDR_carry__4_n_1;
  wire M_AXI_ARADDR_carry__4_n_2;
  wire M_AXI_ARADDR_carry__4_n_3;
  wire M_AXI_ARADDR_carry__5_n_0;
  wire M_AXI_ARADDR_carry__5_n_1;
  wire M_AXI_ARADDR_carry__5_n_2;
  wire M_AXI_ARADDR_carry__5_n_3;
  wire M_AXI_ARADDR_carry__6_n_3;
  wire M_AXI_ARADDR_carry_n_0;
  wire M_AXI_ARADDR_carry_n_1;
  wire M_AXI_ARADDR_carry_n_2;
  wire M_AXI_ARADDR_carry_n_3;
  wire M_AXI_AWADDR_carry__0_n_0;
  wire M_AXI_AWADDR_carry__0_n_1;
  wire M_AXI_AWADDR_carry__0_n_2;
  wire M_AXI_AWADDR_carry__0_n_3;
  wire M_AXI_AWADDR_carry__1_n_0;
  wire M_AXI_AWADDR_carry__1_n_1;
  wire M_AXI_AWADDR_carry__1_n_2;
  wire M_AXI_AWADDR_carry__1_n_3;
  wire M_AXI_AWADDR_carry__2_n_0;
  wire M_AXI_AWADDR_carry__2_n_1;
  wire M_AXI_AWADDR_carry__2_n_2;
  wire M_AXI_AWADDR_carry__2_n_3;
  wire M_AXI_AWADDR_carry__3_n_0;
  wire M_AXI_AWADDR_carry__3_n_1;
  wire M_AXI_AWADDR_carry__3_n_2;
  wire M_AXI_AWADDR_carry__3_n_3;
  wire M_AXI_AWADDR_carry__4_n_0;
  wire M_AXI_AWADDR_carry__4_n_1;
  wire M_AXI_AWADDR_carry__4_n_2;
  wire M_AXI_AWADDR_carry__4_n_3;
  wire M_AXI_AWADDR_carry__5_n_0;
  wire M_AXI_AWADDR_carry__5_n_1;
  wire M_AXI_AWADDR_carry__5_n_2;
  wire M_AXI_AWADDR_carry__5_n_3;
  wire M_AXI_AWADDR_carry__6_n_3;
  wire M_AXI_AWADDR_carry_n_0;
  wire M_AXI_AWADDR_carry_n_1;
  wire M_AXI_AWADDR_carry_n_2;
  wire M_AXI_AWADDR_carry_n_3;
  wire [31:0]addressBase;
  wire address_reg_n_0;
  wire address_reg_n_1;
  wire address_reg_n_10;
  wire address_reg_n_11;
  wire address_reg_n_12;
  wire address_reg_n_13;
  wire address_reg_n_14;
  wire address_reg_n_15;
  wire address_reg_n_16;
  wire address_reg_n_17;
  wire address_reg_n_18;
  wire address_reg_n_19;
  wire address_reg_n_2;
  wire address_reg_n_20;
  wire address_reg_n_21;
  wire address_reg_n_22;
  wire address_reg_n_23;
  wire address_reg_n_24;
  wire address_reg_n_25;
  wire address_reg_n_26;
  wire address_reg_n_27;
  wire address_reg_n_28;
  wire address_reg_n_29;
  wire address_reg_n_3;
  wire address_reg_n_30;
  wire address_reg_n_31;
  wire address_reg_n_32;
  wire address_reg_n_33;
  wire address_reg_n_36;
  wire address_reg_n_37;
  wire address_reg_n_38;
  wire address_reg_n_39;
  wire address_reg_n_40;
  wire address_reg_n_41;
  wire address_reg_n_42;
  wire address_reg_n_43;
  wire address_reg_n_44;
  wire address_reg_n_45;
  wire address_reg_n_46;
  wire address_reg_n_47;
  wire address_reg_n_48;
  wire address_reg_n_49;
  wire address_reg_n_5;
  wire address_reg_n_50;
  wire address_reg_n_51;
  wire address_reg_n_52;
  wire address_reg_n_53;
  wire address_reg_n_54;
  wire address_reg_n_55;
  wire address_reg_n_56;
  wire address_reg_n_57;
  wire address_reg_n_58;
  wire address_reg_n_59;
  wire address_reg_n_6;
  wire address_reg_n_60;
  wire address_reg_n_61;
  wire address_reg_n_62;
  wire address_reg_n_63;
  wire address_reg_n_65;
  wire address_reg_n_66;
  wire address_reg_n_67;
  wire address_reg_n_68;
  wire address_reg_n_69;
  wire address_reg_n_7;
  wire address_reg_n_70;
  wire address_reg_n_71;
  wire address_reg_n_72;
  wire address_reg_n_73;
  wire address_reg_n_74;
  wire address_reg_n_75;
  wire address_reg_n_76;
  wire address_reg_n_77;
  wire address_reg_n_78;
  wire address_reg_n_79;
  wire address_reg_n_8;
  wire address_reg_n_80;
  wire address_reg_n_81;
  wire address_reg_n_82;
  wire address_reg_n_83;
  wire address_reg_n_84;
  wire address_reg_n_85;
  wire address_reg_n_86;
  wire address_reg_n_87;
  wire address_reg_n_88;
  wire address_reg_n_89;
  wire address_reg_n_9;
  wire address_reg_n_90;
  wire address_reg_n_91;
  wire address_reg_n_92;
  wire \axi_araddr[10]_i_2_n_0 ;
  wire \axi_araddr[10]_i_3_n_0 ;
  wire \axi_araddr[10]_i_4_n_0 ;
  wire \axi_araddr[10]_i_5_n_0 ;
  wire \axi_araddr[14]_i_2_n_0 ;
  wire \axi_araddr[14]_i_3_n_0 ;
  wire \axi_araddr[14]_i_4_n_0 ;
  wire \axi_araddr[14]_i_5_n_0 ;
  wire \axi_araddr[18]_i_2_n_0 ;
  wire \axi_araddr[18]_i_3_n_0 ;
  wire \axi_araddr[18]_i_4_n_0 ;
  wire \axi_araddr[18]_i_5_n_0 ;
  wire \axi_araddr[22]_i_2_n_0 ;
  wire \axi_araddr[22]_i_3_n_0 ;
  wire \axi_araddr[22]_i_4_n_0 ;
  wire \axi_araddr[22]_i_5_n_0 ;
  wire \axi_araddr[26]_i_2_n_0 ;
  wire \axi_araddr[26]_i_3_n_0 ;
  wire \axi_araddr[26]_i_4_n_0 ;
  wire \axi_araddr[26]_i_5_n_0 ;
  wire \axi_araddr[2]_i_3_n_0 ;
  wire \axi_araddr[2]_i_4_n_0 ;
  wire \axi_araddr[2]_i_5_n_0 ;
  wire \axi_araddr[2]_i_6_n_0 ;
  wire \axi_araddr[30]_i_2_n_0 ;
  wire \axi_araddr[30]_i_3_n_0 ;
  wire \axi_araddr[6]_i_2_n_0 ;
  wire \axi_araddr[6]_i_3_n_0 ;
  wire \axi_araddr[6]_i_4_n_0 ;
  wire \axi_araddr[6]_i_5_n_0 ;
  wire [31:2]axi_araddr_reg;
  wire \axi_araddr_reg[10]_i_1_n_0 ;
  wire \axi_araddr_reg[10]_i_1_n_1 ;
  wire \axi_araddr_reg[10]_i_1_n_2 ;
  wire \axi_araddr_reg[10]_i_1_n_3 ;
  wire \axi_araddr_reg[10]_i_1_n_4 ;
  wire \axi_araddr_reg[10]_i_1_n_5 ;
  wire \axi_araddr_reg[10]_i_1_n_6 ;
  wire \axi_araddr_reg[10]_i_1_n_7 ;
  wire \axi_araddr_reg[14]_i_1_n_0 ;
  wire \axi_araddr_reg[14]_i_1_n_1 ;
  wire \axi_araddr_reg[14]_i_1_n_2 ;
  wire \axi_araddr_reg[14]_i_1_n_3 ;
  wire \axi_araddr_reg[14]_i_1_n_4 ;
  wire \axi_araddr_reg[14]_i_1_n_5 ;
  wire \axi_araddr_reg[14]_i_1_n_6 ;
  wire \axi_araddr_reg[14]_i_1_n_7 ;
  wire \axi_araddr_reg[18]_i_1_n_0 ;
  wire \axi_araddr_reg[18]_i_1_n_1 ;
  wire \axi_araddr_reg[18]_i_1_n_2 ;
  wire \axi_araddr_reg[18]_i_1_n_3 ;
  wire \axi_araddr_reg[18]_i_1_n_4 ;
  wire \axi_araddr_reg[18]_i_1_n_5 ;
  wire \axi_araddr_reg[18]_i_1_n_6 ;
  wire \axi_araddr_reg[18]_i_1_n_7 ;
  wire \axi_araddr_reg[22]_i_1_n_0 ;
  wire \axi_araddr_reg[22]_i_1_n_1 ;
  wire \axi_araddr_reg[22]_i_1_n_2 ;
  wire \axi_araddr_reg[22]_i_1_n_3 ;
  wire \axi_araddr_reg[22]_i_1_n_4 ;
  wire \axi_araddr_reg[22]_i_1_n_5 ;
  wire \axi_araddr_reg[22]_i_1_n_6 ;
  wire \axi_araddr_reg[22]_i_1_n_7 ;
  wire \axi_araddr_reg[26]_i_1_n_0 ;
  wire \axi_araddr_reg[26]_i_1_n_1 ;
  wire \axi_araddr_reg[26]_i_1_n_2 ;
  wire \axi_araddr_reg[26]_i_1_n_3 ;
  wire \axi_araddr_reg[26]_i_1_n_4 ;
  wire \axi_araddr_reg[26]_i_1_n_5 ;
  wire \axi_araddr_reg[26]_i_1_n_6 ;
  wire \axi_araddr_reg[26]_i_1_n_7 ;
  wire \axi_araddr_reg[2]_i_2_n_0 ;
  wire \axi_araddr_reg[2]_i_2_n_1 ;
  wire \axi_araddr_reg[2]_i_2_n_2 ;
  wire \axi_araddr_reg[2]_i_2_n_3 ;
  wire \axi_araddr_reg[2]_i_2_n_4 ;
  wire \axi_araddr_reg[2]_i_2_n_5 ;
  wire \axi_araddr_reg[2]_i_2_n_6 ;
  wire \axi_araddr_reg[2]_i_2_n_7 ;
  wire \axi_araddr_reg[30]_i_1_n_3 ;
  wire \axi_araddr_reg[30]_i_1_n_6 ;
  wire \axi_araddr_reg[30]_i_1_n_7 ;
  wire \axi_araddr_reg[6]_i_1_n_0 ;
  wire \axi_araddr_reg[6]_i_1_n_1 ;
  wire \axi_araddr_reg[6]_i_1_n_2 ;
  wire \axi_araddr_reg[6]_i_1_n_3 ;
  wire \axi_araddr_reg[6]_i_1_n_4 ;
  wire \axi_araddr_reg[6]_i_1_n_5 ;
  wire \axi_araddr_reg[6]_i_1_n_6 ;
  wire \axi_araddr_reg[6]_i_1_n_7 ;
  wire axi_arvalid0;
  wire axi_awaddr0;
  wire \axi_awaddr[10]_i_2_n_0 ;
  wire \axi_awaddr[10]_i_3_n_0 ;
  wire \axi_awaddr[10]_i_4_n_0 ;
  wire \axi_awaddr[10]_i_5_n_0 ;
  wire \axi_awaddr[14]_i_2_n_0 ;
  wire \axi_awaddr[14]_i_3_n_0 ;
  wire \axi_awaddr[14]_i_4_n_0 ;
  wire \axi_awaddr[14]_i_5_n_0 ;
  wire \axi_awaddr[18]_i_2_n_0 ;
  wire \axi_awaddr[18]_i_3_n_0 ;
  wire \axi_awaddr[18]_i_4_n_0 ;
  wire \axi_awaddr[18]_i_5_n_0 ;
  wire \axi_awaddr[22]_i_2_n_0 ;
  wire \axi_awaddr[22]_i_3_n_0 ;
  wire \axi_awaddr[22]_i_4_n_0 ;
  wire \axi_awaddr[22]_i_5_n_0 ;
  wire \axi_awaddr[26]_i_2_n_0 ;
  wire \axi_awaddr[26]_i_3_n_0 ;
  wire \axi_awaddr[26]_i_4_n_0 ;
  wire \axi_awaddr[26]_i_5_n_0 ;
  wire \axi_awaddr[2]_i_3_n_0 ;
  wire \axi_awaddr[2]_i_4_n_0 ;
  wire \axi_awaddr[2]_i_5_n_0 ;
  wire \axi_awaddr[2]_i_6_n_0 ;
  wire \axi_awaddr[30]_i_2_n_0 ;
  wire \axi_awaddr[30]_i_3_n_0 ;
  wire \axi_awaddr[6]_i_2_n_0 ;
  wire \axi_awaddr[6]_i_3_n_0 ;
  wire \axi_awaddr[6]_i_4_n_0 ;
  wire \axi_awaddr[6]_i_5_n_0 ;
  wire [31:2]axi_awaddr_reg;
  wire \axi_awaddr_reg[10]_i_1_n_0 ;
  wire \axi_awaddr_reg[10]_i_1_n_1 ;
  wire \axi_awaddr_reg[10]_i_1_n_2 ;
  wire \axi_awaddr_reg[10]_i_1_n_3 ;
  wire \axi_awaddr_reg[10]_i_1_n_4 ;
  wire \axi_awaddr_reg[10]_i_1_n_5 ;
  wire \axi_awaddr_reg[10]_i_1_n_6 ;
  wire \axi_awaddr_reg[10]_i_1_n_7 ;
  wire \axi_awaddr_reg[14]_i_1_n_0 ;
  wire \axi_awaddr_reg[14]_i_1_n_1 ;
  wire \axi_awaddr_reg[14]_i_1_n_2 ;
  wire \axi_awaddr_reg[14]_i_1_n_3 ;
  wire \axi_awaddr_reg[14]_i_1_n_4 ;
  wire \axi_awaddr_reg[14]_i_1_n_5 ;
  wire \axi_awaddr_reg[14]_i_1_n_6 ;
  wire \axi_awaddr_reg[14]_i_1_n_7 ;
  wire \axi_awaddr_reg[18]_i_1_n_0 ;
  wire \axi_awaddr_reg[18]_i_1_n_1 ;
  wire \axi_awaddr_reg[18]_i_1_n_2 ;
  wire \axi_awaddr_reg[18]_i_1_n_3 ;
  wire \axi_awaddr_reg[18]_i_1_n_4 ;
  wire \axi_awaddr_reg[18]_i_1_n_5 ;
  wire \axi_awaddr_reg[18]_i_1_n_6 ;
  wire \axi_awaddr_reg[18]_i_1_n_7 ;
  wire \axi_awaddr_reg[22]_i_1_n_0 ;
  wire \axi_awaddr_reg[22]_i_1_n_1 ;
  wire \axi_awaddr_reg[22]_i_1_n_2 ;
  wire \axi_awaddr_reg[22]_i_1_n_3 ;
  wire \axi_awaddr_reg[22]_i_1_n_4 ;
  wire \axi_awaddr_reg[22]_i_1_n_5 ;
  wire \axi_awaddr_reg[22]_i_1_n_6 ;
  wire \axi_awaddr_reg[22]_i_1_n_7 ;
  wire \axi_awaddr_reg[26]_i_1_n_0 ;
  wire \axi_awaddr_reg[26]_i_1_n_1 ;
  wire \axi_awaddr_reg[26]_i_1_n_2 ;
  wire \axi_awaddr_reg[26]_i_1_n_3 ;
  wire \axi_awaddr_reg[26]_i_1_n_4 ;
  wire \axi_awaddr_reg[26]_i_1_n_5 ;
  wire \axi_awaddr_reg[26]_i_1_n_6 ;
  wire \axi_awaddr_reg[26]_i_1_n_7 ;
  wire \axi_awaddr_reg[2]_i_2_n_0 ;
  wire \axi_awaddr_reg[2]_i_2_n_1 ;
  wire \axi_awaddr_reg[2]_i_2_n_2 ;
  wire \axi_awaddr_reg[2]_i_2_n_3 ;
  wire \axi_awaddr_reg[2]_i_2_n_4 ;
  wire \axi_awaddr_reg[2]_i_2_n_5 ;
  wire \axi_awaddr_reg[2]_i_2_n_6 ;
  wire \axi_awaddr_reg[2]_i_2_n_7 ;
  wire \axi_awaddr_reg[30]_i_1_n_3 ;
  wire \axi_awaddr_reg[30]_i_1_n_6 ;
  wire \axi_awaddr_reg[30]_i_1_n_7 ;
  wire \axi_awaddr_reg[6]_i_1_n_0 ;
  wire \axi_awaddr_reg[6]_i_1_n_1 ;
  wire \axi_awaddr_reg[6]_i_1_n_2 ;
  wire \axi_awaddr_reg[6]_i_1_n_3 ;
  wire \axi_awaddr_reg[6]_i_1_n_4 ;
  wire \axi_awaddr_reg[6]_i_1_n_5 ;
  wire \axi_awaddr_reg[6]_i_1_n_6 ;
  wire \axi_awaddr_reg[6]_i_1_n_7 ;
  wire axi_bready_i_1_n_0;
  wire axi_bready_reg_0;
  wire axi_rready_i_1_n_0;
  wire axi_rready_reg_0;
  wire current;
  wire current_1;
  wire [31:0]dataIn;
  wire [31:0]dataOut;
  wire dataReadArray;
  wire data_reg_n_0;
  wire doRead;
  wire doSetup;
  wire doWrite;
  wire last_read;
  wire last_read_reg_0;
  wire last_write;
  wire last_write_reg_0;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [29:0]m00_axi_awaddr;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire m00_axi_bready;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire [31:0]m00_axi_rdata;
  wire m00_axi_rready;
  wire m00_axi_rvalid;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  wire next;
  wire next_0;
  wire \q_reg[0] ;
  wire readTrigger_n_2;
  wire readTrigger_n_4;
  wire read_error_cumulative;
  wire [2:0]read_index;
  wire \read_index[0]_i_1_n_0 ;
  wire \read_index[1]_i_1_n_0 ;
  wire \read_index[2]_i_1_n_0 ;
  wire \read_index_reg[2]_0 ;
  wire \read_index_reg[2]_1 ;
  wire read_issued_reg_0;
  wire reads_done;
  wire requestAccepted;
  wire requestDone;
  wire [31:0]savedDataIn;
  wire start_single_read0;
  wire start_single_read_reg_0;
  wire start_single_write0;
  wire start_single_write_reg_0;
  wire start_single_write_reg_1;
  wire [1:0]state;
  wire \state_reg[0]_0 ;
  wire \state_reg[0]_1 ;
  wire \state_reg[1]_0 ;
  wire \state_reg[1]_1 ;
  wire \state_reg[1]_2 ;
  wire \state_reg[1]_3 ;
  wire writeTrigger_n_10;
  wire writeTrigger_n_11;
  wire writeTrigger_n_12;
  wire writeTrigger_n_13;
  wire writeTrigger_n_14;
  wire writeTrigger_n_15;
  wire writeTrigger_n_16;
  wire writeTrigger_n_17;
  wire writeTrigger_n_18;
  wire writeTrigger_n_19;
  wire writeTrigger_n_2;
  wire writeTrigger_n_20;
  wire writeTrigger_n_21;
  wire writeTrigger_n_22;
  wire writeTrigger_n_23;
  wire writeTrigger_n_24;
  wire writeTrigger_n_25;
  wire writeTrigger_n_26;
  wire writeTrigger_n_27;
  wire writeTrigger_n_28;
  wire writeTrigger_n_29;
  wire writeTrigger_n_30;
  wire writeTrigger_n_31;
  wire writeTrigger_n_32;
  wire writeTrigger_n_33;
  wire writeTrigger_n_34;
  wire writeTrigger_n_35;
  wire writeTrigger_n_36;
  wire writeTrigger_n_4;
  wire writeTrigger_n_5;
  wire writeTrigger_n_6;
  wire writeTrigger_n_7;
  wire writeTrigger_n_8;
  wire writeTrigger_n_9;
  wire write_error_cumulative;
  wire [2:0]write_index;
  wire \write_index[0]_i_1_n_0 ;
  wire \write_index[1]_i_1_n_0 ;
  wire \write_index[2]_i_1_n_0 ;
  wire \write_index_reg[2]_0 ;
  wire \write_index_reg[2]_1 ;
  wire write_issued_reg_0;
  wire writes_done;
  wire writes_done_reg_0;
  wire [0:0]NLW_M_AXI_ARADDR_carry_O_UNCONNECTED;
  wire [3:1]NLW_M_AXI_ARADDR_carry__6_CO_UNCONNECTED;
  wire [3:2]NLW_M_AXI_ARADDR_carry__6_O_UNCONNECTED;
  wire [0:0]NLW_M_AXI_AWADDR_carry_O_UNCONNECTED;
  wire [3:1]NLW_M_AXI_AWADDR_carry__6_CO_UNCONNECTED;
  wire [3:2]NLW_M_AXI_AWADDR_carry__6_O_UNCONNECTED;
  wire [3:1]\NLW_axi_araddr_reg[30]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_axi_araddr_reg[30]_i_1_O_UNCONNECTED ;
  wire [3:1]\NLW_axi_awaddr_reg[30]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_axi_awaddr_reg[30]_i_1_O_UNCONNECTED ;

  FDRE ERROR_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\q_reg[0] ),
        .Q(m00_axi_error),
        .R(data_reg_n_0));
  CARRY4 M_AXI_ARADDR_carry
       (.CI(1'b0),
        .CO({M_AXI_ARADDR_carry_n_0,M_AXI_ARADDR_carry_n_1,M_AXI_ARADDR_carry_n_2,M_AXI_ARADDR_carry_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_30,address_reg_n_31,address_reg_n_32,address_reg_n_33}),
        .O({m00_axi_araddr[5:3],NLW_M_AXI_ARADDR_carry_O_UNCONNECTED[0]}),
        .S({address_reg_n_65,address_reg_n_66,address_reg_n_67,address_reg_n_68}));
  CARRY4 M_AXI_ARADDR_carry__0
       (.CI(M_AXI_ARADDR_carry_n_0),
        .CO({M_AXI_ARADDR_carry__0_n_0,M_AXI_ARADDR_carry__0_n_1,M_AXI_ARADDR_carry__0_n_2,M_AXI_ARADDR_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_26,address_reg_n_27,address_reg_n_28,address_reg_n_29}),
        .O(m00_axi_araddr[9:6]),
        .S({address_reg_n_69,address_reg_n_70,address_reg_n_71,address_reg_n_72}));
  CARRY4 M_AXI_ARADDR_carry__1
       (.CI(M_AXI_ARADDR_carry__0_n_0),
        .CO({M_AXI_ARADDR_carry__1_n_0,M_AXI_ARADDR_carry__1_n_1,M_AXI_ARADDR_carry__1_n_2,M_AXI_ARADDR_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_22,address_reg_n_23,address_reg_n_24,address_reg_n_25}),
        .O(m00_axi_araddr[13:10]),
        .S({address_reg_n_73,address_reg_n_74,address_reg_n_75,address_reg_n_76}));
  CARRY4 M_AXI_ARADDR_carry__2
       (.CI(M_AXI_ARADDR_carry__1_n_0),
        .CO({M_AXI_ARADDR_carry__2_n_0,M_AXI_ARADDR_carry__2_n_1,M_AXI_ARADDR_carry__2_n_2,M_AXI_ARADDR_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_18,address_reg_n_19,address_reg_n_20,address_reg_n_21}),
        .O(m00_axi_araddr[17:14]),
        .S({address_reg_n_77,address_reg_n_78,address_reg_n_79,address_reg_n_80}));
  CARRY4 M_AXI_ARADDR_carry__3
       (.CI(M_AXI_ARADDR_carry__2_n_0),
        .CO({M_AXI_ARADDR_carry__3_n_0,M_AXI_ARADDR_carry__3_n_1,M_AXI_ARADDR_carry__3_n_2,M_AXI_ARADDR_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_14,address_reg_n_15,address_reg_n_16,address_reg_n_17}),
        .O(m00_axi_araddr[21:18]),
        .S({address_reg_n_81,address_reg_n_82,address_reg_n_83,address_reg_n_84}));
  CARRY4 M_AXI_ARADDR_carry__4
       (.CI(M_AXI_ARADDR_carry__3_n_0),
        .CO({M_AXI_ARADDR_carry__4_n_0,M_AXI_ARADDR_carry__4_n_1,M_AXI_ARADDR_carry__4_n_2,M_AXI_ARADDR_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_10,address_reg_n_11,address_reg_n_12,address_reg_n_13}),
        .O(m00_axi_araddr[25:22]),
        .S({address_reg_n_85,address_reg_n_86,address_reg_n_87,address_reg_n_88}));
  CARRY4 M_AXI_ARADDR_carry__5
       (.CI(M_AXI_ARADDR_carry__4_n_0),
        .CO({M_AXI_ARADDR_carry__5_n_0,M_AXI_ARADDR_carry__5_n_1,M_AXI_ARADDR_carry__5_n_2,M_AXI_ARADDR_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_6,address_reg_n_7,address_reg_n_8,address_reg_n_9}),
        .O(m00_axi_araddr[29:26]),
        .S({address_reg_n_89,address_reg_n_90,address_reg_n_91,address_reg_n_92}));
  CARRY4 M_AXI_ARADDR_carry__6
       (.CI(M_AXI_ARADDR_carry__5_n_0),
        .CO({NLW_M_AXI_ARADDR_carry__6_CO_UNCONNECTED[3:1],M_AXI_ARADDR_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,address_reg_n_5}),
        .O({NLW_M_AXI_ARADDR_carry__6_O_UNCONNECTED[3:2],m00_axi_araddr[31:30]}),
        .S({1'b0,1'b0,address_reg_n_2,address_reg_n_3}));
  CARRY4 M_AXI_AWADDR_carry
       (.CI(1'b0),
        .CO({M_AXI_AWADDR_carry_n_0,M_AXI_AWADDR_carry_n_1,M_AXI_AWADDR_carry_n_2,M_AXI_AWADDR_carry_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_30,address_reg_n_31,address_reg_n_32,address_reg_n_33}),
        .O({m00_axi_awaddr[3:1],NLW_M_AXI_AWADDR_carry_O_UNCONNECTED[0]}),
        .S({address_reg_n_36,address_reg_n_37,address_reg_n_38,address_reg_n_39}));
  CARRY4 M_AXI_AWADDR_carry__0
       (.CI(M_AXI_AWADDR_carry_n_0),
        .CO({M_AXI_AWADDR_carry__0_n_0,M_AXI_AWADDR_carry__0_n_1,M_AXI_AWADDR_carry__0_n_2,M_AXI_AWADDR_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_26,address_reg_n_27,address_reg_n_28,address_reg_n_29}),
        .O(m00_axi_awaddr[7:4]),
        .S({address_reg_n_40,address_reg_n_41,address_reg_n_42,address_reg_n_43}));
  CARRY4 M_AXI_AWADDR_carry__1
       (.CI(M_AXI_AWADDR_carry__0_n_0),
        .CO({M_AXI_AWADDR_carry__1_n_0,M_AXI_AWADDR_carry__1_n_1,M_AXI_AWADDR_carry__1_n_2,M_AXI_AWADDR_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_22,address_reg_n_23,address_reg_n_24,address_reg_n_25}),
        .O(m00_axi_awaddr[11:8]),
        .S({address_reg_n_44,address_reg_n_45,address_reg_n_46,address_reg_n_47}));
  CARRY4 M_AXI_AWADDR_carry__2
       (.CI(M_AXI_AWADDR_carry__1_n_0),
        .CO({M_AXI_AWADDR_carry__2_n_0,M_AXI_AWADDR_carry__2_n_1,M_AXI_AWADDR_carry__2_n_2,M_AXI_AWADDR_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_18,address_reg_n_19,address_reg_n_20,address_reg_n_21}),
        .O(m00_axi_awaddr[15:12]),
        .S({address_reg_n_48,address_reg_n_49,address_reg_n_50,address_reg_n_51}));
  CARRY4 M_AXI_AWADDR_carry__3
       (.CI(M_AXI_AWADDR_carry__2_n_0),
        .CO({M_AXI_AWADDR_carry__3_n_0,M_AXI_AWADDR_carry__3_n_1,M_AXI_AWADDR_carry__3_n_2,M_AXI_AWADDR_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_14,address_reg_n_15,address_reg_n_16,address_reg_n_17}),
        .O(m00_axi_awaddr[19:16]),
        .S({address_reg_n_52,address_reg_n_53,address_reg_n_54,address_reg_n_55}));
  CARRY4 M_AXI_AWADDR_carry__4
       (.CI(M_AXI_AWADDR_carry__3_n_0),
        .CO({M_AXI_AWADDR_carry__4_n_0,M_AXI_AWADDR_carry__4_n_1,M_AXI_AWADDR_carry__4_n_2,M_AXI_AWADDR_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_10,address_reg_n_11,address_reg_n_12,address_reg_n_13}),
        .O(m00_axi_awaddr[23:20]),
        .S({address_reg_n_56,address_reg_n_57,address_reg_n_58,address_reg_n_59}));
  CARRY4 M_AXI_AWADDR_carry__5
       (.CI(M_AXI_AWADDR_carry__4_n_0),
        .CO({M_AXI_AWADDR_carry__5_n_0,M_AXI_AWADDR_carry__5_n_1,M_AXI_AWADDR_carry__5_n_2,M_AXI_AWADDR_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({address_reg_n_6,address_reg_n_7,address_reg_n_8,address_reg_n_9}),
        .O(m00_axi_awaddr[27:24]),
        .S({address_reg_n_60,address_reg_n_61,address_reg_n_62,address_reg_n_63}));
  CARRY4 M_AXI_AWADDR_carry__6
       (.CI(M_AXI_AWADDR_carry__5_n_0),
        .CO({NLW_M_AXI_AWADDR_carry__6_CO_UNCONNECTED[3:1],M_AXI_AWADDR_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,address_reg_n_5}),
        .O({NLW_M_AXI_AWADDR_carry__6_O_UNCONNECTED[3:2],m00_axi_awaddr[29:28]}),
        .S({1'b0,1'b0,address_reg_n_0,address_reg_n_1}));
  design_1_dramControllerLITE_0_0_Register address_reg
       (.Q({address_reg_n_5,address_reg_n_6,address_reg_n_7,address_reg_n_8,address_reg_n_9,address_reg_n_10,address_reg_n_11,address_reg_n_12,address_reg_n_13,address_reg_n_14,address_reg_n_15,address_reg_n_16,address_reg_n_17,address_reg_n_18,address_reg_n_19,address_reg_n_20,address_reg_n_21,address_reg_n_22,address_reg_n_23,address_reg_n_24,address_reg_n_25,address_reg_n_26,address_reg_n_27,address_reg_n_28,address_reg_n_29,address_reg_n_30,address_reg_n_31,address_reg_n_32,address_reg_n_33,m00_axi_araddr[1:0]}),
        .S({address_reg_n_0,address_reg_n_1}),
        .SR(data_reg_n_0),
        .addressBase(addressBase),
        .axi_araddr_reg(axi_araddr_reg),
        .axi_awaddr_reg(axi_awaddr_reg),
        .doSetup(doSetup),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_araddr(m00_axi_araddr[2]),
        .\m00_axi_araddr[30] ({address_reg_n_2,address_reg_n_3}),
        .\m00_axi_araddr[30]_0 ({address_reg_n_65,address_reg_n_66,address_reg_n_67,address_reg_n_68}),
        .\m00_axi_araddr[30]_1 ({address_reg_n_69,address_reg_n_70,address_reg_n_71,address_reg_n_72}),
        .\m00_axi_araddr[30]_2 ({address_reg_n_73,address_reg_n_74,address_reg_n_75,address_reg_n_76}),
        .\m00_axi_araddr[30]_3 ({address_reg_n_77,address_reg_n_78,address_reg_n_79,address_reg_n_80}),
        .\m00_axi_araddr[30]_4 ({address_reg_n_81,address_reg_n_82,address_reg_n_83,address_reg_n_84}),
        .\m00_axi_araddr[30]_5 ({address_reg_n_85,address_reg_n_86,address_reg_n_87,address_reg_n_88}),
        .\m00_axi_araddr[30]_6 ({address_reg_n_89,address_reg_n_90,address_reg_n_91,address_reg_n_92}),
        .m00_axi_awaddr(m00_axi_awaddr[0]),
        .\m00_axi_awaddr[30] ({address_reg_n_36,address_reg_n_37,address_reg_n_38,address_reg_n_39}),
        .\m00_axi_awaddr[30]_0 ({address_reg_n_40,address_reg_n_41,address_reg_n_42,address_reg_n_43}),
        .\m00_axi_awaddr[30]_1 ({address_reg_n_44,address_reg_n_45,address_reg_n_46,address_reg_n_47}),
        .\m00_axi_awaddr[30]_2 ({address_reg_n_48,address_reg_n_49,address_reg_n_50,address_reg_n_51}),
        .\m00_axi_awaddr[30]_3 ({address_reg_n_52,address_reg_n_53,address_reg_n_54,address_reg_n_55}),
        .\m00_axi_awaddr[30]_4 ({address_reg_n_56,address_reg_n_57,address_reg_n_58,address_reg_n_59}),
        .\m00_axi_awaddr[30]_5 ({address_reg_n_60,address_reg_n_61,address_reg_n_62,address_reg_n_63}));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[10]_i_2 
       (.I0(axi_araddr_reg[13]),
        .O(\axi_araddr[10]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[10]_i_3 
       (.I0(axi_araddr_reg[12]),
        .O(\axi_araddr[10]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[10]_i_4 
       (.I0(axi_araddr_reg[11]),
        .O(\axi_araddr[10]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[10]_i_5 
       (.I0(axi_araddr_reg[10]),
        .O(\axi_araddr[10]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[14]_i_2 
       (.I0(axi_araddr_reg[17]),
        .O(\axi_araddr[14]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[14]_i_3 
       (.I0(axi_araddr_reg[16]),
        .O(\axi_araddr[14]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[14]_i_4 
       (.I0(axi_araddr_reg[15]),
        .O(\axi_araddr[14]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[14]_i_5 
       (.I0(axi_araddr_reg[14]),
        .O(\axi_araddr[14]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[18]_i_2 
       (.I0(axi_araddr_reg[21]),
        .O(\axi_araddr[18]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[18]_i_3 
       (.I0(axi_araddr_reg[20]),
        .O(\axi_araddr[18]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[18]_i_4 
       (.I0(axi_araddr_reg[19]),
        .O(\axi_araddr[18]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[18]_i_5 
       (.I0(axi_araddr_reg[18]),
        .O(\axi_araddr[18]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[22]_i_2 
       (.I0(axi_araddr_reg[25]),
        .O(\axi_araddr[22]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[22]_i_3 
       (.I0(axi_araddr_reg[24]),
        .O(\axi_araddr[22]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[22]_i_4 
       (.I0(axi_araddr_reg[23]),
        .O(\axi_araddr[22]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[22]_i_5 
       (.I0(axi_araddr_reg[22]),
        .O(\axi_araddr[22]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[26]_i_2 
       (.I0(axi_araddr_reg[29]),
        .O(\axi_araddr[26]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[26]_i_3 
       (.I0(axi_araddr_reg[28]),
        .O(\axi_araddr[26]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[26]_i_4 
       (.I0(axi_araddr_reg[27]),
        .O(\axi_araddr[26]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[26]_i_5 
       (.I0(axi_araddr_reg[26]),
        .O(\axi_araddr[26]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \axi_araddr[2]_i_1 
       (.I0(m00_axi_arvalid),
        .I1(m00_axi_arready),
        .O(axi_arvalid0));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[2]_i_3 
       (.I0(axi_araddr_reg[5]),
        .O(\axi_araddr[2]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[2]_i_4 
       (.I0(axi_araddr_reg[4]),
        .O(\axi_araddr[2]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[2]_i_5 
       (.I0(axi_araddr_reg[3]),
        .O(\axi_araddr[2]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \axi_araddr[2]_i_6 
       (.I0(axi_araddr_reg[2]),
        .O(\axi_araddr[2]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[30]_i_2 
       (.I0(axi_araddr_reg[31]),
        .O(\axi_araddr[30]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[30]_i_3 
       (.I0(axi_araddr_reg[30]),
        .O(\axi_araddr[30]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[6]_i_2 
       (.I0(axi_araddr_reg[9]),
        .O(\axi_araddr[6]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[6]_i_3 
       (.I0(axi_araddr_reg[8]),
        .O(\axi_araddr[6]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[6]_i_4 
       (.I0(axi_araddr_reg[7]),
        .O(\axi_araddr[6]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_araddr[6]_i_5 
       (.I0(axi_araddr_reg[6]),
        .O(\axi_araddr[6]_i_5_n_0 ));
  FDRE \axi_araddr_reg[10] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[10]_i_1_n_7 ),
        .Q(axi_araddr_reg[10]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[10]_i_1 
       (.CI(\axi_araddr_reg[6]_i_1_n_0 ),
        .CO({\axi_araddr_reg[10]_i_1_n_0 ,\axi_araddr_reg[10]_i_1_n_1 ,\axi_araddr_reg[10]_i_1_n_2 ,\axi_araddr_reg[10]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[10]_i_1_n_4 ,\axi_araddr_reg[10]_i_1_n_5 ,\axi_araddr_reg[10]_i_1_n_6 ,\axi_araddr_reg[10]_i_1_n_7 }),
        .S({\axi_araddr[10]_i_2_n_0 ,\axi_araddr[10]_i_3_n_0 ,\axi_araddr[10]_i_4_n_0 ,\axi_araddr[10]_i_5_n_0 }));
  FDRE \axi_araddr_reg[11] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[10]_i_1_n_6 ),
        .Q(axi_araddr_reg[11]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[12] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[10]_i_1_n_5 ),
        .Q(axi_araddr_reg[12]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[13] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[10]_i_1_n_4 ),
        .Q(axi_araddr_reg[13]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[14] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[14]_i_1_n_7 ),
        .Q(axi_araddr_reg[14]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[14]_i_1 
       (.CI(\axi_araddr_reg[10]_i_1_n_0 ),
        .CO({\axi_araddr_reg[14]_i_1_n_0 ,\axi_araddr_reg[14]_i_1_n_1 ,\axi_araddr_reg[14]_i_1_n_2 ,\axi_araddr_reg[14]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[14]_i_1_n_4 ,\axi_araddr_reg[14]_i_1_n_5 ,\axi_araddr_reg[14]_i_1_n_6 ,\axi_araddr_reg[14]_i_1_n_7 }),
        .S({\axi_araddr[14]_i_2_n_0 ,\axi_araddr[14]_i_3_n_0 ,\axi_araddr[14]_i_4_n_0 ,\axi_araddr[14]_i_5_n_0 }));
  FDRE \axi_araddr_reg[15] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[14]_i_1_n_6 ),
        .Q(axi_araddr_reg[15]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[16] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[14]_i_1_n_5 ),
        .Q(axi_araddr_reg[16]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[17] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[14]_i_1_n_4 ),
        .Q(axi_araddr_reg[17]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[18] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[18]_i_1_n_7 ),
        .Q(axi_araddr_reg[18]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[18]_i_1 
       (.CI(\axi_araddr_reg[14]_i_1_n_0 ),
        .CO({\axi_araddr_reg[18]_i_1_n_0 ,\axi_araddr_reg[18]_i_1_n_1 ,\axi_araddr_reg[18]_i_1_n_2 ,\axi_araddr_reg[18]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[18]_i_1_n_4 ,\axi_araddr_reg[18]_i_1_n_5 ,\axi_araddr_reg[18]_i_1_n_6 ,\axi_araddr_reg[18]_i_1_n_7 }),
        .S({\axi_araddr[18]_i_2_n_0 ,\axi_araddr[18]_i_3_n_0 ,\axi_araddr[18]_i_4_n_0 ,\axi_araddr[18]_i_5_n_0 }));
  FDRE \axi_araddr_reg[19] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[18]_i_1_n_6 ),
        .Q(axi_araddr_reg[19]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[20] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[18]_i_1_n_5 ),
        .Q(axi_araddr_reg[20]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[21] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[18]_i_1_n_4 ),
        .Q(axi_araddr_reg[21]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[22] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[22]_i_1_n_7 ),
        .Q(axi_araddr_reg[22]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[22]_i_1 
       (.CI(\axi_araddr_reg[18]_i_1_n_0 ),
        .CO({\axi_araddr_reg[22]_i_1_n_0 ,\axi_araddr_reg[22]_i_1_n_1 ,\axi_araddr_reg[22]_i_1_n_2 ,\axi_araddr_reg[22]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[22]_i_1_n_4 ,\axi_araddr_reg[22]_i_1_n_5 ,\axi_araddr_reg[22]_i_1_n_6 ,\axi_araddr_reg[22]_i_1_n_7 }),
        .S({\axi_araddr[22]_i_2_n_0 ,\axi_araddr[22]_i_3_n_0 ,\axi_araddr[22]_i_4_n_0 ,\axi_araddr[22]_i_5_n_0 }));
  FDRE \axi_araddr_reg[23] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[22]_i_1_n_6 ),
        .Q(axi_araddr_reg[23]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[24] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[22]_i_1_n_5 ),
        .Q(axi_araddr_reg[24]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[25] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[22]_i_1_n_4 ),
        .Q(axi_araddr_reg[25]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[26] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[26]_i_1_n_7 ),
        .Q(axi_araddr_reg[26]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[26]_i_1 
       (.CI(\axi_araddr_reg[22]_i_1_n_0 ),
        .CO({\axi_araddr_reg[26]_i_1_n_0 ,\axi_araddr_reg[26]_i_1_n_1 ,\axi_araddr_reg[26]_i_1_n_2 ,\axi_araddr_reg[26]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[26]_i_1_n_4 ,\axi_araddr_reg[26]_i_1_n_5 ,\axi_araddr_reg[26]_i_1_n_6 ,\axi_araddr_reg[26]_i_1_n_7 }),
        .S({\axi_araddr[26]_i_2_n_0 ,\axi_araddr[26]_i_3_n_0 ,\axi_araddr[26]_i_4_n_0 ,\axi_araddr[26]_i_5_n_0 }));
  FDRE \axi_araddr_reg[27] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[26]_i_1_n_6 ),
        .Q(axi_araddr_reg[27]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[28] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[26]_i_1_n_5 ),
        .Q(axi_araddr_reg[28]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[29] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[26]_i_1_n_4 ),
        .Q(axi_araddr_reg[29]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[2] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[2]_i_2_n_7 ),
        .Q(axi_araddr_reg[2]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[2]_i_2 
       (.CI(1'b0),
        .CO({\axi_araddr_reg[2]_i_2_n_0 ,\axi_araddr_reg[2]_i_2_n_1 ,\axi_araddr_reg[2]_i_2_n_2 ,\axi_araddr_reg[2]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\axi_araddr_reg[2]_i_2_n_4 ,\axi_araddr_reg[2]_i_2_n_5 ,\axi_araddr_reg[2]_i_2_n_6 ,\axi_araddr_reg[2]_i_2_n_7 }),
        .S({\axi_araddr[2]_i_3_n_0 ,\axi_araddr[2]_i_4_n_0 ,\axi_araddr[2]_i_5_n_0 ,\axi_araddr[2]_i_6_n_0 }));
  FDRE \axi_araddr_reg[30] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[30]_i_1_n_7 ),
        .Q(axi_araddr_reg[30]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[30]_i_1 
       (.CI(\axi_araddr_reg[26]_i_1_n_0 ),
        .CO({\NLW_axi_araddr_reg[30]_i_1_CO_UNCONNECTED [3:1],\axi_araddr_reg[30]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_axi_araddr_reg[30]_i_1_O_UNCONNECTED [3:2],\axi_araddr_reg[30]_i_1_n_6 ,\axi_araddr_reg[30]_i_1_n_7 }),
        .S({1'b0,1'b0,\axi_araddr[30]_i_2_n_0 ,\axi_araddr[30]_i_3_n_0 }));
  FDRE \axi_araddr_reg[31] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[30]_i_1_n_6 ),
        .Q(axi_araddr_reg[31]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[3] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[2]_i_2_n_6 ),
        .Q(axi_araddr_reg[3]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[4] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[2]_i_2_n_5 ),
        .Q(axi_araddr_reg[4]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[5] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[2]_i_2_n_4 ),
        .Q(axi_araddr_reg[5]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[6] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[6]_i_1_n_7 ),
        .Q(axi_araddr_reg[6]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_araddr_reg[6]_i_1 
       (.CI(\axi_araddr_reg[2]_i_2_n_0 ),
        .CO({\axi_araddr_reg[6]_i_1_n_0 ,\axi_araddr_reg[6]_i_1_n_1 ,\axi_araddr_reg[6]_i_1_n_2 ,\axi_araddr_reg[6]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_araddr_reg[6]_i_1_n_4 ,\axi_araddr_reg[6]_i_1_n_5 ,\axi_araddr_reg[6]_i_1_n_6 ,\axi_araddr_reg[6]_i_1_n_7 }),
        .S({\axi_araddr[6]_i_2_n_0 ,\axi_araddr[6]_i_3_n_0 ,\axi_araddr[6]_i_4_n_0 ,\axi_araddr[6]_i_5_n_0 }));
  FDRE \axi_araddr_reg[7] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[6]_i_1_n_6 ),
        .Q(axi_araddr_reg[7]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[8] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[6]_i_1_n_5 ),
        .Q(axi_araddr_reg[8]),
        .R(writeTrigger_n_2));
  FDRE \axi_araddr_reg[9] 
       (.C(m00_axi_aclk),
        .CE(axi_arvalid0),
        .D(\axi_araddr_reg[6]_i_1_n_4 ),
        .Q(axi_araddr_reg[9]),
        .R(writeTrigger_n_2));
  FDRE axi_arvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(start_single_read_reg_0),
        .Q(m00_axi_arvalid),
        .R(writeTrigger_n_2));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[10]_i_2 
       (.I0(axi_awaddr_reg[13]),
        .O(\axi_awaddr[10]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[10]_i_3 
       (.I0(axi_awaddr_reg[12]),
        .O(\axi_awaddr[10]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[10]_i_4 
       (.I0(axi_awaddr_reg[11]),
        .O(\axi_awaddr[10]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[10]_i_5 
       (.I0(axi_awaddr_reg[10]),
        .O(\axi_awaddr[10]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[14]_i_2 
       (.I0(axi_awaddr_reg[17]),
        .O(\axi_awaddr[14]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[14]_i_3 
       (.I0(axi_awaddr_reg[16]),
        .O(\axi_awaddr[14]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[14]_i_4 
       (.I0(axi_awaddr_reg[15]),
        .O(\axi_awaddr[14]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[14]_i_5 
       (.I0(axi_awaddr_reg[14]),
        .O(\axi_awaddr[14]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[18]_i_2 
       (.I0(axi_awaddr_reg[21]),
        .O(\axi_awaddr[18]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[18]_i_3 
       (.I0(axi_awaddr_reg[20]),
        .O(\axi_awaddr[18]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[18]_i_4 
       (.I0(axi_awaddr_reg[19]),
        .O(\axi_awaddr[18]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[18]_i_5 
       (.I0(axi_awaddr_reg[18]),
        .O(\axi_awaddr[18]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[22]_i_2 
       (.I0(axi_awaddr_reg[25]),
        .O(\axi_awaddr[22]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[22]_i_3 
       (.I0(axi_awaddr_reg[24]),
        .O(\axi_awaddr[22]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[22]_i_4 
       (.I0(axi_awaddr_reg[23]),
        .O(\axi_awaddr[22]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[22]_i_5 
       (.I0(axi_awaddr_reg[22]),
        .O(\axi_awaddr[22]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[26]_i_2 
       (.I0(axi_awaddr_reg[29]),
        .O(\axi_awaddr[26]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[26]_i_3 
       (.I0(axi_awaddr_reg[28]),
        .O(\axi_awaddr[26]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[26]_i_4 
       (.I0(axi_awaddr_reg[27]),
        .O(\axi_awaddr[26]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[26]_i_5 
       (.I0(axi_awaddr_reg[26]),
        .O(\axi_awaddr[26]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \axi_awaddr[2]_i_1 
       (.I0(m00_axi_awready),
        .I1(m00_axi_awvalid),
        .O(axi_awaddr0));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[2]_i_3 
       (.I0(axi_awaddr_reg[5]),
        .O(\axi_awaddr[2]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[2]_i_4 
       (.I0(axi_awaddr_reg[4]),
        .O(\axi_awaddr[2]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[2]_i_5 
       (.I0(axi_awaddr_reg[3]),
        .O(\axi_awaddr[2]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \axi_awaddr[2]_i_6 
       (.I0(axi_awaddr_reg[2]),
        .O(\axi_awaddr[2]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[30]_i_2 
       (.I0(axi_awaddr_reg[31]),
        .O(\axi_awaddr[30]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[30]_i_3 
       (.I0(axi_awaddr_reg[30]),
        .O(\axi_awaddr[30]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[6]_i_2 
       (.I0(axi_awaddr_reg[9]),
        .O(\axi_awaddr[6]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[6]_i_3 
       (.I0(axi_awaddr_reg[8]),
        .O(\axi_awaddr[6]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[6]_i_4 
       (.I0(axi_awaddr_reg[7]),
        .O(\axi_awaddr[6]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \axi_awaddr[6]_i_5 
       (.I0(axi_awaddr_reg[6]),
        .O(\axi_awaddr[6]_i_5_n_0 ));
  FDRE \axi_awaddr_reg[10] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[10]_i_1_n_7 ),
        .Q(axi_awaddr_reg[10]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[10]_i_1 
       (.CI(\axi_awaddr_reg[6]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[10]_i_1_n_0 ,\axi_awaddr_reg[10]_i_1_n_1 ,\axi_awaddr_reg[10]_i_1_n_2 ,\axi_awaddr_reg[10]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[10]_i_1_n_4 ,\axi_awaddr_reg[10]_i_1_n_5 ,\axi_awaddr_reg[10]_i_1_n_6 ,\axi_awaddr_reg[10]_i_1_n_7 }),
        .S({\axi_awaddr[10]_i_2_n_0 ,\axi_awaddr[10]_i_3_n_0 ,\axi_awaddr[10]_i_4_n_0 ,\axi_awaddr[10]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[11] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[10]_i_1_n_6 ),
        .Q(axi_awaddr_reg[11]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[12] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[10]_i_1_n_5 ),
        .Q(axi_awaddr_reg[12]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[13] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[10]_i_1_n_4 ),
        .Q(axi_awaddr_reg[13]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[14] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[14]_i_1_n_7 ),
        .Q(axi_awaddr_reg[14]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[14]_i_1 
       (.CI(\axi_awaddr_reg[10]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[14]_i_1_n_0 ,\axi_awaddr_reg[14]_i_1_n_1 ,\axi_awaddr_reg[14]_i_1_n_2 ,\axi_awaddr_reg[14]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[14]_i_1_n_4 ,\axi_awaddr_reg[14]_i_1_n_5 ,\axi_awaddr_reg[14]_i_1_n_6 ,\axi_awaddr_reg[14]_i_1_n_7 }),
        .S({\axi_awaddr[14]_i_2_n_0 ,\axi_awaddr[14]_i_3_n_0 ,\axi_awaddr[14]_i_4_n_0 ,\axi_awaddr[14]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[15] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[14]_i_1_n_6 ),
        .Q(axi_awaddr_reg[15]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[16] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[14]_i_1_n_5 ),
        .Q(axi_awaddr_reg[16]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[17] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[14]_i_1_n_4 ),
        .Q(axi_awaddr_reg[17]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[18] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[18]_i_1_n_7 ),
        .Q(axi_awaddr_reg[18]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[18]_i_1 
       (.CI(\axi_awaddr_reg[14]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[18]_i_1_n_0 ,\axi_awaddr_reg[18]_i_1_n_1 ,\axi_awaddr_reg[18]_i_1_n_2 ,\axi_awaddr_reg[18]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[18]_i_1_n_4 ,\axi_awaddr_reg[18]_i_1_n_5 ,\axi_awaddr_reg[18]_i_1_n_6 ,\axi_awaddr_reg[18]_i_1_n_7 }),
        .S({\axi_awaddr[18]_i_2_n_0 ,\axi_awaddr[18]_i_3_n_0 ,\axi_awaddr[18]_i_4_n_0 ,\axi_awaddr[18]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[19] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[18]_i_1_n_6 ),
        .Q(axi_awaddr_reg[19]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[20] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[18]_i_1_n_5 ),
        .Q(axi_awaddr_reg[20]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[21] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[18]_i_1_n_4 ),
        .Q(axi_awaddr_reg[21]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[22] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[22]_i_1_n_7 ),
        .Q(axi_awaddr_reg[22]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[22]_i_1 
       (.CI(\axi_awaddr_reg[18]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[22]_i_1_n_0 ,\axi_awaddr_reg[22]_i_1_n_1 ,\axi_awaddr_reg[22]_i_1_n_2 ,\axi_awaddr_reg[22]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[22]_i_1_n_4 ,\axi_awaddr_reg[22]_i_1_n_5 ,\axi_awaddr_reg[22]_i_1_n_6 ,\axi_awaddr_reg[22]_i_1_n_7 }),
        .S({\axi_awaddr[22]_i_2_n_0 ,\axi_awaddr[22]_i_3_n_0 ,\axi_awaddr[22]_i_4_n_0 ,\axi_awaddr[22]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[23] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[22]_i_1_n_6 ),
        .Q(axi_awaddr_reg[23]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[24] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[22]_i_1_n_5 ),
        .Q(axi_awaddr_reg[24]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[25] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[22]_i_1_n_4 ),
        .Q(axi_awaddr_reg[25]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[26] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[26]_i_1_n_7 ),
        .Q(axi_awaddr_reg[26]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[26]_i_1 
       (.CI(\axi_awaddr_reg[22]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[26]_i_1_n_0 ,\axi_awaddr_reg[26]_i_1_n_1 ,\axi_awaddr_reg[26]_i_1_n_2 ,\axi_awaddr_reg[26]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[26]_i_1_n_4 ,\axi_awaddr_reg[26]_i_1_n_5 ,\axi_awaddr_reg[26]_i_1_n_6 ,\axi_awaddr_reg[26]_i_1_n_7 }),
        .S({\axi_awaddr[26]_i_2_n_0 ,\axi_awaddr[26]_i_3_n_0 ,\axi_awaddr[26]_i_4_n_0 ,\axi_awaddr[26]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[27] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[26]_i_1_n_6 ),
        .Q(axi_awaddr_reg[27]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[28] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[26]_i_1_n_5 ),
        .Q(axi_awaddr_reg[28]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[29] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[26]_i_1_n_4 ),
        .Q(axi_awaddr_reg[29]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[2] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[2]_i_2_n_7 ),
        .Q(axi_awaddr_reg[2]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[2]_i_2 
       (.CI(1'b0),
        .CO({\axi_awaddr_reg[2]_i_2_n_0 ,\axi_awaddr_reg[2]_i_2_n_1 ,\axi_awaddr_reg[2]_i_2_n_2 ,\axi_awaddr_reg[2]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\axi_awaddr_reg[2]_i_2_n_4 ,\axi_awaddr_reg[2]_i_2_n_5 ,\axi_awaddr_reg[2]_i_2_n_6 ,\axi_awaddr_reg[2]_i_2_n_7 }),
        .S({\axi_awaddr[2]_i_3_n_0 ,\axi_awaddr[2]_i_4_n_0 ,\axi_awaddr[2]_i_5_n_0 ,\axi_awaddr[2]_i_6_n_0 }));
  FDRE \axi_awaddr_reg[30] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[30]_i_1_n_7 ),
        .Q(axi_awaddr_reg[30]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[30]_i_1 
       (.CI(\axi_awaddr_reg[26]_i_1_n_0 ),
        .CO({\NLW_axi_awaddr_reg[30]_i_1_CO_UNCONNECTED [3:1],\axi_awaddr_reg[30]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_axi_awaddr_reg[30]_i_1_O_UNCONNECTED [3:2],\axi_awaddr_reg[30]_i_1_n_6 ,\axi_awaddr_reg[30]_i_1_n_7 }),
        .S({1'b0,1'b0,\axi_awaddr[30]_i_2_n_0 ,\axi_awaddr[30]_i_3_n_0 }));
  FDRE \axi_awaddr_reg[31] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[30]_i_1_n_6 ),
        .Q(axi_awaddr_reg[31]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[3] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[2]_i_2_n_6 ),
        .Q(axi_awaddr_reg[3]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[4] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[2]_i_2_n_5 ),
        .Q(axi_awaddr_reg[4]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[5] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[2]_i_2_n_4 ),
        .Q(axi_awaddr_reg[5]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[6] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[6]_i_1_n_7 ),
        .Q(axi_awaddr_reg[6]),
        .R(writeTrigger_n_2));
  CARRY4 \axi_awaddr_reg[6]_i_1 
       (.CI(\axi_awaddr_reg[2]_i_2_n_0 ),
        .CO({\axi_awaddr_reg[6]_i_1_n_0 ,\axi_awaddr_reg[6]_i_1_n_1 ,\axi_awaddr_reg[6]_i_1_n_2 ,\axi_awaddr_reg[6]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[6]_i_1_n_4 ,\axi_awaddr_reg[6]_i_1_n_5 ,\axi_awaddr_reg[6]_i_1_n_6 ,\axi_awaddr_reg[6]_i_1_n_7 }),
        .S({\axi_awaddr[6]_i_2_n_0 ,\axi_awaddr[6]_i_3_n_0 ,\axi_awaddr[6]_i_4_n_0 ,\axi_awaddr[6]_i_5_n_0 }));
  FDRE \axi_awaddr_reg[7] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[6]_i_1_n_6 ),
        .Q(axi_awaddr_reg[7]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[8] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[6]_i_1_n_5 ),
        .Q(axi_awaddr_reg[8]),
        .R(writeTrigger_n_2));
  FDRE \axi_awaddr_reg[9] 
       (.C(m00_axi_aclk),
        .CE(axi_awaddr0),
        .D(\axi_awaddr_reg[6]_i_1_n_4 ),
        .Q(axi_awaddr_reg[9]),
        .R(writeTrigger_n_2));
  FDRE axi_awvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(start_single_write_reg_0),
        .Q(m00_axi_awvalid),
        .R(writeTrigger_n_2));
  LUT2 #(
    .INIT(4'h2)) 
    axi_bready_i_1
       (.I0(m00_axi_bvalid),
        .I1(m00_axi_bready),
        .O(axi_bready_i_1_n_0));
  FDRE axi_bready_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_bready_i_1_n_0),
        .Q(m00_axi_bready),
        .R(writeTrigger_n_2));
  LUT2 #(
    .INIT(4'h4)) 
    axi_rready_i_1
       (.I0(m00_axi_rready),
        .I1(m00_axi_rvalid),
        .O(axi_rready_i_1_n_0));
  FDRE axi_rready_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_rready_i_1_n_0),
        .Q(m00_axi_rready),
        .R(writeTrigger_n_2));
  FDRE \axi_wdata_reg[0] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_36),
        .Q(m00_axi_wdata[0]),
        .R(1'b0));
  FDRE \axi_wdata_reg[10] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_26),
        .Q(m00_axi_wdata[10]),
        .R(1'b0));
  FDRE \axi_wdata_reg[11] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_25),
        .Q(m00_axi_wdata[11]),
        .R(1'b0));
  FDRE \axi_wdata_reg[12] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_24),
        .Q(m00_axi_wdata[12]),
        .R(1'b0));
  FDRE \axi_wdata_reg[13] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_23),
        .Q(m00_axi_wdata[13]),
        .R(1'b0));
  FDRE \axi_wdata_reg[14] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_22),
        .Q(m00_axi_wdata[14]),
        .R(1'b0));
  FDRE \axi_wdata_reg[15] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_21),
        .Q(m00_axi_wdata[15]),
        .R(1'b0));
  FDRE \axi_wdata_reg[16] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_20),
        .Q(m00_axi_wdata[16]),
        .R(1'b0));
  FDRE \axi_wdata_reg[17] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_19),
        .Q(m00_axi_wdata[17]),
        .R(1'b0));
  FDRE \axi_wdata_reg[18] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_18),
        .Q(m00_axi_wdata[18]),
        .R(1'b0));
  FDRE \axi_wdata_reg[19] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_17),
        .Q(m00_axi_wdata[19]),
        .R(1'b0));
  FDRE \axi_wdata_reg[1] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_35),
        .Q(m00_axi_wdata[1]),
        .R(1'b0));
  FDRE \axi_wdata_reg[20] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_16),
        .Q(m00_axi_wdata[20]),
        .R(1'b0));
  FDRE \axi_wdata_reg[21] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_15),
        .Q(m00_axi_wdata[21]),
        .R(1'b0));
  FDRE \axi_wdata_reg[22] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_14),
        .Q(m00_axi_wdata[22]),
        .R(1'b0));
  FDRE \axi_wdata_reg[23] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_13),
        .Q(m00_axi_wdata[23]),
        .R(1'b0));
  FDRE \axi_wdata_reg[24] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_12),
        .Q(m00_axi_wdata[24]),
        .R(1'b0));
  FDRE \axi_wdata_reg[25] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_11),
        .Q(m00_axi_wdata[25]),
        .R(1'b0));
  FDRE \axi_wdata_reg[26] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_10),
        .Q(m00_axi_wdata[26]),
        .R(1'b0));
  FDRE \axi_wdata_reg[27] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_9),
        .Q(m00_axi_wdata[27]),
        .R(1'b0));
  FDRE \axi_wdata_reg[28] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_8),
        .Q(m00_axi_wdata[28]),
        .R(1'b0));
  FDRE \axi_wdata_reg[29] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_7),
        .Q(m00_axi_wdata[29]),
        .R(1'b0));
  FDRE \axi_wdata_reg[2] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_34),
        .Q(m00_axi_wdata[2]),
        .R(1'b0));
  FDRE \axi_wdata_reg[30] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_6),
        .Q(m00_axi_wdata[30]),
        .R(1'b0));
  FDRE \axi_wdata_reg[31] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_5),
        .Q(m00_axi_wdata[31]),
        .R(1'b0));
  FDRE \axi_wdata_reg[3] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_33),
        .Q(m00_axi_wdata[3]),
        .R(1'b0));
  FDRE \axi_wdata_reg[4] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_32),
        .Q(m00_axi_wdata[4]),
        .R(1'b0));
  FDRE \axi_wdata_reg[5] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_31),
        .Q(m00_axi_wdata[5]),
        .R(1'b0));
  FDRE \axi_wdata_reg[6] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_30),
        .Q(m00_axi_wdata[6]),
        .R(1'b0));
  FDRE \axi_wdata_reg[7] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_29),
        .Q(m00_axi_wdata[7]),
        .R(1'b0));
  FDRE \axi_wdata_reg[8] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_28),
        .Q(m00_axi_wdata[8]),
        .R(1'b0));
  FDRE \axi_wdata_reg[9] 
       (.C(m00_axi_aclk),
        .CE(writeTrigger_n_4),
        .D(writeTrigger_n_27),
        .Q(m00_axi_wdata[9]),
        .R(1'b0));
  FDRE axi_wvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(start_single_write_reg_1),
        .Q(m00_axi_wvalid),
        .R(writeTrigger_n_2));
  LUT5 #(
    .INIT(32'h00080000)) 
    \dataReadArray[0][31]_i_2 
       (.I0(m00_axi_rready),
        .I1(m00_axi_rvalid),
        .I2(read_index[1]),
        .I3(read_index[2]),
        .I4(read_index[0]),
        .O(dataReadArray));
  FDRE \dataReadArray_reg[0][0] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[0]),
        .Q(dataOut[0]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][10] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[10]),
        .Q(dataOut[10]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][11] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[11]),
        .Q(dataOut[11]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][12] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[12]),
        .Q(dataOut[12]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][13] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[13]),
        .Q(dataOut[13]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][14] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[14]),
        .Q(dataOut[14]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][15] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[15]),
        .Q(dataOut[15]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][16] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[16]),
        .Q(dataOut[16]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][17] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[17]),
        .Q(dataOut[17]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][18] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[18]),
        .Q(dataOut[18]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][19] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[19]),
        .Q(dataOut[19]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][1] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[1]),
        .Q(dataOut[1]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][20] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[20]),
        .Q(dataOut[20]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][21] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[21]),
        .Q(dataOut[21]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][22] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[22]),
        .Q(dataOut[22]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][23] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[23]),
        .Q(dataOut[23]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][24] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[24]),
        .Q(dataOut[24]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][25] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[25]),
        .Q(dataOut[25]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][26] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[26]),
        .Q(dataOut[26]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][27] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[27]),
        .Q(dataOut[27]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][28] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[28]),
        .Q(dataOut[28]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][29] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[29]),
        .Q(dataOut[29]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][2] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[2]),
        .Q(dataOut[2]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][30] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[30]),
        .Q(dataOut[30]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][31] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[31]),
        .Q(dataOut[31]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][3] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[3]),
        .Q(dataOut[3]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][4] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[4]),
        .Q(dataOut[4]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][5] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[5]),
        .Q(dataOut[5]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][6] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[6]),
        .Q(dataOut[6]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][7] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[7]),
        .Q(dataOut[7]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][8] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[8]),
        .Q(dataOut[8]),
        .R(writeTrigger_n_2));
  FDRE \dataReadArray_reg[0][9] 
       (.C(m00_axi_aclk),
        .CE(dataReadArray),
        .D(m00_axi_rdata[9]),
        .Q(dataOut[9]),
        .R(writeTrigger_n_2));
  design_1_dramControllerLITE_0_0_Register_0 data_reg
       (.Q(savedDataIn),
        .SR(data_reg_n_0),
        .dataIn(dataIn),
        .doSetup(doSetup),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_aresetn(m00_axi_aresetn));
  FDRE last_read_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index_reg[2]_1 ),
        .Q(last_read),
        .R(writeTrigger_n_2));
  FDRE last_write_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index_reg[2]_1 ),
        .Q(last_write),
        .R(writeTrigger_n_2));
  design_1_dramControllerLITE_0_0_Register__parameterized0 readError_reg
       (.axi_rready_reg(axi_rready_reg_0),
        .m00_axi_aclk(m00_axi_aclk),
        .read_error_cumulative(read_error_cumulative),
        .\state_reg[1] (writeTrigger_n_2));
  design_1_dramControllerLITE_0_0_EdgeTrigger readTrigger
       (.ERROR_reg(ERROR_reg_0),
        .SR(data_reg_n_0),
        .current(current),
        .current_1(current_1),
        .doRead(doRead),
        .m00_axi_aclk(m00_axi_aclk),
        .next(next),
        .next_0(next_0),
        .reads_done(reads_done),
        .\state_reg[0] (readTrigger_n_2),
        .\state_reg[0]_0 (state[0]),
        .\state_reg[1] (\state_reg[1]_0 ),
        .\state_reg[1]_0 (readTrigger_n_4),
        .\state_reg[1]_1 (state[1]),
        .writes_done(writes_done));
  LUT2 #(
    .INIT(4'h6)) 
    \read_index[0]_i_1 
       (.I0(\read_index_reg[2]_0 ),
        .I1(read_index[0]),
        .O(\read_index[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \read_index[1]_i_1 
       (.I0(read_index[0]),
        .I1(\read_index_reg[2]_0 ),
        .I2(read_index[1]),
        .O(\read_index[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \read_index[2]_i_1 
       (.I0(read_index[0]),
        .I1(read_index[1]),
        .I2(\read_index_reg[2]_0 ),
        .I3(read_index[2]),
        .O(\read_index[2]_i_1_n_0 ));
  FDRE \read_index_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index[0]_i_1_n_0 ),
        .Q(read_index[0]),
        .R(writeTrigger_n_2));
  FDRE \read_index_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index[1]_i_1_n_0 ),
        .Q(read_index[1]),
        .R(writeTrigger_n_2));
  FDRE \read_index_reg[2] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index[2]_i_1_n_0 ),
        .Q(read_index[2]),
        .R(writeTrigger_n_2));
  LUT5 #(
    .INIT(32'h00000001)) 
    read_issued_i_2
       (.I0(read_issued_reg_0),
        .I1(\read_index_reg[2]_0 ),
        .I2(m00_axi_arvalid),
        .I3(m00_axi_rvalid),
        .I4(last_read),
        .O(start_single_read0));
  FDRE read_issued_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\state_reg[0]_0 ),
        .Q(read_issued_reg_0),
        .R(data_reg_n_0));
  FDRE reads_done_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(last_read_reg_0),
        .Q(reads_done),
        .R(writeTrigger_n_2));
  FDRE requestAccepted_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\state_reg[1]_1 ),
        .Q(requestAccepted),
        .R(data_reg_n_0));
  FDRE requestDone_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(writes_done_reg_0),
        .Q(requestDone),
        .R(data_reg_n_0));
  FDRE start_single_read_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\state_reg[0]_1 ),
        .Q(\read_index_reg[2]_0 ),
        .R(data_reg_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_write_i_2
       (.I0(last_write),
        .I1(\write_index_reg[2]_0 ),
        .I2(m00_axi_wvalid),
        .I3(write_issued_reg_0),
        .I4(m00_axi_awvalid),
        .I5(m00_axi_bvalid),
        .O(start_single_write0));
  FDRE start_single_write_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\state_reg[1]_2 ),
        .Q(\write_index_reg[2]_0 ),
        .R(data_reg_n_0));
  FDRE \state_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(readTrigger_n_2),
        .Q(state[0]),
        .R(data_reg_n_0));
  FDRE \state_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(readTrigger_n_4),
        .Q(state[1]),
        .R(data_reg_n_0));
  design_1_dramControllerLITE_0_0_Register__parameterized0_1 writeError_reg
       (.axi_bready_reg(axi_bready_reg_0),
        .m00_axi_aclk(m00_axi_aclk),
        .\state_reg[1] (writeTrigger_n_2),
        .write_error_cumulative(write_error_cumulative));
  design_1_dramControllerLITE_0_0_EdgeTrigger_2 writeTrigger
       (.E(writeTrigger_n_4),
        .Q(savedDataIn),
        .SR(data_reg_n_0),
        .axi_wvalid_reg(m00_axi_wvalid),
        .current(current_1),
        .current_0(current),
        .dataIn(dataIn),
        .doSetup(doSetup),
        .doWrite(doWrite),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_wready(m00_axi_wready),
        .next(next_0),
        .next_1(next),
        .out({writeTrigger_n_5,writeTrigger_n_6,writeTrigger_n_7,writeTrigger_n_8,writeTrigger_n_9,writeTrigger_n_10,writeTrigger_n_11,writeTrigger_n_12,writeTrigger_n_13,writeTrigger_n_14,writeTrigger_n_15,writeTrigger_n_16,writeTrigger_n_17,writeTrigger_n_18,writeTrigger_n_19,writeTrigger_n_20,writeTrigger_n_21,writeTrigger_n_22,writeTrigger_n_23,writeTrigger_n_24,writeTrigger_n_25,writeTrigger_n_26,writeTrigger_n_27,writeTrigger_n_28,writeTrigger_n_29,writeTrigger_n_30,writeTrigger_n_31,writeTrigger_n_32,writeTrigger_n_33,writeTrigger_n_34,writeTrigger_n_35,writeTrigger_n_36}),
        .\q_reg[0] (writeTrigger_n_2),
        .state(state),
        .\write_index_reg[0] (write_index[0]),
        .\write_index_reg[1] (write_index[1]),
        .\write_index_reg[2] (write_index[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \write_index[0]_i_1 
       (.I0(\write_index_reg[2]_0 ),
        .I1(write_index[0]),
        .O(\write_index[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_index[1]_i_1 
       (.I0(write_index[0]),
        .I1(\write_index_reg[2]_0 ),
        .I2(write_index[1]),
        .O(\write_index[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \write_index[2]_i_1 
       (.I0(write_index[0]),
        .I1(write_index[1]),
        .I2(\write_index_reg[2]_0 ),
        .I3(write_index[2]),
        .O(\write_index[2]_i_1_n_0 ));
  FDRE \write_index_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index[0]_i_1_n_0 ),
        .Q(write_index[0]),
        .R(writeTrigger_n_2));
  FDRE \write_index_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index[1]_i_1_n_0 ),
        .Q(write_index[1]),
        .R(writeTrigger_n_2));
  FDRE \write_index_reg[2] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index[2]_i_1_n_0 ),
        .Q(write_index[2]),
        .R(writeTrigger_n_2));
  FDRE write_issued_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\state_reg[1]_3 ),
        .Q(write_issued_reg_0),
        .R(data_reg_n_0));
  FDRE writes_done_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(last_write_reg_0),
        .Q(writes_done),
        .R(writeTrigger_n_2));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
