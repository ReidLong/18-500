// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Tue Mar 13 13:26:25 2018
// Host        : DESKTOP-QVPG904 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/Reid/Documents/18-500/dram_test/dram_test.srcs/sources_1/bd/design_1/ip/design_1_dramControllerLITE_0_0/design_1_dramControllerLITE_0_0_stub.v
// Design      : design_1_dramControllerLITE_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "dramControllerLITE_v1_0,Vivado 2017.2" *)
module design_1_dramControllerLITE_0_0(dataIn, dataOut, addressBase, doRead, doWrite, 
  requestAccepted, requestDone, m00_axi_awaddr, m00_axi_awprot, m00_axi_awvalid, 
  m00_axi_awready, m00_axi_wdata, m00_axi_wstrb, m00_axi_wvalid, m00_axi_wready, 
  m00_axi_bresp, m00_axi_bvalid, m00_axi_bready, m00_axi_araddr, m00_axi_arprot, 
  m00_axi_arvalid, m00_axi_arready, m00_axi_rdata, m00_axi_rresp, m00_axi_rvalid, 
  m00_axi_rready, m00_axi_aclk, m00_axi_aresetn, m00_axi_error)
/* synthesis syn_black_box black_box_pad_pin="dataIn[31:0],dataOut[31:0],addressBase[31:0],doRead,doWrite,requestAccepted,requestDone,m00_axi_awaddr[31:0],m00_axi_awprot[2:0],m00_axi_awvalid,m00_axi_awready,m00_axi_wdata[31:0],m00_axi_wstrb[3:0],m00_axi_wvalid,m00_axi_wready,m00_axi_bresp[1:0],m00_axi_bvalid,m00_axi_bready,m00_axi_araddr[31:0],m00_axi_arprot[2:0],m00_axi_arvalid,m00_axi_arready,m00_axi_rdata[31:0],m00_axi_rresp[1:0],m00_axi_rvalid,m00_axi_rready,m00_axi_aclk,m00_axi_aresetn,m00_axi_error" */;
  input [31:0]dataIn;
  output [31:0]dataOut;
  input [31:0]addressBase;
  input doRead;
  input doWrite;
  output requestAccepted;
  output requestDone;
  output [31:0]m00_axi_awaddr;
  output [2:0]m00_axi_awprot;
  output m00_axi_awvalid;
  input m00_axi_awready;
  output [31:0]m00_axi_wdata;
  output [3:0]m00_axi_wstrb;
  output m00_axi_wvalid;
  input m00_axi_wready;
  input [1:0]m00_axi_bresp;
  input m00_axi_bvalid;
  output m00_axi_bready;
  output [31:0]m00_axi_araddr;
  output [2:0]m00_axi_arprot;
  output m00_axi_arvalid;
  input m00_axi_arready;
  input [31:0]m00_axi_rdata;
  input [1:0]m00_axi_rresp;
  input m00_axi_rvalid;
  output m00_axi_rready;
  input m00_axi_aclk;
  input m00_axi_aresetn;
  output m00_axi_error;
endmodule
