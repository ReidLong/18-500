// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Tue Mar 13 12:52:31 2018
// Host        : DESKTOP-QVPG904 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/Users/Reid/Documents/18-500/dram_test/dram_test.srcs/sources_1/bd/design_1/ip/design_1_dramTest_tb_0_0/design_1_dramTest_tb_0_0_sim_netlist.v
// Design      : design_1_dramTest_tb_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_dramTest_tb_0_0,top,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "top,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module design_1_dramTest_tb_0_0
   (switch,
    led,
    button,
    clock,
    addressBase,
    writeData,
    readData,
    doRead,
    doWrite,
    requestAccepted,
    requestDone,
    axi_error);
  input [7:0]switch;
  output [7:0]led;
  input [4:0]button;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clock CLK" *) input clock;
  output [31:0]addressBase;
  output [31:0]writeData;
  input [31:0]readData;
  output doRead;
  output doWrite;
  input requestAccepted;
  input requestDone;
  input axi_error;

  wire \<const0> ;
  wire \<const1> ;
  wire [9:2]\^addressBase ;
  wire axi_error;
  wire [4:0]button;
  wire clock;
  wire doRead;
  wire doWrite;
  wire [7:1]\^led ;
  wire [31:0]readData;
  wire requestAccepted;
  wire requestDone;
  wire [7:0]switch;
  wire [9:2]\^writeData ;

  assign addressBase[31] = \<const0> ;
  assign addressBase[30] = \<const0> ;
  assign addressBase[29] = \<const0> ;
  assign addressBase[28] = \<const0> ;
  assign addressBase[27] = \<const0> ;
  assign addressBase[26] = \<const0> ;
  assign addressBase[25] = \<const0> ;
  assign addressBase[24] = \<const0> ;
  assign addressBase[23] = \<const0> ;
  assign addressBase[22] = \<const0> ;
  assign addressBase[21] = \<const0> ;
  assign addressBase[20] = \<const0> ;
  assign addressBase[19] = \<const0> ;
  assign addressBase[18] = \<const0> ;
  assign addressBase[17] = \<const0> ;
  assign addressBase[16] = \<const0> ;
  assign addressBase[15] = \<const0> ;
  assign addressBase[14] = \<const0> ;
  assign addressBase[13] = \<const0> ;
  assign addressBase[12] = \<const0> ;
  assign addressBase[11] = \<const0> ;
  assign addressBase[10] = \<const0> ;
  assign addressBase[9:2] = \^addressBase [9:2];
  assign addressBase[1] = \<const0> ;
  assign addressBase[0] = \<const0> ;
  assign led[7:1] = \^led [7:1];
  assign led[0] = \<const1> ;
  assign writeData[31] = \<const0> ;
  assign writeData[30] = \<const0> ;
  assign writeData[29] = \<const0> ;
  assign writeData[28] = \<const0> ;
  assign writeData[27] = \<const0> ;
  assign writeData[26] = \<const0> ;
  assign writeData[25] = \<const0> ;
  assign writeData[24] = \<const0> ;
  assign writeData[23] = \<const0> ;
  assign writeData[22] = \<const0> ;
  assign writeData[21] = \<const0> ;
  assign writeData[20] = \<const0> ;
  assign writeData[19] = \<const0> ;
  assign writeData[18] = \<const0> ;
  assign writeData[17] = \<const0> ;
  assign writeData[16] = \<const0> ;
  assign writeData[15] = \<const0> ;
  assign writeData[14] = \<const0> ;
  assign writeData[13] = \<const0> ;
  assign writeData[12] = \<const0> ;
  assign writeData[11] = \<const0> ;
  assign writeData[10] = \<const0> ;
  assign writeData[9:2] = \^writeData [9:2];
  assign writeData[1] = \<const0> ;
  assign writeData[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  design_1_dramTest_tb_0_0_top inst
       (.addressBase(\^addressBase ),
        .axi_error(axi_error),
        .button(button),
        .clock(clock),
        .doRead(doRead),
        .doWrite(doWrite),
        .led(\^led ),
        .readData(readData),
        .requestAccepted(requestAccepted),
        .requestDone(requestDone),
        .switch(switch),
        .writeData(\^writeData ));
endmodule

(* ORIG_REF_NAME = "DRAM_tb" *) 
module design_1_dramTest_tb_0_0_DRAM_tb
   (\led[7] ,
    \led[5] ,
    \led[6] ,
    doRead,
    doWrite,
    addressBase,
    writeData,
    led,
    axi_error,
    D,
    Q,
    requestDone,
    requestAccepted,
    clock,
    readData,
    \sig_s_reg[0] );
  output \led[7] ;
  output \led[5] ;
  output \led[6] ;
  output doRead;
  output doWrite;
  output [7:0]addressBase;
  output [7:0]writeData;
  output [3:0]led;
  input axi_error;
  input [7:0]D;
  input [4:0]Q;
  input requestDone;
  input requestAccepted;
  input clock;
  input [31:0]readData;
  input \sig_s_reg[0] ;

  wire [7:0]D;
  wire [4:0]Q;
  wire [7:0]addressBase;
  wire axi_error;
  wire clock;
  wire [3:0]data1;
  wire [3:0]data2;
  wire [3:0]data3;
  wire [3:0]data4;
  wire [3:0]data5;
  wire [3:0]data6;
  wire [3:0]data7;
  wire doRead;
  wire doWrite;
  wire [3:0]led;
  wire \led[1]_INST_0_i_1_n_0 ;
  wire \led[1]_INST_0_i_2_n_0 ;
  wire \led[2]_INST_0_i_1_n_0 ;
  wire \led[2]_INST_0_i_2_n_0 ;
  wire \led[3]_INST_0_i_1_n_0 ;
  wire \led[3]_INST_0_i_2_n_0 ;
  wire \led[4]_INST_0_i_1_n_0 ;
  wire \led[4]_INST_0_i_2_n_0 ;
  wire \led[5] ;
  wire \led[6] ;
  wire \led[7] ;
  wire [31:0]readData;
  wire requestAccepted;
  wire requestDone;
  wire result_reg_n_28;
  wire result_reg_n_29;
  wire result_reg_n_30;
  wire result_reg_n_31;
  wire \sig_s_reg[0] ;
  wire \state[0]_i_1_n_0 ;
  wire \state[0]_i_2_n_0 ;
  wire \state[1]_i_1_n_0 ;
  wire \state[1]_i_2_n_0 ;
  wire \state[2]_i_1_n_0 ;
  wire \state[2]_i_3_n_0 ;
  wire \state[2]_i_4_n_0 ;
  wire [7:0]writeData;

  design_1_dramTest_tb_0_0_Register address_reg
       (.D(D),
        .Q({Q[4],Q[1]}),
        .addressBase(addressBase),
        .clock(clock));
  design_1_dramTest_tb_0_0_Register_0 data_reg
       (.D(D),
        .Q({Q[4],Q[2]}),
        .clock(clock),
        .writeData(writeData));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h04)) 
    doRead_INST_0
       (.I0(\led[5] ),
        .I1(\led[6] ),
        .I2(\led[7] ),
        .O(doRead));
  LUT3 #(
    .INIT(8'h40)) 
    doWrite_INST_0
       (.I0(\led[6] ),
        .I1(\led[5] ),
        .I2(\led[7] ),
        .O(doWrite));
  MUXF7 \led[1]_INST_0 
       (.I0(\led[1]_INST_0_i_1_n_0 ),
        .I1(\led[1]_INST_0_i_2_n_0 ),
        .O(led[0]),
        .S(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[1]_INST_0_i_1 
       (.I0(data3[0]),
        .I1(data2[0]),
        .I2(D[1]),
        .I3(data1[0]),
        .I4(D[0]),
        .I5(result_reg_n_31),
        .O(\led[1]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[1]_INST_0_i_2 
       (.I0(data7[0]),
        .I1(data6[0]),
        .I2(D[1]),
        .I3(data5[0]),
        .I4(D[0]),
        .I5(data4[0]),
        .O(\led[1]_INST_0_i_2_n_0 ));
  MUXF7 \led[2]_INST_0 
       (.I0(\led[2]_INST_0_i_1_n_0 ),
        .I1(\led[2]_INST_0_i_2_n_0 ),
        .O(led[1]),
        .S(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[2]_INST_0_i_1 
       (.I0(data3[1]),
        .I1(data2[1]),
        .I2(D[1]),
        .I3(data1[1]),
        .I4(D[0]),
        .I5(result_reg_n_30),
        .O(\led[2]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[2]_INST_0_i_2 
       (.I0(data7[1]),
        .I1(data6[1]),
        .I2(D[1]),
        .I3(data5[1]),
        .I4(D[0]),
        .I5(data4[1]),
        .O(\led[2]_INST_0_i_2_n_0 ));
  MUXF7 \led[3]_INST_0 
       (.I0(\led[3]_INST_0_i_1_n_0 ),
        .I1(\led[3]_INST_0_i_2_n_0 ),
        .O(led[2]),
        .S(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[3]_INST_0_i_1 
       (.I0(data3[2]),
        .I1(data2[2]),
        .I2(D[1]),
        .I3(data1[2]),
        .I4(D[0]),
        .I5(result_reg_n_29),
        .O(\led[3]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[3]_INST_0_i_2 
       (.I0(data7[2]),
        .I1(data6[2]),
        .I2(D[1]),
        .I3(data5[2]),
        .I4(D[0]),
        .I5(data4[2]),
        .O(\led[3]_INST_0_i_2_n_0 ));
  MUXF7 \led[4]_INST_0 
       (.I0(\led[4]_INST_0_i_1_n_0 ),
        .I1(\led[4]_INST_0_i_2_n_0 ),
        .O(led[3]),
        .S(D[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[4]_INST_0_i_1 
       (.I0(data3[3]),
        .I1(data2[3]),
        .I2(D[1]),
        .I3(data1[3]),
        .I4(D[0]),
        .I5(result_reg_n_28),
        .O(\led[4]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \led[4]_INST_0_i_2 
       (.I0(data7[3]),
        .I1(data6[3]),
        .I2(D[1]),
        .I3(data5[3]),
        .I4(D[0]),
        .I5(data4[3]),
        .O(\led[4]_INST_0_i_2_n_0 ));
  design_1_dramTest_tb_0_0_Register_1 result_reg
       (.Q({data7,data6,data5,data4,data3,data2,data1,result_reg_n_28,result_reg_n_29,result_reg_n_30,result_reg_n_31}),
        .clock(clock),
        .readData(readData),
        .requestDone(requestDone),
        .\sig_s_reg[4] (Q[4]),
        .\state_reg[0] (\led[5] ),
        .\state_reg[1] (\led[6] ),
        .\state_reg[2] (\led[7] ));
  LUT6 #(
    .INIT(64'h00000000FEAE02A2)) 
    \state[0]_i_1 
       (.I0(\led[5] ),
        .I1(\sig_s_reg[0] ),
        .I2(\led[7] ),
        .I3(\state[2]_i_3_n_0 ),
        .I4(\state[0]_i_2_n_0 ),
        .I5(Q[4]),
        .O(\state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEDEDEDED0F0A0A0A)) 
    \state[0]_i_2 
       (.I0(\led[7] ),
        .I1(axi_error),
        .I2(\led[5] ),
        .I3(D[7]),
        .I4(Q[3]),
        .I5(\led[6] ),
        .O(\state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FEAE02A2)) 
    \state[1]_i_1 
       (.I0(\led[6] ),
        .I1(\sig_s_reg[0] ),
        .I2(\led[7] ),
        .I3(\state[2]_i_3_n_0 ),
        .I4(\state[1]_i_2_n_0 ),
        .I5(Q[4]),
        .O(\state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hEDF0)) 
    \state[1]_i_2 
       (.I0(\led[7] ),
        .I1(axi_error),
        .I2(\led[5] ),
        .I3(\led[6] ),
        .O(\state[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0000EE0C)) 
    \state[2]_i_1 
       (.I0(\sig_s_reg[0] ),
        .I1(\led[7] ),
        .I2(\state[2]_i_3_n_0 ),
        .I3(\state[2]_i_4_n_0 ),
        .I4(Q[4]),
        .O(\state[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \state[2]_i_3 
       (.I0(requestAccepted),
        .I1(\led[5] ),
        .I2(requestDone),
        .I3(\led[6] ),
        .I4(Q[0]),
        .O(\state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hE8E8E8E8AAAFAFAF)) 
    \state[2]_i_4 
       (.I0(\led[7] ),
        .I1(axi_error),
        .I2(\led[5] ),
        .I3(Q[3]),
        .I4(D[7]),
        .I5(\led[6] ),
        .O(\state[2]_i_4_n_0 ));
  FDRE \state_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(\state[0]_i_1_n_0 ),
        .Q(\led[5] ),
        .R(1'b0));
  FDRE \state_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(\state[1]_i_1_n_0 ),
        .Q(\led[6] ),
        .R(1'b0));
  FDRE \state_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(\state[2]_i_1_n_0 ),
        .Q(\led[7] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramTest_tb_0_0_Register
   (addressBase,
    Q,
    D,
    clock);
  output [7:0]addressBase;
  input [1:0]Q;
  input [7:0]D;
  input clock;

  wire [7:0]D;
  wire [1:0]Q;
  wire [7:0]addressBase;
  wire clock;

  FDRE \q_reg[2] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[0]),
        .Q(addressBase[0]),
        .R(Q[1]));
  FDRE \q_reg[3] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[1]),
        .Q(addressBase[1]),
        .R(Q[1]));
  FDRE \q_reg[4] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[2]),
        .Q(addressBase[2]),
        .R(Q[1]));
  FDRE \q_reg[5] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[3]),
        .Q(addressBase[3]),
        .R(Q[1]));
  FDRE \q_reg[6] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[4]),
        .Q(addressBase[4]),
        .R(Q[1]));
  FDRE \q_reg[7] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[5]),
        .Q(addressBase[5]),
        .R(Q[1]));
  FDRE \q_reg[8] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[6]),
        .Q(addressBase[6]),
        .R(Q[1]));
  FDRE \q_reg[9] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[7]),
        .Q(addressBase[7]),
        .R(Q[1]));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramTest_tb_0_0_Register_0
   (writeData,
    Q,
    D,
    clock);
  output [7:0]writeData;
  input [1:0]Q;
  input [7:0]D;
  input clock;

  wire [7:0]D;
  wire [1:0]Q;
  wire clock;
  wire [7:0]writeData;

  FDRE \q_reg[2] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[0]),
        .Q(writeData[0]),
        .R(Q[1]));
  FDRE \q_reg[3] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[1]),
        .Q(writeData[1]),
        .R(Q[1]));
  FDRE \q_reg[4] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[2]),
        .Q(writeData[2]),
        .R(Q[1]));
  FDRE \q_reg[5] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[3]),
        .Q(writeData[3]),
        .R(Q[1]));
  FDRE \q_reg[6] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[4]),
        .Q(writeData[4]),
        .R(Q[1]));
  FDRE \q_reg[7] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[5]),
        .Q(writeData[5]),
        .R(Q[1]));
  FDRE \q_reg[8] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[6]),
        .Q(writeData[6]),
        .R(Q[1]));
  FDRE \q_reg[9] 
       (.C(clock),
        .CE(Q[0]),
        .D(D[7]),
        .Q(writeData[7]),
        .R(Q[1]));
endmodule

(* ORIG_REF_NAME = "Register" *) 
module design_1_dramTest_tb_0_0_Register_1
   (Q,
    \state_reg[0] ,
    requestDone,
    \state_reg[1] ,
    \state_reg[2] ,
    \sig_s_reg[4] ,
    readData,
    clock);
  output [31:0]Q;
  input \state_reg[0] ;
  input requestDone;
  input \state_reg[1] ;
  input \state_reg[2] ;
  input [0:0]\sig_s_reg[4] ;
  input [31:0]readData;
  input clock;

  wire [31:0]Q;
  wire clock;
  wire doSave;
  wire [31:0]readData;
  wire requestDone;
  wire [0:0]\sig_s_reg[4] ;
  wire \state_reg[0] ;
  wire \state_reg[1] ;
  wire \state_reg[2] ;

  LUT4 #(
    .INIT(16'h0080)) 
    \q[31]_i_1 
       (.I0(\state_reg[0] ),
        .I1(requestDone),
        .I2(\state_reg[1] ),
        .I3(\state_reg[2] ),
        .O(doSave));
  FDRE \q_reg[0] 
       (.C(clock),
        .CE(doSave),
        .D(readData[0]),
        .Q(Q[0]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[10] 
       (.C(clock),
        .CE(doSave),
        .D(readData[10]),
        .Q(Q[10]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[11] 
       (.C(clock),
        .CE(doSave),
        .D(readData[11]),
        .Q(Q[11]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[12] 
       (.C(clock),
        .CE(doSave),
        .D(readData[12]),
        .Q(Q[12]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[13] 
       (.C(clock),
        .CE(doSave),
        .D(readData[13]),
        .Q(Q[13]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[14] 
       (.C(clock),
        .CE(doSave),
        .D(readData[14]),
        .Q(Q[14]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[15] 
       (.C(clock),
        .CE(doSave),
        .D(readData[15]),
        .Q(Q[15]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[16] 
       (.C(clock),
        .CE(doSave),
        .D(readData[16]),
        .Q(Q[16]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[17] 
       (.C(clock),
        .CE(doSave),
        .D(readData[17]),
        .Q(Q[17]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[18] 
       (.C(clock),
        .CE(doSave),
        .D(readData[18]),
        .Q(Q[18]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[19] 
       (.C(clock),
        .CE(doSave),
        .D(readData[19]),
        .Q(Q[19]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[1] 
       (.C(clock),
        .CE(doSave),
        .D(readData[1]),
        .Q(Q[1]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[20] 
       (.C(clock),
        .CE(doSave),
        .D(readData[20]),
        .Q(Q[20]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[21] 
       (.C(clock),
        .CE(doSave),
        .D(readData[21]),
        .Q(Q[21]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[22] 
       (.C(clock),
        .CE(doSave),
        .D(readData[22]),
        .Q(Q[22]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[23] 
       (.C(clock),
        .CE(doSave),
        .D(readData[23]),
        .Q(Q[23]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[24] 
       (.C(clock),
        .CE(doSave),
        .D(readData[24]),
        .Q(Q[24]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[25] 
       (.C(clock),
        .CE(doSave),
        .D(readData[25]),
        .Q(Q[25]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[26] 
       (.C(clock),
        .CE(doSave),
        .D(readData[26]),
        .Q(Q[26]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[27] 
       (.C(clock),
        .CE(doSave),
        .D(readData[27]),
        .Q(Q[27]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[28] 
       (.C(clock),
        .CE(doSave),
        .D(readData[28]),
        .Q(Q[28]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[29] 
       (.C(clock),
        .CE(doSave),
        .D(readData[29]),
        .Q(Q[29]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[2] 
       (.C(clock),
        .CE(doSave),
        .D(readData[2]),
        .Q(Q[2]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[30] 
       (.C(clock),
        .CE(doSave),
        .D(readData[30]),
        .Q(Q[30]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[31] 
       (.C(clock),
        .CE(doSave),
        .D(readData[31]),
        .Q(Q[31]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[3] 
       (.C(clock),
        .CE(doSave),
        .D(readData[3]),
        .Q(Q[3]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[4] 
       (.C(clock),
        .CE(doSave),
        .D(readData[4]),
        .Q(Q[4]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[5] 
       (.C(clock),
        .CE(doSave),
        .D(readData[5]),
        .Q(Q[5]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[6] 
       (.C(clock),
        .CE(doSave),
        .D(readData[6]),
        .Q(Q[6]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[7] 
       (.C(clock),
        .CE(doSave),
        .D(readData[7]),
        .Q(Q[7]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[8] 
       (.C(clock),
        .CE(doSave),
        .D(readData[8]),
        .Q(Q[8]),
        .R(\sig_s_reg[4] ));
  FDRE \q_reg[9] 
       (.C(clock),
        .CE(doSave),
        .D(readData[9]),
        .Q(Q[9]),
        .R(\sig_s_reg[4] ));
endmodule

(* ORIG_REF_NAME = "Sync" *) 
module design_1_dramTest_tb_0_0_Sync
   (Q,
    switch,
    clock);
  output [7:0]Q;
  input [7:0]switch;
  input clock;

  wire [7:0]Q;
  wire clock;
  wire [7:0]sig_i;
  wire [7:0]switch;

  FDRE \sig_i_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[0]),
        .Q(sig_i[0]),
        .R(1'b0));
  FDRE \sig_i_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[1]),
        .Q(sig_i[1]),
        .R(1'b0));
  FDRE \sig_i_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[2]),
        .Q(sig_i[2]),
        .R(1'b0));
  FDRE \sig_i_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[3]),
        .Q(sig_i[3]),
        .R(1'b0));
  FDRE \sig_i_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[4]),
        .Q(sig_i[4]),
        .R(1'b0));
  FDRE \sig_i_reg[5] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[5]),
        .Q(sig_i[5]),
        .R(1'b0));
  FDRE \sig_i_reg[6] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[6]),
        .Q(sig_i[6]),
        .R(1'b0));
  FDRE \sig_i_reg[7] 
       (.C(clock),
        .CE(1'b1),
        .D(switch[7]),
        .Q(sig_i[7]),
        .R(1'b0));
  FDRE \sig_s_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \sig_s_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \sig_s_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \sig_s_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \sig_s_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \sig_s_reg[5] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \sig_s_reg[6] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \sig_s_reg[7] 
       (.C(clock),
        .CE(1'b1),
        .D(sig_i[7]),
        .Q(Q[7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "Sync" *) 
module design_1_dramTest_tb_0_0_Sync__parameterized0
   (\state_reg[2] ,
    Q,
    requestDone,
    \state_reg[0] ,
    requestAccepted,
    \state_reg[1] ,
    button,
    clock);
  output \state_reg[2] ;
  output [4:0]Q;
  input requestDone;
  input \state_reg[0] ;
  input requestAccepted;
  input \state_reg[1] ;
  input [4:0]button;
  input clock;

  wire [4:0]Q;
  wire [4:0]button;
  wire clock;
  wire requestAccepted;
  wire requestDone;
  wire \sig_i_reg_n_0_[0] ;
  wire \sig_i_reg_n_0_[1] ;
  wire \sig_i_reg_n_0_[2] ;
  wire \sig_i_reg_n_0_[3] ;
  wire \sig_i_reg_n_0_[4] ;
  wire \state_reg[0] ;
  wire \state_reg[1] ;
  wire \state_reg[2] ;

  FDRE \sig_i_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(button[0]),
        .Q(\sig_i_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \sig_i_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(button[1]),
        .Q(\sig_i_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \sig_i_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(button[2]),
        .Q(\sig_i_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \sig_i_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(button[3]),
        .Q(\sig_i_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \sig_i_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(button[4]),
        .Q(\sig_i_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \sig_s_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(\sig_i_reg_n_0_[0] ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \sig_s_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(\sig_i_reg_n_0_[1] ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \sig_s_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(\sig_i_reg_n_0_[2] ),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \sig_s_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(\sig_i_reg_n_0_[3] ),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \sig_s_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(\sig_i_reg_n_0_[4] ),
        .Q(Q[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \state[2]_i_2 
       (.I0(requestDone),
        .I1(Q[0]),
        .I2(\state_reg[0] ),
        .I3(requestAccepted),
        .I4(\state_reg[1] ),
        .I5(Q[3]),
        .O(\state_reg[2] ));
endmodule

(* ORIG_REF_NAME = "top" *) 
module design_1_dramTest_tb_0_0_top
   (addressBase,
    writeData,
    led,
    doRead,
    doWrite,
    button,
    clock,
    switch,
    readData,
    axi_error,
    requestDone,
    requestAccepted);
  output [7:0]addressBase;
  output [7:0]writeData;
  output [6:0]led;
  output doRead;
  output doWrite;
  input [4:0]button;
  input clock;
  input [7:0]switch;
  input [31:0]readData;
  input axi_error;
  input requestDone;
  input requestAccepted;

  wire [7:0]addressBase;
  wire axi_error;
  wire [4:0]button;
  wire [3:3]button_s;
  wire button_sync_n_0;
  wire button_sync_n_3;
  wire button_sync_n_5;
  wire clock;
  wire [8:2]d;
  wire doRead;
  wire doWrite;
  wire enable;
  wire [6:0]led;
  wire p_0_in;
  wire [31:0]readData;
  wire requestAccepted;
  wire requestDone;
  wire [7:0]switch;
  wire [7:7]switch_s;
  wire [7:0]writeData;

  design_1_dramTest_tb_0_0_Sync__parameterized0 button_sync
       (.Q({p_0_in,button_s,button_sync_n_3,enable,button_sync_n_5}),
        .button(button),
        .clock(clock),
        .requestAccepted(requestAccepted),
        .requestDone(requestDone),
        .\state_reg[0] (led[4]),
        .\state_reg[1] (led[5]),
        .\state_reg[2] (button_sync_n_0));
  design_1_dramTest_tb_0_0_Sync switch_sync
       (.Q({switch_s,d}),
        .clock(clock),
        .switch(switch));
  design_1_dramTest_tb_0_0_DRAM_tb testBench
       (.D({switch_s,d}),
        .Q({p_0_in,button_s,button_sync_n_3,enable,button_sync_n_5}),
        .addressBase(addressBase),
        .axi_error(axi_error),
        .clock(clock),
        .doRead(doRead),
        .doWrite(doWrite),
        .led(led[3:0]),
        .\led[5] (led[4]),
        .\led[6] (led[5]),
        .\led[7] (led[6]),
        .readData(readData),
        .requestAccepted(requestAccepted),
        .requestDone(requestDone),
        .\sig_s_reg[0] (button_sync_n_0),
        .writeData(writeData));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
