// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Tue Mar 13 12:52:30 2018
// Host        : DESKTOP-QVPG904 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/Reid/Documents/18-500/dram_test/dram_test.srcs/sources_1/bd/design_1/ip/design_1_dramTest_tb_0_0/design_1_dramTest_tb_0_0_stub.v
// Design      : design_1_dramTest_tb_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "top,Vivado 2017.2" *)
module design_1_dramTest_tb_0_0(switch, led, button, clock, addressBase, writeData, 
  readData, doRead, doWrite, requestAccepted, requestDone, axi_error)
/* synthesis syn_black_box black_box_pad_pin="switch[7:0],led[7:0],button[4:0],clock,addressBase[31:0],writeData[31:0],readData[31:0],doRead,doWrite,requestAccepted,requestDone,axi_error" */;
  input [7:0]switch;
  output [7:0]led;
  input [4:0]button;
  input clock;
  output [31:0]addressBase;
  output [31:0]writeData;
  input [31:0]readData;
  output doRead;
  output doWrite;
  input requestAccepted;
  input requestDone;
  input axi_error;
endmodule
