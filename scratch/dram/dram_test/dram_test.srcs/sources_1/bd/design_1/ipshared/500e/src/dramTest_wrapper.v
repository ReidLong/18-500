module top (
  // IO
  input  wire [ 7:0] switch      ,
  output wire [ 7:0] led         ,
  input  wire [ 4:0] button      ,
  // Infrastructure
  input  wire        clock       ,
  // DRAM Interface
  output wire [31:0] addressBase,
  output wire [31:0]        writeData,
  input wire [31:0] readData,
  output wire doRead,
  output wire doWrite,
  input wire requestAccepted,
  input wire requestDone,
  input wire axi_error
);

  wire [7:0] switch_s;
  wire [4:0] button_s;

  Sync #(.WIDTH(8)) switch_sync (.clock(clock), .sig(switch), .sig_s(switch_s));
  Sync #(.WIDTH(5)) button_sync (.clock(clock), .sig(button), .sig_s(button_s));


  DRAM_tb testBench(
    .switch(switch_s),
    .led(led),
    .button(button_s),
    .clock(clock),
    .addressBase(addressBase),
    .writeData(writeData),
    .readData(readData),
    .doRead(doRead),
    .doWrite(doWrite),
    .requestAccepted(requestAccepted),
    .requestDone(requestDone),
    .axi_error(axi_error)
  );

endmodule