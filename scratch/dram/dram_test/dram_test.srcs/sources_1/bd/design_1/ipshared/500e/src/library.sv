module Sync #(parameter WIDTH = 1) (
  input  logic             clock ,
  input  logic [WIDTH-1:0] sig  ,
  output logic [WIDTH-1:0] sig_s
);

  logic [WIDTH-1:0] sig_i;

  always_ff @(posedge clock) begin
    sig_i <= sig;
    sig_s <= sig_i;
  end

endmodule

module Register #(parameter WIDTH=32, CLEAR_VALUE = 0) (
  output logic [WIDTH-1:0] q    ,
  input  logic [WIDTH-1:0] d    ,
  input  logic             clock, enable, clear
);

  always_ff @(posedge clock)
    if(clear)
      q <= CLEAR_VALUE;
    else if (enable)
      q <= d;

  endmodule // register