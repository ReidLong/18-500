module DRAM_tb(
	input logic [7:0] switch,
	output logic [7:0] led,
	input logic [4:0] button, // D:0, L:1, R:2, U:3, C:4
	input logic clock,

	output logic [31:0] addressBase,
	output logic [31:0] writeData,
	input logic [31:0] readData,
	output logic doRead, doWrite,
	input logic requestAccepted, requestDone,
	input logic axi_error
);

	Register #(.WIDTH(32)) address_reg(.q(addressBase), .d({22'b0, switch, 2'b0}), .clock, .enable(button[1]), .clear(button[4]));
	Register #(.WIDTH(32)) data_reg(.q(writeData), .d({22'b0, switch, 2'b0}), .clock, .enable(button[2]), .clear(button[4]));

	logic [31:0] savedResult;
	logic doSave;
	Register #(.WIDTH(32)) result_reg(.q(savedResult), .d(readData), .clock, .enable(doSave), .clear(button[4]));

	enum logic [2:0] {IDLE, READ_ARMED, READ_REQUEST_WAIT, READ_RESPONSE_WAIT, WRITE_ARMED, WRITE_REQUEST_WAIT, WRITE_RESPONSE_WAIT, ERROR} state, nextState;

	always_ff @(posedge clock)
		if(button[4])
			state <= IDLE;
		else
			state <= nextState;

	always_comb begin
		nextState = state;
		doRead = 1'b0;
		doWrite = 1'b0;
		doSave = 1'b0;
		unique case(state)
			IDLE:
				if(switch[7] && button[3])
					nextState = READ_ARMED;
				else if(~switch[7] && button[3])
					nextState = WRITE_ARMED;
			READ_ARMED: begin
				if(button[0])
					nextState = READ_REQUEST_WAIT;
			end
			READ_REQUEST_WAIT: begin
				doRead = 1'b1;
				if(requestAccepted)
					nextState = READ_RESPONSE_WAIT;
			end
			READ_RESPONSE_WAIT: begin
				if(requestDone) begin
					if(axi_error)
						nextState = ERROR;
					else
						nextState = IDLE;
					doSave = 1'b1;
				end
			end
			WRITE_ARMED: begin
				if(button[0])
					nextState = WRITE_REQUEST_WAIT;
			end
			WRITE_REQUEST_WAIT: begin
				doWrite = 1'b1;
				if(requestAccepted)
					nextState = WRITE_RESPONSE_WAIT;
			end
			WRITE_RESPONSE_WAIT:
				if(requestDone) begin
					if(axi_error)
						nextState = ERROR;
					else
						nextState = IDLE;
				end
			ERROR: nextState = ERROR;
		endcase
	end

	always_comb begin
		led[7:5] = state;
		led[0] = 1'b1;
		unique case(switch[2:0])
			3'd0: led[4:1] = savedResult[3:0];
			3'd1: led[4:1] = savedResult[7:4];
			3'd2: led[4:1] = savedResult[11:8];
			3'd3: led[4:1] = savedResult[15:12];
			3'd4: led[4:1] = savedResult[19:16];
			3'd5: led[4:1] = savedResult[23:20];
			3'd6: led[4:1] = savedResult[27:24];
			3'd7: led[4:1] = savedResult[31:28];
		endcase
	end



endmodule