`default_nettype none

`include "riscv_memory.svh"

module DRAM_tb(
    // IO
    input  logic [ 7:0] switch      ,
    output logic [ 7:0] led         ,
    input  logic [ 4:0] button      , // D:0, L:1, R:2, U:3, C:4
    // Infrastructure
    input  logic        clock       ,
    // DRAM Interface
    output logic [15:0][31:0] writeData,
    input logic [15:0][31:0] readData,
    output logic [31:0] addressBase,
    output logic readEnable,
    output logic writeEnable,
    input logic requestAccepted,
    input logic requestCompleted,
    input logic requestError
);

    DRAMRequest_t request;
    DRAMResponse_t response;

    DRAM_Interface dram_interface();

    assign writeData = dram_interface.writeData;
    assign addressBase = dram_interface.addressBase;
    assign readEnable = dram_interface.readEnable;
    assign writeEnable = dram_interface.writeEnable;
    always_comb begin
        dram_interface.readData = readData;
        dram_interface.requestAccepted = requestAccepted;
        dram_interface.requestCompleted = requestCompleted;
        dram_interface.requestError = requestError;
    end

    logic fatalError;
    logic clear;

    assign clear = button[4];

    DRAM_Wrapper wrapper(.request, .response, .dram(dram_interface.Requester), .clock, .clear, .fatalError);

    Register #(.WIDTH(1)) error_reg(.q(led[7]), .d(fatalError), .clock, .enable(fatalError), .clear);


    Address_t savedAddress;
    Word_t savedData;

    logic [4:0] count;
    logic counterClear;
    logic counterIncrement;

    assign request.physicalAddress = savedAddress + count * 4;

    Register #(.WIDTH($bits(savedAddress))) address_reg(.q(savedAddress), .d({18'b0, switch, 6'b0}), .clock, .enable(button[1]), .clear);
    Register #(.WIDTH($bits(savedData))) data_reg(.q(savedData), .d({22'b0, switch, 2'b0}), .clock, .enable(button[2]), .clear);

    logic [15:0][31:0] savedResult;
    logic doSave;
    Array #(.ELEMENTS(16), .WIDTH(32)) result_array(.index(response.physicalAddress[5:2]), .element(response.dataOut), .array(savedResult), .clock, .clear, .enable(doSave));


    Counter #(.WIDTH($bits(count))) validCount(.clock, .clear(clear | counterClear), .enable(counterIncrement), .q(count));


    enum logic [2:0] {IDLE, READ_ARMED, READ_REQUEST, WRITE_ARMED, WRITE_REQUEST} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;

    always_comb begin
        nextState = state;
        request.readEnable = 1'b0;
        request.writeEnable = 1'b0;
        doSave = 1'b0;
        counterClear = 1'b0;
        counterIncrement = 1'b0;
        request.dataIn = 32'b0;
        unique case(state)
            IDLE: begin
                counterClear = 1'b1;
                if(switch[7] && button[3])
                    nextState = READ_ARMED;
                else if(~switch[7] && button[3])
                    nextState = WRITE_ARMED;
            end
            READ_ARMED: begin
                if(button[0])
                    nextState = READ_REQUEST;
            end
            READ_REQUEST: begin
                request.readEnable = 1'b1;
                if(count == 15 && response.isValid) begin
                    doSave = 1'b1;
                    nextState = IDLE;
                    counterIncrement = 1'b1; // Not really necessary
                end else if(response.isValid) begin
                    doSave = 1'b1;
                    counterIncrement = 1'b1;
                end
            end
            WRITE_ARMED: begin
                if(button[0])
                    nextState = WRITE_REQUEST;
            end
            WRITE_REQUEST: begin
                request.writeEnable = 1'b1;
                request.dataIn = {4'b0, count[3:0], savedData[23:0]};
                if(count == 15 && response.isValid) begin
                    // We are going to force a read to the same address to ensure this behavior works
                    nextState = READ_REQUEST;
                    counterClear = 1'b1;
                end else if(response.isValid) begin
                    counterIncrement = 1'b1;
                end
            end
        endcase
    end

    always_comb begin
        led[6:4] = state;
        unique case(switch[2:0])
            3'd0: led[3:0] = savedResult[switch[6:3]][3:0];
            3'd1: led[3:0] = savedResult[switch[6:3]][7:4];
            3'd2: led[3:0] = savedResult[switch[6:3]][11:8];
            3'd3: led[3:0] = savedResult[switch[6:3]][15:12];
            3'd4: led[3:0] = savedResult[switch[6:3]][19:16];
            3'd5: led[3:0] = savedResult[switch[6:3]][23:20];
            3'd6: led[3:0] = savedResult[switch[6:3]][27:24];
            3'd7: led[3:0] = savedResult[switch[6:3]][31:28];
        endcase
    end

endmodule


module DRAM_mock_tb(
    // IO
    input  logic [ 7:0] switch      ,
    output logic [ 7:0] led         ,
    input  logic [ 4:0] button      , // D:0, L:1, R:2, U:3, C:4
    // Infrastructure
    input  logic        clock
    );

    MyDRAMControl_Interface dram();

    DRAM_mock mock(.dram(dram.Source), .clock, .clear(button[4]));

    DRAM_tb testbench(
        .switch,
        .led,
        .button,
        .clock,
        .addressBase(dram.addressBase),
        .writeData(dram.writeData),
        .readData(dram.readData),
        .readEnable(dram.readEnable),
        .writeEnable(dram.writeEnable),
        .requestAccepted(dram.requestAccepted),
        .requestCompleted(dram.requestCompleted),
        .requestError(dram.requestError)
    );

endmodule
