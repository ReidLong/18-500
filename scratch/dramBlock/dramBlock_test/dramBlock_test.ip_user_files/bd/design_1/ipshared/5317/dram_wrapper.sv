`default_nettype none

`include "riscv_memory.svh"
`include "compiler.svh"

typedef logic [$clog2(`BLOCK_SIZE):0] Index_t;

module DRAM_Wrapper(
    // Internal Signals
    input DRAMRequest_t request,
    output DRAMResponse_t response,
    // DRAM Interface
    DRAM_Interface.Requester dram,
    // Infrastructure
    input logic clock, clear,
    output logic fatalError
    );


    logic loadReadArray;
    logic loadWriteArray;
    Word_t [`BLOCK_SIZE-1:0] writeArray;
    Word_t [`BLOCK_SIZE-1:0] readArray;

    // Big enough that it shouldn't overflow unless reset
    Index_t index;
    logic counterIncrement;
    logic counterClear;

    logic responseValid;

    Counter #(.WIDTH($bits(index))) index_counter(.clock, .clear(clear | counterClear), .enable(counterIncrement), .q(index));

    logic loadAddress;
    Register #(.WIDTH($bits(request.physicalAddress))) address_reg(.q(dram.addressBase), .d(request.physicalAddress), .clock, .enable(loadAddress), .clear);

    assign dram.writeData = writeArray;

    Register #(.WIDTH($bits(readArray))) readArray_reg(.q(readArray), .d(dram.readData), .enable(loadReadArray), .clock, .clear);

    `STATIC_ASSERT($bits(Index_t) >= 4);
    `STATIC_ASSERT($clog2(`BLOCK_SIZE) == 4);
    Array #(.WIDTH($bits(Word_t)), .ELEMENTS(`BLOCK_SIZE)) write_array(.index(index[3:0]), .element(request.dataIn), .array(writeArray), .clock, .clear, .enable(loadWriteArray));

    function DRAMResponse_t createDRAMResponse(Address_t addressBase, Index_t index, Word_t dataOut);
        return '{
            isValid: 1'b1,
            physicalAddress: addressBase + index * ($bits(Word_t) / 8),
            dataOut: dataOut
        };
    endfunction


    enum {IDLE, READ_REQUEST, READ_RESPONSE, READ_UNLOAD, WRITE_LOAD, WRITE_REQUEST, WRITE_RESPONSE, FATAL_ERROR} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;

    always_comb begin
        counterIncrement = 1'b0;
        counterClear = 1'b0;
        nextState = state;
        dram.readEnable = 1'b0;
        dram.writeEnable = 1'b0;
        loadAddress = 1'b0;
        fatalError = 1'b0;
        loadReadArray = 1'b0;
        response = '{default: 0};
        loadWriteArray = 1'b0;
        unique case(state)
            IDLE:
                if(request.readEnable) begin
                    loadAddress = 1'b1;
                    nextState = READ_REQUEST;
                    counterClear = 1'b1;
                end else if(request.writeEnable) begin
                    loadAddress = 1'b1;
                    nextState = WRITE_LOAD;
                    counterClear = 1'b1;
                end
            READ_REQUEST: begin
                dram.readEnable = 1'b1;
                if(dram.requestAccepted)
                    nextState = READ_RESPONSE;
            end
            READ_RESPONSE: begin
                if(dram.requestCompleted) begin
                    loadReadArray = 1'b1;
                    counterClear = 1'b1;
                    if(dram.requestError) begin
                        nextState = FATAL_ERROR;
                        fatalError = 1'b1;
                    end else begin
                        nextState = READ_UNLOAD;

                    end
                end
            end
            READ_UNLOAD: begin
                response = createDRAMResponse(dram.addressBase, index, readArray[index]);
                counterIncrement = 1'b1;
                if(index == `BLOCK_SIZE - 1)
                    nextState = IDLE;
            end
            WRITE_LOAD: begin
                // Assuming request.writeEnable
                response = createDRAMResponse(dram.addressBase, index, request.dataIn);
                counterIncrement = 1'b1;
                loadWriteArray = 1'b1;
                if(index == `BLOCK_SIZE - 1)
                    nextState = WRITE_REQUEST;
                if(!request.writeEnable) begin
                    nextState = FATAL_ERROR;
                    fatalError = 1'b1;
                end
            end
            WRITE_REQUEST: begin
                dram.writeEnable = 1'b1;
                if(dram.requestAccepted)
                    nextState = WRITE_RESPONSE;
            end
            WRITE_RESPONSE: begin
                if(dram.requestCompleted) begin
                    if(dram.requestError) begin
                        nextState = FATAL_ERROR;
                        fatalError = 1'b1;
                    end else begin
                        nextState = IDLE;
                    end
                end
            end
            FATAL_ERROR: fatalError = 1'b1; // spin forever
        endcase
    end


endmodule