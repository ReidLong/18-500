`default_nettype none

`include "riscv_memory.svh"

module DRAM_mock(
    DRAM_Interface.Source dram,
    input logic clock, clear
    );

    assert property (@(posedge clock) !(dram.readEnable && dram.writeEnable));
    // We only support 256 MB of physical memory
    assert property (@(posedge clock) dram.savedAddressBase[31:28] == 4'b0);

    localparam BLOCKS = `PHYSICAL_MEMORY_SIZE/4/`BLOCK_SIZE;
    // localparam BLOCKS = 4;

    logic [15:0][31:0] memory[0:BLOCKS-1];

    enum {IDLE, READ_ACCEPT, READ_COMPLETE, WRITE_ACCEPT, WRITE_COMPLETE} state, nextState;

    logic readEdge, writeEdge;

    EdgeTrigger readTrigger(.signal(dram.readEnable), .isEdge(readEdge), .clock, .clear_n(~clear));
    EdgeTrigger writeTrigger(.signal(dram.writeEnable), .isEdge(writeEdge), .clock, .clear_n(~clear));

    logic doSetup;

    logic [31:0] savedAddressBase;
    logic [15:0][31:0] savedWriteData;

    Register #(.WIDTH(32)) address_reg(
        .q(savedAddressBase),
        .d(dram.addressBase),
        .clock, .enable(doSetup), .clear);

    Register #(.WIDTH($bits(savedWriteData))) data_reg(
        .q(savedWriteData),
        .d(dram.writeData),
        .clock, .enable(doSetup), .clear);


    assign dram.requestError = 1'b0; // No errors

    logic loadMemory;
    logic readMemory;

    always_ff @(posedge clock) begin
        if(loadMemory) begin
            memory[savedAddressBase[27:6]] <= savedWriteData;
            dram.readData <= {16{32'b0}};
        end else if(readMemory)
            dram.readData <= memory[savedAddressBase[27:6]];
        else
            dram.readData <= {16{32'b0}};
    end


    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;

    always_comb begin
        nextState = state;
        dram.requestAccepted = 1'b0;
        dram.requestCompleted = 1'b0;
        doSetup = 1'b0;
        loadMemory = 1'b0;
        readMemory = 1'b0;
        unique case(state)
            IDLE:
                if(readEdge) begin
                    nextState = READ_ACCEPT;
                    doSetup = 1'b1;
                end else if(writeEdge) begin
                    nextState = WRITE_ACCEPT;
                    doSetup = 1'b1;
                end
            READ_ACCEPT: begin
                dram.requestAccepted = 1'b1;
                nextState = READ_COMPLETE;
                readMemory = 1'b1;
            end
            READ_COMPLETE: begin
                dram.requestCompleted = 1'b1;
                nextState = IDLE;
            end
            WRITE_ACCEPT: begin
                dram.requestAccepted = 1'b1;
                nextState = WRITE_COMPLETE;
            end
            WRITE_COMPLETE: begin
                loadMemory = 1'b1;
                dram.requestCompleted = 1'b1;
                nextState = IDLE;
            end
        endcase
    end





endmodule
