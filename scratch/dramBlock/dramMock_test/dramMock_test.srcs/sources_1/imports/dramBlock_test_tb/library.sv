`default_nettype none

module Sync #(parameter WIDTH = 1) (
  input  logic             clock ,
  input  logic [WIDTH-1:0] sig  ,
  output logic [WIDTH-1:0] sig_s
);

  logic [WIDTH-1:0] sig_i;

  always_ff @(posedge clock) begin
    sig_i <= sig;
    sig_s <= sig_i;
  end

endmodule

module Register #(parameter WIDTH=32, CLEAR_VALUE = 0) (
  output logic [WIDTH-1:0] q    ,
  input  logic [WIDTH-1:0] d    ,
  input  logic             clock, enable, clear
);

  always_ff @(posedge clock)
    if(clear)
      q <= CLEAR_VALUE;
    else if (enable)
      q <= d;

endmodule // register

module Array #(parameter ELEMENTS=16, WIDTH=32)(
  input logic [$clog2(ELEMENTS)-1:0] index,
  input logic [WIDTH-1:0] element,
  output logic [ELEMENTS-1:0][WIDTH-1:0] array,
  input logic clock, clear, enable
  );
  
  localparam ZERO_ELEMENT = {WIDTH{1'b0}};
  
  always_ff @(posedge clock)
    if(clear)
      array <= {ELEMENTS{ZERO_ELEMENT}};
    else if(enable)
      array[index] <= element;

endmodule

module Counter #(parameter WIDTH = 8) (
   input  logic         clock  ,
   input  logic         clear  ,
   input  logic         enable ,
   output logic [WIDTH-1:0] q
);

   always_ff @(posedge clock)
      if(clear)
         q <= 'b0;
      else if (enable)
         q <= q + 'd1;

endmodule: Counter

module EdgeTrigger(
    input logic signal,
    output logic isEdge,
    input logic clock, clear_n);

    logic current, next;

    assign isEdge = !next && current;

    always_ff @(posedge clock) begin
        if(~clear_n) begin
            current <= 1'b0;
            next <= 1'b0;
        end else begin
            current <= signal;
            next <= current;
        end
    end

endmodule