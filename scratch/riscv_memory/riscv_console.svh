`ifndef __RISCV_CONSOLE_SVH
`define __RISCV_CONSOLE_SVH

`define VRAM_START 32'h1000
`define VRAM_END 32'h2000
`define VRAM_SIZE ((`VRAM_END) - (`VRAM_START))

`define CONSOLE_WIDTH 80
`define CONSOLE_HEIGHT 25

/* Bits 3:0 are the foreground color, bits 6:4 are the
   background color */
`define FGND_BLACK 8'h0
`define FGND_BLUE  8'h1
`define FGND_GREEN 8'h2
`define FGND_CYAN  8'h3
`define FGND_RED   8'h4
`define FGND_MAG   8'h5
`define FGND_BRWN  8'h6
`define FGND_LGRAY 8'h7 /* Light gray. */
`define FGND_DGRAY 8'h8 /* Dark gray. */
`define FGND_BBLUE 8'h9 /* Bright blue. */
`define FGND_BGRN  8'hA /* Bright green. */
`define FGND_BCYAN 8'hB /* Bright cyan. */
`define FGND_PINK  8'hC
`define FGND_BMAG  8'hD /* Bright magenta. */
`define FGND_YLLW  8'hE
`define FGND_WHITE 8'hF

`define BGND_BLACK 8'h00
`define BGND_BLUE  8'h10
`define BGND_GREEN 8'h20
`define BGND_CYAN  8'h30
`define BGND_RED   8'h40
`define BGND_MAG   8'h50
`define BGND_BRWN  8'h60
`define BGND_LGRAY 8'h70 /* Light gray. */

`endif