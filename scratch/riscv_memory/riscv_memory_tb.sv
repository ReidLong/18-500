`default_nettype none

`include "riscv_memory.svh"
`include "compiler.svh"

`define VIDEO_CHECK_SIZE 10
`define MAX_MEMORY_LATENCY 100
`define MEMORY_START 32'h000
`define MEMORY_LIMIT 32'h10000
// `define MEMORY_ALL_LIMIT `PHYSICAL_MEMORY_SIZE
`define MEMORY_ALL_INCREMENT 4
`define MEMORY_ALL_LIMIT 32'h200000
// `define MEMORY_ALL_LIMIT (`VRAM_END + 32'h2000)

module riscv_memory_tb;

    `STATIC_ASSERT(`MEMORY_ALL_LIMIT <= `PHYSICAL_MEMORY_SIZE);

    logic clock, clear;

    MemoryRequest_t instructionRequest, dataRequest, vgaRequest;
    logic instructionReady, dataReady, vgaReady;
    MemoryResponse_t instructionResponse, dataResponse, vgaResponse;

    Word_t dataExpected, vgaExpected, instructionExpected;
    MemoryRequest_t instructionRequestExpected, dataRequestExpected;

    DRAMResponse_t dramResponse;
    DRAMRequest_t dramRequest;

    logic coreClock, vgaClock;
    logic fatalError;

    DRAM_Interface dram_i();

    RISCV_Memory memory(.*);

    DRAM_Wrapper dram_w(.request(dramRequest), .response(dramResponse), .dram(dram_i.Requester), .clock, .clear, .fatalError);

    DRAM_mock mock(.dram(dram_i.Device), .clock, .clear);

    assign coreClock = clock;
    assign vgaClock = clock;

    initial begin
        clock = 1'b0;
        forever #5 clock = ~clock;
    end

    function automatic MemoryRequest_t createRequest(logic readEnable, logic [3:0] writeEnable, Address_t address, AddressType_t addressType, Word_t writeData);
        return '{
            readEnable: readEnable,
            writeEnable: writeEnable,
            address: address,
            addressType: addressType,
            dataIn: writeData
        };
    endfunction

    function automatic logic isEqualRequest(MemoryRequest_t requestA, MemoryRequest_t requestB);
        // TODO: Does == work with structs?
        return requestA.readEnable == requestB.readEnable
            && requestA.writeEnable == requestB.writeEnable
            && requestA.address == requestB.address
            && requestA.addressType == requestB.addressType
            && requestA.dataIn == requestB.dataIn;
    endfunction

    dataSingleEnable: assert property (@(posedge clock) disable iff(clear) !(dataRequest.readEnable && (|dataRequest.writeEnable)));
    vgaNeverWrite: assert property (@(posedge clock) disable iff(clear) !(|vgaRequest.writeEnable));
    instructionNeverWrite: assert property (@(posedge clock) disable iff(clear) !(|instructionRequest.writeEnable));
    vgaSingleCycle: assert property (@(posedge clock) disable iff(clear) vgaRequest.readEnable |=> vgaResponse.isValid);
    vgaAlwaysReady: assert property (@(posedge clock) disable iff(clear) vgaReady);

    writeVRAMResponse: assert property (@(posedge clock) disable iff(clear) |dataRequest.writeEnable && memory.vram.isInVRAM(dataRequest.address) && dataReady |=> dataReady && dataResponse.isValid && isEqualRequest($past(dataRequest,1), dataResponse.request));

    readVRAMResponseReady: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && memory.vram.isInVRAM(dataRequest.address) && dataReady |=> dataReady);
    readVRAMResponseValid: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && memory.vram.isInVRAM(dataRequest.address) && dataReady |=> dataResponse.isValid);
    readVRAMResponseRequest: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && memory.vram.isInVRAM(dataRequest.address) && dataReady |=> isEqualRequest($past(dataRequest,1), dataResponse.request));
    readVRAMResponseExpected: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && memory.vram.isInVRAM(dataRequest.address) && dataReady |=> dataResponse.dataOut == $past(dataExpected,1));

    vgaInVRAM: assert property (@(posedge clock) disable iff(clear) vgaRequest.readEnable |-> memory.vram.isInVRAM(vgaRequest.address));
    readVGAResponse: assert property (@(posedge clock) disable iff(clear) vgaRequest.readEnable |=> vgaResponse.isValid && isEqualRequest($past(vgaRequest, 1), vgaResponse.request) && vgaResponse.dataOut == $past(vgaExpected, 1));

    readInstructionExpectd: assert property (@(posedge clock) disable iff(clear) instructionResponse.isValid |-> instructionResponse.dataOut == $past(instructionExpected, 1)) else $error("%h != %h", instructionResponse.dataOut, $past(instructionExpected, 1));

    readInstructionRequest: assert property (@(posedge clock) disable iff(clear) instructionResponse.isValid |-> isEqualRequest(instructionResponse.request, $past(instructionRequestExpected, 1)));

    readInstructionValid: assert property (@(posedge clock) disable iff(clear) instructionRequest.readEnable |=> 1'b1 ##[0:`MAX_MEMORY_LATENCY] instructionResponse.isValid);

    readInstructionReady: assert property (@(posedge clock) disable iff(clear) instructionRequest.readEnable && instructionReady |=> (!instructionResponse.isValid && !instructionReady) [*0:`MAX_MEMORY_LATENCY] ##1 instructionResponse.isValid && instructionReady);

    readDataExpected: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && dataReady |=> (!dataResponse.isValid [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && dataResponse.dataOut == $past(dataExpected, 1))) else $error("%h != %h @ %h", dataResponse.dataOut, $past(dataExpected, 1), $past(dataRequestExpected.address, 1));

    readDataRequest: assert property (@(posedge clock) disable iff(clear)dataRequest.readEnable && dataReady |=> !dataResponse.isValid [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && isEqualRequest(dataResponse.request, $past(dataRequestExpected,1)));

    readDataValid: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && dataReady |=> !dataResponse.isValid [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid);

    readDataReady: assert property (@(posedge clock) disable iff(clear) dataRequest.readEnable && dataReady |=> (!dataResponse.isValid && !dataReady) [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && dataReady);

    writeDataReadyValid: assert property (@(posedge clock) disable iff(clear) |dataRequest.writeEnable && dataReady |=> (!dataResponse.isValid && !dataReady) [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && dataReady);

    writeDataRequest: assert property (@(posedge clock) disable iff(clear) |dataRequest.writeEnable && dataReady |=> (!dataResponse.isValid && !dataReady) [*0:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && isEqualRequest(dataResponse.request, $past(dataRequestExpected, 1)));

    writeDataCache: assert property (@(posedge clock) disable iff(clear) |dataRequest.writeEnable && dataReady && !memory.vram.isInVRAM(dataRequest.address) |=> (!dataReady && !dataResponse.isValid) [*1:`MAX_MEMORY_LATENCY] ##1 dataResponse.isValid && dataReady);


    class MemoryTestThread;
        MemoryRequest_t tempRequest;
        task assertIdle();
            @(posedge clock);
            assert (instructionReady);
            assert (dataReady);
            assert (vgaReady);
            assert (!instructionResponse.isValid);
            assert (!dataResponse.isValid);
            assert (!vgaResponse.isValid);
        endtask

        task writeVRAM(Address_t address, Word_t writeData);
            assert(memory.vram.isInVRAM(address));
            $display("Writing %h to address %h", writeData, address);

            tempRequest = createRequest(1'b0, 4'hf, address, PHYSICAL_ADDRESS, writeData);
            dataRequest <= tempRequest;
            dataRequestExpected <= tempRequest;
            while(!dataReady) begin
                $display("Waiting for data to be ready for a VRAM write request");
                @(posedge clock);
            end
            // VRAM should be single cycle
        endtask

        task readVRAM(Address_t address, Word_t expected);
            assert(memory.vram.isInVRAM(address));
            tempRequest = createRequest(1'b1, 4'h0, address, PHYSICAL_ADDRESS, `WORD_POISON);
            dataRequest <= tempRequest;
            dataRequestExpected <= tempRequest;
            dataExpected <= expected;
            while(!dataReady)
                @(posedge clock);
            // VRAM should be single cycle

        endtask

        task readVGA(Address_t address, Word_t expected);
            assert(memory.vram.isInVRAM(address));
            assert(vgaReady);
            vgaRequest <= createRequest(1'b1, 4'h0, address, PHYSICAL_ADDRESS, `WORD_POISON);
            vgaExpected <= expected;
            // VRAM should always be a 1-cycle operation
        endtask

        task readInstruction(Address_t address, Word_t expected);
            assert(!memory.vram.isInVRAM(address));
            tempRequest = createRequest(1'b1, 4'h0, address, PHYSICAL_ADDRESS, `WORD_POISON);
            instructionRequest <= tempRequest;
            while(!instructionReady) begin
                $display("Waiting for instruction ready");
                @(posedge clock);
            end
            instructionRequestExpected <= tempRequest;
            instructionExpected <= expected;

        endtask

        task waitInstruction();
            for(int i = 0; i < `MAX_MEMORY_LATENCY && !instructionResponse.isValid; i++) begin
                instructionRequest <= memory.createEmptyRequest();
                // $display("Waiting for instruction to be valid");
                @(posedge clock);
            end
            assert(instructionResponse.isValid);
            // while(!instructionResponse.isValid)
                // @(posedge clock);

        endtask

        task readData(Address_t address, Word_t expected);
            tempRequest = createRequest(1'b1, 4'h0, address, PHYSICAL_ADDRESS, `WORD_POISON);
            dataRequest <= tempRequest;
            dataRequestExpected <= tempRequest;
            dataExpected <= expected;
            while(!dataReady) begin
                $display("Waiting for data ready");
                @(posedge clock);
            end
        endtask

        task writeData(Address_t address, Word_t writeData);
            tempRequest = createRequest(1'b0, 4'hf, address, PHYSICAL_ADDRESS, writeData);
            dataRequest <= tempRequest;
            dataRequestExpected <= tempRequest;
            while(!dataReady)
                @(posedge clock);
        endtask

        task waitData();
            for(int i = 0; i < `MAX_MEMORY_LATENCY && !dataResponse.isValid; i++) begin
                dataRequest <= memory.createEmptyRequest();
                // $display("Waiting for data to be valid");
                @(posedge clock);
            end
            assert(dataResponse.isValid);
        endtask

        task waitDataAndInstruction();
            automatic logic foundInstruction, foundData;
            foundInstruction = instructionResponse.isValid;
            foundData = dataResponse.isValid;
            for(int i = 0; i < `MAX_MEMORY_LATENCY && !(foundInstruction && foundData); i++) begin
                dataRequest <= memory.createEmptyRequest();
                instructionRequest <= memory.createEmptyRequest();
                // $display("Waiting for data and instruction to be valid");
                @(posedge clock);
                if(instructionResponse.isValid)
                    foundInstruction = 1'b1;
                if(dataResponse.isValid)
                    foundData = 1'b1;
            end
            assert(foundData && foundInstruction);
        endtask

        task resetAll();
            instructionRequest <= memory.createEmptyRequest();
            dataRequest <= memory.createEmptyRequest();
            vgaRequest <= memory.createEmptyRequest();
        endtask

    endclass : MemoryTestThread

    class RandomVideoWord;
        rand logic [7:0] character[1:0];
        rand logic [7:0] color[1:0];

        function Word_t makeWord();
            return {color[1], character[1], color[0], character[0]};
        endfunction
    endclass

    class RandomValidVideoWord extends RandomVideoWord;
        // Bit 7 is not valid for a color code
        constraint validColor {
            foreach(color[i]) color[i][7] == 1'b0;
        }
    endclass

    RandomVideoWord videoWords[];
    Word_t instructionWords[];

    initial begin
        MemoryTestThread thread = new;
        videoWords = new[`VIDEO_CHECK_SIZE];
        instructionWords = {32'h00001197, 32'h00018193, 32'h014000ef, 32'h00050113, 32'h00058193, 32'h00a00513,32'h00000073};
        thread.resetAll();
        dataExpected = ~`WORD_POISON;
        vgaExpected = ~`WORD_POISON;
        instructionExpected = ~`WORD_POISON;


        clear = 1'b1;
        repeat(5) @(posedge clock);
        clear = 1'b0;
        @(posedge clock);
        $display("Clear complete. Starting tests");
        repeat(5) begin
            thread.assertIdle();
            @(posedge clock);
        end
        // $display("Starting memory check");
        // for(int block_index = 32'h800; block_index < 32'h900; block_index += 1) begin
        //     $display("%h:: %h", block_index, mock.memory[block_index]);
        // end

        // $display("Memory check0: %h", mock.memory[0]);
        // $display("Memory check1: %h", mock.memory[16'h800]);
        // $display("Memory check2: %h", mock.memory[16'h800][0]);
        assert(mock.memory[16'h4000][0] == 32'h00001197) else $fatal("Bad Memory: %h", mock.memory[16'h800][0]);

        // Validate VRAM
        foreach(videoWords[i]) begin
            videoWords[i] = new;
            assert(videoWords[i].randomize());
            thread.writeVRAM(`VRAM_START + i * 4, videoWords[i].makeWord());
            @(posedge clock);
        end

        foreach(videoWords[i]) begin
            $display("Reading: %d", i);
            thread.readVRAM(`VRAM_START + i*4, videoWords[i].makeWord());
            thread.readVGA(`VRAM_START + i*4, videoWords[i].makeWord());
            @(posedge clock);
        end
        thread.resetAll();
        @(posedge clock);
        thread.assertIdle();

        for(int q = 0; q < $size(videoWords); q++) begin
            automatic int j;
            j = $size(videoWords) - 1 - q;
            thread.readVRAM(`VRAM_START + q * 4, videoWords[q].makeWord());
            thread.readVGA(`VRAM_START + j * 4, videoWords[j].makeWord());
            @(posedge clock);
        end

        thread.resetAll();
        @(posedge clock);


        $display("Trying to read instruction");

        thread.readInstruction(`TEXT_START, 32'h00001197);
        @(posedge clock);
        $display("Reseting instruction request");
        thread.resetAll();
        $display("Waiting for instruction valid");
        thread.waitInstruction();
        foreach(instructionWords[i]) begin
            $display("Reading instructions: %d [%h]", i, instructionWords[i]);
            thread.readInstruction(`TEXT_START + 4*i, instructionWords[i]);
            @(posedge clock);
            thread.waitInstruction();
        end

        thread.resetAll();
        @(posedge clock);

        foreach(instructionWords[i]) begin
            thread.readData(`TEXT_START + 4*i, instructionWords[i]);
            @(posedge clock);
            thread.waitData();
        end

        thread.resetAll();
        @(posedge clock);

        for(int q = 0; q < $size(instructionWords); q++) begin
            automatic int j;
            j = $size(instructionWords) - 1 - q;
            thread.readInstruction(`TEXT_START + 4*q, instructionWords[q]);
            thread.readData(`TEXT_START + 4*j, instructionWords[j]);
            @(posedge clock);
            thread.waitDataAndInstruction();
        end
        thread.resetAll();
        @(posedge clock);

        videoWords = new[$size(instructionWords)];
        for(int j = 0; j < $size(instructionWords); j++) begin
            thread.readInstruction(`TEXT_START + 4*j, instructionWords[j]);
            videoWords[j] = new;
            assert(videoWords[j].randomize());
            thread.writeVRAM(`VRAM_START + j * 4, videoWords[j].makeWord());
            @(posedge clock);
            thread.waitDataAndInstruction();
        end
        thread.resetAll();
        @(posedge clock);

        $display("Starting DRAM tests");
        assert(mock.getWord(`TEXT_START) == 32'h00001197);

        // Cache Miss on Write
        thread.writeData(32'h10100, 32'hAABBCCDD);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        // The word should be in the cache and this is write back
        assert(mock.getWord(32'h10100) == 32'h0);

        $display("DRAM write succeeded");
        @(posedge clock);

        // Cache Hit on Read
        thread.readData(32'h10100, 32'hAABBCCDD);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        @(posedge clock);

        // Cache Hit on Write
        for(int j = 1; j < 16; j++) begin
            thread.writeData(32'h10100 + j * 4, 32'hAA000000 + j);
            @(posedge clock);
            thread.waitData();
        end
        thread.resetAll();
        @(posedge clock);

        $display("Write/Read Succeeded");

        // Cache Miss-Evict on Read
        thread.readData(32'h11104, 32'h0);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        assert(mock.getWord(32'h10100) == 32'hAABBCCDD) else $error("Word not found: %h", mock.getWord(32'h10100));

        $display("DRAM write back succeeded");


        // Cache Miss-Evict on Write
        thread.writeData(32'h10104, 32'h77887788);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        // Not write through
        assert(mock.getWord(32'h10104) == 32'hAA000001);

        // Cache Miss-Evict on instruction read
        assert(mock.getWord(`TEXT_START + 32'h100) == 32'h0);
        thread.readInstruction(`TEXT_START + 32'h100, 32'h0);
        @(posedge clock);
        thread.resetAll();
        thread.waitInstruction();
        assert(mock.getWord(32'h10104) == 32'h77887788);

        thread.writeData(32'h00bc, 32'h99880000);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        thread.writeData(32'h20bc, 32'h55660033);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();
        thread.readData(32'h00bc, 32'h99880000);
        @(posedge clock);
        thread.resetAll();
        thread.waitData();

        $display("Writing first of memory");
        for(int a = `MEMORY_START; a < `MEMORY_LIMIT; a += 4) begin
            if(a[7:0] == 16'h0)
                $display("Writing: %h", a);
            thread.writeData(a, a);
            @(posedge clock);
            thread.waitData();

        end
        thread.resetAll();
        @(posedge clock);


         for(int a = `MEMORY_START; a < `MEMORY_LIMIT; a += 4) begin
            if(a[7:0] == 16'h0)
                $display("Reading %h", a);
            thread.readData(a, a);
            @(posedge clock);
            thread.waitData();
        end
        thread.resetAll();
        @(posedge clock);

        $display("Writing all of memory");
        for(int a = `VRAM_END; a < `MEMORY_ALL_LIMIT; a += `MEMORY_ALL_INCREMENT) begin
            if(a[15:0] == 16'h0)
                $display("Writing: %h", a);
            thread.writeData(a, a);
            @(posedge clock);
            thread.waitData();

        end
        thread.resetAll();
        repeat(3) @(posedge clock);

        $display("Trying to execute instructions and data at same time with collisions");

        thread.readData(32'h2000, 32'h2000);
        thread.readInstruction(32'h3000, 32'h3000);
        @(posedge clock);
        thread.waitDataAndInstruction();

        thread.readData(32'h3100, 32'h3100);
        thread.readInstruction(32'h2100, 32'h2100);
        @(posedge clock);
        thread.waitDataAndInstruction();


        for(int a = `VRAM_END; a < `MEMORY_ALL_LIMIT; a += `MEMORY_ALL_INCREMENT) begin
            automatic int b = `MEMORY_ALL_LIMIT - a - `MEMORY_ALL_INCREMENT + `VRAM_END;
            if(a[15:0] == 16'h0)
                $display("Reading %h and %h", a, b);
            thread.readData(a, a);
            thread.readInstruction(b, b);
            @(posedge clock);
            thread.waitDataAndInstruction();
        end
        thread.resetAll();
        @(posedge clock);

        $display("running special race test: %d", $time);

        thread.readInstruction(32'h100140, 32'h100140);
        @(posedge clock);
        thread.writeData(32'h101148, 32'hce45f666);
        thread.readInstruction(32'h100144, 32'h100144);
        @(posedge clock);
        thread.readInstruction(32'h100144, 32'h100144);
        @(posedge clock);
        // thread.waitInstruction();
        // thread.resetAll();
        repeat(100) @(posedge clock);
        thread.readData(32'h101148, 32'hce45f666);
        @(posedge clock);
        thread.waitData();



        repeat(5) @(posedge clock);

        $display("All tests finished!");

        $finish;

    end


endmodule