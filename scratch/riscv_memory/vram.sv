`default_nettype none

`include "riscv_memory.svh"
`include "compiler.svh"
`include "riscv_console.svh"

// Assume that all vram is direct mapped (virtual address == physical address)
// This enables us to skip any TLB checks when trying to write/read VRAM

module VRAM (
    input  MemoryRequest_t  dataRequest, vgaRequest,
    // Visible on the next clock cycle after the request is asserted
    output MemoryResponse_t dataResponse, vgaResponse,
    // Infrastructure Signals
    input  logic            coreClock, vgaClock, clear
);

    function automatic logic isInVRAM(Address_t address);
        return `VRAM_START <= address && address < `VRAM_END;
    endfunction

    function automatic Address_t calculateVRAMIndex(Address_t address);
        Address_t index = address - `VRAM_START;
        return index;
    endfunction

    function automatic MemoryResponse_t packResponse(MemoryRequest_t savedRequest, Word_t word);
        logic requestValid = savedRequest.readEnable || (|savedRequest.writeEnable);
        AddressTranslationRequest_t fakeTranslationRequest = '{
            virtualAddress: savedRequest.address,
            isValid: 1'b1
        };
        AddressTranslationResponse_t fakeTranslationResponse = '{
            request: fakeTranslationRequest,
            physicalAddress: savedRequest.address,
            isValid: 1'b1,
            translationType: VRAM_DIRECT_MAP
        };

        logic inVRAM = isInVRAM(savedRequest.address);
        return '{
            request     : savedRequest,
            translation : fakeTranslationResponse,
            dataOut : word,
            isValid : inVRAM && requestValid,
            responseType: VRAM
        };
    endfunction

    `STATIC_ASSERT(`VRAM_SIZE > 0);
    `STATIC_ASSERT(`VRAM_START % `PAGE_SIZE == 0);
    `STATIC_ASSERT(`VRAM_END % `PAGE_SIZE == 0);

    Address_t dataIndex, vgaIndex;

    assign dataIndex = calculateVRAMIndex(dataRequest.address);
    assign vgaIndex  = calculateVRAMIndex(vgaRequest.address);

    localparam NB_COL = 4;
    localparam COL_WIDTH = 8;

    `STATIC_ASSERT($bits(Word_t) == NB_COL * COL_WIDTH);
    Word_t dataWord, vgaWord;

    xilinx_bram_2clock_byteWrite #(.NB_COL(NB_COL),.COL_WIDTH(COL_WIDTH),.RAM_DEPTH(1024)) vram (
        .addra(vgaIndex[11:2]          ),
        .dina (`WORD_POISON           ),
        .clka (vgaClock               ),
        .wea  (4'b0                   ),
        .douta(vgaWord                ),
        .addrb(dataIndex[11:2]         ),
        .dinb (dataRequest.dataIn     ),
        .clkb (coreClock              ),
        .web  (dataRequest.writeEnable),
        .doutb(dataWord               )
    );

    MemoryRequest_t savedDataRequest, savedVGARequest;

    Register #(.WIDTH($bits(MemoryRequest_t))) dataRequest_reg (
        .q  (savedDataRequest),
        .d   (dataRequest    ),
        .enable (1'b1            ),
        .clock(coreClock       ),
        .clear
    );
    Register #(.WIDTH($bits(MemoryRequest_t))) vgaRequest_reg (
        .q  (savedVGARequest),
        .d   (vgaRequest     ),
        .enable (1'b1           ),
        .clock(vgaClock       ),
        .clear
    );

    // Visible on the next clock edge
    assign dataResponse = packResponse(savedDataRequest, dataWord);
    assign vgaResponse  = packResponse(savedVGARequest, vgaWord);

endmodule