`default_nettype none

`include "riscv_memory.svh"
`include "compiler.svh"

typedef enum {INST_NEW_REQUEST, INST_SAVED_REQUEST, INST_NO_REQUEST} InstructionSelect_t;
typedef enum {INST_WRITE_BACK, INST_FETCH, DATA_WRITE_BACK, DATA_FETCH, DRAM_DISABLE} DRAMSelect_t;
typedef enum {DATA_NEW_REQUEST, DATA_SAVED_REQUEST, DATA_WRITE_REQUEST, DATA_NO_REQUEST} DataSelect_t;

module MainMemory(
    input MemoryRequest_t instructionRequest, dataRequest,
    output logic instructionReady, dataReady,
    // Visible on the clock edge after request is asserted while ready is valid
    output MemoryResponse_t instructionResponse, dataResponse,

    // DRAM Interface
    output DRAMRequest_t dramRequest,
    input DRAMResponse_t dramResponse,

    // Infrastructure
    input logic clock, clear
);

    // Virtual Address (32 bits): {VPN0[31:22], VPN1[21:12], INDEX[11:6], BLOCK_OFFSET[5:2], BYTE_OFFSET[1:0]}
    // Physical Address (32 bits): {TAG[31:12], INDEX[11:6], BLOCK_OFFSET[5:2], BYTE_OFFSET[1:0]} = {PPN[31:12], PP0[11:0]}

    `STATIC_ASSERT($bits(CacheTag_t) == 20);
    function automatic CacheTag_t calculateTag(Address_t address);
        return address[31:12];
    endfunction

    `STATIC_ASSERT($bits(CacheIndex_t) == 6);
    function automatic CacheIndex_t calculateIndex(Address_t address);
        return address[11:6];
    endfunction

    `STATIC_ASSERT($bits(CacheBlockOffset_t) == 4);
    function automatic CacheBlockOffset_t calculateBlockOffset(Address_t address);
        return address[5:2];
    endfunction

    function automatic Address_t getPhysicalAddress(CacheResponse_t response);
        return {response.tagEntry.tag, response.request.index, response.request.blockOffset, 2'b0};
    endfunction

    logic cacheLocked; // Driven by dram FSM
    logic cacheIdle;
    logic instructionCacheHit, dataCacheHit;
    logic instructionDRAMFillComplete, dataDRAMFillComplete; // Driven by DRAM FSM
    InstructionSelect_t instructionSelect; // Driven by instruction FSM
    DataSelect_t dataSelect; // Driven by data FSM
    logic instructionRequestDRAM, dataRequestDRAM; // Driven by instruction/data FSM
    logic instructionMissWasDirty, dataMissWasDirty;
    logic dramOperationDone;
    DRAMSelect_t dramSelect; // Driven by DRAM FSM
    logic dramClearCounter, dramIncrementCounter; // Driven by DRAM FSM
    logic dramRequestValid; // Driven by DRAM FSM
    logic saveInstructionCacheResponse, saveDataCacheResponse; // Driven by instruction/data FSM
    logic saveInstructionTranslationResponse, saveDataTranslationResponse; // Driven by instruction/data FSM
    logic instructionResponseValid, dataResponseValid; // Driven by instruction/data FSM

    MemoryRequest_t savedInstructionRequest, savedDataRequest;
    CacheResponse_t instructionCacheResponse, dataCacheResponse;
    CacheResponse_t savedInstructionCacheResponse, savedDataCacheResponse;
    AddressTranslationResponse_t savedInstructionTranslationResponse, savedDataTranslationResponse;
    AddressTranslationResponse_t instructionTranslationResponse, dataTranslationResponse;
    CacheRequest_t selectedInstructionCacheRequest, selectedDataCacheRequest;
    CacheRequest_t dramInstructionCacheRequest, dramDataCacheRequest;
    AddressTranslationRequest_t instructionTranslationRequest, dataTranslationRequest;

    assign cacheIdle = (instructionSelect == INST_NO_REQUEST && dataSelect == DATA_NO_REQUEST);

    function automatic logic isCacheHit(CacheResponse_t response, AddressTranslationResponse_t translation);
        return response.tagEntry.isValid && translation.isValid && response.tagEntry.tag == calculateTag(translation.physicalAddress);
    endfunction

    assign instructionCacheHit = isCacheHit(instructionCacheResponse, instructionTranslationResponse);
    assign dataCacheHit = isCacheHit(dataCacheResponse, dataTranslationResponse);

    function automatic logic isCacheDirty(CacheResponse_t response);
        return response.tagEntry.isValid && response.tagEntry.isDirty;
    endfunction

    assign instructionMissWasDirty = isCacheDirty(savedInstructionCacheResponse);
    assign dataMissWasDirty = isCacheDirty(savedDataCacheResponse);

    Register #(.WIDTH($bits(CacheResponse_t))) instructionCacheResponse_reg(
        .q(savedInstructionCacheResponse),
        .d(instructionCacheResponse),
        .enable(saveInstructionCacheResponse),
        .clock,
        .clear
    );

    Register #(.WIDTH($bits(CacheResponse_t))) dataCacheResponse_reg(
        .q(savedDataCacheResponse),
        .d(dataCacheResponse),
        .enable(saveDataCacheResponse),
        .clock,
        .clear
    );

    Register #(.WIDTH($bits(MemoryRequest_t))) instructionRequest_reg(
        .q(savedInstructionRequest),
        .d(instructionRequest),
        .enable(instructionReady),
        .clear,
        .clock
    );

    Register #(.WIDTH($bits(MemoryRequest_t))) dataRequest_reg(
        .q(savedDataRequest),
        .d(dataRequest),
        .enable(dataReady),
        .clear,
        .clock
    );

    Register #(.WIDTH($bits(AddressTranslationResponse_t))) instructionTranslation_reg(
        .q(savedInstructionTranslationResponse),
        .d(instructionTranslationResponse),
        .enable(saveInstructionTranslationResponse),
        .clear,
        .clock
    );

    Register #(.WIDTH($bits(AddressTranslationResponse_t))) dataTranslation_reg(
        .q(savedDataTranslationResponse),
        .d(dataTranslationResponse),
        .enable(saveDataTranslationResponse),
        .clear,
        .clock
    );


    CacheBlockOffset_t dramBlockOffset;
    `STATIC_ASSERT($bits(CacheBlockOffset_t) == 4);
    logic [4:0] dramValidCount;

    Counter #(.WIDTH($bits(dramValidCount))) dramValid_counter(
        .q(dramValidCount),
        .enable(dramResponse.isValid),
        .clear(clear | dramClearCounter),
        .clock
    );

    Counter #(.WIDTH(4)) dramBlockOffset_counter(
        .q(dramBlockOffset),
        .enable(dramIncrementCounter | dramResponse.isValid),
        .clear(clear | dramClearCounter),
        .clock
    );

    function automatic DRAMRequest_t createDRAMRequest(Address_t physicalAddress, logic isWriteBack, Word_t dataIn);
        return '{
            physicalAddress: physicalAddress,
            readEnable: ~isWriteBack,
            writeEnable: isWriteBack,
            dataIn: dataIn
        };
    endfunction

    function automatic Address_t createPhysicalAddress(CacheResponse_t cacheResponse, CacheBlockOffset_t offset);
        Address_t physicalAddress = getPhysicalAddress(cacheResponse);
        return {physicalAddress[31:6], offset, 2'b0};
    endfunction

    function automatic CacheRequest_t createEmptyCacheRequest();
        return '{
            index: 6'b0,
            blockOffset: 4'b0,
            tag: `CACHE_TAG_POISON,
            dataIn: `WORD_POISON,
            requestType: CACHE_READ,
            isValid: 1'b0,
            writeEnable: 4'b0
        };
    endfunction

    DRAMRequest_t newRequest, savedRequest;
    DRAMResponse_t savedDRAMResponse;
    logic dramForceNewRequest;

    // logic dramRequestLoad;
    // logic delayedDRAMRequestLoad;

    // Register #(.WIDTH($bits(delayedDRAMRequestLoad))) dramRequestLoad_reg(.q(delayedDRAMRequestLoad), .d(dramRequestLoad), .enable(1'b1), .clear, .clock);

    // logic delayedDRAMRequestValid;
    // Register #(.WIDTH($bits(delayedDRAMRequestValid))) dramRequestValid_reg(.q(delayedDRAMRequestValid), .d(dramRequestValid), .enable(1'b1), .clear, .clock);

    Register #(.WIDTH($bits(savedRequest))) dramRequest_reg(
        .q(savedRequest),
        .d(newRequest), .enable(!dramResponse.isValid), .clear(dramResponse.isValid | clear), .clock);

    Register #(.WIDTH($bits(savedDRAMResponse))) dramResponse_reg(
        .q(savedDRAMResponse), .d(dramResponse), .enable(1'b1), .clear, .clock);

    // always_comb begin
    //     dramRequest = '{default:0};
    //     if(dramRequestValid) begin
    //         if(delayedDRAMRequestLoad || dramResponse.isValid)
    //             dramRequest = newRequest;
    //         else
    //             dramRequest = savedRequest;
    //     end
    // end

    // logic [3:0] blockOffsetAdjustment;

    // always_comb begin
    //     if(dramRequestLoad)
    //         blockOffsetAdjustment = 0;
    //     else if(dramResponse.isValid) begin
    //         assert(!dramRequestLoad);
    //         blockOffsetAdjustment = 2;
    //     end else begin
    //         assert(!dramRequestLoad)
    //         assert(!dramResponse.isValid)
    //         blockOffsetAdjustment = 1;
    //     end
    // end

    always_comb begin
        newRequest = '{default:0};
        dramInstructionCacheRequest = createEmptyCacheRequest();
        dramDataCacheRequest = createEmptyCacheRequest();
        unique case (dramSelect)
            INST_WRITE_BACK: begin
                newRequest = createDRAMRequest(getPhysicalAddress(instructionCacheResponse), 1'b1, instructionCacheResponse.dataOut);
                dramInstructionCacheRequest = createCacheRequest(createPhysicalAddress(savedInstructionCacheResponse, dramBlockOffset), CACHE_READ, `WORD_POISON, 4'b0);
                if(savedDRAMResponse.isValid || dramForceNewRequest)
                    dramRequest = newRequest;
                else
                    dramRequest = savedRequest;
            end
            DATA_WRITE_BACK: begin
                newRequest = createDRAMRequest(getPhysicalAddress(dataCacheResponse), 1'b1, dataCacheResponse.dataOut);
                dramDataCacheRequest = createCacheRequest(createPhysicalAddress(savedDataCacheResponse, dramBlockOffset), CACHE_READ, `WORD_POISON, 4'b0);
                if(savedDRAMResponse.isValid || dramForceNewRequest)
                    dramRequest = newRequest;
                else
                    dramRequest = savedRequest;
            end
            INST_FETCH: begin
                if(dramResponse.isValid) begin
                    dramInstructionCacheRequest = createCacheRequest(dramResponse.physicalAddress, CACHE_DRAM_FILL, dramResponse.dataOut, 4'hF);
                end
                dramRequest = createDRAMRequest(
                    {savedInstructionTranslationResponse.physicalAddress[31:6], 6'h0}, 1'b0, `WORD_POISON);
            end
            DATA_FETCH: begin
                if(dramResponse.isValid) begin
                    dramDataCacheRequest = createCacheRequest(dramResponse.physicalAddress, CACHE_DRAM_FILL, dramResponse.dataOut, 4'hF);
                end
                dramRequest = createDRAMRequest(
                    {savedDataTranslationResponse.physicalAddress[31:6],6'h0}, 1'b0, `WORD_POISON);
            end
            DRAM_DISABLE: dramRequest = '{default:0};
        endcase

        if(!dramRequestValid)
            dramRequest = '{default:0};

    end

    // This will ensure that we see the operation complete on the same clock cycle as the DRAM control responds. The DRAM FSM will then stop asserting that we have an operation we want the DRAM interface to handle
    // assign dramOperationDone = (dramValidCount == 4'hF) && dramResponse.isValid;
    assign dramOperationDone = (dramValidCount == 5'h10);

    function automatic CacheRequest_t createCacheRequest(
        Address_t address,
        CacheRequestType_t requestType,
        Word_t dataIn,
        logic [3:0] writeEnable
    );
        CacheIndex_t index;
        CacheBlockOffset_t blockOffset;
        CacheTag_t tag;

        tag = calculateTag(address);
        index = calculateIndex(address);
        blockOffset = calculateBlockOffset(address);

        if(requestType == CACHE_READ) begin
            // This is a safety check
            tag = `CACHE_TAG_POISON;
        end

        return '{
            index: index,
            blockOffset: blockOffset,
            tag: tag,
            dataIn: dataIn,
            requestType: requestType,
            isValid: 1'b1,
            writeEnable: writeEnable
        };
    endfunction



    always_comb begin
        selectedInstructionCacheRequest = createEmptyCacheRequest();
        selectedDataCacheRequest = createEmptyCacheRequest();
        instructionTranslationRequest = '{default:0};
        dataTranslationRequest = '{default:0};

        unique case (instructionSelect)
            INST_NEW_REQUEST: begin
                selectedInstructionCacheRequest = createCacheRequest(instructionRequest.address, CACHE_READ, `WORD_POISON, 4'b0);
                instructionTranslationRequest = createTranslationRequest(instructionRequest);
            end
            INST_SAVED_REQUEST: begin
                selectedInstructionCacheRequest = createCacheRequest(savedInstructionRequest.address, CACHE_READ, `WORD_POISON, 4'b0);
                instructionTranslationRequest = createTranslationRequest(savedInstructionRequest);
            end
            INST_NO_REQUEST: selectedInstructionCacheRequest = dramInstructionCacheRequest;
        endcase
        unique case (dataSelect)
            DATA_NEW_REQUEST: begin
                selectedDataCacheRequest = createCacheRequest(dataRequest.address, CACHE_READ, `WORD_POISON, 4'b0);
                dataTranslationRequest = createTranslationRequest(dataRequest);
            end
            DATA_SAVED_REQUEST: begin
                selectedDataCacheRequest = createCacheRequest(savedDataRequest.address, CACHE_READ, `WORD_POISON, 4'b0);
                dataTranslationRequest = createTranslationRequest(savedDataRequest);
            end
            DATA_WRITE_REQUEST: begin
                selectedDataCacheRequest = createCacheRequest(savedDataRequest.address, CACHE_WRITE, savedDataRequest.dataIn, savedDataRequest.writeEnable);
                dataTranslationRequest = createTranslationRequest(savedDataRequest);
            end
            DATA_NO_REQUEST: selectedDataCacheRequest = dramDataCacheRequest;
        endcase
    end

    Cache cache(
        .instructionRequest(selectedInstructionCacheRequest),
        .instructionResponse(instructionCacheResponse),

        .dataRequest(selectedDataCacheRequest),
        .dataResponse(dataCacheResponse),

        .clock,
        .clear
    );

    function automatic AddressTranslationRequest_t createTranslationRequest(MemoryRequest_t request);
        logic isValid = request.readEnable || (|request.writeEnable);
        return '{
            virtualAddress: request.address,
            isValid: isValid
        };
    endfunction

    // assign instructionTranslationRequest = createTranslationRequest(instructionRequest);
    // assign dataTranslationRequest = createTranslationRequest(dataRequest);

    Translation tlb(
        .instructionTranslationRequest,
        .instructionTranslationResponse,

        .dataTranslationRequest,
        .dataTranslationResponse,

        .clock,
        .clear
    );



    Instruction_fsm instructionControl(
        // Inputs
        .cacheLocked,
        .cacheHit(instructionCacheHit),
        .dramFillComplete(instructionDRAMFillComplete),
        // Outputs
        .requestSelect(instructionSelect),
        .requestDRAM(instructionRequestDRAM),
        .saveResponse(saveInstructionCacheResponse),
        .outputValid(instructionResponseValid),
        .saveTranslation(saveInstructionTranslationResponse),
        // External Inputs
        .newRequest(instructionRequest),
        // External Outpus
        .ready(instructionReady),
        // Infrastructure
        .clock,
        .clear
    );

    Data_fsm dataControl(
        // Inputs
        .cacheLocked,
        .cacheHit(dataCacheHit),
        .dramFillComplete(dataDRAMFillComplete),
        // Outputs
        .requestSelect(dataSelect),
        .requestDRAM(dataRequestDRAM),
        .saveResponse(saveDataCacheResponse),
        .outputValid(dataResponseValid),
        .saveTranslation(saveDataTranslationResponse),
        // External Signals
        .newRequest(dataRequest),
        .savedRequest(savedDataRequest),
        .ready(dataReady),
        // Infrastructure
        .clock,
        .clear
    );

    DRAM_fsm dramControl(
        // Inputs
        .instructionMissWasDirty,
        .dataMissWasDirty,
        .cacheIdle,
        .dramOperationDone,
        .instructionRequestFill(instructionRequestDRAM),
        .dataRequestFill(dataRequestDRAM),
        // Outputs
        .cacheLocked,
        .select(dramSelect),
        .instructionRequestDone(instructionDRAMFillComplete),
        .dataRequestDone(dataDRAMFillComplete),
        .clearCounter(dramClearCounter),
        .incrementCounter(dramIncrementCounter),
        .dramRequestValid,
        .forceNewRequest(dramForceNewRequest),

        // Infrastructure
        .clock,
        .clear
    );

    function automatic MemoryResponse_t createMemoryResponse(MemoryRequest_t request, AddressTranslationResponse_t translation, Word_t dataOut, logic isValid);
        return '{
            request: request,
            translation: translation,
            dataOut: dataOut,
            isValid: isValid
        };
    endfunction


    assign instructionResponse = createMemoryResponse(savedInstructionRequest, instructionTranslationResponse, instructionCacheResponse.dataOut, instructionResponseValid);

    assign dataResponse = createMemoryResponse(savedDataRequest, dataTranslationResponse, dataCacheResponse.dataOut, dataResponseValid);



endmodule

module Instruction_fsm (
    // Status Points
    input  logic               cacheLocked, cacheHit,
    input  logic               dramFillComplete,
    // Control Points
    output InstructionSelect_t requestSelect   ,
    output logic               requestDRAM     ,
    output logic saveResponse,
    output logic outputValid,
    output logic saveTranslation,
    // External Signals
    input  MemoryRequest_t     newRequest         ,
    output logic               ready           ,
    // Infrastructure
    input  logic               clock, clear
);

    enum {INST_IDLE, INST_READ, INST_WAIT} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= INST_IDLE;
        else
            state <= nextState;

    always_comb begin
        ready = 1'b0;
        nextState = state;
        requestSelect = INST_NO_REQUEST;
        requestDRAM = 1'b0;
        saveResponse = 1'b0;
        outputValid = 1'b0;
        saveTranslation = 1'b0;

        unique case (state)
            INST_IDLE: begin
                if(cacheLocked) begin
                    ready = 1'b0;
                end else if(newRequest.readEnable) begin
                    assert(!cacheLocked);
                    // This is required to avoid circular dependencies since it is likely readEnable is only asserted if ready is asserted
                    ready = 1'b1;
                    nextState = INST_READ;
                    requestSelect = INST_NEW_REQUEST;
                end else begin
                    // At this point, we have nothing to do, so we idle
                    assert(!cacheLocked);
                    ready = 1'b1;
                end
            end
            INST_READ: begin
                if(!cacheHit) begin
                    // Handle cache miss
                    requestDRAM = 1'b1;
                    saveResponse = 1'b1;
                    saveTranslation = 1'b1;
                    nextState = INST_WAIT;
                end else if(cacheLocked) begin
                    assert(cacheHit);
                    outputValid = 1'b1;
                    // Somebody is trying to lock the cache, so we need to go to idle
                    nextState = INST_IDLE;
                end else if(newRequest.readEnable) begin
                    assert(!cacheLocked);
                    assert(cacheHit);
                    ready = 1'b1;
                    requestSelect = INST_NEW_REQUEST;
                    nextState = INST_READ; // current state
                    outputValid = 1'b1;
                end else begin
                    assert(!cacheLocked);
                    assert(!newRequest.readEnable);
                    assert(cacheHit);
                    ready = 1'b1;
                    nextState = INST_IDLE;
                    outputValid = 1'b1;
                end
            end
            INST_WAIT: begin
                if(dramFillComplete) begin
                    requestSelect = INST_SAVED_REQUEST;
                    nextState = INST_READ;
                    requestDRAM = 1'b1;
                end else begin
                    // Stay here doing nothing
                    requestDRAM = 1'b1;
                end
            end
        endcase
    end
endmodule

module Data_fsm(
    // Status points
    input logic cacheLocked, cacheHit,
    input logic dramFillComplete,

    // Control Points
    output DataSelect_t requestSelect,
    output logic requestDRAM,
    output logic saveResponse,
    output logic outputValid,
    output logic saveTranslation,

    // External Signals
    input MemoryRequest_t newRequest, savedRequest,
    output logic ready,

    // Infrastructure
    input logic clock, clear
);

    enum {DATA_IDLE, DATA_READ, DATA_WRITE, DATA_WAIT} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= DATA_IDLE;
        else
            state <= nextState;

    logic newRequestValid;
    assign newRequestValid = newRequest.readEnable || (|newRequest.writeEnable);

    always_comb begin
        nextState = state;
        ready = 1'b0;
        requestSelect = DATA_NO_REQUEST;
        requestDRAM = 1'b0;
        outputValid = 1'b0;
        saveResponse = 1'b0;
        saveTranslation = 1'b0;
        unique case (state)
            DATA_IDLE: begin
                if(cacheLocked) begin
                    ready = 1'b0;
                end else if(newRequestValid) begin
                    assert(!cacheLocked);
                    // This is required to avoid circular dependencies since it is likely the new request is predicated on ready being asserted
                    ready = 1'b1;
                    nextState = DATA_READ;
                    requestSelect = DATA_NEW_REQUEST;
                end else begin
                    assert(!cacheLocked);
                    ready = 1'b1;
                end
            end
            DATA_READ: begin
                if(!cacheHit) begin
                    // Handle cache miss
                    saveResponse = 1'b1;
                    saveTranslation = 1'b1;
                    requestDRAM = 1'b1;
                    nextState = DATA_WAIT;
                end else if(|savedRequest.writeEnable) begin
                    assert(cacheHit);
                    // It would be an interesting optimization to assert "outputValid" here since technically we know the operation will complete on the next cycle. This would prevent stalling in the common case for store operations.
                    // We have a cache hit, but the operation isn't complete because we have a write to execute
                    requestSelect = DATA_WRITE_REQUEST;
                    nextState = DATA_WRITE;
                end else if(cacheLocked) begin
                    assert(cacheHit);
                    assert(!(|savedRequest.writeEnable));
                    // At this point we have finished the operation; however, the cache is trying to be locked so we need to go to idle
                    outputValid = 1'b1;
                    nextState = DATA_IDLE;
                end else if(newRequestValid) begin
                    assert(cacheHit);
                    assert(!cacheLocked);
                    ready = 1'b1;
                    requestSelect = DATA_NEW_REQUEST;
                    nextState = DATA_READ;
                    outputValid = 1'b1;
                end else begin
                    assert(!cacheLocked);
                    assert(!newRequestValid);
                    assert(cacheHit);
                    ready = 1'b1;
                    nextState = DATA_IDLE;
                    outputValid = 1'b1;
                end
            end
            DATA_WRITE: begin
                // Since we own the lock on the cache (because we are executing) our prior read should ensure that the write is always valid and a cache hit
                assert(cacheHit);
                outputValid = 1'b1;
                if(cacheLocked) begin
                    nextState = DATA_IDLE;
                end else if(newRequestValid) begin
                    assert(!cacheLocked);
                    ready = 1'b1;
                    requestSelect = DATA_NEW_REQUEST;
                    nextState = DATA_READ;
                end else begin
                    assert(!cacheLocked);
                    assert(!newRequestValid);
                    ready = 1'b1;
                    nextState = DATA_IDLE;
                end
            end
            DATA_WAIT: begin
                if(dramFillComplete) begin
                    // Re-execute the old request
                    requestSelect = DATA_SAVED_REQUEST;
                    nextState = DATA_READ;
                    requestDRAM = 1'b1;
                end else begin
                    requestDRAM = 1'b1;
                end
            end
        endcase
    end

endmodule

module DRAM_fsm(
    // Status Points
    input logic instructionMissWasDirty, dataMissWasDirty,
    input logic cacheIdle,
    input logic dramOperationDone,
    // These fill request signals must be held until done is asserted
    input logic instructionRequestFill, dataRequestFill,


    // Control Points
    output logic cacheLocked,
    output DRAMSelect_t select,
    output logic instructionRequestDone, dataRequestDone,
    output logic clearCounter, incrementCounter,
    output logic dramRequestValid,
    output logic forceNewRequest,

    // External Signals

    // Infrastructure
    input logic clock, clear
);

    enum {DRAM_IDLE, DRAM_LOCK, DRAM_WRITE_BACK_START, DRAM_WRITE_BACK_LOOP, DRAM_MISS_CLEAN, DRAM_WRITE_BACK_START_2} state, nextState;
    enum {INST, DATA, NONE} mode, nextMode;

    always_ff @(posedge clock)
        if(clear) begin
            state <= DRAM_IDLE;
            mode <= NONE;
        end else begin
            state <= nextState;
            mode <= nextMode;
        end

    always_comb begin
        cacheLocked = 1'b0;
        select = DRAM_DISABLE;
        instructionRequestDone = 1'b0;
        dataRequestDone = 1'b0;
        nextState = state;
        nextMode = mode;
        clearCounter = 1'b0;
        incrementCounter = 1'b0;
        dramRequestValid = 1'b0;
        forceNewRequest = 1'b0;
        unique case(state)
            DRAM_IDLE:
                if(dataRequestFill) begin
                    nextMode = DATA;
                    nextState = DRAM_LOCK;
                end else if (instructionRequestFill) begin
                    nextMode = INST;
                    nextState = DRAM_LOCK;
                end else begin
                    nextMode = NONE;
                end
            DRAM_LOCK: begin
                cacheLocked = 1'b1;
                if(cacheIdle) begin
                    unique case (mode)
                        DATA: nextState = dataMissWasDirty ? DRAM_WRITE_BACK_START : DRAM_MISS_CLEAN;
                        INST: nextState = instructionMissWasDirty ? DRAM_WRITE_BACK_START : DRAM_MISS_CLEAN;
                    endcase
                    clearCounter = 1'b1;
                end
            end
            DRAM_WRITE_BACK_START: begin
                cacheLocked = 1'b1;
                nextState = DRAM_WRITE_BACK_START_2;
                incrementCounter = 1'b1;
                unique case(mode)
                    DATA: select = DATA_WRITE_BACK;
                    INST: select = INST_WRITE_BACK;
                endcase
            end
            DRAM_WRITE_BACK_START_2: begin
                cacheLocked = 1'b1;
                nextState = DRAM_WRITE_BACK_LOOP;
                forceNewRequest = 1'b1;
                dramRequestValid = 1'b1;
                unique case(mode)
                    DATA: select = DATA_WRITE_BACK;
                    INST: select = INST_WRITE_BACK;
                endcase
            end
            DRAM_WRITE_BACK_LOOP: begin
                cacheLocked = 1'b1;
                unique case(mode)
                    DATA: select = DATA_WRITE_BACK;
                    INST: select = INST_WRITE_BACK;
                endcase
                if(dramOperationDone) begin
                    nextState = DRAM_MISS_CLEAN;
                    clearCounter = 1'b1;
                    // dramRequestValid = 1'b1;
                end else begin
                    dramRequestValid = 1'b1;
                end
            end
            DRAM_MISS_CLEAN: begin
                cacheLocked = 1'b1;
                unique case (mode)
                    DATA: select = DATA_FETCH;
                    INST: select = INST_FETCH;
                endcase
                if(dramOperationDone) begin
                    // This will allow at least one operation to advance (whichever one was just filled) to ensure progress
                    nextState = DRAM_IDLE;
                    unique case(mode)
                        DATA: dataRequestDone = 1'b1;
                        INST: instructionRequestDone = 1'b1;
                    endcase
                end else begin
                    dramRequestValid = 1'b1;
                end
            end
        endcase
    end

endmodule
