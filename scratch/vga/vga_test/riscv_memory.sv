`default_nettype none

`include "riscv_memory.svh"

// Port A will always be instruction or VGA
// Port B will always be data store/load

module RISCV_Memory (
    input  MemoryRequest_t  instructionRequest, dataRequest, vgaRequest,
    output logic            instructionReady, dataReady, vgaReady,
    // Visible on the clock edge after request asserted with ready valid
    output MemoryResponse_t instructionResponse, dataResponse, vgaResponse,
    // Infrastructure
    input DRAMResponse_t dramResponse,
    output DRAMRequest_t dramRequest,
    input  logic            coreClock, vgaClock, clear
);

    MemoryRequest_t  dataVRAMRequest, dataCacheRequest;
    MemoryResponse_t dataVRAMResponse, dataCacheResponse;
    logic memoryDataReady;

    function automatic MemoryRequest_t createEmptyRequest();
        return '{
            readEnable: 1'b0,
            writeEnable: 4'b0,
            address: `ADDRESS_POISON,
            addressType: PHYSICAL_ADDRESS,
            dataIn: `WORD_POISON
        };
    endfunction

    // This will route the data request to only one of the modules (effectively a fancy mux)
    always_comb begin
        if(vram.isInVRAM(dataRequest.address)) begin
            dataVRAMRequest  = dataRequest;
            dataCacheRequest = createEmptyRequest();
        end else begin
            assert(!vram.isInVRAM(dataRequest.address));
            dataVRAMRequest = createEmptyRequest();
            dataCacheRequest = dataRequest;
        end
    end

    // We are always ready to take another VGA request since these can't fault
    assign vgaReady = 1'b1;
    assign dataReady = memoryDataReady || vram.isInVRAM(savedDataRequest.address);

    VRAM vram (
        .dataRequest (dataVRAMRequest ),
        .dataResponse(dataVRAMResponse),
        .vgaRequest                    ,
        .vgaResponse                   ,
        .coreClock                     ,
        .vgaClock                      ,
        .clear
    );

    // We only need one signal for data ready which is controlled by the main memory subsystem because the main memory system should always assert ready unless it is busy. If the request is to VRAM, it will always be satisfied after a single cycle, so ready should continue to be asserted.
    MainMemory memory (
        .instructionRequest                    ,
        .instructionReady                      ,
        .instructionResponse                   ,
        .dataRequest        (dataCacheRequest ),
        .dataReady          (memoryDataReady)                   ,
        .dataResponse       (dataCacheResponse),
        .dramRequest,
        .dramResponse,
        .clock(coreClock)                                 ,
        .clear
    );

    MemoryRequest_t savedDataRequest;
    Register #(.WIDTH($bits(MemoryRequest_t))) dataRequest_reg (
        .q  (savedDataRequest),
        .d   (dataRequest     ),
        .enable (dataReady       ),
        .clock(coreClock       ),
        .clear
    );


    // This will route the data response from the appropriate source
    always_comb begin
        if(vram.isInVRAM(savedDataRequest.address)) begin
            dataResponse = dataVRAMResponse;
        end else begin
            assert(clear || !vram.isInVRAM(savedDataRequest.address));
            dataResponse = dataCacheResponse;
        end
    end

endmodule