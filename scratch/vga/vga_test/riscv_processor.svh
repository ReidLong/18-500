`ifndef __RISCV_PROCESSOR_SVH
`define __RISCV_PROCESSOR_SVH


interface ExternalIO_Interface;
    logic [7:0] switch;
    logic [4:0] button;
    logic [7:0] led;

    modport Device(
        input switch, button,
        output led
    );

    modport Board(
        output switch, button,
        input led
    );

endinterface

`endif