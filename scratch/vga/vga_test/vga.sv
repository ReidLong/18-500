`default_nettype none

`include "riscv_memory.svh"
`include "riscv_console.svh"
`include "compiler.svh"

module counter #(
    parameter WIDTH = 8
    )(
    input  logic clk, rst, en,
    output logic [WIDTH-1:0] cnt
    );
    
    always_ff @(posedge clk) begin
        if (rst) begin
            cnt <= '0;
        end else if (en) begin
            cnt <= cnt + 1;
        end
    end

endmodule

module vga_data (
    input  logic       CLK_100, rst,
    output logic [9:0] HDATA, VDATA,
    output logic       VGA_HS, VGA_VS,
    output logic       valid
    );

    // Timing information taken from http://tinyvga.com/vga-timing/640x400@70Hz
    // Clock divider from 100 MHZ to 25.175 MHz
    logic clk;
    clk_wiz_0 clock_div(clk, CLK_100);

    // Row state
    parameter ROWS = 400;
    parameter V_FP_LINES   = 12;
    parameter V_SYNC_LINES = 2;
    parameter V_BP_LINES   = 35;
    parameter ROW_LINES = ROWS + V_FP_LINES + V_SYNC_LINES + V_BP_LINES;

    logic row_rst, row_en;
    logic [$clog2(ROW_LINES)-1:0] row;
    counter #($clog2(ROW_LINES)) row_counter(clk, row_rst|rst, row_en, row);

    typedef enum { ROW_VALID, ROW_FP, ROW_SYNC, ROW_BP } row_state_t;
    row_state_t row_state, next_row_state;
    always_ff @(posedge clk) begin
        if (rst) row_state <= ROW_VALID;
        else row_state <= next_row_state;
    end

    // Column state
    parameter COLS = 640;
    parameter H_FP_PIXELS   = 16;
    parameter H_SYNC_PIXELS = 96;
    parameter H_BP_PIXELS   = 48;
    parameter COL_PIXELS = COLS + H_FP_PIXELS + H_SYNC_PIXELS + H_BP_PIXELS;

    logic col_rst, col_en;
    logic [$clog2(COL_PIXELS)-1:0] col;
    counter #($clog2(COL_PIXELS)) col_counter(clk, col_rst|rst, col_en, col);

    typedef enum { COL_VALID, COL_FP, COL_SYNC, COL_BP } col_state_t;
    col_state_t col_state, next_col_state;
    always_ff @(posedge clk) begin
        if (rst) col_state <= COL_VALID;
        else col_state <= next_col_state;
    end

    // State logic
    always_comb begin
        case (row_state)
            ROW_VALID: next_row_state = row == (ROWS-1) ? ROW_FP : ROW_VALID;
            ROW_FP:    next_row_state = row == (ROWS+V_FP_LINES-1) ? ROW_SYNC : ROW_FP;
            ROW_SYNC:  next_row_state = row == (ROWS+V_FP_LINES+V_SYNC_LINES-1) ? ROW_BP : ROW_SYNC;
            ROW_BP:    next_row_state = row == (ROWS+V_FP_LINES+V_SYNC_LINES+V_BP_LINES-1) ? ROW_VALID : ROW_BP;
        endcase // row_state
        
        case (col_state)
            COL_VALID: next_col_state = col == (COLS-1) ? COL_FP : COL_VALID;
            COL_FP:    next_col_state = col == (COLS+H_FP_PIXELS-1) ? COL_SYNC : COL_FP;
            COL_SYNC:  next_col_state = col == (COLS+H_FP_PIXELS+H_SYNC_PIXELS-1) ? COL_BP : COL_SYNC;
            COL_BP:    next_col_state = col == (COLS+H_FP_PIXELS+H_SYNC_PIXELS+H_BP_PIXELS-1) ? COL_VALID : COL_BP;
        endcase // col_state
    end

    // Control logic
    always_comb begin
        row_rst = 0;
        row_en = 0;

        col_rst = 0;
        col_en = 1;

        // Increment row and reset columns
        if (col == (COLS + H_FP_PIXELS + H_SYNC_PIXELS + H_BP_PIXELS) - 1) begin
            col_rst = 1;
            row_en = 1;
        end

        // Reset row
        if (row == (ROWS + V_FP_LINES + V_SYNC_LINES + V_BP_LINES) - 1) begin
            row_rst = 1;
        end
    end

    // Output logic
    always_comb begin
        HDATA = col;
        VDATA = row;
        valid = (col_state == COL_VALID && row_state == ROW_VALID);

        VGA_VS = ~(row_state == ROW_SYNC);
        VGA_HS = ~(col_state == COL_SYNC);
    end

endmodule

module cga_colors (
                   input logic [3:0]   index,
                   output logic [11:0] color
                   );

   // TODO: these values might need to be changed
   always_comb begin
      case (index)
         'h0: color = { 4'b0000, 4'b0000, 4'b0000 };
         'h1: color = { 4'b0000, 4'b0000, 4'b1000 };
         'h2: color = { 4'b0000, 4'b1000, 4'b0000 };
         'h3: color = { 4'b0000, 4'b1000, 4'b1000 };
         'h4: color = { 4'b1000, 4'b0000, 4'b0000 };
         'h5: color = { 4'b1000, 4'b0000, 4'b1000 };
         'h6: color = { 4'b1000, 4'b0100, 4'b0000 };
         'h7: color = { 4'b1000, 4'b1000, 4'b1000 };
         'h8: color = { 4'b0100, 4'b0100, 4'b0100 };
         'h9: color = { 4'b0100, 4'b0100, 4'b1100 };
         'ha: color = { 4'b0100, 4'b1100, 4'b0100 };
         'hb: color = { 4'b0100, 4'b1100, 4'b1100 };
         'hc: color = { 4'b1100, 4'b0100, 4'b0100 };
         'hd: color = { 4'b1100, 4'b0100, 4'b1100 };
         'he: color = { 4'b1100, 4'b1100, 4'b0100 };
         'hf: color = { 4'b1100, 4'b1100, 4'b1100 };
      endcase
   end

endmodule

module vga
   #(
     parameter CONSOLE_SIZE = `CONSOLE_WIDTH * `CONSOLE_HEIGHT
     )
   (
    input logic                             clk, rst,
    input logic [7:0]                       cur_color,
    input logic [6:0]                       ascii_code,
    output logic [$clog2(CONSOLE_SIZE)-1:0] char_idx,
    output logic [3:0]                      r, g, b,
    output logic                            hs, vs
    );

   // Handle VGA row and column generation here probably
   localparam VGA_WIDTH  = 640;
   localparam VGA_HEIGHT = 400;

   logic [9:0] vga_row, vga_col;
   logic vga_hs, vga_vs, vga_valid;
   vga_data datagen(clk, rst, vga_col, vga_row, vga_hs, vga_vs, vga_valid);

   // Increment counters for characters
   localparam CHAR_WIDTH  = 8;
   localparam CHAR_HEIGHT = 16;
   logic [$clog2(`CONSOLE_HEIGHT)-1:0]      char_row;
   logic [$clog2(`CONSOLE_WIDTH)-1:0]       char_col;

   always_comb begin
      char_row  = vga_row / CHAR_HEIGHT;
      char_col  = vga_col / CHAR_WIDTH;
      char_idx  = char_row * `CONSOLE_WIDTH + char_col;
   end

   // Increment counter within characters
   logic [3:0]                              ascii_row;
   logic [2:0]                              ascii_col;

   always_comb begin
      ascii_row  = vga_row % CHAR_HEIGHT;
      ascii_col  = vga_col % CHAR_WIDTH;
   end

   logic                                    ascii_out;
   charmap charmap(clk, ascii_code, ascii_row, ascii_col, ascii_out);

   logic disp;
   // assign disp = ascii_out && vga_valid;
   // One my system this register resulted in the columns being off by one
   always_ff @(posedge clk) begin
      disp <= ascii_out && vga_valid;
   end

   // logic [3:0]  fg_index, bg_index;
   // logic [11:0] fg_color, bg_color;
   // assign { bg_index, fg_index } = cur_color;
   // cga_colors fg_cga(fg_index, fg_color);
   // cga_colors bg_cga(bg_index, bg_color);

   always_ff @(posedge clk) begin
      { r, g, b } <= disp ? '1 : '0;
      hs <= vga_hs;
      vs <= vga_vs;
   end

endmodule

module VGAController(
    input MemoryResponse_t vgaResponse,
    output MemoryRequest_t vgaRequest,
    output logic [7:0] led,
    input logic [7:0] switch,
    input logic [4:0] button,
    input logic vgaClock, clear,
    output logic [3:0] vga_r, vga_g, vga_b,
    output logic vga_hs, vga_vs
    );

    function MemoryRequest_t createRequest(logic readEnable, Address_t vramIndex);
        assert (readEnable == 1'b0 || (vramIndex & 32'b1) == 32'b0);
        return '{
            readEnable: readEnable,
            writeEnable: 4'b0,
            // This is going to clear the lowest 2 bits to ensure all address requests are word aligned
            address: (vramIndex << 1) + `VRAM_START,
            addressType: PHYSICAL_ADDRESS,
            dataIn: `WORD_POISON
        };
    endfunction

    function logic [15:0] calculateRow(Address_t vramAddress);
        return ((vramAddress - `VRAM_START) >> 1) / `CONSOLE_WIDTH;
    endfunction

    function logic [15:0] calculateColumn(Address_t vramAddress);
        return ((vramAddress - `VRAM_START) >> 1) % `CONSOLE_WIDTH;
    endfunction

    logic [11:0] index;
    logic clearCounter;
    logic incrementIndex;
    logic decrementIndex;

    localparam CONSOLE_SIZE = `CONSOLE_WIDTH * `CONSOLE_HEIGHT;
    logic [$clog2(CONSOLE_SIZE)-1:0] char_idx;

    `STATIC_ASSERT(2**$bits(index) >= CONSOLE_SIZE);

    assign clearCounter = index >= (CONSOLE_SIZE - 2);
    assign index = char_idx & 12'hFFE;

    // always_ff @(posedge vgaClock) begin
    //     if(clearCounter)
    //         index <= 12'b0;
    //     else if(incrementIndex && index < CONSOLE_SIZE - 2)
    //         index <= index + 12'd2;
    //     else if(decrementIndex && index > 12'b0)
    //         index <= index - 12'd2;
    // end

    assert property (@(posedge vgaClock) vgaResponse.isValid |->
        (`VRAM_START <= vgaResponse.request.address
            && vgaResponse.request.address < `VRAM_END));


    assign vgaRequest = createRequest(1'b1, index);

    enum {IDLE, INCREMENT_ARM, DECREMENT_ARM} state, nextState;

    always_ff @(posedge vgaClock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;

    always_comb begin
        nextState = state;
        incrementIndex = 1'b0;
        decrementIndex = 1'b0;
        unique case(state)
            IDLE: begin
                if(button[3] && switch[7] && ~switch[6])
                    nextState = INCREMENT_ARM;
                else if(button[3] && switch[7] && switch[6])
                    nextState = DECREMENT_ARM;
            end
            INCREMENT_ARM:
                if(button[0]) begin
                    incrementIndex = 1'b1;
                    nextState = IDLE;
                end
            DECREMENT_ARM:
                if(button[0]) begin
                    decrementIndex = 1'b1;
                    nextState = IDLE;
                end
        endcase
    end


    // This state is from the previous cycles request (1 cycle latency on request to vga data)
    logic [15:0] row, col;
    logic [1:0][7:0] character, color;


    assign row = calculateRow(vgaResponse.request.address);
    assign col = calculateColumn(vgaResponse.request.address);
    assign character[0] = vgaResponse.dataOut[7:0];
    assign color[0] = vgaResponse.dataOut[15:8];
    assign character[1] = vgaResponse.dataOut[23:16];
    assign color[1] = vgaResponse.dataOut[31:24];

    logic [1:0][7:0] chars_prev;

    always_ff @(posedge vgaClock) begin
        chars_prev <= character;
    end

    // // VGA Output logic
    // logic [9:0] hdata, vdata;
    // logic hs, vs, valid;
    // vga_data vga (vgaClock, hdata, vdata, hs, vs, valid);

    // always_comb begin
    //     if (valid) begin
    //         vga_r = switch[7:4];
    //         vga_g = switch[3:0];
    //         vga_b = 4'b1111;
    //     end else begin
    //         vga_r = 4'b0000;
    //         vga_g = 4'b0000;
    //         vga_b = 4'b0000;
    //     end

    //     vga_vs = vs;
    //     vga_hs = hs;
    // end
    
    // Synchronous reset
    logic rst;
    assign rst = button[4];

    logic char_offset;
    always_ff @(posedge vgaClock) begin
        char_offset <= char_idx[0];
    end

    vga vga(vgaClock, rst, 
        color[char_offset], character[char_offset][6:0], 
        char_idx, vga_r, vga_g, vga_b, vga_hs, vga_vs);

    always_comb
        case(switch)
            8'd0: led = row[7:0];
            8'd1: led = col[7:0];
            8'd2: led = character[0];
            8'd3: led = color[0];
            8'd4: led = character[1];
            8'd5: led = color[1];
            8'd6: led = vgaResponse.request.address[7:0];
            8'd7: led = vgaResponse.request.address[15:8];
            8'd9: led = {vgaResponse.isValid, index[6:0]};
            // 8'd11: led = { vga_r, vga_g };
            // 8'd15: led = { 1'b0, ~valid, ~vga_vs, ~vga_hs, 1'b0, valid, vga_vs, vga_hs };
            default: led = 8'hF0;
        endcase

endmodule