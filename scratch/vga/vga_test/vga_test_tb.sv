`default_nettype none

`include "riscv_memory.svh"
`include "riscv_console.svh"

module VGA_tb(
    // IO
    input  logic [ 7:0] switch      ,
    output logic [ 7:0] led         ,
    input  logic [ 4:0] button      , // D:0, L:1, R:2, U:3, C:4
    output logic [ 3:0] vga_r, vga_g, vga_b,
    output logic        vga_vs, vga_hs,
    // Infrastructure
    input  logic        clock       ,
    // DRAM Interface
    output logic [15:0][31:0] writeData,
    input logic [15:0][31:0] readData,
    output logic [31:0] addressBase,
    output logic readEnable,
    output logic writeEnable,
    input logic requestAccepted,
    input logic requestCompleted,
    input logic requestError
);

    logic clear;
    assign clear = button[4];

    logic vgaClock;
    assign vgaClock = clock;

    DRAMRequest_t dramRequest;
    DRAMResponse_t dramResponse;

    DRAM_Interface dram_interface();

    always_comb begin
        dram_interface.readData = readData;
        dram_interface.requestAccepted = requestAccepted;
        dram_interface.requestCompleted = requestCompleted;
        dram_interface.requestError = requestError;
        writeData = dram_interface.writeData;
        addressBase = dram_interface.addressBase;
        readEnable = dram_interface.readEnable;
        writeEnable = dram_interface.writeEnable;
    end

    logic fatalError;

    DRAM_Wrapper wrapper(.request(dramRequest), .response(dramResponse), .dram(dram_interface.Requester), .clock, .clear, .fatalError);

    MemoryRequest_t vgaRequest, dataRequest;
    MemoryResponse_t vgaResponse, dataResponse;

    // always_comb begin
    //     case(switch)
    //         8'd0: led = dramRequest.physicalAddress[7:0];
    //         8'd1: led = dramRequest.physicalAddress[15:8];
    //         8'd2: led = dramRequest.physicalAddress[23:16];
    //         8'd3: led = dramRequest.physicalAddress[31:24];
    //         8'd4: led = vgaResponse.dataOut[7:0];
    //         8'd5: led = vgaResponse.dataOut[15:8];
    //         8'd6: led = vgaResponse.dataOut[23:16];
    //         8'd7: led = vgaResponse.dataOut[31:24];
    //         8'd8: led = {dramRequest.readEnable, vgaResponse.isValid, 6'b0};
    //         default: led = 8'h33;
    //     endcase
    // end
    // TODO: Synchronize the buttons with the appropriate clock
    VGAController vga(.vgaRequest, .vgaResponse, .led, .switch, .button, .vgaClock, .clear,
        .vga_r, .vga_g, .vga_b, .vga_hs, .vga_vs);

    VRAM vram(.dataRequest, .dataResponse, .vgaRequest, .vgaResponse, .coreClock(clock), .vgaClock, .clear);

    VRAM_AutoLoader loader(.dataRequest, .dataResponse, .dramRequest, .dramResponse, .button, .switch, .clock, .clear);

    // VRAMLoader loader(.dataRequest, .dataResponse, .dramRequest, .dramResponse, .switch, .button, .clock, .clear);

    // VRAM_ManualLoader loader(.dataResponse, .dataRequest, .switch, .button, .clock, .clear);
    // assign dramRequest = '{default:0};

endmodule

module VRAM_AutoLoader(
    output MemoryRequest_t dataRequest,
    input MemoryResponse_t dataResponse,
    output DRAMRequest_t dramRequest,
    input DRAMResponse_t dramResponse,
    input logic [4:0] button,
    input logic [7:0] switch,
    input logic clock, clear
);

    Address_t savedAddress;
    Address_t nextAddress;
    logic loadNextAddress;

    always_comb begin
        if(savedAddress + 32'd64 >= 32'h1000)
            nextAddress = 33'h0;
        else
            nextAddress = savedAddress + 32'd64;
    end

    Register #(.WIDTH($bits(savedAddress))) address_reg(.q(savedAddress), .d(nextAddress), .clock, .enable(loadNextAddress), .clear);

    enum {IDLE, FILL, SETUP} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;


    logic fillDone;
    logic dramRequestValid;
    logic dramClearCounter;
    logic dramIncrement;
    logic [3:0] dramValidCount;
    logic [3:0] vgaWriteEnable;

    assign fillDone = (dramValidCount == 4'hF) && dramResponse.isValid;

    Counter #(.WIDTH(4)) dramValid_counter(
        .q(dramValidCount), .enable(dramIncrement), .clear(clear | dramClearCounter), .clock);

    always_comb begin
        nextState = state;
        dramRequestValid = 1'b0;
        dramClearCounter = 1'b0;
        dramIncrement = 1'b0;
        vgaWriteEnable = 4'b0;
        loadNextAddress = 1'b0;
        unique case(state)
            IDLE: begin
                if(button[3] && ~switch[7]) begin
                    nextState = SETUP;
                end
            end
            SETUP: begin
                dramClearCounter = 1'b1;
                nextState = FILL;
            end
            FILL: begin
                dramRequestValid = 1'b1;
                if(fillDone) begin
                    nextState = SETUP;
                    loadNextAddress = 1'b1;
                end
                if(dramResponse.isValid) begin
                    dramIncrement = 1'b1;
                    vgaWriteEnable = 4'hf;
                end
            end
        endcase
    end

    always_comb begin
        dramRequest = '{
            physicalAddress: savedAddress + `VRAM_START,
            readEnable: dramRequestValid,
            writeEnable: 1'b0,
            dataIn: `WORD_POISON
        };
        dataRequest = '{
            readEnable: 1'b0,
            writeEnable: vgaWriteEnable,
            address: dramResponse.physicalAddress,
            addressType: PHYSICAL_ADDRESS,
            dataIn: dramResponse.dataOut
        };
    end

    // Whenever we see a result from dram, on the next cycle we would also see a valid back from the VRAM since VRAM always is a cache hit
    assert property (@(posedge clock) dramResponse.isValid |=> dataResponse.isValid);

   

endmodule

module VRAM_ManualLoader(
    output MemoryRequest_t dataRequest,
    input MemoryResponse_t dataResponse,
    input logic [7:0] switch,
    input logic [4:0] button,
    input logic clock, clear
);

    Address_t savedAddress;
    logic [3:0][7:0] savedData;

    Array #(.ELEMENTS(4), .WIDTH(8)) data_array(.index(switch[1:0]), .element({switch[7:2], 2'b0}), .array(savedData), .clock, .clear, .enable(button[2]));

    Register #(.WIDTH($bits(savedAddress))) address_reg(.q(savedAddress), .d({22'b0, switch[7:0], 2'b0}), .clock, .enable(button[1]), .clear);

    assign dataRequest = '{
        readEnable: 1'b0,
        writeEnable: (button[0] ? 4'hf : 4'h0),
        address: (`VRAM_START + savedAddress),
        addressType: PHYSICAL_ADDRESS,
        dataIn: savedData
    };


endmodule


module VRAMLoader(
    output MemoryRequest_t dataRequest,
    input MemoryResponse_t dataResponse,
    output DRAMRequest_t dramRequest,
    input DRAMResponse_t dramResponse,
    input logic [7:0] switch,
    input logic [4:0] button,
    input logic clock, clear
    );

    // Acts like a manually triggered cache fill that fills VRAM

    Address_t savedAddress;

    Register #(.WIDTH($bits(savedAddress))) address_reg(.q(savedAddress), .d({18'b0, switch[7:0], 6'b0}), .clock, .enable(button[1]), .clear);

    enum {IDLE, FILL_ARM, FILL} state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;


    logic fillDone;
    logic dramRequestValid;
    logic dramClearCounter;
    logic dramIncrement;
    logic [3:0] dramValidCount;
    logic [3:0] vgaWriteEnable;

    assign fillDone = (dramValidCount == 4'hF) && dramResponse.isValid;

    Counter #(.WIDTH(4)) dramValid_counter(
        .q(dramValidCount), .enable(dramIncrement), .clear(clear | dramClearCounter), .clock);

    always_comb begin
        nextState = state;
        dramRequestValid = 1'b0;
        dramClearCounter = 1'b0;
        dramIncrement = 1'b0;
        vgaWriteEnable = 4'b0;
        unique case(state)
            IDLE:
                if(button[3] && ~switch[7]) begin
                    nextState = FILL_ARM;
                    dramClearCounter = 1'b1;
                end
            FILL_ARM:
                if(button[0])
                    nextState = FILL;
            FILL: begin
                dramRequestValid = 1'b1;
                if(fillDone) begin
                    nextState = IDLE;
                end
                if(dramResponse.isValid) begin
                    dramIncrement = 1'b1;
                    vgaWriteEnable = 4'hf;
                end
            end
        endcase
    end

    always_comb begin
        dramRequest = '{
            physicalAddress: savedAddress + `VRAM_START,
            readEnable: dramRequestValid,
            writeEnable: 1'b0,
            dataIn: `WORD_POISON
        };
        dataRequest = '{
            readEnable: 1'b0,
            writeEnable: vgaWriteEnable,
            address: dramResponse.physicalAddress,
            addressType: PHYSICAL_ADDRESS,
            dataIn: dramResponse.dataOut
        };
    end

    // Whenever we see a result from dram, on the next cycle we would also see a valid back from the VRAM since VRAM always is a cache hit
    assert property (@(posedge clock) dramResponse.isValid |=> dataResponse.isValid);

endmodule


module VGA_mock_tb(
    // IO
    input  logic [ 7:0] switch      ,
    output logic [ 7:0] led         ,
    input  logic [ 4:0] button      , // D:0, L:1, R:2, U:3, C:4
    // Infrastructure
    input  logic        clock
    );

    DRAM_Interface dram();

    DRAM_mock mock(.dram(dram.Source), .clock, .clear(button[4]));

    VGA_tb testbench(
        .switch,
        .led,
        .button,
        .clock,
        .addressBase(dram.addressBase),
        .writeData(dram.writeData),
        .readData(dram.readData),
        .readEnable(dram.readEnable),
        .writeEnable(dram.writeEnable),
        .requestAccepted(dram.requestAccepted),
        .requestCompleted(dram.requestCompleted),
        .requestError(dram.requestError)
    );

endmodule

module vcs_top;

    logic [7:0] switch, led;
    logic [4:0] button;
    logic clock;

    VGA_mock_tb dut(.*);

    initial begin
        clock = 1'b0;
        forever #5 clock = ~clock;
    end

    initial begin
        switch = 8'h0;
        button = 5'h0;
        @(posedge clock);
        button[4] = 1'b1;
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        button[4] = 1'b0;
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        button[3] = 1'b1;
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        button[3] = 1'b0;
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        button[2] = 1'b1;
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        button[2] = 1'b0;

        #10000;
        $finish;
    end

endmodule
