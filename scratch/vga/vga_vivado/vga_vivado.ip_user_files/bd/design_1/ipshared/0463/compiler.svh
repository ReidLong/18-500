`ifndef __COMPILER
`define __COMPILER

`define STRINGIFY(x) `"x`"

`define STATIC_ASSERT(CONDITION) \
generate\
    if(!(CONDITION)) begin\
        $error("%s.%d Condition Failed: %s", `__FILE__, `__LINE__, `STRINGIFY(CONDITION));\
        end\
        endgenerate

`endif