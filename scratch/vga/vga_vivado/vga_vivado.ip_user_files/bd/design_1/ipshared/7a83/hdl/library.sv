module Register #(parameter WIDTH=32, CLEAR_VALUE = 0) (
  output logic [WIDTH-1:0] q    ,
  input  logic [WIDTH-1:0] d    ,
  input  logic             clock, enable, clear_n
);

  always_ff @(posedge clock)
    if(~clear_n)
      q <= CLEAR_VALUE;
    else if (enable)
      q <= d;

  endmodule // register


module EdgeTrigger(
    input logic signal,
    output logic isEdge,
    input logic clock, clear_n);

    logic current, next;

    assign isEdge = !next && current;

    always_ff @(posedge clock) begin
        if(~clear_n) begin
            current <= 1'b0;
            next <= 1'b0;
        end else begin
            current <= signal;
            next <= current;
        end
    end

endmodule