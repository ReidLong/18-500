set_property IOSTANDARD LVCMOS33 [get_ports led]
set_property IOSTANDARD LVCMOS18 [get_ports switch]
set_property IOSTANDARD LVCMOS18 [get_ports button]
set_property IOSTANDARD LVCMOS33 [get_ports vga_r]
set_property IOSTANDARD LVCMOS33 [get_ports vga_g]
set_property IOSTANDARD LVCMOS33 [get_ports vga_b]
set_property IOSTANDARD LVCMOS33 [get_ports vga_hs]
set_property IOSTANDARD LVCMOS33 [get_ports vga_vs]

set_property PACKAGE_PIN T22 [get_ports {led[0]}]
set_property PACKAGE_PIN T21 [get_ports {led[1]}]
set_property PACKAGE_PIN U22 [get_ports {led[2]}]
set_property PACKAGE_PIN U21 [get_ports {led[3]}]
set_property PACKAGE_PIN V22 [get_ports {led[4]}]
set_property PACKAGE_PIN W22 [get_ports {led[5]}]
set_property PACKAGE_PIN U19 [get_ports {led[6]}]
set_property PACKAGE_PIN U14 [get_ports {led[7]}]
set_property PACKAGE_PIN F22 [get_ports {switch[0]}]
set_property PACKAGE_PIN G22 [get_ports {switch[1]}]
set_property PACKAGE_PIN H22 [get_ports {switch[2]}]
set_property PACKAGE_PIN F21 [get_ports {switch[3]}]
set_property PACKAGE_PIN H19 [get_ports {switch[4]}]
set_property PACKAGE_PIN H18 [get_ports {switch[5]}]
set_property PACKAGE_PIN H17 [get_ports {switch[6]}]
set_property PACKAGE_PIN M15 [get_ports {switch[7]}]
set_property PACKAGE_PIN R16 [get_ports {button[0]}];  # "BTND"
set_property PACKAGE_PIN N15 [get_ports {button[1]}];  # "BTNL"
set_property PACKAGE_PIN R18 [get_ports {button[2]}];  # "BTNR"
set_property PACKAGE_PIN T18 [get_ports {button[3]}];  # "BTNU"
set_property PACKAGE_PIN P16 [get_ports {button[4]}];  # "BTNC"
set_property PACKAGE_PIN Y21  [get_ports {vga_b[0]}];  # "VGA-B1"
set_property PACKAGE_PIN Y20  [get_ports {vga_b[1]}];  # "VGA-B2"
set_property PACKAGE_PIN AB20 [get_ports {vga_b[2]}];  # "VGA-B3"
set_property PACKAGE_PIN AB19 [get_ports {vga_b[3]}];  # "VGA-B4"
set_property PACKAGE_PIN AB22 [get_ports {vga_g[0]}];  # "VGA-G1"
set_property PACKAGE_PIN AA22 [get_ports {vga_g[1]}];  # "VGA-G2"
set_property PACKAGE_PIN AB21 [get_ports {vga_g[2]}];  # "VGA-G3"
set_property PACKAGE_PIN AA21 [get_ports {vga_g[3]}];  # "VGA-G4"
set_property PACKAGE_PIN AA19 [get_ports {vga_hs}];    # "VGA-HS"
set_property PACKAGE_PIN V20  [get_ports {vga_r[0]}];  # "VGA-R1"
set_property PACKAGE_PIN U20  [get_ports {vga_r[1]}];  # "VGA-R2"
set_property PACKAGE_PIN V19  [get_ports {vga_r[2]}];  # "VGA-R3"
set_property PACKAGE_PIN V18  [get_ports {vga_r[3]}];  # "VGA-R4"
set_property PACKAGE_PIN Y19  [get_ports {vga_vs}];    # "VGA-VS"