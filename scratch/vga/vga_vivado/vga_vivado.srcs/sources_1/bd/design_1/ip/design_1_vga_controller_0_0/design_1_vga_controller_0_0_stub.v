// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Apr  4 08:37:16 2018
// Host        : DESKTOP-QVPG904 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/Reid/Documents/18-500/scratch/vga/vga_vivado/vga_vivado.srcs/sources_1/bd/design_1/ip/design_1_vga_controller_0_0/design_1_vga_controller_0_0_stub.v
// Design      : design_1_vga_controller_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "top,Vivado 2017.4" *)
module design_1_vga_controller_0_0(switch, led, button, vga_r, vga_g, vga_b, vga_vs, 
  vga_hs, clock, writeData, readData, addressBase, readEnable, writeEnable, requestAccepted, 
  requestCompleted, requestError)
/* synthesis syn_black_box black_box_pad_pin="switch[7:0],led[7:0],button[4:0],vga_r[3:0],vga_g[3:0],vga_b[3:0],vga_vs,vga_hs,clock,writeData[511:0],readData[511:0],addressBase[31:0],readEnable,writeEnable,requestAccepted,requestCompleted,requestError" */;
  input [7:0]switch;
  output [7:0]led;
  input [4:0]button;
  output [3:0]vga_r;
  output [3:0]vga_g;
  output [3:0]vga_b;
  output vga_vs;
  output vga_hs;
  input clock;
  output [511:0]writeData;
  input [511:0]readData;
  output [31:0]addressBase;
  output readEnable;
  output writeEnable;
  input requestAccepted;
  input requestCompleted;
  input requestError;
endmodule
