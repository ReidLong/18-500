module top (
  // IO
  input  wire [ 7:0] switch      ,
  output wire [ 7:0] led         ,
  input  wire [ 4:0] button      ,
  output wire [ 3:0] vga_r, vga_g, vga_b,
  output wire        vga_vs, vga_hs,
  // Infrastructure
  input  wire        clock       ,
  // DRAM Interface
  output wire [15:0][31:0] writeData,
  input wire [15:0][31:0] readData,
  output wire [31:0] addressBase,
  output wire readEnable,
  output wire writeEnable,
  input wire requestAccepted,
  input wire requestCompleted,
  input wire requestError
);

  wire [7:0] switch_s;
  wire [4:0] button_s;

  // assign led = switch_s;
  // assign writeData = 0;
  // assign addressBase = 0;
  // assign readEnable = 0;
  // assign writeEnable = 0;
  Sync #(.WIDTH(8)) switch_sync (.clock(clock), .sig(switch), .sig_s(switch_s));
  Sync #(.WIDTH(5)) button_sync (.clock(clock), .sig(button), .sig_s(button_s));


  VGA_tb testBench(
    .switch(switch_s),
    .led(led),
    .button(button_s),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b),
    .vga_hs(vga_hs),
    .vga_vs(vga_vs),
    .clock(clock),
    .writeData(writeData),
    .readData(readData),
    .addressBase(addressBase),
    .readEnable(readEnable),
    .writeEnable(writeEnable),
    .requestAccepted(requestAccepted),
    .requestCompleted(requestCompleted),
    .requestError(requestError)
  );

endmodule

module top_mock(
  // IO
  input  wire [ 7:0] switch      ,
  output wire [ 7:0] led         ,
  input  wire [ 4:0] button      ,
  // Infrastructure
  input  wire        clock
  );

  wire [7:0] switch_s;
  wire [4:0] button_s;

  Sync #(.WIDTH(8)) switch_sync (.clock(clock), .sig(switch), .sig_s(switch_s));
  Sync #(.WIDTH(5)) button_sync (.clock(clock), .sig(button), .sig_s(button_s));

  VGA_mock_tb testBench(
    .switch(switch_s),
    .button(button_s),
    .led(led),
    .clock(clock)
  );


endmodule
